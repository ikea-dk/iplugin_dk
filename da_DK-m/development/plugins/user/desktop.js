/*

               
 | |  _  _  ._ 
 |_| _> (/_ |  
               

*/

(function(){

	var User = {
		logOutReq: function(callback, context) {
			var response = new Samuraj.Common.xhr();
			var cartUrl = "/webapp/wcs/stores/servlet/Logoff?langId=" + SamurajSetting.Country.langId + "&storeId=" + SamurajSetting.Country.storeId + "&rememberMe=true"; //https://secure.ikea.com
			var DONE = 4;
			var OK = 200;
			response.context = context;
			response.open('GET', cartUrl, true);
			response.onreadystatechange = function() {
				if (response.readyState === DONE) {
					if (response.status === OK) {
						setTimeout(function(){
							if (typeof callback == "function") callback(response.context);
						}, 500);
					}
				}
			}
			response.send(null);
		},
		logOut: function(input, callback, context){
			var c = Samuraj.Common;
			if (c.varExist(context)){
				if (!c.isObject(context)){
					var t = context;
					context = {};
					context.content = t;
				}
			} else context = {};
			context.redirecturl = input.redirecturl;
			var data = {
				html: "<div>" + Samuraj.Translation.data.checkout.logoutpopuptext + "</div>" +  Samuraj.Template.options.loader,
				timeoutclose: 10000
			}
			if (c.varExist(input.includeclass)) data.includeclass = input.includeclass;
			if (c.varExist(input.includecontentclass)) data.includecontentclass = input.includecontentclass;

			Samuraj.Element.addModal(data);

			Samuraj.Product.getProductsFromCart(function(products, context){
				context.products = products;
				Samuraj.User.logOutReq(function(context){
					Samuraj.Product.addProducts(context.products, function(context){
						 if (Samuraj.User.redirecturls[context.redirecturl])
							context.redirecturl = Samuraj.User.redirecturls[context.redirecturl]
						 if (typeof callback == "function") callback(context)
						 else {
							window.location.replace(context.redirecturl);
							if (window.samuraj_modal){
								Samuraj.Common.removeClass(window.samuraj_modal, "samuraj-show");
								setTimeout(function(){
									Samuraj.Element.remove(window.samuraj_modal);
								},1000)
							}
						 }
					}, context);
				}, context);
			}, context);
		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "User",
		objects: [
			{name: "User", fn: User}
		],
		dependencies: ["Product"],
		bootup: function(){
			//Initiate User
			User.redirecturls = {
				m2cart: "https://secure.ikea.com/webapp/wcs/stores/servlet/M2OrderItemDisplay?storeId=" + SamurajSetting.Country.storeId + "&langId=" + SamurajSetting.Country.langId + "&catalogId=11001&orderId=.&priceexclvat=&newLinks=true",
				m2home: "https://m2.ikea.com/" + SamurajSetting.Country.homePath + "/",
				irwcart: "//www.ikea.com/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=" + SamurajSetting.Country.storeId + "&langId=" + SamurajSetting.Country.langId + "&catalogId=11001&orderId=.&priceexclvat=&newLinks=true",
				irwhome: "//www.ikea.com/" + SamurajSetting.Country.homePath + "/",
			}

			Samuraj.Log.add({
				cat: "User",
				sub: "Timing",
				text: "Loading done",
				timing: performance.now() - SamurajTiming				
			});
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();

