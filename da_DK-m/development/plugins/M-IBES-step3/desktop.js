/* eslint-disable func-names */

(function (){
    // Check ready state
        function ready(fn) {
            if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
                fn();
            } else {
                document.addEventListener('DOMContentLoaded', fn);
            }
        }
       // let run = function () {
           // if (document.URL.indexOf('/ww8.ikea.com/clickandcollect/dk/basketupdate/updatebasketaddresses/redirect') > -1) {
    
    // change wording "Vælg levering"     
        const pickupHeadline = document.getElementsByClassName('col-sm-12')[0].children[0];
        const pickupText = 'Din levering ';
        pickupHeadline.innerHTML = pickupText;
        pickupHeadline.setAttribute("style", "display: block; padding: 0px 10px 10px 10px; font-size: 14px; border-bottom: 1px solid #ccc");

    // change the wording "Leveringsdato" to "Du får dine vare leveret:"
        const leveringsdato = document.getElementsByClassName("boxPart")[1].children[0];
        const changedText = "Du får dine vare leveret:";
        leveringsdato.innerHTML = changedText;        

    // Format the text below delivery date
        const deliveryTxt = document.getElementsByClassName('col-xs-12')[1];
        const newText = 'Hvis du har valgt:<br><b>Kantstenslevering til 599.-</b><br>skal du selv bærer dine varer ind<br><br><b>Levering med fuld indbæring for 799.-</b><br>bærer vi dine varer helt ind i ét rum, som du selv vælger';
        deliveryTxt.innerHTML = newText;
        deliveryTxt.setAttribute("style", "display: block; padding-top: 25px");
        }
)();
        //};
        //ready(run);
    // })
    // ();
