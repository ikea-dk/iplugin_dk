function setM2Footer(){
    //prevents that the header is loaded more than once
    setTimeout(function(){
        if (!document.querySelector('.ui-page-active #m2-footer')) {

            document.querySelector('.ui-page-active .ikea-footer').style.display = 'none';

            var main = document.querySelector('.ui-page-active');
            var m2_footer = document.createElement("footer");
            m2_footer.id ="m2-footer";
            m2_footer.setAttribute("class","footer");
            m2_footer.setAttribute("role","contentinfo");
            m2_footer.innerHTML = "<nav class='footer__nav'>" +
                                    "<ul class='footer__list'>" +
                                        "<li class='footer__list-item'>" + "<strong class='footer__item-headline'>IKEA Danmark</strong>" +
                                            "<ul class='footer__inner-list'>" + 
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/this-is-ikea/' class='footer__item-link'>Om IKEA</a></li>" + 
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/stores/' class='footer__item-link'>Varehuse og åbningstider</a></li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/stores/restaurant-pub7612ad61' class='footer__item-link'>Restaurant</a></li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/this-is-ikea/work-with-us/' class='footer__item-link'>Job i IKEA</a></li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/newsroom/' class='footer__item-link'>Presse</a></li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/this-is-ikea/sustainable-everyday/' class='footer__item-link'>Bæredygtighed</a></li>" +
                                            "</ul>" +
                                        "</li>" +
                                        "<li class='footer__list-item'> <strong class='footer__item-headline'>Kundeservice</strong>" + 
                                            "<ul class='footer__inner-list'>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/' class='footer__item-link'>Find svar eller kontakt os</a> </li>" + 
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/servicer-pub4d35b0e1' class='footer__item-link'>Servicer</a> </li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/retur-og-reklamation-pube58ebb20' class='footer__item-link'>Retur og reklamation</a> </li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/tegneprogrammer-pub1806be21' class='footer__item-link'>Tegneprogrammer</a> </li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/katalog-og-brochurer-pub12a8d4d1' class='footer__item-link'>Katalog og brochurer</a> </li>" +
                                            "</ul>" +
                                        "</li>" +
                                        "<li class='footer__list-item'> <strong class='footer__item-headline'>Privatlivspolitik</strong> " + 
                                            "<ul class='footer__inner-list'>" + 
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/handtering-af-persondata-pubedaf4a31' class='footer__item-link'>Håndtering af persondata</a> </li>" +
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/cookie-politik-pube3d07691' class='footer__item-link'>Cookie politik</a> </li>" + 
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/salgs-og-leveringsbetingelser-pube817fb91' class='footer__item-link'>Salgs- og leveringsbetingelser</a> </li>" + 
                                                "<li class='footer__inner-item'> <a href='https://m2.ikea.com/dk/da/customer-service/vores-webshop-er-e-maerket-puba200e5b1' class='footer__item-link'>e-mærket</a> </li>" + 
                                                "<li class='footer__inner-item'> <a href='https://www.ikea.com/dk/da/' class='footer__item-link'>Vis IKEA.dk: Standard</a> </li>" + 
                                            "</ul>" +
                                        "</li>" + 
                                        "<li class='footer__list-item footer__list-item-social'> <strong class='footer__item-headline' id='footer__item-headline'>Følg IKEA på:</strong>" + 
                                            "<ul class='footer__inner-list' aria-labelledby='footer__item-headline'>" + 
                                                "<li class='footer__inner-item'> <a href='https://www.facebook.com/IKEADanmark/' class='footer__item-link'> <img class='footer__item-image' src='https://m2.ikea.com/dk/da/header-footer/static/social-media/facebook.bdb6070cf9c9392fe2872279c4c1b2a6.svg' alt='Facebook'> </a> </li>" +
                                                "<li class='footer__inner-item'> <a href='https://www.instagram.com/ikeadanmark/' class='footer__item-link'> <img class='footer__item-image' src='https://m2.ikea.com/dk/da/header-footer/static/social-media/instagram.5e2a32aa83c6fc2c64e6ef889bff044f.svg' alt='Instagram'> </a> </li>" +
                                                "<li class='footer__inner-item'> <a href='https://www.pinterest.com/ikeadanmark/' class='footer__item-link'> <img class='footer__item-image' src='https://m2.ikea.com/dk/da/header-footer/static/social-media/pinterest.badd80be3c57f7ddd38778a36d630f28.svg' alt='Pinterest'> </a> </li>" +
                                                "<li class='footer__inner-item'> <a href='https://www.youtube.com/user/IKEADK' class='footer__item-link'> <img class='footer__item-image' src='https://m2.ikea.com/dk/da/header-footer/static/social-media/youtube.8bc71f2758cf18128451e8f0251a58a5.svg' alt='YouTube'> </a> </li>" +
                                            "</ul>" + 
                                        "</li>" +
                                    "</ul>" + 
                                "</nav>" + 
                                "<div class='footer__copyright'> © Inter IKEA Systems B.V. 1999-2017 </div>";
                
            main.appendChild(m2_footer);
        }

    }, 100);
}

$(window).on('navigate', function(){
    setM2Footer();
});

setM2Footer();