/*
  __                       
 (_   _. ._ _      ._ _. o 
 __) (_| | | | |_| | (_| | 
                        _| 
*/

//Override other Samuraj vars, only for testing purposes
//window.Samuraj = undefined; window.SamurajMaxymiser = undefined; window.SamurajSetting = undefined;
//window.SamurajVersionOverride = "preview";
//----------------------

window.SamurajTiming = performance.now();
window.Samuraj = window.Samuraj;
window.SamurajMaxymiser = window.SamurajMaxymiser;
window.SamurajVersionOverride = window.SamurajVersionOverride;
window.SamurajSetting = window.SamurajSetting || {
	Common: {
		uselocation: false,
	},
	
	Country: {
		homePath: "dk/da",
		locale: "da_DK",
		storeId: "14",
		langId: "-12",
		numeric: {
			thousandseparator: ".",
			decimalseparator: ",",
			beforeprice: "",
			decimalconditionalafterprice: ".-",
			afterprice: "",
			decimalsum: false
		}
	},
	
	//Either a datasource file with datasources...   (flexible to edit, tiny bit slower)

	
	DataSources: {
		preview: "https://m2.ikea.com/dk/da/data-sources/eecf19c042f911e8977f211ade21a132.json",
		live: "https://m2.ikea.com/dk/da/data-sources/997061e042f511e89fcf555f905336ee.json"
	}
	

	
	// ... or load the datasources directly          (less flexible to edit, loading of these start earlier)
	/*
	DirectLoad: {
		live: {
			
			Addon: [
				"https://www.prougno.com/ikea/dk/data-sources/live/Addon.json"
			],
			DisplayTimerTemplates: [
				"https://www.prougno.com/ikea/dk/data-sources/live/DisplayTimer_Templates_Iframes.json",
				"https://www.prougno.com/ikea/dk/data-sources/live/DisplayTimer_Templates_Tags.json"
			],
			DisplayTimer: [
				"https://www.prougno.com/ikea/dk/data-sources/live/DisplayTimer_Banners.json",
				"https://www.prougno.com/ikea/dk/data-sources/live/DisplayTimer_Iframes.json",
				"https://www.prougno.com/ikea/dk/data-sources/live/DisplayTimer_Tags.json"
			]
		},
		preview:{
			Addon: [
				"https://www.prougno.com/ikea/dk/data-sources/preview/Addon.json"
			],
			DisplayTimerTemplates: [
				"https://www.prougno.com/ikea/dk/data-sources/preview/DisplayTimer_Templates_Iframes.json",
				"https://www.prougno.com/ikea/dk/data-sources/preview/DisplayTimer_Templates_Tags.json"
			],
			DisplayTimer: [
				"https://www.prougno.com/ikea/dk/data-sources/preview/DisplayTimer_Banners.json",
				"https://www.prougno.com/ikea/dk/data-sources/preview/DisplayTimer_Iframes.json",
				"https://www.prougno.com/ikea/dk/data-sources/preview/DisplayTimer_Tags.json"
			]
		}
	}
	*/
};


(function() {
    'use strict';
    try {

		var LoadSamuraj = function(callback){

			Samuraj.Version.load('samuraj', true, function(e) {
				if (e.preview) {
					Samuraj.Version.preview = true;
				} else {
					Samuraj.Version.preview = false;
				}

				window.SamurajQueue = window.SamurajQueue || [];

				window.SamurajMaxymiser = window.SamurajMaxymiser || {};
				var SamurajTemp = {
					Source: {
						sources: {},
						store: {},
						fillstore: function() {
							var c = Samuraj.Common;
							var datasources = SamurajSetting.DataSources.live;
							if (Samuraj.Version.forcePreview || Samuraj.Version.preview){
								datasources = SamurajSetting.DataSources.preview;
							}
							if (SamurajSetting.DirectLoad) {
								var directload = SamurajSetting.DirectLoad.live;
								if (Samuraj.Version.forcePreview || Samuraj.Version.preview){
									directload = SamurajSetting.DirectLoad.preview;
								}
								for (var listname in directload){
									var files = c.toArray(directload[listname]);
									Samuraj.Log.add({
										cat: listname,
										sub: "Timing",
										text: "File started",
										timing: performance.now() - SamurajTiming				
									});
									if (files.length > 0){
										Samuraj.DataSource.JSON.loadfile(files, function(data, listname) {
											Samuraj.Source.store[listname] = data;
											Samuraj.Log.add({
												cat: listname,
												sub: "Timing",
												text: "File loaded",
												timing: performance.now() - SamurajTiming				
											});
											Samuraj.Source.waitingRoom();
										}, listname);
									} else {
										Samuraj.Source.store[listname] = [];
										Samuraj.Log.add({
											cat: listname,
											sub: "Timing",
											text: "File loaded",
											timing: performance.now() - SamurajTiming				
										});
										Samuraj.Source.waitingRoom();
									}
								}
							}
							Samuraj.DataSource.JSON.loadfile(datasources, function(data) {
								for (var listname in data.datasources){
									var files = c.toArray(data.datasources[listname]);
									Samuraj.Log.add({
										cat: listname,
										sub: "Timing",
										text: "File started",
										timing: performance.now() - SamurajTiming				
									});
									if (files.length > 0){
										Samuraj.DataSource.JSON.loadfile(files, function(data, listname) {
											Samuraj.Source.store[listname] = data;
											Samuraj.Log.add({
												cat: listname,
												sub: "Timing",
												text: "File loaded",
												timing: performance.now() - SamurajTiming				
											});
											Samuraj.Source.waitingRoom();
										}, listname);
									} else {
										Samuraj.Source.store[listname] = [];
										Samuraj.Log.add({
											cat: listname,
											sub: "Timing",
											text: "File loaded",
											timing: performance.now() - SamurajTiming				
										});
										Samuraj.Source.waitingRoom();
									}
								}
							});
						},
						load: function(listnames, callback, context) {
							Samuraj.Source.RequestList = Samuraj.Source.RequestList || [];
							Samuraj.Source.RequestList.push({
								listnames: listnames,
								callback: callback,
								context: context
							});
							Samuraj.Source.waitingRoom();
						},
						waitingRoom: function() {
							if (Samuraj.Source.RequestList){
								for (var i = Samuraj.Source.RequestList.length-1; i > -1; i--) {
									var data = [];
									var datacomplete = true;
									var f = Samuraj.Source.RequestList[i];
									var listnames = f.listnames.split(" ");
									for (var j = 0; j < listnames.length; j++) {
										var listname = listnames[j]
											, optional = false;
										if (listnames[j].slice(0, 1) == "~") {
											listname = listnames[j].slice(1, listnames[j].length);
											optional = true;
										}
										if (!Samuraj.Source.store[listname] && !optional) {
											datacomplete = false;
										} else {
											data = Samuraj.Merge.merge(data, Samuraj.Source.store[listname]);
										}
									}
									if (datacomplete){
										Samuraj.Source.RequestList.splice(i, 1);
										if (Samuraj.Common.isFunction(f.callback)) 
											f.callback(data, f.context);
									}
								}
							}
						}

					},
					Common: {
						getProp: function(input) {
							//recursive function that loops through object and check content to avoid errors
							if (this.isString(input.prop) && (this.isObject(input.object) || this.isElement(input.object))) {
								if (!input.either) {
									input.originobject = input.object;
									input.either = input.prop.split("|");
								}
								var arr = input.either[0].split(".");
								if (input.object[arr[0]]) {
									if (arr.length > 1) {
										input.object = input.object[arr[0]];
										input.either[0] = arr.slice(1, arr.length).join().replace(/,/g, '.');
										return this.getProp(input);
									} else {
										if (this.isFunction(input.success))
											input.success(input.object[arr[0]]);
										else
											return input.object[arr[0]];
									}
								} else {
									input.either.splice(0, 1);
									if (input.either[0]){
										input.object = input.originobject;
										return this.getProp(input);
									} else {
										if (this.isFunction(input.failure))
											input.failure();
										else
											return undefined;
									}
								}
							}
						},
						getCustomAttribute: function(targetElem, attr) {
							var value = "";
							try {
								var regex = new RegExp(attr + "\=[" + '"' + "|'](.*?)[" + '"' + "|']");
								var valueAttr = targetElem.outerHTML.match(regex);
								value = valueAttr[1];
								if (!isNaN(value) && value.length < 4 && value.length > 0) {
									value = parseInt(value.replace(/\D/g, ''));
								}
							} catch (err) {}
							return value;
						},
						getElementsByClassName: function(className, holdingElement) {
							if (!holdingElement)
								holdingElement = document;
							var found = [];
							var elements = holdingElement.getElementsByTagName("*");

							for (var i = 0; i < elements.length; i++) {
								var names = elements[i].className.split(' ');
								for (var j = 0; j < names.length; j++) {
									if (names[j] == className)
										found.push(elements[i]);
								}
							}
							return found;
						},
						getMetaContent: function(attribute, search) {
							try {
								var metas = document.getElementsByTagName('meta');
								for (var i = 0; i < metas.length; i++) {
									var attr = metas[i].getAttribute(attribute);
									if (this.varExist(attr)) {
										if (attr.toLowerCase() == search.toLowerCase()) {
											return metas[i].getAttribute('content');
										}
									}
								}
								return '';
							} catch (e) {
								return ''
							}
						},
						checkurl: function(input, returnindex) {
							if (!this.isObject(input)) input = {masks: input, returnindex: returnindex};

							var bool = true;
							if (this.varExist(returnindex))
								if (returnindex)
									bool = false;
							var masks = []
							  , found = false;
							if (this.isArray(input.masks))
								masks = input.masks;
							else
								masks.push(input.masks);

							var initialurl = document.URL.split("?")[0];
							if (this.varExist(input.includeparams)) if (input.includeparams) initialurl = document.URL;

							for (var n = 0; n < masks.length; n++) {
								var url = {
									doc: initialurl.toLowerCase(),
									mask: masks[n].toLowerCase()
								}
								var parts = url.mask.split("*");
								url.doctemp = url.doc.replace("/?", "?").replace("/#", "#");
								if (url.doctemp.slice(url.doctemp.length - 1, url.doctemp.length) == "/")
									url.doctemp = url.doctemp.slice(0, url.doctemp.length - 1);
								for (var i = 0; i < parts.length; i++) {
									var either = parts[i].split("|");
									for (var j = 0; j < either.length; j++) {
										either[j] = either[j].replace("/?", "?").replace("/#", "#");
										if (either[j].slice(either[j].length - 1, either[j].length) == "/")
											either[j] = either[j].slice(0, either[j].length - 1);

										var loc = url.doctemp.indexOf(either[j]);
										if (loc > -1) {
											parts[i] = either[j];
											break;
										}
									}
									if (loc > -1) {
										if (i == parts.length - 1 || (i == parts.length - 2 && parts[parts.length - 1].length == 0)) {
											if (parts[i].length == 0 || parts[parts.length - 1].length == 0) {
												if (bool)
													return true;
												else
													return n;
											} else {
												if (parts[i].match(/[?|#]/g)) {
													if (parts[i] == url.doctemp) {
														if (bool)
															return true;
														else
															return n;
													}
													break;
												} else {
													url.doctempnoparam = /.+?[?|#]|.+/.exec(url.doctemp);
													url.docref = url.doctempnoparam[0].slice(loc, url.doctempnoparam[0].length).replace(/[?|#]/g, "");
													if (parts[i] == url.docref) {
														if (bool)
															return true;
														else
															return n;
													}
													break;
												}
											}

										}
										url.doctemp = url.doctemp.slice(loc + parts[i].length, url.doctemp.length);
									} else
										break;

								}
							}
							if (bool)
								return false;
							else
								return -1;
						},
						
						monitorUrlChange: function (input) {
							if (this.isObject(input)){
								if (this.isFunction(input.onchange)){
									var firsturl = input.url;
									input.url = input.url || document.URL;
									input.time = input.time || 100;

									if (document.URL != input.url || !this.varExist(firsturl, true)) {
										input.onchange(input);
										input.url = document.URL;
									}
									setTimeout(function() {
										Samuraj.Common.monitorUrlChange(input);
									}, input.time);
								}
							}
						},
						monitorDomChange: function (input) {
							if (this.isObject(input) || this.isFunction(input)){
								if (this.isFunction(input)) input = {onchange: input};
								if (this.isFunction(input.onchange)){
									input.baseelem = input.baseelem || document;
									
									if (window.MutationObserver){
										var observer = new MutationObserver(input.onchange);
										var config = { 
											attributes: false, 
											childList: true, 
											subtree: true 
										};
										input.baseelem = this.toArray(input.baseelem);
										for (var i=0; i<input.baseelem; i++){
											observer.observe(input.baseelem[i], config);
										}
									} else { 
										var changed = false;
										var dom = input.baseelem.getElementsByTagName("*");
										var i, len;
										if (this.varExist(input.dom)){
											len = input.dom.length;
											if (input.len !== len)
												changed = true;
											else {
												for (i=0;i<len;i++){
													if (input.dom[i].tagName !== dom[i].tagName){
														changed = true;
														break;
													}
												}
											}
										} else {
											len = dom.length;
											changed = true;
										}

										if (changed){
											input.dom = dom;
											input.len = len;
											input.onchange(input);
										}

										var continuemonitor = true;
										input.time = input.time || 100;
										if (input.timeout > 0) input.timeout -= input.time;
										if (input.timeout <= 0) continuemonitor = false;
										if (continuemonitor){
											setTimeout(function() {
												Samuraj.Common.monitorDomChange(input);
											}, input.time);
										}
									}
								}
							}
						},
						monitorElementChange: function (input) {
							var c = this;
							if (c.isObject(input) || c.isFunction(input)){
								if (c.isFunction(input)) input = {onchange: input};
								if (c.isFunction(input.onchange)){
									Samuraj.Element.get(input.selector, function(selector, elem){
										
										if (window.MutationObserver){
											var observer = new MutationObserver(input.onchange);
											var config = { 
												attributes: false, 
												childList: true, 
												subtree: true 
											};
											for (var i=0; i<elem.length; i++){
												observer.observe(elem[i], config);
											}
										} else {
										
											var elemstring = "";
											for (var i=0; i<elem.length; i++){
												elemstring += elem[i].innerHTML;
											}
											input.time = input.time || 100;
											var changed = false;
											if (input.oldelemstring){
												if (elemstring !== input.oldelemstring){
													changed = true;
												}
											} else {
												changed = true;
											}											
											if (changed){
												input.oldelemstring = elemstring;
												input.onchange(input);
												setTimeout(function() {
													Samuraj.Common.monitorElementChange(input);
												}, input.time);
											} else {
												var continuemonitor = true;
												if (input.timeout > 0) input.timeout -= input.time;
												if (input.timeout <= 0) continuemonitor = false;
												if (continuemonitor){
													setTimeout(function() {
														Samuraj.Common.monitorElementChange(input);
													}, input.time);
												}
											}
										}
									});
								}
							}
						},

						checkMobile: function() {
							return this.checkurl("*m2.ikea.|m2.ppe.*");
						},
						setDeviceCookie: function() {
							var c = this
							  , value = ""
							  , vars = c.getUrlVars();
							if (c.varExist(vars.device)) {
								if (vars.device.toLowerCase() == "mobile") {
									c.setCookie("device", "mobile");
								} else if (vars.device.toLowerCase() == "clear") {
									c.clearCookie("device");
								} else {
									c.setCookie("device", "desktop");
								}
							}
						},
						checkMaxymiserOverrideCookie: function() {
							var c = this
							  , value = ""
							  , vars = c.getUrlVars();
							if (c.varExist(vars.maxymiser)) {
								if (vars.maxymiser.toLowerCase() == "variant") {
									c.setCookie("maxymiser", "variant");
								} else if (vars.maxymiser.toLowerCase() == "clear") {
									c.clearCookie("maxymiser");
								} else {
									c.setCookie("maxymiser", "default");
								}
							}
							var cookie = c.getCookie("maxymiser");
							if (c.varExist(cookie, true)) {
								if (c.varExist(window.SamurajMaxymiser)) {
									window.SamurajMaxymiser.override = cookie;
								} else {
									window.SamurajMaxymiser = {};
									window.SamurajMaxymiser.override = cookie;
								}
							}

						},
						setCookie: function(cn, value, days) {
							var expires = '';
							if (days) {
								var date = new Date();
								date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
								expires = '; expires=' + date.toGMTString();
							}
							document.cookie = cn + '=' + value + expires + ';domain=.' + this.removeSubDomain(window.location.hostname) + ';path=/';
						},
						clearCookie: function(cn) {
							document.cookie = cn + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT' + ';domain=.' + this.removeSubDomain(window.location.hostname) + ';path=/';
						},
						getCookie: function(cn) {
							var match = document.cookie.match(new RegExp('(^|; )' + cn + '=([^;]+)'));
							if (match){
								if (match.length >= 2)
									return match[2];
							}
						},
						checkStorage: function(){

						},
						setStorage: function(cn, value, days) {
							if (typeof(Storage) !== "undefined") {
								if (days)
									localStorage.setItem(cn, value);
								else
									sessionStorage.setItem(cn, value);
							} else {
								this.setCookie(cn, value, days);
							}
						},
						clearStorage: function(cn) {
							if (typeof(Storage) !== "undefined") {
								localStorage.removeItem("lastname");
							} else {
								this.setCookie(cn, value, days);
							}
						},
						getStorage: function(cn) {
							if (typeof(Storage) !== "undefined") {
								var s;
								s = sessionStorage.getItem(cn);
								if (s) return s;
								else {
									s = localStorage.getItem(cn);
									if (s) return s;
								}
							} else {
								return this.getCookie(cn);
							}
						},

						removeSubDomain: function(hostn) {
							var is_co = hostn.match(/\.co\./);
							hostn = hostn.split('.');
							hostn = hostn.slice(is_co ? -3 : -2);
							hostn = hostn.join('.');
							return hostn;
						},
						varExist: function(variable, notEmpty) {
							if (typeof variable == 'undefined')
								return false;
							if (variable === 'undefined')
								return false;
							if (variable === null)
								return false;
							if (typeof notEmpty !== 'undefined')
								if (notEmpty)
									if (variable == "")
										return false;
							return true;
						},
						waitVarExist: function(input, callback, context) {
							window.setTimeout(function() {

								try {
									var endvar = window;
									var varchain = input.globalvarname.split(".");
									for (var i = 0; i < varchain.length; i++) {
										endvar = endvar[varchain[i]];
									}
									if (typeof endvar !== 'undefined') {
										callback({
											endvar: endvar,
											status: "success"
										}, context);
										return;
									}
								} catch (err) {}

								input.timeout = input.timeout - input.interval;
								if (input.timeout > 0)
									Samuraj.Common.waitVarExist(input, callback, context);
								else
									callback({
										endvar: undefined,
										status: "failed"
									}, context);

							}, input.interval);
						},
						hasClass: function(elem, cls) {
							if (elem){
								if (this.varExist(elem.className, true))
									return !!elem.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
								else
									return false;
							}
						},
						addClass: function(elem, cls) {
							if (elem){
								if (!this.hasClass(elem, cls)) {
									var t = elem.className + " " + cls;
									elem.className = t.replace("  ", " ");
								}
							}
						},
						removeClass: function(elem, cls) {
							var elems = this.toArray(elem);
							for (var i=0; i<elems.length; i++){
								if (this.hasClass(elems[i], cls)) {
									var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
									elems[i].className = elems[i].className.replace(reg, ' ');
								}
							}
						},
						addTimeoutClass: function(elem, cls, timeout){
							if (elem) {
								this.addClass(elem, cls);
								this.removeClass(elem, "samuraj-hidden");
								setTimeout(function(){
									Samuraj.Common.removeClass(elem, cls);
								}, timeout || 1000)
							}		
						},
						toArray: function(elems){
							var arr = [];
							if (this.isArray(elems)) arr = elems;
							else if (this.isNodeList(elems)){
								for (var i=0; i<elems.length; i++)
									arr.push(elems[i]);
							} else if (this.isObject(elems) || this.isElement(elems))
								arr.push(elems);
							else if (this.varExist(elems, true))
								arr.push(elems);
							return arr;
						},
						formatNumber: function(input) {
							var numeric = SamurajSetting.Country.numeric;
							var result = "", decimalseparator = "", decimalconditionalafterprice = "";
							if (!this.isObject(input)) input = {value: input}
							input.type = input.type || "int";
							if (input.type == "float") {
								var nr = this.toFloat(input);
								var nrstr = nr.toString();

								if (nrstr.indexOf(".") == -1) {
									decimalconditionalafterprice = numeric.decimalconditionalafterprice;
								} else {
									nr = nr.toFixed(2);
									nr = nr.replace(/\./g, numeric.decimalseparator);
								}
							} else {
								var nr = this.toInt(input);
								decimalconditionalafterprice = numeric.decimalconditionalafterprice;

							}

							return numeric.beforeprice + nr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, numeric.thousandseparator) + decimalconditionalafterprice + numeric.afterprice;
						},
						toInt: function(input) {
							var numeric = SamurajSetting.Country.numeric;
							if (!this.isObject(input)) input = {value: input}
							try {
								if (this.isInt(input.value)){
									try{
										input.value = parseFloat(input.value);
										return input.value;
									}catch(err){
										return input.value;
									}
								}else if (this.isFloat(input.value)){
									try{
										input.value = Math.round(input.value);
										return input.value;
									}catch(err){
										return input.value;
									}
								} else {
									input.value = input.value.replace(/[^-0-9,.]/g, "");
									if (input.format == "price"){
										//input.value = input.value.replace(/&nbsp;/g, " ");
										var regex = new RegExp('\\' + numeric.thousandseparator,  "g");
										input.value = input.value.replace(regex, "");
										var regex = new RegExp('\\' + numeric.decimalseparator,  "g");
										input.value = input.value.replace(regex, ".");
									}

									if (input.value.slice(0, 1) == "-") {
										var t = parseInt(input.value);
										if (isNaN(t)) t = 0;
										return t - (t * 2);
									} else {
										var t = parseInt(input.value);
										if (isNaN(t)) t = 0;
										return t;
									}
								}
							} catch (err) {
								return -1
							}
						},
						isInt: function(value) {
							if (isNaN(value)) {
								return false;
							}
							var x = parseFloat(value);
							var y = (x | 0) === x;
							return y;
						},
						toFloat: function(input) {
							var numeric = SamurajSetting.Country.numeric;
							if (!this.isObject(input)) input = {value: input}
							try {
								if (this.isInt(input.value) || this.isFloat(input.value)){
									try{
										input.value = parseFloat(input.value);
										return input.value;
									}catch(err){
										return input.value;
									}
								} else {
									input.value = input.value.replace(/[^-0-9,.]/g, "");
									if (input.format == "price"){
										//input.value = input.value.replace(/&nbsp;/g, " ");
										var regex = new RegExp('\\' + numeric.thousandseparator,  "g");
										input.value = input.value.replace(regex, "");
										var regex = new RegExp('\\' + numeric.decimalseparator,  "g");
										input.value = input.value.replace(regex, ".");
									}
									if (input.value.slice(0, 1) == "-") {
										var t = parseFloat(input.value);
										if (isNaN(t)) t = 0;
										return t - (t * 2);
									} else {
										var t = parseFloat(input.value);
										if (isNaN(t)) t = 0;
										return t;
									}
								}
							} catch (err) {
								return -1
							}
						},
						isFloat: function(n) {
							return Number(n) === n && n % 1 !== 0;
						},
						isString: function(obj) {
							return Object.prototype.toString.call(obj) === "[object String]";
						},
						isNumeric: function(obj) {
							return Object.prototype.toString.call(obj) === "[object Number]";
						},
						isBoolean: function(obj) {
							return Object.prototype.toString.call(obj) === "[object Boolean]";
						},
						isArray: function(obj) {
							return Object.prototype.toString.call(obj) === "[object Array]";
						},
						isNodeList: function(obj) {
							return Object.prototype.toString.call(obj) === "[object NodeList]";
						},
						isObject: function(obj) {
							var prot =  Object.prototype.toString.call(obj);
							return prot === "[object Object]" || prot === "[object Window]";
						},
						isElement: function(obj, returntype) {
							var type = Object.prototype.toString.call(obj);
							if (type.indexOf("Element")> -1){
								if (this.varExist(returntype)){
									if (returntype) return type.replace(/\[|\]|HTML|object| |Element/g, "");
									else return true;
								} else return true;
							} else return false;
						},
						isFunction: function(obj) {
							return Object.prototype.toString.call(obj) == "[object Function]";
						},
						getUrlVars: function(url) {
							var url = url || document.URL;
							var vars = {};
							var parts = url.replace(/[?&]+([^=&]+)=([^&#]*)/gi, function(m, key, value) {
								if (vars[key]) {
									if (vars[key]instanceof Array) {
										vars[key].push(value);
									} else {
										vars[key] = [vars[key], value];
									}
								} else {
									vars[key] = value;
								}
							});
							return vars;
						},
						getUrlHash: function(url) {
							var url = url || document.URL;
							var hash = "";
							var parts = url.replace(/([#]+[^&?]*)/gi, function(m, value) {
								hash = value;
							});
							return hash;
						},
						getProdNumUrl: function(url) {
							try {
								if (url)
									var pn = url.match(/[s|S|\/|-]\d{8}/g);
								else
									var pn = document.URL.match(/[s|S|\/|-]\d{8}/g);
								if (pn){
									if (pn.length > 0)
										return pn[0].replace('/', '').replace('-', '');
									else
										return "";
								} else return ""
							} catch (err) {
								return "";
							}
						},
						checkProdUrl: function(prodnum) {
							if (prodnum.toString().replace(/\D/g, "") == this.getProdNumUrl())
								return true;
							else
								return false;
						},
						xhr: function() {
							try {
								return new XMLHttpRequest();
							} catch (e) {}
							try {
								return new ActiveXObject("Msxml3.XMLHTTP");
							} catch (e) {}
							try {
								return new ActiveXObject("Msxml2.XMLHTTP.6.0");
							} catch (e) {}
							try {
								return new ActiveXObject("Msxml2.XMLHTTP.3.0");
							} catch (e) {}
							try {
								return new ActiveXObject("Msxml2.XMLHTTP");
							} catch (e) {}
							try {
								return new ActiveXObject("Microsoft.XMLHTTP");
							} catch (e) {}
							return null;
						},
						addEvent: function(elem, type, callback) {
							if (elem.addEventListener)
								return elem.addEventListener(type, callback, false);
							else
								return elem.attachEvent('on' + type, callback);
						},
						removeEvent: function(elem, type, func) {
							if (elem.removeEventListener)
								elem.removeEventListener(type, func, false);
							else {
								elem.detachEvent("on" + type, func);
							}
						},
						triggerEvent: function(elem, type, callback){
							if (document.createEvent) {
								var event = document.createEvent('HTMLEvents');
								event.initEvent(type, true, false);
								elem.dispatchEvent(event);
								if (typeof callback == "function") callback();
							} else {
								elem.fireEvent("on" + type);
								if (typeof callback == "function") callback();
							}
						},
						onetime: function(elem, type, callback) {
							var c = Samuraj.Common;
							c.addEvent(elem, type, function(e) {
								/*
								c.removeEvent(e.target, e.type, arguments.callee)
								*/
								callback(e);
							});
						},
						uridecode: function(value) {
							try {
								value = decodeURIComponent(value.replace(/\+/g, " "));
								return value;
							} catch (err) {}

						},
						urldecoderecur: function(value) {
							if (value.indexOf('%') != -1) {
								return this.urldecoderecur(this.uridecode(value));
							}

							return value;
						},
						uriencode: function(value) {
							try {
								value = encodeURIComponent(value).replace(/'/g, "%27").replace(/"/g, "%22");
								return value
							} catch (err) {}
						},
						goto: function (href, target) {
							var a = document.createElement("a");
							a.target = target || "";
							a.href = href;
							document.body.appendChild(a);
							a.click();
							document.body.removeChild(a);
						},						
						isMobile: {
							Android: function() {
								return navigator.userAgent.match(/Android/i);
							},
							BlackBerry: function() {
								return navigator.userAgent.match(/BlackBerry/i);
							},
							iOS: function() {
								return navigator.userAgent.match(/iPhone|iPad|iPod/i);
							},
							Opera: function() {
								return navigator.userAgent.match(/Opera Mini/i);
							},
							Windows: function() {
								return navigator.userAgent.match(/IEMobile/i);
							},
							any: function() {
								return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
							}
						},
						formatDate: function(date){
							if (date.from.date){
								date.from.time = date.from.time || "00:00";
								date.from.formatted = date.from.date + " - " + date.from.time; 
							} else {
								var datefrom = new Date(date.from);
								var tempdate = tempdate  || {};
								tempdate.from = {};
								tempdate.from.date = datefrom.getFullYear() + '.' + this.setTwoDigits(datefrom.getMonth() + 1) + '.' + this.setTwoDigits(datefrom.getDate());
								tempdate.from.time = this.setTwoDigits(datefrom.getHours()) + ':' + this.setTwoDigits(datefrom.getMinutes());
								tempdate.from.formatted = tempdate.from.date + " - " + tempdate.from.time;
							}
							if (date.to.date){
								date.to.time = date.from.time || "00:00";
								date.to.formatted = date.from.date + " - " + date.from.time; 
							} else {
								var dateto = new Date(date.to);
								var tempdate = tempdate  || {};
								tempdate.to = {};
								tempdate.to.date = dateto.getFullYear() + '.' + this.setTwoDigits(dateto.getMonth() + 1) + '.' + this.setTwoDigits(dateto.getDate());
								tempdate.to.time = this.setTwoDigits(dateto.getHours()) + ':' + this.setTwoDigits(dateto.getMinutes());
								tempdate.to.formatted = tempdate.to.date + " - " + tempdate.to.time;
							}
							if (tempdate)
								date = tempdate;
							return date;
						},
						formatDays: function(daynumber){
							daynumber = Samuraj.Common.toInt(daynumber);
							switch(daynumber) {
								case 0:
									return "Sun";
									break;
								case 1:
									return "Mon";
									break;
								case 2:
									return "Tue";
									break;
								case 3:
									return "Wed";
									break;
								case 4:
									return "Thu";
									break;
								case 5:
									return "Fri";
									break;
								case 6:
									return "Sat";
									break;
							}
						},
						setTwoDigits: function(val) {
							if (val && val < 10)
								return '0' + val;
							return val;
						}

					},
					//Common
					AJAX: {
						load: function(url, input, context){
							var c = Samuraj.Common;
							var type = "",
								method = "get";
							if (c.isObject(input)){
								type = input.type || "";
								method = input.method || "get";
							}
							var response = new c.xhr();
							var DONE = 4;
							var OK = 200;
							response.context = context;
							if (type.toLowerCase() == "json")
								response.overrideMimeType("application/json");
							response.open(method, url, true);
							response.onreadystatechange = function() {
								var c = Samuraj.Common;
								if (c.isObject(input)){
									if (response.readyState === DONE) {
										if (response.status === OK) {
											try {
												var obj = JSON.parse(response.responseText);
												if (c.isFunction(input.success))
													input.success(obj, response.context);

											} catch (e) {
												if (c.isFunction(input.fail))
													input.fail(response, response.context);
											}
										} else {
											if (c.isFunction(input.fail))
												input.fail(response, response.context);
										}
									}
								}
							}
							response.send(null);
						}
					},
					DataSource: {
						JSON: {
							corsbust: function(file, callback, context) {
								this.cachebust(function(time, file) {
									var c = Samuraj.Common
									  , device = "";
									if (c.checkurl("*m2.ikea.*"))
										device = "m2";
									else
										device = "irw";
									callback(file += "?" + device + time);
								}, file);

							},
							cachebust: function(callback, context) {
								var t = Samuraj.Time;
								t.get(function(time, context) {
									//time.slice(11, 15).replace(/:/g, "");
									var hour = time.slice(11, 13);
									var minute = time.slice(14, 16);
									var half = "0";
									if (Samuraj.Common.toInt(minute) > 29)
										half = "5";
									callback(hour + half, context);
								}, context);
							},
							combine: function(data) {
								var c = Samuraj.Common;
								var combined = [];
								for (var i = 0; i < data.length; i++) {

									var prop = Object.getOwnPropertyNames(data[i]);
									for (var j = 0; j < prop.length; j++) {

										if (c.isArray(data[i][prop[j]])) {
											combined[prop[j]] = combined[prop[j]] || [];
											for (var k = 0; k < data[i][prop[j]].length; k++) {
												combined[prop[j]].push(data[i][prop[j]][k]);
											}
										} else if (c.isObject(data[i][prop[j]])) {
											combined[prop[j]] = combined[prop[j]] || {};
											var objprop = Object.getOwnPropertyNames(data[i][prop[j]]);
											for (var k = 0; k < objprop.length; k++) {
												combined[prop[j]][objprop[k]] = data[i][prop[j]][objprop[k]];
											}

										} else {
											combined[prop[j]] = data[i][prop[j]] || "";
										}

									}
								}
								return combined;
							},
							
							loadfile: function(file, callback, context) {
								var c = Samuraj.Common;
								var failed = 0
								  , success = 0
								  , data = [];

								var files = c.toArray(file);
								var total = files.length;
								for (var i = 0; i < files.length; i++) {
									this.corsbust(files[i], function(file) {
										Samuraj.DataSource.store = Samuraj.DataSource.store || [];
										var store = Samuraj.DataSource.store;
										
										var response = new Samuraj.Common.xhr();
										var DONE = 4;
										var OK = 200;
										response.context = context;
										response.overrideMimeType("application/json");
										response.open('GET', file, true);
										response.onreadystatechange = function() {
											if (response.readyState === DONE) {
												if (response.status === OK) {
													try {
														var obj = JSON.parse(response.responseText);
														Samuraj.DataSource.store.push({
															url: response.responseURL,
															obj: obj
														});
														//Special for loading displaytimerdata
														if (obj.displaytimerdata){
															for (var i=0; i<obj.displaytimerdata.length; i++){
																obj.displaytimerdata[i].filepath = response.responseURL;
															}
														}
														//-----------------------
														data.push(obj);
														success++;
													} catch (e) {
														Samuraj.DataSource.store.push({
															url: response.responseURL
														});
														failed++;
													}
												} else {
													Samuraj.DataSource.store.push({
														url: response.responseURL
													});
													failed++;
												}
												if (success + failed == total) {
													callback(Samuraj.DataSource.JSON.combine(data), response.context);
												}
											}
										}
										response.send(null);
									});
								}
							},
							load: function(file, callback, context) {
								var c = Samuraj.Common;
								var failed = 0
								  , success = 0
								  , data = [];

								var files = c.toArray(file);
								var total = files.length;
								for (var i = 0; i < files.length; i++) {
									this.corsbust(files[i], function(file) {
										Samuraj.DataSource.store = Samuraj.DataSource.store || [];
										var store = Samuraj.DataSource.store;
										var found = false;
										for (var s=0; s<store.length; s++){
											if (store[s].url == file){
												if (store[s].obj){
													data.push(store[s].obj);
													success++;
												} else {
													failed++;
												}
												found = true;
												break;
											}
										}
										if (found){
											if (success + failed == total) {
												callback(Samuraj.DataSource.JSON.combine(data), context);
											}
										} 
									});
								}
							}
						},
						Cxense: {
							load: function(cxwid, callback, context) {
								//var category = document.querySelectorAll("meta[name='IRWStats.categoryLocal']").length > 0 ? document.querySelectorAll("meta[name='IRWStats.categoryLocal']")[0].content: "";
								try {
									window.cX = window.cX || {};
									window.cX.callQueue = window.cX.callQueue || [];
									window.cX.callQueue.push(['insertWidget', {
										widgetId: cxwid,
										renderFunction: function(data, context) {
											if (data.response.items.length > 0) {
												// data.response has all elementens including product id
												var response = data.response;
												response.products = [];
												try {
													var items = response.items;
													for (var i = 0; i < items.length; i++)
														response.products.push(items[i]['ike-productid']);
												} catch (err) {
													Samuraj.Log.add({
														cat: "Samuraj",
														sub: "Error",
														text: "Catch - Cxense - Loading products - " + err,
														err: err,
													});
												}
												callback(response, context);

											}
										},
										context: context
									}]);

								} catch (err) {
									Samuraj.Log.add({
										cat: "Samuraj",
										sub: "Error",
										text: "Catch - Cxense - load - " + err,
										err: err,
									});
								}
							},

							loadParams: function(cxwid, params, callback) {
								//var category = document.querySelectorAll("meta[name='IRWStats.categoryLocal']").length > 0 ? document.querySelectorAll("meta[name='IRWStats.categoryLocal']")[0].content: "";
								try {
									window.cX = window.cX || {};
									window.cX.callQueue = window.cX.callQueue || [];
									window.cX.callQueue.push(['insertWidget', {
										widgetId: cxwid,
										renderFunction: function(data, context) {
											if (data.response.items.length > 0) {
												// data.response inneholder alle elementene inkludert produktid
												callback(data.response);
											}
										}
									}, {
										context: {
											"parameters": params
										}
									}]);

								} catch (err) {}
							}
						}
					},
					//DataSource
					Customer: {
						getlocation: function(callback){
							this.location = this.location || Samuraj.Common.getStorage("samuraj_location");
							this.location = this.validatelocation(this.location);
							
							if (this.location.city == "na" && this.location.zipcode == "na" && this.location.region == "na" && this.location.country == "na"){
								if (SamurajSetting.Country.locale == "no_NO"){
									Samuraj.Customer.checkcxense(this.location, function(data){
										callback(data);
									});
								} else {
									this.location = {city: "other", region: "other", country: "other"};
									Samuraj.Common.setStorage("samuraj_location", JSON.stringify(this.location));
									callback(this.location);
								}
							} else {
								callback(this.location);
							}
						},
						validatelocation: function(data){
							var c = Samuraj.Common;
							var default_data = {city: "na", zipcode: "na", region: "na", country: "na"};
							if (!c.isObject(data)){
								try{
									data = JSON.parse(this.location);
								} catch(err){}
							}
							if (c.isObject(data)){
								if (data.items){
									if (data.items[0]){
										data.items[0].zipcode = data.items[0].zipcode || data.items[0].postalCode;
										delete data.items[0].postalCode;
										if (data.items[0].city && data.items[0].zipcode && data.items[0].region && data.items[0].country){
											data = data.items[0];
										} else data = default_data;
									} else data = default_data;
								} else {
									if (!(data.city && data.zipcode && data.region && data.country)){
										data = default_data;
									}
								}
							} else data = default_data;
							return data;
						},
						checkcxense: function(data, callback){
							Samuraj.DataSource.Cxense.load("cb1b26e2ebdeeb72190ba03a226decc3e3db5504", function(data){
								data = Samuraj.Customer.validatelocation(data);
								if (!(data.city == "na" && data.zipcode == "na" && data.region == "na" & data.country == "na")){
									Samuraj.Common.setStorage("samuraj_location", JSON.stringify(data));
									callback(data);
								} else {
									Samuraj.Customer.tries = Samuraj.Customer.tries || 0;
									Samuraj.Customer.tries++;
									if (Samuraj.Customer.tries < 2){
										window.cX = window.cX || {};
										window.cX.callQueue = window.cX.callQueue || [];
										cX.callQueue.push(['sendPageViewEvent', {
											location: "https://www.ikea.no/dummy"
										}, function() {
											cX.callQueue.push(['invoke', function() {
												setTimeout(function() {
													Samuraj.Customer.checkcxense(data, function(data){
														callback(data);
													});
												}, 500);
											}]);
										}]);
									} else if (Samuraj.Customer.tries < 8){
										setTimeout(function() {
											Samuraj.Customer.checkcxense(data, function(data){
												callback(data);
											});
										}, 200);
									} else {
										callback(Samuraj.Customer.location);
									}
								}
							});

						}
					},
					//Customer
					Element: {
						get: function(input, callback, context) {
							var c = Samuraj.Common;
							if (!c.isObject(input))
								var input = {
									selector: input
								};
							if (input.selector){
								input.baseelem = input.baseelem || document;
								if (!c.isArray(input.selector)) 
									input.selector = input.selector.split("|");
								if (input.selector.length > 1)
									this.geteither(input, callback, context);
								else {
									input.selector = c.urldecoderecur(input.selector[0]);
									
									// Check for anchor hash # in selector
									if (input.selector !== '#' && input.selector.indexOf("#_") == -1 && input.selector.indexOf("#/") == -1) {
										var timeout = input.timeout || 5000;
										var interval = input.interval || 20;

										if (input.selector.indexOf("~") > -1) {
											input.selector = input.selector.replace("#", "").replace(/_/g, " ");
																						
											if (window.MutationObserver){

												//use document body or the element from input as base element
												input.baseelem = input.baseelem.body || input.baseelem;

												var checkForElement = function(){
													Samuraj.Element.getrecur(input.baseelem, input.selector, function(elem, context) {
														if (elem){
															observer.disconnect();
															if (input.returnstatus)
																elem.status = "success";
															
															if  (c.isFunction(input.success)){
																input.elem = [elem];
																input.status = "success";
																input.originalselector = input.selector;
																input.selector = Samuraj.Element.getSelector(elem);
																input.success(input);
															} else if (c.isFunction(callback))
																callback(Samuraj.Element.getSelector(elem), [elem], context);
															else
																return elem;
														} 
													}, context);
												};
												var observer = new MutationObserver(checkForElement);
												var config = { 
													attributes: false, 
													childList: true, 
													subtree: true 
												};
												
												//Start observing
												observer.observe(input.baseelem, config);
												checkForElement();

												//Stop observing after timeout
												window.setTimeout(function() {
													if (observer){
														observer.disconnect();
														if (input.returnstatus){
															var elem = {};
															elem.status = "failed";
															if  (c.isFunction(input.failure)){
																input.elem = [elem];
																input.status = "failed";
																input.originalselector = input.selector;
																input.failure(input);
															} else if (c.isFunction(callback))
																callback(input.selector, [elem], context);
														}
													}
												}, timeout);

											} else {
												//Below IE 11 support
												window.setTimeout(function() {
													Samuraj.Element.getrecur(document.getElementsByTagName('body')[0], input.selector, function(elem, context) {
														if (elem){
															if (input.returnstatus)
																elem.status = "success";
															
															if  (c.isFunction(input.success)){
																input.elem = [elem];
																input.status = "success";
																input.originalselector = input.selector;
																input.selector = Samuraj.Element.getSelector(elem);
																input.success(input);
															} else if (c.isFunction(callback))
																callback(Samuraj.Element.getSelector(elem), [elem], context);
															else
																return elem;
															if (observer)
																observer.disconnect();
														} else {
															input.timeout = timeout - interval;
															if (input.timeout > 0)
																Samuraj.Element.get(input, callback, context);
															else {
																if (input.returnstatus){
																	var elem = {};
																	elem.status = "failed";
																	if  (c.isFunction(input.failure)){
																		input.elem = [elem];
																		input.status = "failed";
																		input.originalselector = input.selector;
																		input.failure(input);
																	} else if (c.isFunction(callback))
																		callback(input.selector, [elem], context);
																}
															}
														}
													}, context);
												}, interval);
											}
										} else {
											
											if (window.MutationObserver){

												var checkForElement = function(){
													var elem = input.baseelem.querySelectorAll(input.selector);
													if (c.varExist(input.elementnumber)){
														input.elementnumber = c.toInt(input.elementnumber);
														if (elem.length >= c.toInt(input.elementnumber)){
															elem = [elem[input.elementnumber]];
														} else elem = [];
													}
													if (elem.length > 0) {
														observer.disconnect();
														if (input.returnstatus)
															elem.status = "success";
														
														if  (c.isFunction(input.success)){
															input.elem = elem;
															input.status = "success";
															input.success(input);
														} else if (c.isFunction(callback))
															callback(input.selector, elem, context);
														else
															return elem;
													} 
												};
												var observer = new MutationObserver(checkForElement);
												var config = { 
													attributes: false, 
													childList: true, 
													subtree: true 
												};
												
												//Start observing
												observer.observe(input.baseelem, config);
												checkForElement();

												//Stop observing after timeout
												window.setTimeout(function() {
													if (observer){
														observer.disconnect();
														if (input.returnstatus){
															var elem = {};
															elem.status = "failed";
															if  (c.isFunction(input.failure)){
																input.elem = elem;
																input.status = "failed";
																input.failure(input);
															} else if (c.isFunction(callback))
																callback(input.selector, elem, context);
														}
													}
												}, timeout);

											} else {

												//Below IE 11 support
												var elem = input.baseelem.querySelectorAll(input.selector);
												if (c.varExist(input.elementnumber)){
													input.elementnumber = c.toInt(input.elementnumber);
													if (elem.length >= c.toInt(input.elementnumber)){
														elem = [elem[input.elementnumber]];
													} else elem = [];
												}
												if (elem.length > 0) {
													if (input.returnstatus){
														elem = {
															elem: elem,
															status: "success"
														}
													}
													if  (c.isFunction(input.success)){
														input.elem = elem;
														input.status = "success";
														input.success(input);
													} else if (c.isFunction(callback))
														callback(input.selector, elem, context);
													else
														return elem.elem || elem;
												} else {
													window.setTimeout(function() {
														var elem = input.baseelem.querySelectorAll(input.selector);
														if (c.varExist(input.elementnumber)){
															input.elementnumber = c.toInt(input.elementnumber);
															if (elem.length >= c.toInt(input.elementnumber)){
																elem = [elem[input.elementnumber]];
															} else elem = [];
														}
														if (elem.length > 0) {
															if (c.varExist(input.returnstatus)){
																if (input.returnstatus){
																	elem = {
																		elem: elem,
																		status: "success"
																	}
																}
															}
															if  (c.isFunction(input.success)){
																input.elem = elem;
																input.status = "success";
																input.success(input);
															} else if (c.isFunction(callback))
																callback(input.selector, elem, context);
															else
																return elem.elem || elem;
														} else {
															input.timeout = timeout - interval;
															if (input.timeout > 0)
																Samuraj.Element.get(input, callback, context);
															else {
																if (c.varExist(input.returnstatus)){
																	if (input.returnstatus){
																		var elem = {};
																		elem.status = "failed";
																		callback(input.selector, elem, context);
																	}
																}
															}
														}
													}, interval);
												}
											}
										}
									}
								}
							}
						},
						geteither: function(input, callback, context){
							var c = Samuraj.Common;
							if (!c.varExist(input.selector))
								if (c.varExist(input, true))
									var input = {
										selector: input
									};
							if (!c.isArray(input.selector)) input.selector = input.selector.split("|");
							var found = false;
							for (var i=0; i<input.selector.length; i++){
								var t_input = {
									selector: input.selector[i],
									elementnumber: input.elementnumber,
									timeout: input.timeout,
									interval: input.interval,
									returnstatus: input.returnstatus
								}
								this.get(t_input, function(selector, elem, context){
									if (!found) callback(selector, elem, context);
									found = true;

								}, context);
							}
						},
						getrecur: function(elem, searchtxt, callback, context) {
							var c = Samuraj.Common;
							searchtxt = searchtxt.replace("~", "");
							if (elem.childNodes.length == 1) {
								callback(elem, context);
								return;
							} else {
								for (var i = 0; i < elem.childNodes.length; i++) {
									var nn = elem.childNodes[i].nodeName;
									if (nn == "#text") {
										var haystack = elem.childNodes[i].data;
										if (haystack.indexOf(searchtxt) > -1) {
											callback(elem, context);
											return;
										}
									} else if (nn !== "#comment") {
										//nn !== "#text" &&
										var haystack = this.stripHTML(elem.childNodes[i].innerHTML);
										if (haystack.indexOf(searchtxt) > -1) {
											if (c.varExist(elem.childNodes[i].childNodes))
												this.getrecur(elem.childNodes[i], searchtxt, callback, context);
											else
												callback(elem.childNodes[i], context);
											return;
										}
									}
								}

								var haystack = this.stripHTML(elem.innerHTML);
								if (haystack.indexOf(searchtxt) > -1) {
									callback(elem, context);
									return;
								}
								callback('undefined', context);
							}
						},
						stripHTML: function(html){
							html = html.replace(/&amp;/gmi, "&");
							html = html.replace(/&lt;/gmi, "<");
							html = html.replace(/&gt;/gmi, ">");
							html = html.replace(/&nbsp;/gmi, " ");
							html = html.replace(/<script\b[^>]*>[\s\S]*?<\/script>/gmi, "");
							html = html.replace(/<style\b[^>]*>[\s\S]*?<\/style>/gmi, "");
							html = html.replace(/\s?<[^>]*>\s?/gmi, "");
							html = html.replace(/\s{3,}/gmi, "");
							return html;
						},
						countChildNodes: function(elem) {
							var count = 0;
							try {
								for (var i = 0; i < elem.childNodes.length; i++) {
									var nn = elem.childNodes[i].nodeName;
									if (nn !== "#text" && nn !== "#comment")
										count++;
								}
							} catch (err) {
								return 0
							}
							return count;
						},
						getChildNodes: function(elem) {
							var arr = [];
							try {
								for (var i = 0; i < elem.childNodes.length; i++) {
									var nn = elem.childNodes[i].nodeName;
									if (nn !== "#comment")
										arr.push(elem.childNodes[i]);
								}
							} catch (err) {
								return [];
							}
							return arr;
						},
						nextSibling: function(elem) {
							if (elem.nextElementSibling)
								return elem.nextElementSibling;
							do {
								elem = elem.nextSibling
							} while (elem && elem.nodeType !== 1);return elem;
						},
						hide: function (input){
							this.setcss(input, "display", "none");
						},
						show: function (input){
							this.setcss(input, "display", "block");
						},
						setcss: function(input, prop, value){
							var c = Samuraj.Common,
								elem, selelem;
							if (c.isObject(input)){
								if (c.varExists(input.selector)){
									if (!c.isArray(input.selector))
										input.selector.split("|");
									for (var i=0; i<input.selector.length; i++)
										this.get(input.selector[i], function(selector, elem){
											for (var j=0; j<elem.length; j++)
												elem[j].style.setProperty(prop, value);
										});
								} else {
									elem = c.toArray(input);
									for (var j=0; j<elem.length; j++)
										elem[j].style.setProperty(prop, value);
								}
							} else if (c.isArray(input) || c.isNodeList(input)){
								selelem = c.toArray(input);
								for (var i=0; i<selelem.length; i++){
									if (c.isObject(selelem[i]))
										selelem[i].style.setProperty(prop, value);
									else {
										this.get(selelem[i], function(selector, elem){
											for (var j=0; j<elem.length; j++)
												elem[j].style.setProperty(prop, value);
										});
									}
								}
							} else {
								input.split("|");
								for (var i=0; i<input.selector.length; i++)
									this.get(input.selector[i], function(selector, elem){
										for (var j=0; j<elem.length; j++)
											elem[j].style.setProperty(prop, value);
									});
							}
						},

						hidestyle: function(input) {
							var c = Samuraj.Common;
							if (!c.isObject(input))
								input = {selector: ""}
							if (c.isObject(input)) {
								if (!c.varExist(input.selector)){
									if (!c.varExist(input.elem))
										input.elem = input;
									for (var i = 0; i < input.elem.length; i++) {
										input.selector += this.getSelector(input.elem[i]);
										if (i < input.elem.length - 1)
											input.selector += ', ';
									}
								} else {
									if (c.isObject(input.elem)){
										if (input.selector.length>0) input.selector += ", ";
										for (var i = 0; i < input.elem.length; i++) {
											input.selector += this.getSelector(input.elem[i]);
											if (i < input.elem.length - 1)
												input.selector += ', ';
										}
									}
								}
							}

							if (!c.varExist(input.selector)) {
								if (c.varExist(input, true)){
									var input = {selector: input};
								}
							}
							input.id = input.id || "";
							if (input.id.length>0)
								input.id = "-" + input.id;
							var hse = document.querySelector("head style#samuraj-hse" + input.id);
							if (!c.varExist(hse)) {
								var hse = document.createElement("style");
								hse.id = "samuraj-hse" + input.id;
								document.head.appendChild(hse);
							}
							hse.innerHTML += input.selector + "{visibility:hidden!important}";
							setTimeout(function() {
								Samuraj.Element.showall("samuraj-hse" + input.id);
							}, 2000);
						},
						showall: function(id) {
							var c = Samuraj.Common;
							if(c.varExist(id))
								if (id.length>0)
									id = "-" + id;
							var hse = document.querySelector("head style#samuraj-hse" + id);
							if (c.varExist(hse)){
								var timeout = 100;
								Samuraj.Version.load('samurajshowalltimout1', false, function(e) {
									timeout = 1;
								});
								Samuraj.Version.load('samurajshowalltimout20', false, function(e) {
									timeout = 20;
								});
								Samuraj.Version.load('samurajshowalltimout50', false, function(e) {
									timeout = 50;
								});
								setTimeout(function(){
									hse.innerHTML = "";
								}, 100);
							}

						},
						getSelector: function(elem) {
							if (elem == null)
								return '';
							if (elem.parentElement == null)
								return 'html'
							return this.getSelector(elem.parentElement) + '>' + ':nth-child(' + this.getIndex(elem) + ')';
						},
						getIndex: function(elem) {
							if (elem == null)
								return -1;
							if (elem.parentElement == null)
								return 0;
							var parent = elem.parentElement;
							for (var index = 0; index < parent.childElementCount; index++)
								if (parent.children[index] == elem)
									return index + 1;
						},
						inject: function(elem, targetelem, placing){
							var run = true;
							if (!Samuraj.Common.isElement(elem)) run = false;
							if (run){
								switch(placing) {
									case "before":
										targetelem.parentNode.insertBefore(elem, targetelem);
										break;
									case "append":
										targetelem.appendChild(elem);
										break;
									case "prepend":
										targetelem.insertBefore(elem, targetelem.firstChild);
										break;
									case "replace":
										targetelem.innerHTML = "";
										targetelem.appendChild(elem);
										break;
									case "after":
										targetelem.parentNode.insertBefore(elem, targetelem.nextSibling);
										break;
									default:
										targetelem.parentNode.insertBefore(elem, targetelem.nextSibling);
										break;
								}
							}
						},
						remove: function(elem){
							var c = Samuraj.Common;
							elem = c.toArray(elem);
							for (var i=0; i<elem.length; i++){
								if (c.isElement(elem[i])) {
									try{
										elem[i].parentNode.removeChild(elem[i]);
									} catch(err){}
								}
							}
						},
						addModal:function(input){
							/* EXAMPLE

							Samuraj.Element.addModal({
								html: '<div style="font-size: 16px; min-height: 150px; text-align:left;"><img src="https://secure.ikea.com/ms/no_NO/img/sofa/ikon_fargevalg_50x45.png" style="float: left; height:100px">Takk for bestillingen din på stoffprøver. I løpet av 2 til 4 virkedager kan du forvente å få stoffprøvene levert i posten.<br><br>Lurer du på noe, ring IKEA Kundesenter på telefon 915 02340.</div>',
								includeclass: '',
								includecontentclass: '',
								autoclose: false,
								timeoutclose: 5000
							});
							*/

							var c = Samuraj.Common;
							if (!c.varExist(input.html)) if (!c.isObject(input)) input = {html: input};
							this.samuraj_modal = document.createElement("div");
							this.samuraj_modal.id = "samuraj-modal";
							var cls = "samuraj-modal"
							if (c.varExist(input.includeclass)) cls += " " + input.includeclass;
							this.samuraj_modal.className = cls;
							cls = "samuraj-modal-content"
							var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
							var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

							if (c.varExist(input.includecontentclass)) cls += " " + input.includecontentclass;
							this.samuraj_modal.innerHTML = "<div class=\"" + cls + "\" style=\"max-height:" + (h-200) + "px\">" +
								"<span id=\"samuraj-modal-close\" class=\"samuraj-modal-close\" style=\"visibility: hidden\">&times;</span>" +
								input.html +
								"</div>";
							this.get("body", function(selector, elem){
								elem[0].appendChild(Samuraj.Element.samuraj_modal);

								setTimeout(function(){
									Samuraj.Common.addClass(Samuraj.Element.samuraj_modal, "samuraj-show");
								},50);
								input.timeoutclose = input.timeoutclose || 1;
								var closed = false;
								setTimeout(function(){
									var c = Samuraj.Common;
									if (!closed){
										if (input.autoclose) {
											c.removeClass(Samuraj.Element.samuraj_modal, "samuraj-show");
											setTimeout(function(){
												var m = document.querySelectorAll(".samuraj-modal");
												for (var i=0; i<m.length; i++)
													Samuraj.Element.remove(m[i]);
												delete Samuraj.Element.samuraj_modal;
											},1000);
											closed = true;
										}
										if (!closed){
											Samuraj.Element.samuraj_modal_close = Samuraj.Element.samuraj_modal.querySelector("#samuraj-modal-close");
											Samuraj.Element.samuraj_modal_close.style.setProperty("visibility", "")

											c.addEvent(Samuraj.Element.samuraj_modal_close, "click", function(e){
												Samuraj.Common.removeClass(Samuraj.Element.samuraj_modal, "samuraj-show");
												setTimeout(function(){
													var m = document.querySelectorAll(".samuraj-modal");
													for (var i=0; i<m.length; i++)
														Samuraj.Element.remove(m[i]);
													delete Samuraj.Element.samuraj_modal;
												},1000);
											});
										}
									}
								}, input.timeoutclose);
								c.addEvent(window, "click", function(e){
									if (e.target == Samuraj.Element.samuraj_modal) {
										Samuraj.Common.removeClass(Samuraj.Element.samuraj_modal, "samuraj-show");
										setTimeout(function(){
											var m = document.querySelectorAll(".samuraj-modal");
											for (var i=0; i<m.length; i++)
												Samuraj.Element.remove(m[i]);
											delete Samuraj.Element.samuraj_modal;
										},1000);
										closed = true;
									}
								});
							});
						},
						dismiss: function() {
							this.common = {
								getPoint: function(e) {
									var point = {};
									if (e.targetTouches) {
										/* Prefer Touch Events */
										point.x = e.targetTouches[0].clientX;
										point.y = e.targetTouches[0].clientY;
									} else {
										/* Either Mouse event or Pointer Event */
										point.x = e.clientX;
										point.y = e.clientY;
									}
									return point;
								},
								requestAnimFrame: function(callback) {
									callback(window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame);
								},
								onAnimFrame: function() {
									if (!this.rafPending) {
										return;
									}
									var differenceInX = this.initialTouchPos.x - this.lastTouchPos.x;
									var newXTransform = (this.currentXPosition - differenceInX) + 'px';
									var transformStyle = 'translateX(' + newXTransform + ')';
									this.elem.style.webkitTransform = transformStyle;
									this.elem.style.MozTransform = transformStyle;
									this.elem.style.msTransform = transformStyle;
									this.elem.style.transform = transformStyle;
									this.rafPending = false;
								},
								updatePosition: function() {
									if (this.initialTouchPos !== null && this.lastTouchPos !== null) {
										var differenceInX = this.initialTouchPos.x - this.lastTouchPos.x;
										this.currentXPosition = this.currentXPosition - differenceInX;
										/* Go to the default state and change */
										var newState = this.STATE_DEFAULT;
										/* Check if we need to change state to left or right based on slop value */
										if (Math.abs(differenceInX) > this.slopValue) {
											if (this.currentState === this.STATE_DEFAULT) {
												if (differenceInX > 0) {
													newState = this.STATE_LEFT_SIDE;
												} else {
													newState = this.STATE_RIGHT_SIDE;
												}
											} else {
												if (this.currentState === this.STATE_LEFT_SIDE && differenceInX > 0) {
													newState = this.STATE_DEFAULT;
												} else if (this.currentState === this.STATE_RIGHT_SIDE && differenceInX < 0) {
													newState = this.STATE_DEFAULT;
												}
											}
										} else {
											newState = this.currentState;
										}
										var changeState = this.common.changeState.bind(this);
										changeState(newState);
										this.elem.style.setProperty("transition", 'all 150ms ease-out');
									}
								},
								changeState: function(newState) {
									var transformStyle;
									switch (newState) {
									case this.STATE_DEFAULT:
										this.currentXPosition = 0;
										break;
									case this.STATE_LEFT_SIDE:
										this.currentXPosition = -(this.itemWidth + this.originRect.x - this.handleSize);
										setTimeout(function() {
											this.elem.style.setProperty("display", "none");
										}
										.bind(this), 300);
										if (Samuraj.Common.isFunction(this.tracking))
											this.tracking("left");
										break;
									case this.STATE_RIGHT_SIDE:
										this.currentXPosition = this.bodyRect.width - this.originRect.x - this.handleSize;
										setTimeout(function() {
											this.elem.style.setProperty("display", "none");
										}
										.bind(this), 300);
										if (Samuraj.Common.isFunction(this.tracking))
											this.tracking("right");
										break;
									}
									transformStyle = "";
									if (this.currentXPosition !== 0)
										transformStyle = 'translateX(' + this.currentXPosition + 'px)';
									this.elem.style.msTransform = transformStyle;
									this.elem.style.MozTransform = transformStyle;
									this.elem.style.webkitTransform = transformStyle;
									this.elem.style.transform = transformStyle;
									this.currentState = newState;
								}
							};
							this.load = function(elem, targets) {
								/* Check if pointer events are supported. */
								this.STATE_DEFAULT = 1;
								this.STATE_LEFT_SIDE = 2;
								this.STATE_RIGHT_SIDE = 3;
								this.rafPending = false;
								this.initialTouchPos = null;
								this.lastTouchPos = null;
								this.currentState = this.STATE_DEFAULT;
								this.handleSize = 10;
								this.currentXPosition = 0;
								this.elem = elem;
								this.targets = targets;
								this.setPositions = function() {
									this.bodyRect = document.body.getBoundingClientRect();
									this.originRect = this.elem.getBoundingClientRect();
									this.itemWidth = this.originRect.width;
									this.slopValue = this.itemWidth * (1 / 4);
								};
								this.setPositions();
								/* On resize, change the slop value */
								window.addEventListener('resize', function(e) {
									this.setPositions();
									/* var changeState = this.common.changeState.bind(this); */
									/* changeState(this.currentState); */
								}.bind(this));
				
								if (window.PointerEvent) {
									/* Add Pointer Event Listener */
									this.elem.addEventListener('pointerdown', this.start, true);
									this.elem.addEventListener('pointermove', this.move, true);
									this.elem.addEventListener('pointerup', this.end, true);
									this.elem.addEventListener('pointercancel', this.end, true);
								} else {
									/* Add Touch Listener */
									this.elem.addEventListener('touchstart', this.start, true);
									this.elem.addEventListener('touchmove', this.move, true);
									this.elem.addEventListener('touchend', this.end, true);
									this.elem.addEventListener('touchcancel', this.end, true);
									/* Add Mouse Listener */
									this.elem.addEventListener('mousedown', this.start, true);
								}
							};
				
							/* Handle the start of gestures */
							this.start = function(e) {
								e.preventDefault();
								if (e.touches && e.touches.length > 1) {
									return;
								}
								var validtarget = false;
								for (var i = 0; i < this.targets.length; i++) {
									if (this.targets[i] == e.target)
										validtarget = true;
								}
								if (validtarget) {
									/* Add the move and end listeners */
									if (window.PointerEvent) {
										e.target.setPointerCapture(e.pointerId);
									} else {
										/* Add Mouse Listeners */
										document.addEventListener('mousemove', this.move, true);
										document.addEventListener('mouseup', this.end, true);
									}
									this.initialTouchPos = this.common.getPoint(e);
									this.elem.style.transition = 'initial';
								}
							}.bind(this);
				
							this.move = function(e) {
								e.preventDefault();
								if (!this.initialTouchPos) {
									return;
								}
								this.lastTouchPos = this.common.getPoint(e);
								if (this.rafPending) {
									return;
								}
								this.rafPending = true;
								this.common.requestAnimFrame(this.common.onAnimFrame.bind(this));
							}.bind(this);
				
							/* Handle end gestures */
							this.end = function(e) {
								e.preventDefault();
								if (e.touches && e.touches.length > 0) {
									return;
								}
								this.rafPending = false;
								/* Remove Event Listeners */
								if (window.PointerEvent) {
									e.target.releasePointerCapture(e.pointerId);
								} else {
									/* Remove Mouse Listeners */
									document.removeEventListener('mousemove', this.handleGestureMove, true);
									document.removeEventListener('mouseup', this.handleGestureEnd, true);
								}
								var updatePosition = this.common.updatePosition.bind(this);
								updatePosition();
								this.initialTouchPos = null;
							}.bind(this);
						}
					},
					//Element
					
					Tracking: {
						send: function(input){
							var c = Samuraj.Common;
							if (c.checkMobile()){
					
								// M2 section
					
								if (input.params){

									/*
									Samuraj.Tracking.send({
										category: 'ecommerce',
										action: 'add_to_cart',
										label: product_name - " - " + product_artnr,
										params: {
											value: product_price,
											currency: "NOK",
											items: [
												{
													id: product_artnr,
													name: product_name,
													price: product_price.toString(),
													quantity: 1
												}
											]
										}
									});
									{
										action: 'add_to_cart',
										params: {
											value: 1719.2,
											currency: 'NOK',
											items: [{
												id: '59241253',
												name: 'songesand',
												brand: 'SONGESAND-serien',
												category: '25205',
												price: '1719.2',
												quantity: 1,
												variant: '160x200 cm,Luröy'
											}]
										}
									}
									*/
									if (input.action){
										input.name = input.action;
										delete input.action;
										if (input.category) delete input.category;
										if (input.label) delete input.label;
										
										for (var prop in input.params) { 
											if (input.params.hasOwnProperty(prop)) {
												if (prop == "value"){
													input.params.value = parseInt(input.params.value.toString().replace(/[\s\.]/g, "").replace(/\,/g, ".").replace(/\-/g, "00").replace(/[^0-9\.]/g, ""));
												}
												if (prop == "items"){
													input.params.items = Samuraj.Common.toArray(input.params.items);
													for (var i=0; i<input.params.items.length; i++){
														
														for (var item_prop in input.params.items[i]) {
															if (input.params.items[i].hasOwnProperty(item_prop)) {
																if (item_prop == "id"){
																	input.params.items[i][item_prop] = input.params.items[i][item_prop].toString();
																}
																if (item_prop == "price"){
																	input.params.items[i][item_prop] = input.params.items[i][item_prop].toString().replace(/[\s\.]/g, "").replace(/\,/g, ".").replace(/\-/g, "00").replace(/[^0-9\.]/g, "");
																}
																if (item_prop == "quantity"){
																	input.params.items[i][item_prop] = input.params.items[i][item_prop].toString().replace(/[^0-9\.]/g, "");
																}
															}
														}
													}
												}
											}
										}
										
										if (window.sendEvent) 
											window.sendEvent(input);
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.action is not defined",
											input: input
										});
									}
								} else {

									/*
									{
										category: 'local_tracking_filter',
										action: 'filter_by_color',
										label: 'green',
										custom: {
											buyability: 'Yes',
											ratings_value: '1'
										}
									}
									*/
									var all_ok = true;
									if (input.category){
										input.event_category = input.category;
										delete input.category;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.category is not defined",
											input: input
										});
										all_ok = false;
									}
									if (input.action){
										input.event_action = input.action;
										delete input.action;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.action is not defined",
											input: input
										});
										all_ok = false;
									}
					
									if (input.label){
										input.event_label = input.label || "";
										delete input.label;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.label is not defined",
											input: input
										});
										all_ok = false;
									}
									if (all_ok && window.sendEvent) window.sendEvent(input);
								} 
							} else {
								
								// IRW section

								if (input.params){

									/*
									Samuraj.Tracking.send({
										category: 'ecommerce',
										action: 'add_to_cart',
										label: product_name - " - " + product_artnr,
										params: {
											value: product_price,
											currency: "NOK",
											items: [
												{
													id: product_artnr,
													name: product_name,
													price: product_price.toString(),
													quantity: 1
												}
											]
										}
									});
									{
										action: 'add_to_cart',
										params: {
											value: 1719.2,
											currency: 'NOK',
											items: [{
												id: '59241253',
												name: 'songesand',
												brand: 'SONGESAND-serien',
												category: '25205',
												price: '1719.2',
												quantity: 1,
												variant: '160x200 cm,Luröy'
											}]
										}
									}
									*/

									var all_ok = true;
									if (input.category){
										input.event_category = input.category;
										delete input.category;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.category is not defined",
											input: input
										});
										all_ok = false;
									}
									if (input.action){
										input.event_action = input.action;
										delete input.action;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.action is not defined",
											input: input
										});
										all_ok = false;
									}
						
									if (input.label){
										input.event_label = input.label || "";
										delete input.label;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.label is not defined",
											input: input
										});
										all_ok = false;
									}

									//Custom needs converting for IRW
									
									for (var prop in input.params) { 
										if (input.params.hasOwnProperty(prop)) {
											if (prop == "items"){
												input.params.items = Samuraj.Common.toArray(input.params.items);
												for (var i=0; i<input.params.items.length; i++){
													
													for (var item_prop in input.params.items[i]) {
														if (input.params.items[i].hasOwnProperty(item_prop)) {
															if (item_prop == "name"){
																input.product_names = input.product_names || [];
																input.product_names.push(input.params.items[i][item_prop]);
															}
															if (item_prop == "id"){
																input.product_ids = input.product_ids || [];
																input.product_ids.push(input.params.items[i][item_prop].toString());
															}
															if (item_prop == "price"){
																input.product_prices_vat = input.product_prices_vat || [];
																input.product_prices_vat.push(input.params.items[i][item_prop].toString().replace(/[\s\.]/g, "").replace(/\,/g, ".").replace(/\-/g, "00").replace(/[^0-9\.]/g, ""));
															}
															if (item_prop == "quantity"){
																input.product_units = input.product_units || [];
																input.product_units.push(input.params.items[i][item_prop].toString().replace(/[^0-9]/g, ""));
															}
														}
													}
												}
											}
										}
									}
									delete input.params;            
									
									if (all_ok && utag) utag.link(input);

								} else {
								
									/*
									{
										category: 'local_tracking_filter',
										action: 'filter_by_color',
										label: 'green',
										custom: {
											buyability: 'Yes',
											ratings_value: '1'
										}
									}
									*/
									var all_ok = true;
									if (input.category){
										input.event_category = input.category;
										delete input.category;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.category is not defined",
											input: input
										});
										all_ok = false;
									}
									if (input.action){
										input.event_action = input.action;
										delete input.action;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.action is not defined",
											input: input
										});
										all_ok = false;
									}
						
									if (input.label){
										input.event_label = input.label || "";
										delete input.label;
									} else {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "Tracking - input.label is not defined",
											input: input
										});
										all_ok = false;
									}
									
									//Default settings
									input.visit_country = "no";
									input.visit_language = "no";
									input.site_platform = "irw";

									//Custom needs converting for IRW
									if (input.custom){
										for (var prop in input.custom) { 
											if (input.custom.hasOwnProperty(prop)) {
												input[prop] = input.custom[prop];
											}
										}
										delete input.custom;            
									}
									
									if (all_ok && utag) utag.link(input);
								}
					
							}
					
						}
					},
					//Tracking
					Targeting: {
						queue: [],
						reload: true,
						products: function(crit, callback, context){
							//Samuraj.Page.loaded(function(){
								var c = Samuraj.Common,
									t = Samuraj.Targeting;
								if (c.varExist(t.data)){
									if (t.data.currentpage !== document.URL || t.reload){
										if (!t.triggered){
											t.triggered = true;
											//t.data = {};
											//setTimeout(function(){
												t.allProducts(function(data){
													t.data = data;
													t.triggered = false;
													if (c.isFunction(callback))
														callback(t.filterProducts(crit, t.data), context);
													t.waitingRoom();
												});
											//}, 500);
										} else {
											t.queue = t.queue || [];
											t.queue.unshift({
												crit: crit,
												callback: callback,
												context: context
											});
										}
									} else {
										if (c.isFunction(callback))
											callback(t.filterProducts(crit, t.data), context);
									}
								} else {
									if (!t.triggered){
										t.triggered = true;
										//t.data = {};
										t.allProducts(function(data){
											t.data = data;
											t.triggered = false;
											if (c.isFunction(callback))
												callback(t.filterProducts(crit, t.data), context);
											t.waitingRoom();
										});
									} else {
										t.queue = t.queue || [];
										t.queue.unshift({
											crit: crit,
											callback: callback,
											context: context
										});
									}
								}
								t.reload = false;

							//});
						},
						waitingRoom: function(){
							var c = Samuraj.Common,
								t = Samuraj.Targeting;
							if (!t.bouncer){
								t.bouncer = true;
								for (var i = t.queue.length-1; i>-1; i--){
									if (c.isFunction(t.queue[i].callback))
										t.queue[i].callback(t.filterProducts(t.queue[i].crit, t.data), t.queue[i].context);
									t.queue.splice(i, 1);
								}
								t.bouncer = false;
								if (t.queue.length > 0)
									this.waitingRoom();
							}
						},
						allProducts: function (callback, context) {
							var c = Samuraj.Common,
								e = Samuraj.Element;
							var data = {},
								elem;
							data.currentpage = document.URL;
							data.pagetype = "na";
							data.dom = {
								main: {
									elems: [],
									data: [],
									loc: []
								},
								other: {
									compl: {
										elems: [],
										data: [],
										loc: []
									},
									more: {
										elems: [],
										data: [],
										loc: []
									},
									spr: {
										elems: [],
										data: [],
										loc: []
									}

								}
							}
							if (c.checkMobile()){

								//M2

								if (c.checkurl("*/" + SamurajSetting.Country.homePath + "/p/*")){
									data.pagetype = "pip";
									e.get("footer.footer", function(selector, elem, data){
										data.dom.main = {
											elems: [document],
											data: [],
											loc: [
												{selector: "#pip-carousel", placing: "prepend"},
												{selector: ".price-package", placing: "before"},
												{selector: ".product-pip__purchase", placing: "before"},
												{selector: "#stock-check", placing: "before"},
												{selector: "div.product-pip__top-container", placing: "after"},
												{selector: "main div.product-pip", placing: "append"}
											]
										}
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											artnr = c.getProdNumUrl().replace(/\D/g, "");
											elem = data.dom.main.elems[i].querySelector("div.product-pip__price-package h1.product-pip__product-heading span.pip-header-font");
											if (elem) name = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("div.product-pip__price-package h1.product-pip__product-heading span.normal-font");
											if (elem) desc = elem.innerHTML;
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else if (c.checkurl("*/" + SamurajSetting.Country.homePath + "/search/*")) {
									data.pagetype = "search";
									e.get("footer.footer", function(selector, elem, data){
										data.dom.main = {
											elems: document.querySelectorAll("span.product-compact"),
											data: [],
											loc: [
												{selector: ".image-claim-height", placing: "prepend"},
												{selector: ".product-compact__name", placing: "before"},
												{selector: "a", placing: "after"},
												{selector: "a", placing: "after"}
											]
										}
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											elem = data.dom.main.elems[i].querySelector("a");
											if (elem) artnr = c.getProdNumUrl(elem.href).replace(/\D/g, "");
											elem = data.dom.main.elems[i].querySelector("span.product-compact__name");
											if (elem) name = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("span.product-compact__type");
											if (elem) desc = elem.innerHTML;
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else if (c.checkurl("*/shop/wishlist/*")) {
									data.pagetype = "wishlist";
									e.get("footer.footer", function(selector, elem, data){
										data.dom.main = {
											elems: document.querySelectorAll(".productlist .product"),
											data: [],
											loc: [
												{selector: "div._Rfxh_._Rfx2_", placing: "after"}
											]
										}
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											elem = data.dom.main.elems[i].querySelector("span.product_description-article-number");
											if (elem) artnr = elem.innerHTML.replace(/[^0-9]/g,"");
											elem = data.dom.main.elems[i].querySelector("div._Rfx3_ h2");
											if (elem) name = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("span.product_description-type");
											if (elem) desc = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("span.product__description-design-text");
											if (elem) desc += ", " + elem.innerHTML;
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else if (!c.checkurl("*/webapp/wcs/stores/servlet/*")) {
									data.pagetype = "plp";
									e.get("footer.footer", function(selector, elem, data){
										data.dom.main = {
											elems: document.querySelectorAll("div.product-compact"),
											data: [],
											loc: [
												{selector: ".image-claim-height", placing: "prepend"},
												{selector: ".product-compact__name", placing: "before"},
												{selector: "a", placing: "after"},
												{selector: "a", placing: "after"}
											]
										}
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											elem = data.dom.main.elems[i].querySelector("a");
											if (elem) artnr = c.getProdNumUrl(elem.href).replace(/\D/g, "");
											elem = data.dom.main.elems[i].querySelector("span.product-compact__name");
											if (elem) name = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("span.product-compact__type");
											if (elem) desc = elem.innerHTML;
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else {
									data.dom.main = {
										elems: [document],
										data: [],
										loc: []
									}
									if (Samuraj.Common.isFunction(callback))
										callback(data, context);
								}
							} else {
								
								//IRW

								if (c.checkurl("*/catalog/products/*")){
									data.pagetype = "pip";
									e.get("div#footer", function(selector, elem, data){
										setTimeout(function(){
											data.dom.main = {
												elems: [document],
												data: [],
												loc: [
													{selector: "#mainImgConatiner", placing: "prepend"},
													{selector: "#rightNavInfoDiv", placing: "before"},
													{selector: "#rightNavInfoDiv", placing: "after"},
													{selector: "#stockResultHolder", placing: "before"},
													{selector: "#pipContainer", placing: "after"},
													{selector: "#footer", placing: "before"}
												]
											}
											var max = data.dom.main.elems.length;
											for (var i=0; i<max; i++){
												var artnr = "", name = "", desc = "";
												elem = data.dom.main.elems[i].querySelector("div.itemNumber #itemNumber");
												if (elem) artnr = elem.innerHTML.replace(/[^0-9]/g,"");
												elem = data.dom.main.elems[i].querySelector("div#productInfoWrapper2 #name");
												if (elem) name = elem.innerHTML;
												elem = data.dom.main.elems[i].querySelector("div#productInfoWrapper2 #type");
												if (elem) desc = elem.innerHTML;
												data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
											}

											data.dom.other.compl = {
												elems: document.querySelectorAll("#complementaryProductContainer li.prodClass"),
												data: [],
												loc: [
													{selector: ".svgButtonContainer", placing: "before"}
												]
											}
											var max = data.dom.other.compl.elems.length;
											for (var i=0; i<max; i++){
												var artnr = "", name = "", desc = "";
												artnr = data.dom.other.compl.elems[i].id.split("_")[1].replace(/[^0-9]/g,"");
												elem = data.dom.other.compl.elems[i].querySelector("span.prodName");
												if (elem) name = elem.innerHTML;
												elem = data.dom.other.compl.elems[i].querySelector("span.prodDesc");
												if (elem) desc = elem.innerHTML;
												data.dom.other.compl.data.push({artnr: artnr, name: name, desc: desc});
											}
											data.dom.other.more = {
												elems: document.querySelectorAll("#moreProdModule li.prodClass"),
												data: [],
												loc: [
													{selector: "a", placing: "append"}
												]
											}
											var max = data.dom.other.more.elems.length;
											for (var i=0; i<max; i++){
												var artnr = "", name = "", desc = "";
												artnr = data.dom.other.more.elems[i].id.split("_")[1].replace(/[^0-9]/g,"");
												elem = data.dom.other.more.elems[i].querySelector("span.prodName");
												if (elem) name = elem.innerHTML;
												elem = data.dom.other.more.elems[i].querySelector("span.prodDesc");
												if (elem) desc = elem.innerHTML;
												data.dom.other.more.data.push({artnr: artnr, name: name, desc: desc});
											}

											data = Samuraj.Targeting.clean(data);
											if (Samuraj.Common.isFunction(callback))
												callback(data, context);
										},50);
									}, data);
								} else if (c.checkurl("*/catalog/availability/*")){
									data.pagetype = "availability";
									e.get("div#footer", function(selector, elem, data){
										data.dom.main = {
											elems: document.querySelectorAll(".sc_product_container"),
											data: [],
											loc: [
												{selector: ".sc_product_img_container", placing: "after"}
											]
										};
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											elem = data.dom.main.elems[i].querySelector("a#pipUrl");
											if (elem) artnr = /([^sS\/]*)\/?$/g.exec(elem.href)[1];
											elem = data.dom.main.elems[i].querySelector("span#name");
											if (elem) name = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("span#type");
											if (elem) desc = elem.innerHTML;
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else if (c.checkurl("*/search/*")) {
									data.pagetype = "search";
									e.get("div#footer", function(selector, elem, data){
										data.dom.main = {
											elems: document.querySelectorAll("#productsTable .productContainer"),
											data: [],
											loc: [
												{selector: "img.prodImg", placing: "before"},
												{selector: ".prodName", placing: "before"},
												{selector: ".moreInfo .buttonsContainer", placing: "before"},
												{selector: ".buttonsContainer", placing: "before"}
											]
										}
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											artnr = data.dom.main.elems[i].id.split("_")[1].replace(/[^0-9]/g,"");
											elem = data.dom.main.elems[i].querySelector("span.prodName");
											if (elem) name = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("span.prodDesc");
											if (elem) desc = elem.innerHTML;
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else if (c.checkurl("*/InterestItemDisplay")) {
									data.pagetype = "wishlist";
									e.get("div#footer", function(selector, elem, data){
										data.dom.main = {
											elems: document.querySelectorAll("#productsContainer table tbody tr"),
											data: [],
											loc: [
												{ selector: "td.colBuyable", placing: "after"}
											]
										}
										var elems = [];
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											if (c.varExist(data.dom.main.elems[i].id, true)){
												if (data.dom.main.elems[i].id.indexOf("tr_") > -1)
													elems.push(data.dom.main.elems[i]);
											}
										}
										data.dom.main.elems = elems;
										max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											artnr = data.dom.main.elems[i].id.split("_")[1].replace(/[^0-9]/g,"");
											elem = data.dom.main.elems[i].querySelector("span.prodName");
											if (elem) name = elem.innerHTML;
											elem = data.dom.main.elems[i].querySelector("span.prodDesc");
											if (elem) desc = elem.innerHTML;
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else if (!c.checkurl("*/webapp/wcs/stores/servlet/*")) {
									data.pagetype = "plp";
									e.get("div#footer", function(selector, elem, data){
										//elems: document.querySelectorAll("#productLists .product .products"),
										data.dom.main = {
											elems: document.querySelectorAll(".productLists .product, .product-list .product"),
											data: [],
											loc: [
												{selector: ".image", placing: "before"},
												{selector: ".productDetails", placing: "before"},
												{selector: ".moreInfo", placing: "before"},
												{selector: ".moreInfo .buttonsContainer", placing: "before"},
												{selector: ".moreInfo .buttonsContainer", placing: "after"}
											]
										}
										var max = data.dom.main.elems.length;
										for (var i=0; i<max; i++){
											var artnr = "", name = "", desc = "";
											/*
											if (c.hasClass(data.dom.main.elems[i], "threeColumn")){
												artnr = data.dom.main.elems[i].id.split("_")[1].replace(/[^0-9]/g,"");
												elem = data.dom.main.elems[i].querySelector("div.productDetails .productTitle");
												if (elem) name = elem.innerHTML;
												elem = data.dom.main.elems[i].querySelector("div.productDetails .productDesp");
												if (elem) desc = elem.innerHTML;
											} else {
												*/
												elem = data.dom.main.elems[i].querySelector("div.productDetails .productTitle");
												if (elem) {
													name = elem.innerHTML;
													artnr = /([^sS\/]*)\/?$/g.exec(elem.parentNode.parentNode.href)[1];
													if (artnr == "undefined"){
														artnr = /([^sS\/]*)\/?$/g.exec(elem.parentNode.href)[1];
													}
												}
												elem = data.dom.main.elems[i].querySelector("div.productDetails .productDesp");
												if (elem) desc = elem.innerHTML;
											//}
											data.dom.main.data.push({artnr: artnr, name: name, desc: desc});
										}
										data = Samuraj.Targeting.clean(data);
										if (Samuraj.Common.isFunction(callback))
											callback(data, context);
									}, data);
								} else {
									if (Samuraj.Common.isFunction(callback))
										callback(data, context);
								}
							}

						},
						clean: function(data){
							for (var i=0; i<data.dom.main.data.length; i++){
								data.dom.main.data[i].desc = data.dom.main.data[i].desc.replace(/&nbsp;/g, "");
							}
							for (var i=0; i<data.dom.other.compl.data.length; i++){
								data.dom.other.compl.data[i].desc = data.dom.other.compl.data[i].desc.replace(/&nbsp;/g, "");
							}
							for (var i=0; i<data.dom.other.more.data.length; i++){
								data.dom.other.more.data[i].desc = data.dom.other.more.data[i].desc.replace(/&nbsp;/g, "");
							}
							return data;
						},

						filterProducts: function(crit, input, callback, context) {
							var c = Samuraj.Common;
							var maxi, maxj, maxk;
							
							if (c.varExist(input.dom)) {
								var output = this.newoutput(input);
								var output_art = this.newoutput(input);
								if (crit.artnr){
									if (!c.varExist(crit.artnr.incl) && !c.varExist(crit.artnr.excl)){
										var t = {incl: [], excl:[]}
										crit.artnr = crit.artnr.toString().replace(/[^0-9,]/g,"");
										crit.artnr = c.toArray(crit.artnr);
										for (var i=0; i<crit.artnr.length; i++){
											var split_ie = crit.artnr[i].split("!");
											if (split_ie[0])
												t.incl.push(split_ie[0]);
											if (split_ie[1])
												t.excl.push(split_ie[1]);
										}
										crit.artnr = t;
									}
									if (c.varExist(crit.artnr.incl)){
										crit.artnr.incl = c.toArray(crit.artnr.incl);
										var t = [];
										for (var i=0; i<crit.artnr.incl.length; i++){
											crit.artnr.incl[i] = crit.artnr.incl[i].toString().replace(/[^0-9,]/g,"");
											var split_artnr = crit.artnr.incl[i].split(",");
											for (var j=0; j<split_artnr.length; j++){
												if (split_artnr[j].length >= 3)
													t.push(split_artnr[j]);
											}
										}
										crit.artnr.incl = t;
									} else crit.artnr.incl = [];

 									if (c.varExist(crit.artnr.excl)){
										crit.artnr.excl = c.toArray(crit.artnr.excl);
										var t = [];
										for (var i=0; i<crit.artnr.excl.length; i++){
											crit.artnr.excl[i] = crit.artnr.excl[i].toString().replace(/[^0-9,]/g,"");
											var split_artnr = crit.artnr.excl[i].split(",");
											for (var j=0; j<split_artnr.length; j++){
												if (split_artnr[j].length >= 3)
													t.push(split_artnr[j]);
											}
										}
										crit.artnr.excl = t;
									} else crit.artnr.excl = [];
								} else crit.artnr = {incl: [], excl: []};

								if (crit.name){
									if (!c.varExist(crit.name.incl) && !c.varExist(crit.name.excl)){
										var t = {incl: [], excl:[]}
										if (crit.name.replace(/\s/g, "") == "")
											crit.name = "";
										crit.name = c.toArray(crit.name);
										for (var i=0; i<crit.name.length; i++){
											var split_ie = crit.name[i].split("!");
											if (split_ie[0])
												t.incl.push(split_ie[0]);
											if (split_ie[1])
												t.excl.push(split_ie[1]);
										}
										crit.name = t;
									}
									if (c.varExist(crit.name.incl)){
										crit.name.incl = c.toArray(crit.name.incl);
										var t = [];
										for (var i=0; i<crit.name.incl.length; i++){
											if (crit.name.incl[i].replace(/\s/g, "") == "")
												crit.name.incl[i] = "";
											var split_name = crit.name.incl[i].split("|");
											for (var j=0; j<split_name.length; j++){
												t.push(split_name[j]);
											}
										}
										crit.name.incl = t;
									}  else crit.name.incl = [];
									if (c.varExist(crit.name.excl)){
										crit.name.excl = c.toArray(crit.name.excl);
										var t = [];
										for (var i=0; i<crit.name.excl.length; i++){
											if (crit.name.excl[i].replace(/\s/g, "") == "")
												crit.name.excl[i] = "";
											var split_name = crit.name.excl[i].split("|");
											for (var j=0; j<split_name.length; j++){
												t.push(split_name[j]);
											}
										}
										crit.name.excl = t;
									}  else crit.name.excl = [];
								}  else crit.name = {incl: [], excl: []};


								if (crit.desc){
									if (!c.varExist(crit.desc.incl) && !c.varExist(crit.desc.excl)){
										var t = {incl: [], excl:[]}
										if (crit.desc.replace(/\s/g, "") == "")
											crit.desc = "";
										crit.desc = c.toArray(crit.desc);
										for (var i=0; i<crit.desc.length; i++){
											var split_ie = crit.desc[i].split("!");
											if (split_ie[0])
												t.incl.push(split_ie[0]);
											if (split_ie[1])
												t.excl.push(split_ie[1]);
										}
										crit.desc = t;
									}
									if (c.varExist(crit.desc.incl)){
										crit.desc.incl = c.toArray(crit.desc.incl);
										var t = [];
										for (var i=0; i<crit.desc.incl.length; i++){
											if (crit.desc.incl[i].replace(/\s/g, "") == "")
												crit.desc.incl[i] = "";
											var split_desc = crit.desc.incl[i].split("|");
											for (var j=0; j<split_desc.length; j++){
												t.push(split_desc[j]);
											}
										}
										crit.desc.incl = t;
									}  else crit.desc.incl = [];
									if (c.varExist(crit.desc.excl)){
										crit.desc.excl = c.toArray(crit.desc.excl);
										var t = [];
										for (var i=0; i<crit.desc.excl.length; i++){
											if (crit.desc.excl[i].replace(/\s/g, "") == "")
												crit.desc.excl[i] = "";
											var split_desc = crit.desc.excl[i].split("|");
											for (var j=0; j<split_desc.length; j++){
												t.push(split_desc[j]);
											}
										}
										crit.desc.excl = t;
									}  else crit.desc.excl = [];
								} else crit.desc = {incl: [], excl: []};
								
								if (crit.artnr.incl.length > 0 || crit.artnr.excl.length > 0 ||
									crit.name.incl.length > 0 || crit.name.excl.length > 0 ||
									crit.desc.incl.length > 0 || crit.desc.excl.length > 0) {
									output.hascrit = true;	
								} else {
									output.hascrit = false;
								}

								if (crit.name.incl.length > 0 || crit.name.excl.length > 0){
									output = this.filterName(crit, input, output, "main");
									if (c.varExist(output.dom.other)){
										output = this.filterName(crit, input, output, "other.compl");
										output = this.filterName(crit, input, output, "other.more");
									}
								} else if (crit.desc.incl.length > 0 || crit.desc.excl.length > 0){
									output = this.filterDesc(crit, input, output, "main");
									if (c.varExist(output.dom.other)){
										output = this.filterDesc(crit, input, output, "other.compl");
										output = this.filterDesc(crit, input, output, "other.more");
									}
								}
								if (crit.artnr.incl.length > 0 || crit.artnr.excl.length > 0){
									maxi = input.dom.main.data.length;
									for (var i=0; i<maxi; i++){
										
										var excl = false;
										maxj = crit.artnr.excl.length;
										for (var j=0; j<maxj; j++){
											if (input.dom.main.data[i].artnr.indexOf(crit.artnr.excl[j]) > -1){
												for (var k=output.dom.main.data.length-1; k>-1; k--){
													if (output.dom.main.data[k].artnr.indexOf(crit.artnr.excl[j]) > -1){
														output.dom.main.elems.splice(k, 1);
														output.dom.main.data.splice(k, 1);
													}
												}
												excl = true;
											}
										}
										if (!excl){
											var incl = false;
											if (crit.artnr.incl.length > 0){
												maxj = crit.artnr.incl.length;
												for (var j=0; j<maxj; j++){
													if (input.dom.main.data[i].artnr.indexOf(crit.artnr.incl[j]) > -1){
														incl = true;
														for (var k=output.dom.main.data.length-1; k>-1; k--){
															if (output.dom.main.data[k].artnr.indexOf(crit.artnr.incl[j]) > -1)
																incl = false;
														}
														break;
													}
												}
											} 
											//else if (crit.artnr.excl.length > 0)
											//	incl = true;
												
											if (incl){
												output.dom.main.elems.push(input.dom.main.elems[i]);
												output.dom.main.data.push(input.dom.main.data[i]);
											}
										}
									}

									if (c.varExist(output.dom.other)){
										maxi = input.dom.other.compl.data.length;
										for (var i=0; i<maxi; i++){
											var excl = false;
											maxj = crit.artnr.excl.length;
											for (var j=0; j<maxj; j++){
												if (input.dom.other.compl.data[i].artnr.indexOf(crit.artnr.excl[j]) > -1){
													for (var k=output.dom.other.compl.data.length-1; k>-1; k--){
														if (output.dom.other.compl.data[k].artnr.indexOf(crit.artnr.excl[j]) > -1){
															output.dom.other.compl.elems.splice(k, 1);
															output.dom.other.compl.data.splice(k, 1);
														}
													}
													excl = true;
												}
											}
											if (!excl){
												var incl = false;
												if (crit.artnr.incl.length > 0){
													maxj = crit.artnr.incl.length;
													for (var j=0; j<maxj; j++){
														if (input.dom.other.compl.data[i].artnr.indexOf(crit.artnr.incl[j]) > -1){
															incl = true;
															for (var k=output.dom.other.compl.data.length-1; k>-1; k--){
																if (output.dom.other.compl.data[k].artnr.indexOf(crit.artnr.incl[j]) > -1)
																	incl = false;
															}
															break;
														}
													}
												} 
												//else incl = true;
												if (incl){
													output.dom.other.compl.elems.push(input.dom.other.compl.elems[i]);
													output.dom.other.compl.data.push(input.dom.other.compl.data[i]);
												}
											}
										}
										maxi = input.dom.other.more.data.length;
										for (var i=0; i<maxi; i++){
											var excl = false;
											maxj = crit.artnr.excl.length;
											for (var j=0; j<maxj; j++){
												if (input.dom.other.more.data[i].artnr.indexOf(crit.artnr.excl[j]) > -1){
													for (var k=output.dom.other.more.data.length-1; k>-1; k--){
														if (output.dom.other.more.data[k].artnr.indexOf(crit.artnr.excl[j]) > -1){
															output.dom.other.more.elems.splice(k, 1);
															output.dom.other.more.data.splice(k, 1);
														}
													}
													excl = true;
												}
											}
											if (!excl){
												var incl = false;
												if (crit.artnr.incl.length > 0){
													maxj = crit.artnr.incl.length;
													for (var j=0; j<maxj; j++){
														if (input.dom.other.more.data[i].artnr.indexOf(crit.artnr.incl[j]) > -1){
															incl = true;
															for (var k=output.dom.other.more.data.length-1; k>-1; k--){
																if (output.dom.other.more.data[k].artnr.indexOf(crit.artnr.incl[j]) > -1)
																	incl = false;
															}
															break;
														}
													}
												}
												//else incl = true;
												if (incl){
													output.dom.other.more.elems.push(input.dom.other.more.elems[i]);
													output.dom.other.more.data.push(input.dom.other.more.data[i]);
												}
											}
										}
									}
									/*
									if (crit.artnr.incl.length == 0){
										var input = this.filteredinput(output);
										var output = this.newoutput(input);
										if (crit.name.incl.length > 0 || crit.name.excl.length > 0){
											output = this.filterName(crit, input, output, "main");
											if (c.varExist(output.dom.other)){
												output = this.filterName(crit, input, output, "other.compl");
												output = this.filterName(crit, input, output, "other.more");
											}
										} else if (crit.desc.incl.length > 0 || crit.desc.excl.length > 0){
											output = this.filterDesc(crit, input, output, "main");
											if (c.varExist(output.dom.other)){
												output = this.filterDesc(crit, input, output, "other.compl");
												output = this.filterDesc(crit, input, output, "other.more");
											}
										}
									}
									*/
								}
							}
							if (c.isFunction(callback)) callback(output, context);
							else return output;

						},
						filterName: function(crit, input, output, objstr){
							var c = Samuraj.Common;
							var maxi, maxj, maxk;
							if (!c.isArray(objstr)) objstr = objstr.split(".");
							var current_input = input.dom;
							var current_output = output.dom;
							for (var i=0; i<objstr.length; i++){
								current_input = current_input[objstr[i]];
								current_output = current_output[objstr[i]];
							}
							maxi = current_input.data.length;
							for (var i=0; i<maxi; i++){

								var excl = false;
								maxj = crit.name.excl.length;
								for (var j=0; j<maxj; j++){
									if (current_input.data[i].name.toLowerCase().indexOf(crit.name.excl[j].toLowerCase()) > -1) {
										excl = true;
									}
								}
								if (!excl){
									var incl = false;
									if (crit.name.incl.length > 0){
										maxj = crit.name.incl.length;
										for (var j=0; j<maxj; j++){
											if (current_input.data[i].name.toLowerCase().indexOf(crit.name.incl[j].toLowerCase()) > -1) {
												incl = true;
											}
										}
									} else incl = true;
									if (incl){
										current_output.elems.push(current_input.elems[i]);
										current_output.data.push(current_input.data[i]);
									}
								}
							}
							if (crit.desc.incl.length > 0 || crit.desc.excl.length > 0){
								var input = this.filteredinput(output);
								var output = this.newoutput(input);

								output = this.filterDesc(crit, input, output, "main");
								if (c.varExist(output.dom.other)){
									output = this.filterDesc(crit, input, output, "other.compl");
									output = this.filterDesc(crit, input, output, "other.more");
								}
							}

							return output;
						},
						filterDesc: function(crit, input, output, objstr){
							var c = Samuraj.Common;
							var maxi, maxj, maxk;
							if (!c.isArray(objstr)) objstr = objstr.split(".");
							var current_input = input.dom;
							var current_output = output.dom;
							for (var i=0; i<objstr.length; i++){
								current_input = current_input[objstr[i]];
								current_output = current_output[objstr[i]];
							}
							maxi = current_input.data.length;
							for (var i=0; i<maxi; i++){
								var excl = false;

								maxj = crit.desc.excl.length;
								for (var j=0; j<maxj; j++){
									if (current_input.data[i].desc.toLowerCase().indexOf(crit.desc.excl[j].toLowerCase()) > -1) {
										excl = true;
									}
								}
								if (!excl){
									var incl = false;
									if (crit.desc.incl.length > 0){
										maxj = crit.desc.incl.length;
										for (var j=0; j<maxj; j++){
											if (current_input.data[i].desc.toLowerCase().indexOf(crit.desc.incl[j].toLowerCase()) > -1) {
												incl = true;
											}
										}
									} else incl = true;
									if (incl){
										current_output.elems.push(current_input.elems[i]);
										current_output.data.push(current_input.data[i]);
									}
								}
							}
							return output;
						},
						newoutput: function(input){
							var c = Samuraj.Common;
							var output = {};
							output.pagetype = input.pagetype;
							output.hascrit = input.hascrit;
							output.dom = {};
							output.dom.main = {};
							output.dom.main.elems = [];
							output.dom.main.data = [];
							output.dom.main.loc = input.dom.main.loc;
							if (c.varExist(input.dom.other)){
								output.dom.other = {};
								output.dom.other.compl = {};
								output.dom.other.compl.elems = [];
								output.dom.other.compl.data = [];
								output.dom.other.compl.loc = input.dom.other.compl.loc;
								output.dom.other.more = {};
								output.dom.other.more.elems = [];
								output.dom.other.more.data = [];
								output.dom.other.more.loc = input.dom.other.more.loc;
							}
							return output;
						},
						filteredinput: function(input){
							var newinput = input;
							return newinput;
						}
					},
					//Targeting
					Validation: {
						settings: {
							override: false,
							predefined: {}
						},
						setPredefinedUrls: function(){
							var predef = this.settings.predefined;
							predef.irw = [ "*www.|preview.*" ];
							predef.m2 = [ "*m2.*" ];
							
							if (Samuraj.Common.checkMobile()){
								predef.pip = [ "*/" + SamurajSetting.Country.homePath + "/p/*" ];
								predef.plp = [ "*/" + SamurajSetting.Country.homePath + "/cat/*" ];
								predef.search = [ "*/search/*" ];	
								predef.wishlist = [ "*/shop/wishlist/*" ];
								predef.availability = [];
				
								predef.all = [ "*/" + SamurajSetting.Country.homePath + "/*" ];
							} else {
								predef.pip = [ "*/catalog/products/*" ];
								predef.plp = [ "*/catalog/categories/*" ];
								predef.search = [ "*/search/*" ];	
								predef.wishlist = [ "*/InterestItemDisplay" ];
								predef.availability = [ "*/catalog/availability/*" ];
								
								predef.all = [ "*/" + SamurajSetting.Country.homePath + "/*" ];
							}
						},
						getPredefinedUrls: function(settings){
							var c = Samuraj.Common;
							var predef = this.settings.predefined;
							var urls = [];
							var platform = [];
							if (settings.irw && settings.m2){
								predef.irw = c.toArray(predef.irw);
								predef.m2 = c.toArray(predef.m2);
								
								for (var i=0; i<predef.irw.length; i++){
									for (var j=0; j<predef.m2.length; j++){
										platform.push((predef.irw[i] + predef.m2[j]).replace("**", "|"));
									}
								}
							} else if (settings.irw){
								predef.irw = c.toArray(predef.irw);
								platform = predef.irw;
							} else if (settings.m2){
								predef.m2 = c.toArray(predef.m2);
								platform = predef.m2;
							}
				
							for (var prop in settings){
								if (prop !== "irw" && prop !== "m2"){
									if (settings[prop] && predef[prop]){
										predef[prop] = c.toArray(predef[prop]);
										if (platform.length > 0){
											for (var i=0; i<platform.length; i++){
												for (var j=0; j<predef[prop].length; j++){
													urls.push((platform[i] + predef[prop][j]).replace("**", "*"));
												}
											}
										} else 
											urls = urls.concat(predef[prop]);
									}
								}
							}
							if (urls.length == 0)
								urls = platform;
							return urls;
						},
						//Validate date and time within timeframe. Format iso:  YYYY-MM-DDThh:mm:ss.000Z
						dateValidation: function(input) {
							input.name = input.name || "none";
							input.defaultreturn = input.defaultreturn || true;
							var c = Samuraj.Common;
							var module = input.module;
							module.validation = module.validation || {};
											
							try {
								if (module.when){
									var within_dates = false;
									if (module.when.dates){
										module.when.dates = c.toArray(module.when.dates);
										if (module.when.dates.length > 0){
											module.validation.dates = module.when.dates || {};
											var dates = module.when.dates;
											for (var i = 0; i < dates.length; i++) {
												if (dates[i].from){
													if (c.isObject(dates[i].from)){
														if (c.varExist(dates[i].from.date, true)){
															var da = dates[i].from.date.replace(/[^0-9]/g, "");
															if (da.length == 8){
																da = da.slice(0,4) + "-" + da.slice(4,6) + "-" + da.slice(6,8);
																var ti = "00:00";
																if (c.varExist(dates[i].from.time, true)){												
																	var ti_temp = dates[i].from.time.replace(/[^0-9]/g, "");
																	if (ti_temp.length == 4)
																		ti = ti_temp.slice(0,2) + ":" + ti_temp.slice(2,4);
																}
																dates[i].from = new Date(da + "T" + ti + ":00.000Z");
															} else {
																dates[i].from = new Date(1000000000000000);
																Samuraj.Log.add({
																	cat: "Samuraj",
																	sub: "Error",
																	text: input.name + " - From Date format must be YYYYMMDD or YYYY-MM-DD",
																	module: module
																});
															}
														} else 
															dates[i].from = new Date(-1000000000000000);
													}
												} else 
													dates[i].from = new Date(-1000000000000000);

												if (dates[i].to){
													if (c.isObject(dates[i].to)){
														if (c.varExist(dates[i].to.date, true)){
															var da = dates[i].to.date.replace(/[^0-9]/g, "");
															if (da.length == 8){
																da = da.slice(0,4) + "-" + da.slice(4,6) + "-" + da.slice(6,8);
																var ti = "00:00";
																if (c.varExist(dates[i].to.time, true)){												
																	var ti_temp = dates[i].to.time.replace(/[^0-9]/g, "");
																	if (ti_temp.length == 4)
																		ti = ti_temp.slice(0,2) + ":" + ti_temp.slice(2,4);
																}
																dates[i].to = new Date(da + "T" + ti + ":00.000Z");
															} else {
																dates[i].to = new Date(-1000000000000000);
																Samuraj.Log.add({
																	cat: "Samuraj",
																	sub: "Error",
																	text: input.name + " - To Date format must be YYYYMMDD or YYYY-MM-DD",
																	module: module
																});
															}
														} else 
															dates[i].to = new Date(1000000000000000);									
													}
												} else 
													dates[i].to = new Date(1000000000000000);

												if (!c.varExist(dates[i].from, true))
													dates[i].from = new Date(-1000000000000000);
												if (!c.varExist(dates[i].to, true))
													dates[i].to = new Date(1000000000000000);  
												var From = new Date(dates[i].from)
												, To = new Date(dates[i].to)
												, server = new Date(Samuraj.Time.now);
												
												if (Samuraj.DisplayTimer) 
													module.validation.dates[i] = Samuraj.Common.formatDate({from: From.toISOString(), to: To.toISOString()});

												if (server.getTime() > From.getTime() && server.getTime() < To.getTime()){
													module.validation.dates[i].status = "now";	
													within_dates = true;
												} else if (server.getTime() > To.getTime())
													module.validation.dates[i].status = "past";
												else if (server.getTime() < From.getTime())
													module.validation.dates[i].status = "future";
												

												if (Samuraj.Validation.settings.override)
													within_dates = true;
												
											}
										} else 
											within_dates = input.defaultreturn;

										if (within_dates) {
											if (module.when.intervals){
												this.timeValidation({
													name: input.name,
													defaultreturn: input.defaultreturn,
													module: module, 
													success: function(module){
														Samuraj.Log.add({
															cat: input.name,
															sub: "Module_" + (module.id || module.modulenr),
															text: "Valid date/time",
															module: module			
														});
														if (c.isFunction(input.success))
															input.success(module);
													},
													failure: function(module){
														Samuraj.Log.add({
															cat: input.name,
															sub: "Module_" + (module.id || module.modulenr),
															text: "Not valid time",
															module: module			
														});
														if (c.isFunction(input.failure))
															input.failure(module);
													}
												});
											} else {
												if (c.isFunction(input.success))
													input.success(module);
											}
										} else {
											if (c.isFunction(input.failure))
												input.failure(module);
										}

									} else if (module.when.intervals){
										this.timeValidation({
											name: input.name,
											defaultreturn: input.defaultreturn,
											module: module, 
											success: function(module){
												Samuraj.Log.add({
													cat: input.name,
													sub: "Module_" + (module.id || module.modulenr),
													text: "Valid date/time",
													module: module			
												});
												if (c.isFunction(input.success))
													input.success(module);
													
											},
											failure: function(module){
												Samuraj.Log.add({
													cat: input.name,
													sub: "Module_" + (module.id || module.modulenr),
													text: "Not valid time",
													module: module			
												});
												if (c.isFunction(input.failure))
													input.failure(module);
											}
										});
									} else {
										if (input.defaultreturn){
											Samuraj.Log.add({
												cat: input.name,
												sub: "Module_" + (module.id || module.modulenr),
												text: "Valid date",
												module: module			
											});
											if (c.isFunction(input.success))
												input.success(module);
										} else {
											Samuraj.Log.add({
												cat: input.name,
												sub: "Module_" + (module.id || module.modulenr),
												text: "Not valid date",
												module: module			
											});
											if (c.isFunction(input.failure))
												input.failure(module);
										}
									}
								} else {
									Samuraj.Log.add({
										cat: input.name,
										sub: "Module_" + (module.id || module.modulenr),
										text: "No date",
										module: module			
									});
									if (input.defaultreturn){
										if (c.isFunction(input.success))
											input.success(module);
									} else {
										if (c.isFunction(input.failure))
											input.failure(module);
									}
								}
							} catch (err) {
								Samuraj.Log.add({
									cat: "Samuraj",
									sub: "Error",
									text:  input.name + " - Catch - dateValidation - " + err,
									err: err,
									module: module
								});
								if (c.isFunction(input.failure))
									input.failure(module);
							}

						},
						timeValidation: function(input) {
							input.name = input.name || "none";
							input.defaultreturn = input.defaultreturn || true;
							var c = Samuraj.Common;
							var module = input.module;
							try {
								var within_times = false;
								for (var v=0; v<module.when.intervals.length; v++){
									module.validation.intervals = module.when.intervals || [];
									if (module.when.intervals[v].times){
										if (module.when.intervals[v].times.length > 0){
											var times = module.when.intervals[v].times;
											for (var i = 0; i < times.length; i++) {
												var time = times[i];
												time.split = {
													from: time.from.split(":")
													, to: time.to.split(":")
												}
												if (time.split.from.length > 1 && time.split.to.length > 1){
													var server = new Date(Samuraj.Time.now);
													time.relevant_dates = {
														from: new Date(server.getFullYear(),server.getMonth(),server.getDate(),time.split.from[0],time.split.from[1],0,0)
														, to: new Date(server.getFullYear(),server.getMonth(),server.getDate(),time.split.to[0],time.split.to[1],0,0)
													}

													if (server.getTime() > time.relevant_dates.from.getTime() && server.getTime() < time.relevant_dates.to.getTime()){
														module.validation.intervals[v].times[i].status = "now";
														within_times = true;
													} else if (server.getTime() > time.relevant_dates.to.getTime())
														module.validation.intervals[v].times[i].status = "past";
													else if (server.getTime() < time.relevant_dates.from.getTime())
														module.validation.intervals[v].times[i].status = "future";
												}
											}
										} else within_times = input.defaultreturn;
									} else within_times = input.defaultreturn;

									var within_days = false;
									if (module.when.intervals[v].days){
										var days_array = [];
										module.when.intervals[v].days = c.toArray(module.when.intervals[v].days);
										if (module.when.intervals[v].days.length > 0){
											for (var i = 0; i < module.when.intervals[v].days.length; i++){
												var days = module.when.intervals[v].days[i].toString().replace(/[^0-9,]/g,"");
												days = days.split(",");
												for (var j = 0; j < days.length; j++){
													var server = new Date(Samuraj.Time.now);
													if (server.getDay().toString() == days[j]){
														days_array.push({day: days[j], status: true});
														within_days = true;
													} else 
														days_array.push({day: days[j], status: false});
												}
											}
										} else within_days = input.defaultreturn;
										module.validation.intervals[v].days = days_array;
									} else within_days = input.defaultreturn;
								}
								
								if ((within_times && within_days) || Samuraj.Validation.settings.override){
									if (c.isFunction(input.success))
										input.success(module);
								} else {
									if (c.isFunction(input.failure))
										input.failure(module);
								}

							} catch (err) {
								Samuraj.Log.add({
									cat: "Samuraj",
									sub: "Error",
									text:  input.name + " - Catch - timeValidation - " + err,
									err: err,
									module: module
								});
								if (c.isFunction(input.failure))
									input.failure(module);
							}
						},
						validateDevice: function(input){
							input.name = input.name || "none";
							var c = Samuraj.Common;
							var module = input.module;
							module.validation = module.validation || {};
							if (module.where && module.where.device){
								module.validation.device = "both";
								if (c.isMobile.any()){
									if (module.where.device.toLowerCase() == "mobile" || module.where.device.toLowerCase() == "both" || module.where.device == "" || Samuraj.Validation.settings.override){
										module.validation.device = "mobile";
										Samuraj.Log.add({
											cat: input.name,
											sub: "Module_" + (module.id || module.modulenr),
											text: "Valid device - mobile",
											module: module			
										});
										if (c.isFunction(input.success))
											input.success(module);
									} else {
										Samuraj.Log.add({
											cat: input.name,
											sub: "Module_" + (module.id || module.modulenr),
											text: "Not valid device - mobile",
											module: module			
										});
										if (c.isFunction(input.failure))
											input.failure(module);
									}
								} else {
									if (module.where.device.toLowerCase() == "desktop" || module.where.device.toLowerCase() == "both" || module.where.device == "" || Samuraj.Validation.settings.override){
										module.validation.device = "desktop";
										Samuraj.Log.add({
											cat: input.name,
											sub: "Module_" + (module.id || module.modulenr),
											text: "Valid device - desktop",
											module: module			
										});
										if (c.isFunction(input.success))
											input.success(module);
									} else {
										Samuraj.Log.add({
											cat: input.name,
											sub: "Module_" + (module.id || module.modulenr),
											text: "Not valid device - desktop",
											module: module			
										});
										if (c.isFunction(input.failure))
											input.failure(module);
									}
								}
							} else {
								if (c.isFunction(input.success))
									input.success(module);
							}
						},
						validatePage: function(input){
							input.name = input.name || "none";
							input.uselocation = input.uselocation || false;
							var c = Samuraj.Common;
							var module = input.module;
							//var module = module;
							if (module.where){
								if (module.where.pages) {
									module.where.pages = c.toArray(module.where.pages);
									var all_empty = true;
									for (var p = 0; p < module.where.pages.length; p++) {
										if (module.where.pages[p].predefined){
											var predefurls = this.getPredefinedUrls(module.where.pages[p].predefined);
											if (module.where.pages[p].urls) {
												module.where.pages[p].urls = c.toArray(module.where.pages[p].urls);
												module.where.pages[p].urls = predefurls.concat(module.where.pages[p].urls);
											} else module.where.pages[p].urls = predefurls;
										}
										if (module.where.pages[p].urls) {
											module.where.pages[p].urls = c.toArray(module.where.pages[p].urls);
											if (module.where.pages[p].urls.length > 0) {
												for (var u=0; u<module.where.pages[p].urls.length; u++) {
													if (module.where.pages[p].urls[u] !== "")
														all_empty = false;
												}
											}
											var checkreturn = c.checkurl(module.where.pages[p].urls, true);
											if (checkreturn > -1) {
												module.foundpageindex = p;
												module.foundurlindex = checkreturn;
												module.locations = c.toArray(module.where.pages[p].locations);
												
												if (module.where.customers){
													this.validateCustomer({
														name: input.name,
														uselocation: input.uselocation,
														module: module,
														success: function(module){
															Samuraj.Log.add({
																cat: input.name,
																sub: "Module_" + (module.id || module.modulenr),
																text: "Valid page/customer - PageNum: " + module.foundpageindex,
																module: module			
															});
															if (c.isFunction(input.success))
																input.success(module);
														},
														failure: function(module){
															Samuraj.Log.add({
																cat: input.name,
																sub: "Module_" + (module.id || module.modulenr),
																text: "Not valid customer - PageNum: " + module.foundpageindex,
																module: module			
															});
															if (c.isFunction(input.failure))
																input.failure(module);
														}
													});
												} else {
													Samuraj.Log.add({
														cat: input.name,
														sub: "Module_" + (module.id || module.modulenr),
														text: "Valid page - PageNum: " + module.foundpageindex,
														module: module			
													});
													if (c.isFunction(input.success))
														input.success(module);
												}
											} else {
												Samuraj.Log.add({
													cat: input.name,
													sub: "Module_" + (module.id || module.modulenr),
													text: "Not valid page - PageNum: " + p,
													module: module			
												});
												if (c.isFunction(input.failure))
													input.failure(module);
											}
										}
									}
									if (all_empty) {
										Samuraj.Log.add({
											cat: input.name,
											sub: "Module_" + (module.id || module.modulenr),
											text: "Pages empty",
											module: module			
										});
										if (c.isFunction(input.success))
											input.success(module);
									}
								} else {
									if (c.isFunction(input.success))
										input.success(module);
								}
							} else {
								if (c.isFunction(input.success))
									input.success(module);
							}

						},
						validateCustomer: function(input){
							input.name = input.name || "none";
							input.uselocation = input.uselocation || false;
							var c = Samuraj.Common;
							var module = input.module;
							if (module.where.customers) {
								var all_cities_include = [], all_zipcodes_include = [], all_regions_include = [], all_countries_include = []
								var all_cities_exclude = [], all_zipcodes_exclude = [], all_regions_exclude = [], all_countries_exclude = [];
								var within_cities = false, within_zipcodes = false, within_regions = false, within_countries = false;
								var all_empty = true;
								module.where.customers = c.toArray(module.where.customers);
								if (module.where.customers.length > 0){
									for (var i = 0; i < module.where.customers.length; i++) {
										if (module.where.customers[i].places){
											module.where.customers[i].places = c.toArray(module.where.customers[i].places);
											if (module.where.customers[i].places.length > 0){
												all_empty = false;
												for (var p = 0; p < module.where.customers[i].places.length; p++) {
													//Cities
													if (module.where.customers[i].places[p].cities){
														if (module.where.customers[i].places[p].cities.incl){
															module.where.customers[i].places[p].cities.incl = c.toArray(module.where.customers[i].places[p].cities.incl);
															if (module.where.customers[i].places[p].cities.incl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].cities.incl.length; ci++){
																	var cities = module.where.customers[i].places[p].cities.incl[ci].toString().split("|");
																	for (var j = 0; j < cities.length; j++){	
																		if (c.varExist(cities[j], true))
																			all_cities_include.push(cities[j]);
																	}
																}
															}
														}
														if (module.where.customers[i].places[p].cities.excl){
															module.where.customers[i].places[p].cities.excl = c.toArray(module.where.customers[i].places[p].cities.excl);
															if (module.where.customers[i].places[p].cities.excl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].cities.excl.length; ci++){
																	var cities = module.where.customers[i].places[p].cities.excl[ci].toString().split("|");
																	for (var j = 0; j < cities.length; j++){	
																		if (c.varExist(cities[j], true))
																			all_cities_exclude.push(cities[j]);
																	}
																}
															}
														}
														if (!module.where.customers[i].places[p].cities.incl && !module.where.customers[i].places[p].cities.excl)
															within_cities = true;
													} else within_cities = true;
													//Zipcodes
													if (module.where.customers[i].places[p].zipcodes){
														if (module.where.customers[i].places[p].zipcodes.incl){
															module.where.customers[i].places[p].zipcodes.incl = c.toArray(module.where.customers[i].places[p].zipcodes.incl);
															if (module.where.customers[i].places[p].zipcodes.incl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].zipcodes.incl.length; ci++){
																	var zipcodes = module.where.customers[i].places[p].zipcodes.incl[ci].toString().split("|");
																	for (var j = 0; j < zipcodes.length; j++){	
																		if (c.varExist(zipcodes[j], true))
																			all_zipcodes_include.push(zipcodes[j]);
																	}
																}
															}
														}
														if (module.where.customers[i].places[p].zipcodes.excl){
															module.where.customers[i].places[p].zipcodes.excl = c.toArray(module.where.customers[i].places[p].zipcodes.excl);
															if (module.where.customers[i].places[p].zipcodes.excl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].zipcodes.excl.length; ci++){
																	var zipcodes = module.where.customers[i].places[p].zipcodes.excl[ci].toString().split("|");
																	for (var j = 0; j < zipcodes.length; j++){	
																		if (c.varExist(zipcodes[j], true))
																			all_zipcodes_exclude.push(zipcodes[j]);
																	}
																}
															}
														}
														if (!module.where.customers[i].places[p].zipcodes.incl && !module.where.customers[i].places[p].zipcodes.excl)
															within_zipcodes = true;
													} else within_zipcodes = true;
													//Regions
													if (module.where.customers[i].places[p].regions){
														if (module.where.customers[i].places[p].regions.incl){
															module.where.customers[i].places[p].regions.incl = c.toArray(module.where.customers[i].places[p].regions.incl);
															if (module.where.customers[i].places[p].regions.incl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].regions.incl.length; ci++){
																	var regions = module.where.customers[i].places[p].regions.incl[ci].toString().split("|");
																	for (var j = 0; j < regions.length; j++){	
																		if (c.varExist(regions[j], true))
																			all_regions_include.push(regions[j]);
																	}
																}
															}
														}
														if (module.where.customers[i].places[p].regions.excl){
															module.where.customers[i].places[p].regions.excl = c.toArray(module.where.customers[i].places[p].regions.excl);
															if (module.where.customers[i].places[p].regions.excl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].regions.excl.length; ci++){
																	var regions = module.where.customers[i].places[p].regions.excl[ci].toString().split("|");
																	for (var j = 0; j < regions.length; j++){	
																		if (c.varExist(regions[j], true))
																			all_regions_exclude.push(regions[j]);
																	}
																}
															}
														}
														if (!module.where.customers[i].places[p].regions.incl && !module.where.customers[i].places[p].regions.excl)
															within_regions = true;
													} else within_regions = true;
													//Countries
													if (module.where.customers[i].places[p].countries){
														if (module.where.customers[i].places[p].countries.incl){
															module.where.customers[i].places[p].countries.incl = c.toArray(module.where.customers[i].places[p].countries.incl);
															if (module.where.customers[i].places[p].countries.incl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].countries.incl.length; ci++){
																	var countries = module.where.customers[i].places[p].countries.incl[ci].toString().split("|");
																	for (var j = 0; j < countries.length; j++){	
																		if (c.varExist(countries[j], true))
																			all_countries_include.push(countries[j]);
																	}
																}
															}
														}
														if (module.where.customers[i].places[p].regions.excl){
															module.where.customers[i].places[p].regions.excl = c.toArray(module.where.customers[i].places[p].regions.excl);
															if (module.where.customers[i].places[p].regions.excl.length > 0){
																for (var ci = 0; ci < module.where.customers[i].places[p].regions.excl.length; ci++){
																	var regions = module.where.customers[i].places[p].regions.excl[ci].toString().split("|");
																	for (var j = 0; j < regions.length; j++){	
																		if (c.varExist(regions[j], true))
																			all_countries_exclude.push(regions[j]);
																	}
																}
															}
														}
														if (!module.where.customers[i].places[p].countries.incl && !module.where.customers[i].places[p].countries.excl)
															within_countries = true;
													} else within_countries = true;

													if (all_cities_include.length + all_zipcodes_include.length + all_regions_include.length + all_countries_include.length + 
														all_cities_exclude.length + all_zipcodes_exclude.length + all_regions_exclude.length + all_countries_exclude.length > 0){
														
														if (input.uselocation){
															Samuraj.Customer.getlocation(function(data){
																//Cities
																if (data.city){
																	var both_empty = true;
																	if (all_cities_include.length > 0){
																		module.validation.cities = all_cities_include;
																		both_empty = false;
																		for (var i = 0; i < all_cities_include.length; i++){	
																			if (data.city.toLowerCase() == all_cities_include[i].toLowerCase()){
																				module.validation.cities[i] = {city: all_cities_include[i], status: true};
																				within_cities = true;
																			} else
																				module.validation.cities[i] = {city: all_cities_include[i], status: false};
																		}
																	}
																	if (all_cities_exclude.length > 0){
																		module.validation.cities_excl = all_cities_exclude;
																		both_empty = false;
																		for (var i = 0; i < all_cities_exclude.length; i++){	
																			if (data.city.toLowerCase() == all_cities_exclude[i].toLowerCase()){
																				module.validation.cities_excl[i] = {city: all_cities_exclude[i], status: true};
																				within_cities = false; 
																			} else
																				module.validation.cities_excl[i] = {city: all_cities_exclude[i], status: false};
																		}
																	}
																	if (both_empty) within_cities = true;
																}
																//Zipcodes
																if (data.zipcode){
																	var both_empty = true;
																	if (all_zipcodes_include.length > 0){
																		module.validation.zipcodes = all_zipcodes_include;
																		both_empty = false;
																		for (var i = 0; i < all_zipcodes_include.length; i++){	
																			if (all_zipcodes_include[i].toLowerCase().indexOf('-') > -1){
																				var zipcode_from_to = all_zipcodes_include[i].toLowerCase().split('-');
																				var zipcode_from = parseInt(zipcode_from_to[0].toLowerCase().replace(/[^0-9]/g, ''));
																				var zipcode_to = parseInt(zipcode_from_to[1].toLowerCase().replace(/[^0-9]/g, ''));
																				var zipcode_customer = parseInt(data.zipcode.toLowerCase().replace(/[^0-9]/g, ''));
																				if (zipcode_customer >= zipcode_from && zipcode_customer <= zipcode_to){
																					module.validation.zipcodes[i] = {zipcode: all_zipcodes_include[i], status: true};
																					within_zipcodes = true;
																				} else 
																					module.validation.zipcodes[i] = {zipcode: all_zipcodes_include[i], status: false};
																			} else if (data.zipcode.toLowerCase() == all_zipcodes_include[i].toLowerCase()){
																				module.validation.zipcodes[i] = {zipcode: all_zipcodes_include[i], status: true};
																				within_zipcodes = true;
																			} else
																				module.validation.zipcodes[i] = {zipcode: all_zipcodes_include[i], status: false};
																		}
																	}
																	if (all_zipcodes_exclude.length > 0){
																		module.validation.zipcodes_excl = all_zipcodes_exclude;
																		both_empty = false;
																		for (var i = 0; i < all_zipcodes_exclude.length; i++){	
																			if (all_zipcodes_exclude[i].toLowerCase().indexOf('-') > -1){
																				var zipcode_from_to = all_zipcodes_exclude[i].toLowerCase().split('-');
																				var zipcode_from = parseInt(zipcode_from_to[0].toLowerCase().replace(/[^0-9]/g, ''));
																				var zipcode_to = parseInt(zipcode_from_to[1].toLowerCase().replace(/[^0-9]/g, ''));
																				var zipcode_customer = parseInt(data.zipcode.toLowerCase().replace(/[^0-9]/g, ''));
																				if (zipcode_customer > zipcode_from && zipcode_customer < zipcode_to){
																					module.validation.zipcodes_excl[i] = {zipcode: all_zipcodes_exclude[i], status: true};
																					within_zipcodes = false;
																				}
																			} else if (data.zipcode.toLowerCase() == all_zipcodes_exclude[i].toLowerCase()){
																				module.validation.zipcodes_excl[i] = {zipcode: all_zipcodes_exclude[i], status: true};
																				within_zipcodes = false; 
																			} else
																				module.validation.zipcodes_excl[i] = {zipcode: all_zipcodes_exclude[i], status: false};
																		}
																	}
																	if (both_empty) within_zipcodes = true;
																}
																//Regions
																if (data.region){
																	var both_empty = true;
																	if (all_regions_include.length > 0){
																		both_empty = false;
																		for (var i = 0; i < all_regions_include.length; i++){	
																			if (data.region.toLowerCase() == all_regions_include[i].toLowerCase()){
																				module.validation.regions[i] = {region: all_regions_include[i], status: true};
																				within_regions = true; 
																			} else
																				module.validation.regions[i] = {region: all_regions_include[i], status: false};
																		}
																	}
																	if (all_regions_exclude.length > 0){
																		both_empty = false;
																		for (var i = 0; i < all_regions_exclude.length; i++){	
																			if (data.region.toLowerCase() == all_regions_exclude[i].toLowerCase()){
																				module.validation.regions_excl[i] = {region: all_regions_exclude[i], status: true};
																				within_regions = false; 
																			} else
																				module.validation.regions_excl[i] = {region: all_regions_exclude[i], status: false};
																		}
																	}
																	if (both_empty) within_regions = true;
																}
																//Countries
																if (data.country){
																	var both_empty = true;
																	if (all_countries_include.length > 0){
																		both_empty = false;
																		for (var i = 0; i < all_countries_include.length; i++){	
																			if (data.country.toLowerCase() == all_countries_include[i].toLowerCase()){
																				module.validation.countries[i] = {country: all_countries_include[i], status: true};
																				within_countries = true; 
																			} else
																				module.validation.countries[i] = {country: all_countries_include[i], status: false};
																		}
																	}
																	if (all_countries_exclude.length > 0){
																		both_empty = false;
																		for (var i = 0; i < all_countries_exclude.length; i++){	
																			if (data.country.toLowerCase() == all_countries_exclude[i]){
																				module.validation.countries_excl[i] = {country: all_countries_exclude[i], status: true};
																				within_countries = false; 
																			} else
																				module.validation.countries_excl[i] = {country: all_countries_exclude[i], status: false};
																		}
																	}
																	if (both_empty) within_countries = true;
																}
																
																Samuraj.Log.add({
																	cat: input.name,
																	sub: "Module_" + (module.id || module.modulenr),
																	text: "Geolocation: country: " + within_countries + ", region: " + within_regions + ", cities: " + within_cities + ", zipcodes: " + within_zipcodes ,
																	module: module			
																});

																if ((within_cities && within_zipcodes && within_regions && within_countries) || Samuraj.Validation.settings.override){
																	if (c.isFunction(input.success))
																		input.success(module);
																} else {
																	if (c.isFunction(input.failure))
																		input.failure(module);
																}

															});

														} else {
															if (c.isFunction(input.success))
																input.success(module);
														}
													} else {
														if (c.isFunction(input.success))
															input.success(module);
													}
												}
											}
										}
									}
									if (all_empty){
										if (c.isFunction(input.success))
											input.success(module);
									}
								} else {
									if (c.isFunction(input.success))
										input.success(module);
								}
							} else {
								if (c.isFunction(input.success))
									input.success(module);
							}

						}
					},	
					//Validation
					Page: {
						loaded: function(callback, context) {
							var c = Samuraj.Common;
							if (!c.varExist(Samuraj.PageReadyList))
								Samuraj.PageReadyList = [];
							if (!c.varExist(Samuraj.PageReadyFired))
								Samuraj.PageReadyFired = false;
							if (!c.varExist(Samuraj.PageReadyEventHandlersInstalled))
								Samuraj.PageReadyEventHandlersInstalled = false;

							if (typeof callback !== "function") {
								throw new TypeError("callback for Page.loaded(fn) must be a function");
							}

							if (Samuraj.PageReadyFired) {
								setTimeout(function() {
									callback(context);
								}, 1);
								return;
							} else {
								Samuraj.PageReadyList.push({
									fn: callback,
									context: context
								});
							}
							if (document.readyState === "complete" || (!document.attachEvent && document.readyState === "interactive")) {
								setTimeout(this.waitingRoom, 1);
							} else if (!Samuraj.PageReadyEventHandlersInstalled) {
								if (document.addEventListener) {
									document.addEventListener("DOMContentLoaded", this.waitingRoom, false);
									window.addEventListener("load", this.waitingRoom, false);
								} else {
									document.attachEvent("onreadystatechange", this.readyStateChange);
									window.attachEvent("onload", this.waitingRoom);
								}
								Samuraj.PageReadyEventHandlersInstalled = true;
							}
						},
						readyStateChange: function() {
							if (document.readyState === "complete") {
								this.waitingRoom();
							}
						},
						waitingRoom: function() {
							if (!Samuraj.PageReadyFired) {
								Samuraj.PageReadyFired = true;
								for (var i = 0; i < Samuraj.PageReadyList.length; i++) {
									Samuraj.PageReadyList[i].fn.call(window, Samuraj.PageReadyList[i].context);
								}
								Samuraj.PageReadyList = [];
							}
						},
						check: function(data) {
							var c = Samuraj.Common;
							if (c.varExist(data.url, true)) {}

						},

					},
					//Page

					Time: {
						get: function(callback, context) {
							this.checkDateType();
							var c = Samuraj.Common,
								t = Samuraj.Time;
							t.now = t.now || new Date().toISOString();
							t.set = t.set || false;
							t.QueryStarted = t.QueryStarted || false;
							t.RequestList = t.RequestList || [];

							if (t.set) {
								if (c.isFunction(callback))
									callback(t.now, context);
								else
									return t.now;
							} else {
								if (c.isFunction(callback))
									t.RequestList.push({
										fn: callback,
										context: context
									});
								if (!t.QueryStarted) {
									t.QueryStarted = true;
									this.getServerTime(function() {
										Samuraj.Time.waitingRoom();
									});
								}
							}
						},
						checkDateType: function() {
							if (!Date.prototype.toISOString) {
								//(function() {
								var pad = function(number) {
									if (number < 10) {
										return '0' + number;
									}
									return number;
								}
								Date.prototype.toISOString = function() {
									return this.getUTCFullYear() + '-' + pad(this.getUTCMonth() + 1) + '-' + pad(this.getUTCDate()) + 'T' + pad(this.getUTCHours()) + ':' + pad(this.getUTCMinutes()) + ':' + pad(this.getUTCSeconds()) + '.' + (this.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) + 'Z';
								}
								;
								//}());
							}
						},
						getServerTime: function(callback, context) {
							var c = Samuraj.Common;
							if (!Samuraj.Time.set) {
								//Set local time as default
								Samuraj.Time.now = new Date().toISOString();

								var time = document.getElementById("time");
								if (c.getCookie("samuraj_diff")) {
									//Check for time difference cookie, use that as it is quicker.
									//Refresh diff for next load
									var response = new Samuraj.Common.xhr();
									response.onreadystatechange = function() {
										if (response.readyState == 4) {
											if (response.status == 200) {
												var headerDate = new Date(response.getResponseHeader('Date'));
												var Local = new Date();
												var Server = new Date(headerDate.toISOString());
												var timeDiff = Server.getTime() - Local.getTime();
												Samuraj.Common.setCookie("samuraj_diff", timeDiff);
											}
										}
									}
									;
									response.open("HEAD", window.location.href.toString(), true);
									response.send();

									//Set servertime from local and diff
									var diff = c.getCookie("samuraj_diff");
									var Local = new Date(Samuraj.Time.now);
									Samuraj.Time.now = new Date(Local.getTime() + c.toInt(diff)).toISOString();
									Samuraj.Time.set = true;
									if (typeof callback == "function")
										callback(context);
								} else if (c.varExist(time)) {
									//Check for time dom, it is quickest.
									//Time div on M2
									Samuraj.Time.now = new Date(parseInt(time.innerText) * 1e3).toISOString();
									Samuraj.Time.set = true;
									if (typeof callback == "function")
										callback(context);
								} else {
									//Get servertime from head async
									var response = new Samuraj.Common.xhr();
									response.context = context;
									response.onreadystatechange = function() {
										if (response.readyState == 4) {
											if (response.status == 200) {
												var headerDate = new Date(response.getResponseHeader('Date'));
												var Local = new Date();
												Samuraj.Time.now = headerDate.toISOString();

												var Server = new Date(Samuraj.Time.now);
												var timeDiff = Server.getTime() - Local.getTime();
												Samuraj.Common.setCookie("samuraj_diff", timeDiff);
											}
											Samuraj.Time.set = true;
											if (typeof callback == "function")
												callback(context);
										}
									}
									;
									response.open("HEAD", window.location.href.toString(), true);
									response.send();
								}
							} else {
								if (typeof callback == "function") callback(context);
							}
						},
						dateValidation: function(from, to) {
							try {
								var From = new Date(from)
								  , To = new Date(to)
								  , Server = new Date(this.get());
								if (Server.getTime() > From.getTime() && Server.getTime() < To.getTime())
									return true;
								else
									return false;

							} catch (err) {
								return false;
							}
						},
						dateValidationTimes: function(times){
							try {
								var found = false;
								for (var i = 0; i < times.length; i++) {
									if (this.dateValidation(times[i].date_from, times[i].date_to))
										found = true;
								}
								return found;
							} catch (err) {
								return false;
							}
						},
						waitingRoom: function() {
							for (var i = 0; i < Samuraj.Time.RequestList.length; i++) {
								Samuraj.Time.RequestList[i].fn.call(window, Samuraj.Time.now, Samuraj.Time.RequestList[i].context);
							}
							Samuraj.Time.RequestList = [];
						}
					},
					//Time
					Template: {
						options: {
							loader: "<div class=\"samuraj-spinner\"> <div class=\"samuraj-bounce1\"></div> <div class=\"samuraj-bounce2\"></div> <div class=\"samuraj-bounce3\"></div></div>",
							addtocartloader: "<div class=\"samuraj-spinner addToCartLoad\"> <div class=\"samuraj-bounce1\"></div> <div class=\"samuraj-bounce2\"></div> <div class=\"samuraj-bounce3\"></div></div>",
							freightcalcloader: "<div class=\"samuraj-spinner samuraj-freightcalc-loader samuraj-pup-viscon\"> <div class=\"samuraj-bounce1\"></div> <div class=\"samuraj-bounce2\"></div> <div class=\"samuraj-bounce3\"></div></div>"
						},
						load: function(input) {
							var c = Samuraj.Common;
							if (c.varExist(input.htmlcontent)) {
								try {
									var html = ""
									  , options = {};
									input.htmlcontent = c.toArray(input.htmlcontent);
									for (var i=0; i<input.htmlcontent.length; i++){
										html += input.htmlcontent[i];
									}
									input.htmlcontent = html;
									if (c.varExist(input.htmloptions))
										options = input.htmloptions;

									options = Samuraj.Merge.merge(options, this.options);
									if (c.varExist(input.texttemplates))
										options = Samuraj.Merge.merge(options, input.texttemplates);
									this.alloptions = options;

									var re = /<%([^%>]+)?%>/g, reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g, code = 'var r=[];\n', cursor = 0, match;
									var add = function(line, js) {
										js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') : (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
										return add;
									}
									while (match = re.exec(html)) {
										add(html.slice(cursor, match.index))(match[1], true);
										cursor = match.index + match[0].length;
									}
									add(html.substr(cursor, html.length - cursor));
									code += 'return r.join("");';
									input.htmlcontent = new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
									if (!c.varExist(input.cyclecount))
										input.cyclecount = 20;
									var m = input.htmlcontent.match(/<%.+%>/g);
									if (m){
										if (m.length > 0 && input.cyclecount > 0) {
											input.cyclecount--;
											return this.load(input);
										} else return input.htmlcontent;
									} else return input.htmlcontent;
								} catch (err) {
									return input.htmlcontent;
								}
							}

						}
					},
					Translation: {
						data: {},
						get: function(prop){
							return this.data[prop];
						}
					},
					Queue: {
						dependencyqueue: [],
						loaded: [],
						load: function(input){
							var obj_length = input.objects.length;
							var firstload = true;
							for (var i=0; i<obj_length; i++){
								//Check loaded array for the current object
								if (this.loaded.indexOf(input.objects[i].name) == -1){
									this.loaded.push(input.objects[i].name);
									window.Samuraj[input.objects[i].name] = input.objects[i].fn;
								} else {
									firstload = false;
								}
							}
							if (firstload){
								var allok = true;
								for (var i=0; i<input.dependencies.length; i++){
									if (!window.Samuraj[input.dependencies[i]]){
										allok = false;
										break;
									}
								}
								if (allok) {
									input.bootup();
									this.waitingRoom();
								} else {
									this.dependencyqueue.push(input);
								}
							}
						},
						waitingRoom: function(){
							var c = Samuraj.Common;
							var allok = true;
							for (var i = this.dependencyqueue.length-1; i>-1; i--){
								for (var j=0; j<this.dependencyqueue[i].dependencies.length; j++){
									if (!window.Samuraj[this.dependencyqueue[i].dependencies[j]]){
										allok = false;
										break;
									}
								}
								if (allok) {
									this.dependencyqueue[i].bootup();
									this.dependencyqueue.splice(i, 1);
									this.waitingRoom();
									break;
								}
							}
						},
						dump: function(queue){
							queue = queue || "SamurajQueue";
							while (window[queue].length > 0){
								var input = window[queue][0];
								this.load(input);
								window[queue].splice(0,1);
							}
						}
					},
					Log: {
						/*
						Samuraj.Log.add({
							cat: "Samuraj",
							sub: "Error",
							text: "Tracking - input.action is not defined",
							input: input
						});
						*/
						data: {},
						add: function(input){
							var catarr = this.data["none"] || {};
							if (input.cat){
								this.data[input.cat] = this.data[input.cat] || {};
								catarr = this.data[input.cat];
							}
							delete input.cat;

							catarr["none"] = catarr["none"] || [];
							var subarr = catarr["none"];
							if (input.sub){
								catarr[input.sub] = catarr[input.sub] || [];
								subarr = catarr[input.sub];
							}
							delete input.sub;
							
							subarr.push(input);	
						}
					}
				};
				Samuraj.Merge.merge(Samuraj, SamurajTemp);

				
				/*  EXAMPLE
				var template =
					'My <%this.skill%>:<%if(this.showSkills) {%>' +
						'<%for(var index in this.skills) {%>' +
						'<a href="#"><%this.skills[index]%></a>' +
						'<%}%>' +
					'<%} else {%>' +
						'<p>none</p>' +
					'<%}%>';
					Samuraj.Template.load({htmlcontent: template, htmloptions: {skill: "test"}});
				*/


				/*


				 |   _   _.  _| o ._   _
				 |_ (_) (_| (_| | | | (_|
									   _|

				*/

				//Override preview for testing
				//Samuraj.Version.forcePreview = true;
				//*********************************
				
				var Preparation = function(callback) {
					var c = Samuraj.Common
					  , t = Samuraj.Time;
					t.checkDateType();
					
				};

				var Initiate = (function() {
					
					//Set Samuraj system time
					Samuraj.Time.get(function(time){

						//Check for Samuraj preview tool
						Samuraj.Preview.check(function(){

							Samuraj.Log.add({
								cat: "Samuraj",
								sub: "Timing",
								text: "Initiate",
								timing: performance.now() - SamurajTiming				
							});
							window.Extension = window.Samuraj;

							//Set predefined urls for page types
							Samuraj.Validation.setPredefinedUrls();

							//Check for and set device cookie
							Samuraj.Common.setDeviceCookie();

							//Check for and set Maxymiser Override cookie
							Samuraj.Common.checkMaxymiserOverrideCookie();

							//Load queued functions
							Samuraj.Queue.dump();
							
							//Preload datasources
							Samuraj.Source.fillstore();

						});
					});
				})();

				if (Samuraj.Common.isFunction(callback))
					callback();

			});
		}


		//First time load of Samuraj and initial functions defined
        if (typeof Samuraj == 'undefined') {

            Samuraj = {

                Version: {
                    store: [],
                    save: function(key, value) {
                        var c = this;
                        if (!c.varExist(c.store[key], true))
                            c.store[key] = value;
                    },
                    load: function(cn, on_off, callback, context) {
                        var c = this
                          , value = "";

                        if (typeof SamurajVersionOverride !== 'undefined') {
                            c.save("versionoverride", SamurajVersionOverride);
                            if (c.varExist(context))
                                context.preview = false;
                            else {
                                var context = {};
                                context.preview = false;
                            }
                            if (SamurajVersionOverride == "preview")
                                context.preview = true;
                            if (typeof callback == "function")
                                callback(context);
                        } else {
                            if (c.checkurl({masks: ["*" + cn + "=on*"], includeparams: true}))
                                value = 'on';
                            if (c.checkurl({masks: ["*" + cn + "=off*"], includeparams: true}))
                                value = 'off';
                            if (c.checkurl({masks: ["*" + cn + "=preview*"], includeparams: true}))
                                value = 'preview';
                            if (c.checkurl({masks: ["*" + cn + "=clear*"], includeparams: true}))
                                c.clearCookie(cn);
                            if (value !== "") {
                                c.setCookie(cn, value);
                                c.save(cn, value);
                            }

                            if (c.varExist(context))
                                context.preview = false;
                            else {
                                var context = {};
                                context.preview = false;
                            }
                            if (c.getCookie(cn) == "preview")
                                context.preview = true;
                            if (c.getCookie(cn) == "on" || c.getCookie(cn) == "preview")
                                on_off = true;
                            else if (c.getCookie(cn) == "off")
                                on_off = false;
                            if (on_off) {
                                if (typeof callback == "function")
                                    callback(context);
                            }
                        }
                    },
                    checkurl: function(input, returnindex) {
                        if (!this.isObject(input)) input = {masks: input, returnindex: returnindex};

                        var bool = true;
                        if (this.varExist(returnindex))
                            if (returnindex)
                                bool = false;
                        var masks = []
                          , found = false;
                        if (this.isArray(input.masks))
                            masks = input.masks;
                        else
                            masks.push(input.masks);

                        var initialurl = document.URL.split("?")[0];
                        if (this.varExist(input.includeparams)) if (input.includeparams) initialurl = document.URL;

                        for (var n = 0; n < masks.length; n++) {
                            var url = {
                                doc: initialurl.toLowerCase(),
                                mask: masks[n].toLowerCase()
                            }
                            var parts = url.mask.split("*");
                            url.doctemp = url.doc.replace("/?", "?").replace("/#", "#");
                            if (url.doctemp.slice(url.doctemp.length - 1, url.doctemp.length) == "/")
                                url.doctemp = url.doctemp.slice(0, url.doctemp.length - 1);
                            for (var i = 0; i < parts.length; i++) {
								//var nor = parts[i].split("!");
								
								//var either = nor[0].split("|");
								var either = parts[i].split("|");
                                for (var j = 0; j < either.length; j++) {
                                    either[j] = either[j].replace("/?", "?").replace("/#", "#");
                                    if (either[j].slice(either[j].length - 1, either[j].length) == "/")
                                        either[j] = either[j].slice(0, either[j].length - 1);

                                    var loc = url.doctemp.indexOf(either[j]);
                                    if (loc > -1) {
                                        parts[i] = either[j];
                                        break;
                                    }
								}
								/*
								if (nor.length > 1){
									var either = nor[1].split("|");
									for (var j = 0; j < either.length; j++) {
										either[j] = either[j].replace("/?", "?").replace("/#", "#");
										if (either[j].slice(either[j].length - 1, either[j].length) == "/")
											either[j] = either[j].slice(0, either[j].length - 1);

										var loc = url.doctemp.indexOf(either[j]);
										if (loc == -1) {
											parts[i] = "";
											break;
										}
									}
								}
								*/
                                if (loc > -1) {
                                    if (i == parts.length - 1 || (i == parts.length - 2 && parts[parts.length - 1].length == 0)) {
                                        if (parts[i].length == 0 || parts[parts.length - 1].length == 0) {
                                            if (bool)
                                                return true;
                                            else
                                                return n;
                                        } else {
                                            if (parts[i].match(/[?|#]/g)) {
                                                if (parts[i] == url.doctemp) {
                                                    if (bool)
                                                        return true;
                                                    else
                                                        return n;
                                                }
                                                break;
                                            } else {
                                                url.doctempnoparam = /.+?[?|#]|.+/.exec(url.doctemp);
                                                url.docref = url.doctempnoparam[0].slice(loc, url.doctempnoparam[0].length).replace(/[?|#]/g, "");
                                                if (parts[i] == url.docref) {
                                                    if (bool)
                                                        return true;
                                                    else
                                                        return n;
                                                }
                                                break;
                                            }
                                        }

                                    }
                                    url.doctemp = url.doctemp.slice(loc + parts[i].length, url.doctemp.length);
                                } else
                                    break;

                            }
                        }
                        if (bool)
                            return false;
                        else
                            return -1;
                    },
                    setCookie: function(cn, value, days) {
                        var expires = '';
                        if (days) {
                            var date = new Date();
                            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                            expires = '; expires=' + date.toGMTString();
                        }
                        document.cookie = cn + '=' + value + expires + ';domain=.' + this.removeSubDomain(window.location.hostname) + ';path=/';
                    },
                    clearCookie: function(cn) {
                        document.cookie = cn + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT' + ';domain=.' + this.removeSubDomain(window.location.hostname) + ';path=/';
                    },
                    getCookie: function(cn) {
						var match = document.cookie.match(new RegExp('(^|; )' + cn + '=([^;]+)'));
						if (match){
							if (match.length >= 2)
								return match[2];
						}
					},
                    removeSubDomain: function(hostn) {
                        var is_co = hostn.match(/\.co\./);
                        hostn = hostn.split('.');
                        hostn = hostn.slice(is_co ? -3 : -2);
                        hostn = hostn.join('.');
                        return hostn;
                    },
                    varExist: function(variable, notEmpty) {
                        if (typeof variable == 'undefined')
                            return false;
                        if (variable === 'undefined')
                            return false;
                        if (variable === null)
                            return false;
                        if (typeof notEmpty !== 'undefined')
                            if (notEmpty)
                                if (variable == "")
                                    return false;
                        return true;
                    },
                    isInt: function(value) {
                        if (isNaN(value)) {
                            return false;
                        }
                        var x = parseFloat(value);
                        return (x | 0) === x;
                    },
                    isArray: function(obj) {
                        return Object.prototype.toString.call(obj) === "[object Array]";
                    },
                    isObject: function(obj) {
						var prot =  Object.prototype.toString.call(obj);
						return prot === "[object Object]" || prot === "[object Window]";
                    },
                    isFunction: function(obj) {
                        return Object.prototype.toString.call(obj) == "[object Function]";
                    },
                    checkMobile: function() {
                        return this.checkurl("*m2.ikea.|m2.ppe.*");
                    },

                },
                //Version
                IncludeScript: {
                    load: function(data, callback, context) {
                        var c = Samuraj.Version;
						var validpage = true;
						if (c.isArray(data)) {
							for (var i=0; i<data.length; i++)
								this.load(data[i], callback, context)
						} else {	
							if (c.varExist(data.urlMask))
								validpage = c.checkurl(data.urlMask);
							if (validpage) {
								if (!c.varExist(Samuraj.ScriptLib))
									Samuraj.ScriptLib = [];
								if (!c.varExist(data.scriptUrl) && !c.varExist(data.styleUrl)) {
									try {
										if (data.slice(data.length - 3, data.length).toLowerCase() == ".js")
											var data = {
												"scriptUrl": data
											};
										else if (data.slice(data.length - 4, data.length).toLowerCase() == ".css")
											var data = {
												"styleUrl": data
											};
									} catch (err) {
										Samuraj.Log.add({
											cat: "Samuraj",
											sub: "Error",
											text: "IncludeScript - Check string - " + err,
											err: err,
											input: data
										});
									}
								}
								if (!c.varExist(data.syncLoad))
									data.syncLoad = false;

								if (c.varExist(data.styleUrl, true)) {
									if (this.updateLib(data, 0)) {
										this.loadStyle(data, callback, context)
									} else {
										this.waitingRoom(data, function() {
											if (typeof callback == "function")
												callback(context);
										});
									}
								} else if (c.varExist(data.scriptUrl, true)) {
									if (this.updateLib(data, 0)) {
										this.loadScript(data, callback, context)
									} else {
										this.waitingRoom(data, function() {
											if (typeof callback == "function")
												callback(context);
										});
									}
								}
							}
						}
                    },
                    loadScript: function(data, callback, context) {
                        var c = Samuraj.Version;
                        var js = document.createElement("script");
                        if (c.varExist(data.scriptId, true))
                            js.id = data.scriptId;
                        if (c.varExist(data.attributes)){
                        	for (var i=0; i<data.attributes.length; i++){
                        		js.setAttribute("data-" + data.attributes[i].name, data.attributes[i].value);
                        	}
                        }
                        js.type = "text/javascript";
                        js.data = data;
                        js.onload = function() {
                            Samuraj.IncludeScript.updateLib(this.data, 1);
                            if (typeof callback == "function")
                                callback(context);
                        }
                        ;
                        js.src = data.scriptUrl;
                        /*
                        if (data.syncLoad){
                            Samuraj.Page.loaded(function(){});
                            if (!Samuraj.PageReadyFired){
                                document.write('<script type="text/javascript" src="' + data.scriptUrl + '"></script>');
                            }
                        } else
                        */
                        //document.getElementsByTagName("head")[0].appendChild(js);
                        document.head.appendChild(js);
                    },
                    loadStyle: function(data, callback, context) {
                        var c = Samuraj.Version;
                        var css = document.createElement("link");
                        if (c.varExist(data.styleId, true))
                            css.id = data.styleId;
                        css.rel = "stylesheet";
                        css.type = "text/css";
                        css.href = data.styleUrl;
                        document.head.appendChild(css);

                        if (c.varExist(data.scriptUrl, true)) {
                            this.loadScript(data, callback, context);
                        } else {
                            Samuraj.IncludeScript.updateLib(data, 1);
                            if (typeof callback == "function")
                                callback(context);
                        }
                    },
                    updateLib: function(data, val) {
                        var i = this.checkLib(data);
                        if (i > -1) {
                            if (Samuraj.ScriptLib[i].loaded == 0) {
                                Samuraj.ScriptLib[i].loaded = val;
                            }
                            return false;
                        } else {
                            Samuraj.ScriptLib.push({
                                data: data,
                                "loaded": 0
                            });
                            return true;
                        }
                    },
                    checkLib: function(data) {
                        var c = Samuraj.Version;
                        try {
                            for (var i = 0; i < Samuraj.ScriptLib.length; i++) {
                                if (c.varExist(data.scriptUrl, true) && c.varExist(data.styleUrl, true))
                                    if (c.varExist(Samuraj.ScriptLib[i].data.scriptUrl, true) && c.varExist(Samuraj.ScriptLib[i].data.styleUrl, true))
                                        if (data.scriptUrl.toLowerCase().indexOf(Samuraj.ScriptLib[i].data.scriptUrl.toLowerCase()) > -1 && data.styleUrl.toLowerCase().indexOf(Samuraj.ScriptLib[i].data.styleUrl.toLowerCase()) > -1)
                                            return i;
                                if (c.varExist(data.scriptUrl, true))
                                    if (c.varExist(Samuraj.ScriptLib[i].data.scriptUrl, true))
                                        if (data.scriptUrl.toLowerCase().indexOf(Samuraj.ScriptLib[i].data.scriptUrl.toLowerCase()) > -1)
                                            return i;
                                if (c.varExist(data.styleUrl, true))
                                    if (c.varExist(Samuraj.ScriptLib[i].data.styleUrl, true))
                                        if (data.styleUrl.toLowerCase().indexOf(Samuraj.ScriptLib[i].data.styleUrl.toLowerCase()) > -1)
                                            return i;
                            }
                        } catch (err) {
							Samuraj.Log.add({
								cat: "Samuraj",
								sub: "Error",
								text: "IncludeScript - checkLib - " + err,
								err: err,
								input: data
							});
                        }
                        return -1;
                    },
                    waitingRoom: function(data, callback, context) {
                        var timeout = 400;
                        var queue = setInterval(function() {
                            if (Samuraj.ScriptLib[Samuraj.IncludeScript.checkLib(data)].loaded == 1) {
                                window.clearInterval(queue);
                                if (typeof callback == "function")
                                    callback(context);
                            }
                            timeout--;
                            if (timeout < 0)
                                window.clearInterval(queue);
                        }, 25)
                    }
                },
                //IncludeScript
                Merge: {
					merge: function(destination, source, concat_array) {
						if (concat_array){
							destination = destination.concat(source);
						} else {
							for (var property in source){
								if (source.hasOwnProperty(property))
									destination[property] = source[property];
							}
						}
						return destination;
					},
					deepmerge: function(dest, src, exclude_array, concat_array) {
						var c = Samuraj.Common;
						
						if (c.isString(dest) || c.isNumeric(dest) || c.isBoolean(dest)){
							dest = src;
						} else {
							
							for (var key in src){
								/*
								var do_merge = true;
								if (c.isArray(exclude_array)){
									if (exclude_array.indexOf(key) > -1){
										do_merge = false;
									}
								}
								if (do_merge){
								*/
								
								if (dest[key]){
									if (c.isArray(dest[key]) && c.isArray(src[key])){
										for (var i=0; i<src[key].length; i++){
											if (dest[key][i]){
												if (concat_array) 
													dest[key].concat(src[key]);
												else 
													dest[key][i] = this.deepmerge(dest[key][i], src[key][i], exclude_array, concat_array);
											} else
												dest[key][i] = src[key][i];
										}
									} else if (c.isArray(dest[key])){
										dest[key].push(src[key]);
									} else if (c.isArray(src[key])){
										dest[key] = c.toArray(dest[key]);
										dest[key].concat(src[key]);
									} else if (c.isObject(src[key])){
										dest[key] = this.deepmerge(dest[key], src[key], exclude_array, concat_array);
									} else {
										dest[key] = src[key];
									}
								} else	
									dest[key] = src[key];
								//}
							}
						}
						return dest;
					}

				},
				//Merge

				Preview: {
					check: function(callback){
						var c = Samuraj.Common;
						var previewtool_cookie = c.getCookie("samuraj_tool");
						if (previewtool_cookie && previewtool_cookie.indexOf("/") > -1){
							c.setCookie("samuraj_st", "on");

							var files = previewtool_cookie.split("|");
							if (files[1]){
								Samuraj.IncludeScript.load("https://ww8.ikea.com/samuraj" + files[1]);
							}
							if (files[0]){
								Samuraj.IncludeScript.load("https://ww8.ikea.com/samuraj" + files[0], function(){
									Samuraj.Preview.init();
									callback();
								});
							}							
						} else {
							if (document.URL.indexOf('?samurajtool') > -1 || document.URL.indexOf('&samurajtool') > -1) {
								window.location.href = "https://ww8.ikea.com/samuraj/mimi/login/" + Math.floor((Math.random() * 100000) + 1) +  ".php";
							} else {
								callback();
							}
						}
					},
					init: function() {
						var c = Samuraj.Common;
						var timeCookie = c.getCookie('samuraj_time');
						if (timeCookie) {
							SamurajSetting.Common.testcookieset = true;
							if (timeCookie == "all") {
								Samuraj.Validation.settings.override = true;
							} else {
								Samuraj.Time.now = timeCookie;
								Samuraj.Time.set = "true";
							}

							//Create Samuraj preview tool
							Samuraj.Tool.create();

						} else if (c.getCookie('samuraj_st')) {
							SamurajSetting.Common.testcookieset = true;
							Samuraj.Validation.settings.override = false;
			
							c.setCookie("samuraj_time", Samuraj.Time.now);

							//Create Samuraj preview tool
							Samuraj.Tool.create();
						}
					}
				}
				//Preview

            }
            //Samuraj

			LoadSamuraj();
			
        }
    } catch (err) {
		Samuraj.Log.add({
			cat: "Samuraj",
			sub: "Error",
			text: "General Error - " + err,
			err: err
		});
	}
}
)();