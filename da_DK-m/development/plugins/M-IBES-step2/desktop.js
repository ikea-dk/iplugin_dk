"use strict";

(function () {
  // Check ready state
  function readyNow(fn1) {
    if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
      fn1();
    } else {
      document.addEventListener('DOMContentLoaded', fn1);
    }
  }

  var runNow = function runNow() {
    if (document.getElementsByClassName("mobileBox") != null) {
      if (document.URL.indexOf('https://ww8.ikea.com/clickandcollect/dk/collect/customerdata') > -1 || document.URL.indexOf('https://ww8.ikea.com/clickandcollect/dk/basketupdate/updatebasketaddresses/redirect') > -1) {
        
      // remove login link. 
        var removeLogin = document.getElementsByClassName("loginContainer")[0];
        removeLogin.children[0].setAttribute("style", "display:none"); // Move - choose deliverly date and time, up to the first element. 

        var fromListInOrder = document.getElementsByClassName("mobileBox")[0].children[0].children[2];
        var diliverlyTimeElement = document.getElementsByClassName("col-sm-12")[0];
        var hrElement = document.createElement('hr');
        fromListInOrder.insertBefore(diliverlyTimeElement, fromListInOrder.children[0]);
        fromListInOrder.insertBefore(hrElement, fromListInOrder.children[1]); // hide survery

        var customerSurvery = document.getElementsByClassName("customerConsentSurvey")[0];
        customerSurvery.setAttribute("style", "display:none"); // checked the radio button to hous

        var checkboxWhereDoYouLive = document.getElementsByName('basket[invoiceAddress][voQuestionnaireAnswerSet][QuestionnaireType]')[0];
        checkboxWhereDoYouLive.setAttribute("checked", "checked");
        checkboxWhereDoYouLive.setAttribute("value", "HOUSE"); // show the hidden mandatory elements

        var expressData = document.getElementsByClassName('expressData row')[0];
        expressData.children[0].children[2].removeAttribute('style'); // the dom is changin postions.. 
        // so some times form-group [28] is a text field and some time it is a checkbox.
        // the same gos for form-group [29]

        var mandatoryNumberOne = document.getElementsByClassName('form-group')[28];
        var mandatoryNumberTwo = document.getElementsByClassName('form-group')[29];

        if (mandatoryNumberOne.children[1].children[3].type === "text") {
          mandatoryNumberOne.children[1].children[3].removeAttribute("style");
          mandatoryNumberOne.children[1].children[3].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[3].setAttribute("value", "000");
          mandatoryNumberOne.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[1].removeAttribute("disabled");
        } 
        else if (mandatoryNumberOne.children[1].children[2].type === "checkbox") {
          mandatoryNumberOne.children[1].children[2].removeAttribute("style");
          mandatoryNumberOne.children[1].children[2].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[2].setAttribute("checked", "checked");
          mandatoryNumberOne.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[1].removeAttribute("disabled");
        }

        if (mandatoryNumberTwo.children[1].children[3].type === "text") {
          mandatoryNumberTwo.children[1].children[3].removeAttribute('style');
          mandatoryNumberTwo.children[1].children[3].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[3].setAttribute("value", "000");
          mandatoryNumberTwo.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[1].removeAttribute("disabled");
        } 
        else if (mandatoryNumberTwo.children[1].children[2].type === "checkbox") {
          mandatoryNumberTwo.children[1].children[2].removeAttribute('style');
          mandatoryNumberTwo.children[1].children[2].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[2].setAttribute("checked", "checked");
          mandatoryNumberTwo.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[1].removeAttribute("disabled");
        } // hide deliverly infomation. 


        var deliverlyInfomation = document.getElementById("questionnaireBlock");
        deliverlyInfomation.setAttribute("style", "display:none");
        var mandatoryField = document.getElementsByClassName("mandatory_hint")[0];
        mandatoryField.setAttribute("style", "display:none");
      }
    }
  }
  readyNow(runNow);
})();