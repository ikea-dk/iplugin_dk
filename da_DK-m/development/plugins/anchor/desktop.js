/*


  /\  ._   _ |_   _  ._
 /--\ | | (_ | | (_) |


*/

(function(){

	var Anchor = {
		interupted: false,
		go: function(selector, scrollDiff) {
			var c = Samuraj.Common
			  , el = Samuraj.Element
			  , p = Samuraj.Page;
			try {
				this.onclick();
				if (selector) {
					el.get(selector, function(selector, elem) {
						if (elem.length) {
							if (c.isArray(elem) || c.isNodeList(elem))
								elem = elem[0];
							if (c.varExist(scrollDiff))
								elem.scrollDiff = c.toInt(scrollDiff)
							else
								elem.scrollDiff = c.toInt(0)
							Samuraj.Anchor.scroll(elem, 0, true);
						}
					});
				} else {
					/*
					c.onetime(document, "click", function() {
						Samuraj.Anchor.interupted = true;
					});
					*/
					p.loaded(function() {
						var c = Samuraj.Common
						  , hash = c.getUrlHash()
						  , vars = c.getUrlVars();
						if (c.varExist(hash, true) || c.varExist(vars.anc, true)) {
							if (!c.varExist(hash, true))
								hash = "#" + vars.anc;
							if (hash !== '#' && hash.indexOf("#/") == -1) {
								var selector = decodeURI(hash);
								selector = selector.replace(/\.p\./g, "|").replace(/\.t\./g, "~");
								if (selector.indexOf('|') > -1) {
									selector = selector.split('|');
									var scrollDiff = selector[1];
									selector = selector[0];
								}
								el.get(selector, function(selector, elem, scrollDiff) {
									setTimeout(function() {
										if (!Samuraj.Anchor.interupted) {
											elem = elem[0];
											var c = Samuraj.Common
											  , vars = c.getUrlVars();
											if (c.varExist(scrollDiff))
												elem.scrollDiff = c.toInt(scrollDiff);
											else
												elem.scrollDiff = c.toInt(0);

											Samuraj.Anchor.scroll(elem, 0, true);
										}
									}, 100);
								}, scrollDiff);
							}
						}
					});
				}
			} catch (err) {}
		},
		onclick: function() {
			var c = Samuraj.Common
			  , el = Samuraj.Element
			  , p = Samuraj.Page;
			p.loaded(function() {
				var a = document.querySelectorAll("a");
				for (var i = 0; i < a.length; i++) {
					var selector = a[i].getAttribute('href');
					if (c.varExist(selector)) {
						if (selector.indexOf("#") > -1) {
							
							//Add posibility to use link buttons in Publicera: removes https:// and absolute links for same page
							if (document.URL.split('#')[0].split('?')[0] == selector.split('#')[0].split('?')[0] || selector.indexOf('https://#') > -1) {
								var hash = c.getUrlHash(selector);
								a[i].setAttribute('href', hash);
								selector = a[i].getAttribute('href');
							}

							//Fix faulty fabric sample links in Norway IRW pages
							var link = selector.split('#');
							if (link[1] && link[1].indexOf('trekk') > -1) {
								if (link[0] && link[0].replace(/http(s)?:\/\/www.ikea.com/g, "") == window.location.pathname)
									a[i].setAttribute('href', "#order-fabric-sample");
								else
									a[i].setAttribute('href', link[0] + "#order-fabric-sample");
								selector = a[i].getAttribute('href');
							}


							if (selector !== '#' && selector.split('#')[0].length == 0 && selector.indexOf("#/") == -1 && selector.indexOf("#content") == -1) {
								if (selector.indexOf('|') > -1) {
									selector = selector.split('|');
									var scrollDiff = selector[1];
									selector = selector[0];
								}
								if (c.varExist(scrollDiff))
									a[i].scrollDiff = c.toInt(scrollDiff);
								else
									a[i].scrollDiff = c.toInt(0);
								a[i].scrollSelector = selector;
								c.addEvent(a[i], 'click', function(e) {
									e.scrollDiff = this.scrollDiff;
									el.get(this.scrollSelector, function(selector, elem, e) {
										elem = elem[0];
										e.preventDefault();
										elem.scrollDiff = e.scrollDiff;
										Samuraj.Anchor.scroll(elem, 0, true);
									}, e);
								});
							}
						}
					}
				}
			});
		},
		expandcollapsable: function(elem) {
			try {
				var c = Samuraj.Common
				  , parent = elem.parentNode;
				if (c.varExist(parent)) {
					while (parent.nodeName !== "BODY") {
						if (c.varExist(parent.className)) {
							if (parent.className == "collapsable-area js-collapsable-area component") {
								var collapsarea = parent.querySelector("div.collapsable-area__content.js-collapsable-area__content");
								collapsarea.setAttribute('aria-hidden', 'false');
								collapsarea.setAttribute('style', 'max-height: 6000px;');

								var collapsbuttoncontainer = parent.querySelector("div.collapsable-area__button-container.js-collapsable-area__button-container");
								collapsbuttoncontainer.setAttribute('aria-hidden', 'false');

								var collapsbutton = collapsbuttoncontainer.querySelector("button.js-collapsable-area__button");
								collapsbutton.setAttribute('aria-expanded', 'true');

								var collapsbuttonmore = collapsbutton.querySelector("div.collapsable-area__more.js-collapsable-area__more");
								collapsbuttonmore.setAttribute('aria-hidden', 'true');

								var collapsbuttonless = collapsbutton.querySelector("div.collapsable-area__less.js-collapsable-area__less");
								collapsbuttonless.setAttribute('aria-hidden', 'false');

								break;
							}
						}
						parent = parent.parentNode;
					}
				}
			} catch (err) {
				Samuraj.Log.add({
					cat: "Anchor",
					sub: "Error",
					text:  "Catch - expandcollapsable - " + err,
					err: err,
					elem: elem
				});
			}
		},
		scroll: function(elem, duration, animate) {
			try {
				this.expandcollapsable(elem);

				var c = Samuraj.Common
				  , scrollDiff = 0;
				if (c.varExist(elem.scrollDiff))
					scrollDiff = elem.scrollDiff;
				if (animate) {
					var startingY = window.pageYOffset,
						elemY = window.pageYOffset + elem.getBoundingClientRect().top,
						targetY = document.body.scrollHeight - elemY < window.innerHeight ? document.body.scrollHeight - window.innerHeight : elemY,
						diff = targetY + scrollDiff - startingY,
						easing = function(t) {
							return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
						},
						start;

					if (!diff)
						return;
					if (duration == 0){
						duration = parseInt(diff/3);
						if (duration < 400)
							duration = 400;
						if (duration > 2000)
							duration = 2000;
					}

					window.requestAnimationFrame(function step(timestamp) {
						if (!start)
							start = timestamp;
						var time = timestamp - start
						  , percent = Math.min(time / duration, 1)
						  , percent = easing(percent);
						window.scrollTo(0, startingY + diff * percent);
						if (time < duration)
							window.requestAnimationFrame(step);
					});
				} else {
					var elemY = window.pageYOffset + elem.getBoundingClientRect().top
					  , targetY = document.body.scrollHeight - elemY < window.innerHeight ? document.body.scrollHeight - window.innerHeight : elemY;
					window.scrollTo(0, targetY + scrollDiff);
				}
			} catch (err) {
				Samuraj.Log.add({
					cat: "Anchor",
					sub: "Error",
					text:  "Catch - scroll - " + err,
					err: err,
					elem: elem
				});
			}
		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "Anchor",
		objects: [
			{name: "Anchor", fn: Anchor}
		],
		dependencies: [],
		bootup: function(){
			//Initiate Anchor
			Samuraj.Anchor.go();
			Samuraj.Log.add({
				cat: "Anchor",
				sub: "Timing",
				text: "Loading done",
				timing: performance.now() - SamurajTiming				
			});
		}
	}

	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();


