'use strict';

(function () {
    // Check ready state
    function readyNow(fn1) {
        if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
            fn1();
        } else {
            document.addEventListener('DOMContentLoaded', fn1);
        }
    }
    var runNow = function runNow() {
        if (document.URL.indexOf('https://secure.ikea.com/webapp/wcs/stores/servlet/') > -1 ) {
            if (document.getElementById('privateCustomer').checked && document.URL.includes("mCheckoutWelcomeView")) {
                var insertInfoSpanToFrom = document.getElementById("signup_checkout_private");

                // take use of a existing country element
                var country = document.getElementById("signup_checkout_private").children[18];
                country.removeAttribute('type'); // do not hide
                country.setAttribute('value', 'Danmark');
                country.setAttribute('id', 'countryDenmark');
                country.setAttribute('name', 'basket[invoiceAddress][country]');
                country.setAttribute('class', 'tooltip');
                country.disabled = true;

                // tooltip div
                var tooltip_div2 = document.createElement("div");
                tooltip_div2.setAttribute('id', 'myTooltipDiv');
                tooltip_div2.setAttribute('class', 'tooltip');
                insertInfoSpanToFrom.appendChild(tooltip_div2);

                // Title on country
                var para = document.createElement("p");
                var p = document.createTextNode("Land");
                para.appendChild(p);
                para.setAttribute('id', 'myTitle');
                para.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
                para.onclick = function openCountry() {
                    var px = document.getElementById('myModalCountry');
                    if (px.style.display === "none") {
                        px.style.display = "block";
                    } else {
                        px.style.display = "none";
                    }
                };
                insertInfoSpanToFrom.appendChild(para);

                var modalPopUp = document.createElement('div');
                modalPopUp.setAttribute('id', 'myModalCountry');
                modalPopUp.setAttribute('class', 'modalCountry');
                modalPopUp.innerHTML = '<div class=\'countryPopup\' >\n                                                <p>Vi leverer til hele Danmark. Hvis du vil have leveret til udlandet, kan du l\xE6se mere                                                 \n                                                <a href=\'https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering\'>her.</a></p>\n                                            </div>';

                insertInfoSpanToFrom.appendChild(modalPopUp);

                // inster child in order
                tooltip_div2.insertBefore(para, tooltip_div2.childNodes[0]);
                tooltip_div2.insertBefore(country, tooltip_div2.childNodes[1]);
                tooltip_div2.insertBefore(myModalCountry, tooltip_div2.childNodes[2]);

                // place before e-mail field
                var mailElement = document.getElementById('email1_field');
                mailElement.before(myTooltipDiv);
            }
            if (document.getElementById('privateCustomer').checked && document.URL.includes('mCheckoutAddressView')) {
                var insertInfoSpanToFrom = document.getElementById("signup_checkout_private");

                // take use of a existing country element
                var country = document.getElementById("signup_checkout_private").children[19];
                country.removeAttribute('type'); // do not hide
                country.setAttribute('value', 'Danmark');
                country.setAttribute('id', 'countryDenmark');
                country.setAttribute('name', 'basket[invoiceAddress][country]');
                country.setAttribute('class', 'tooltip');
                country.disabled = true;

                // tooltip div
                var tooltip_div2 = document.createElement("div");
                tooltip_div2.setAttribute('id', 'myTooltipDiv');
                tooltip_div2.setAttribute('class', 'tooltip');
                insertInfoSpanToFrom.appendChild(tooltip_div2);

                // Title on country
                var para = document.createElement("p");
                var p = document.createTextNode("Land");
                para.appendChild(p);
                para.setAttribute('id', 'myTitle');
                para.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
                para.onclick = function openCountry() {
                    var px = document.getElementById('myModalCountry');
                    if (px.style.display === "none") {
                        px.style.display = "block";
                    } else {
                        px.style.display = "none";
                    }
                };
                insertInfoSpanToFrom.appendChild(para);

                var modalPopUp = document.createElement('div');
                modalPopUp.setAttribute('id', 'myModalCountry');
                modalPopUp.setAttribute('class', 'modalCountry');
                modalPopUp.innerHTML = '<div class=\'countryPopup\' >\n                                                <p>Vi leverer til hele Danmark. Hvis du vil have leveret til udlandet, kan du l\xE6se mere                                                 \n                                                <a href=\'https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering\'>her.</a></p>\n                                            </div>';

                insertInfoSpanToFrom.appendChild(modalPopUp);

                // inster child in order
                tooltip_div2.insertBefore(para, tooltip_div2.childNodes[0]);
                tooltip_div2.insertBefore(country, tooltip_div2.childNodes[1]);
                tooltip_div2.insertBefore(myModalCountry, tooltip_div2.childNodes[2]);

                // place before e-mail field
                var mailElement = document.getElementById('email1_field');
                mailElement.before(myTooltipDiv);
            }
            if (document.getElementById('businessCustomer').checked && document.URL.includes('mCheckoutAddressView')) {
                var insertInfoSpanToFrom = document.getElementById("signup_checkout_business");

                // take use of a existing country element
                var country = document.getElementById("signup_checkout_business").children[20];
                country.removeAttribute('type'); // do not hide 
                country.setAttribute('value', 'Danmark');
                country.setAttribute('id', 'countryDenmark');
                country.setAttribute('name', 'basket[invoiceAddress][country]');
                country.setAttribute('class', 'tooltip');
                country.disabled = true;

                // tooltip div
                var tooltip_div3 = document.createElement("div");
                tooltip_div3.setAttribute('id', 'myTooltipDiv');
                tooltip_div3.setAttribute('class', 'tooltip');
                insertInfoSpanToFrom.appendChild(tooltip_div3);

                // Title on country
                var para = document.createElement("p");
                var p = document.createTextNode("Land");
                para.appendChild(p);
                para.setAttribute('id', 'myTitle');
                para.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
                para.onclick = function openCountry() {
                    var bx = document.getElementById('myModalCountry');
                    if (bx.style.display === "none") {
                        bx.style.display = "block";
                    } else {
                        bx.style.display = "none";
                    }
                };
                insertInfoSpanToFrom.appendChild(para);

                var modalPopUp = document.createElement('div');
                modalPopUp.setAttribute('id', 'myModalCountry');
                modalPopUp.setAttribute('class', 'modalCountry');
                modalPopUp.innerHTML = '<div class=\'countryPopup\' >\n                                                <p>Vi leverer til hele Danmark. Hvis du vil have leveret til udlandet, kan du l\xE6se mere                                                 \n                                                <a href=\'https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering\'>her.</a></p>\n                                            </div>';
                insertInfoSpanToFrom.appendChild(modalPopUp);

                // inster child in order
                tooltip_div3.insertBefore(para, tooltip_div3.childNodes[0]);
                tooltip_div3.insertBefore(country, tooltip_div3.childNodes[1]);
                tooltip_div3.insertBefore(myModalCountry, tooltip_div3.childNodes[2]);

                // place before e-mail field
                var mailElement = document.getElementById('email1_field');
                mailElement.before(myTooltipDiv);


                // NO BusinessCart
                var postionOfBusinessCartBox = document.getElementById('signup_checkout_business').children[11];

                var divBusinessCartBox = document.createElement("div");
                divBusinessCartBox.setAttribute('id', 'businessCartBox');
                divBusinessCartBox.innerHTML = "<p class='collapsible_IKEABUSINESSCARD'>IKEA BUSINESS Kort - klik her</p><div class='content_IKEABUSINESSCARD'><p class='boxBoby' >Du kan ikke betale online med IKEA BUSINESS kort i øjeblikket.<br> Kontakt <a target='_blank' href='https://www.ikea.com/dk/da/catalog/categories/business/ikea_business/?preferedui=desktop#businesslokal'>dit nærmeste IKEA BUSINESS</a>, hvis du ønsker at bruge kortet.</p></div><br>";
                postionOfBusinessCartBox.before(divBusinessCartBox);
                
                var coll = document.getElementsByClassName("collapsible_IKEABUSINESSCARD");
                var i;
                
                for (i = 0; i < coll.length; i++) {
                    coll[i].addEventListener("click", function () {
                        var content = this.nextElementSibling;
                        if (content.style.display === "block") {
                            content.style.display = "none";
                        } else {
                            content.style.display = "block";
                        }
                    });
                }
            }
        }
    };
    readyNow(runNow);
})();