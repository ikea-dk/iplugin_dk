/*

       _ ___ 
  /\  |_) |  
 /--\ |  _|_ 
             

*/


(function () {
    
    var Parameters = {
        isEcom: true,  // shoppingcart or shoppinglist default
        Consumer: 'DKSAMURAJ', // your Consumer
        Contract: '40627', // your Contract
        Country: 'dk',
        Language: 'da'
    };

    /*
	var Parameters = {
		isEcom: true,  // shoppingcart or shoppinglist default
		Consumer: 'IRW', // your Consumer
		Contract: '26705', // your Contract
		Country: 'no',
		Language: 'no'
    };
   
    var Parameters = {
        isEcom: true,  // shoppingcart or shoppinglist default
        Consumer: 'MAMMUT', // your Consumer
        Contract: '37249', // your Contract
        Country: 'no',
        Language: 'no'
    };
    */

	var API = {};

	(function () {
    'use strict';
    var jq = function () {
    };
    var arr = [], document = window.document, getProto = Object.getPrototypeOf, slice = arr.slice, concat = arr.concat,
        push = arr.push, indexOf = arr.indexOf, class2type = {}, toString = class2type.toString,
        hasOwn = class2type.hasOwnProperty, fnToString = hasOwn.toString,
        ObjectFunctionString = fnToString.call(Object), support = {}, isFunction = function (t) {
            return "function" == typeof t && "number" != typeof t.nodeType
        }, isWindow = function (t) {
            return null != t && t === t.window
        };
    jq.fn = jq.prototype;
    support = {};

    function toType(t) {
        return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? class2type[toString.call(t)] || "object" : typeof t
    }

    function isArrayLike(t) {
        var e = !!t && "length" in t && t.length, r = toType(t);
        return !isFunction(t) && !isWindow(t) && ("array" === r || 0 === e || "number" == typeof e && 0 < e && e - 1 in t)
    }

    jq.extend = jq.fn.extend =
        function () {
            var t, e, r, n, o, a, i = arguments[0] || {}, s = 1, c = arguments.length, l = !1;
            for ("boolean" == typeof i && (l = i, i = arguments[s] || {}, s++), "object" == typeof i || isFunction(i) || (i = {}), s === c && (i = this, s--); s < c; s++) if (null != (t = arguments[s])) for (e in t) r = i[e], i !== (n = t[e]) && (l && n && (jq.isPlainObject(n) || (o = Array.isArray(n))) ? (o ? (o = !1, a = r && Array.isArray(r) ? r : []) : a = r && jq.isPlainObject(r) ? r : {}, i[e] = jq.extend(l, a, n)) : void 0 !== n && (i[e] = n));
            return i
        }, jq.extend({
        isReady: !0, error: function (t) {
            throw new Error(t)
        }, noop: function () {
        }, isPlainObject: function (t) {
            var e, r;
            return !(!t || "[object Object]" !== toString.call(t)) && (!(e = getProto(t)) || "function" == typeof(r = hasOwn.call(e, "constructor") && e.constructor) && fnToString.call(r) === ObjectFunctionString)
        }, isEmptyObject: function (t) {
            var e;
            for (e in t) return !1;
            return !0
        }, globalEval: function (t) {
            DOMEval(t)
        }, each: function (t, e) {
            var r, n = 0;
            if (isArrayLike(t)) for (r = t.length; n < r && !1 !== e.call(t[n], n, t[n]); n++) ; else for (n in t) if (!1 === e.call(t[n], n, t[n])) break;
            return t
        }, trim: function (t) {
            return null == t ? "" : (t + "").replace(rtrim, "")
        }, makeArray: function (t, e) {
            var r = e || [];
            return null != t && (isArrayLike(Object(t)) ? jq.merge(r, "string" == typeof t ? [t] : t) : push.call(r, t)), r
        }, inArray: function (t, e, r) {
            return null == e ? -1 : indexOf.call(e, t, r)
        }, merge: function (t, e) {
            for (var r = +e.length, n = 0, o = t.length; n < r; n++) t[o++] = e[n];
            return t.length = o, t
        }, grep: function (t, e, r) {
            for (var n = [], o = 0, a = t.length, i = !r; o < a; o++) !e(t[o], o) !== i && n.push(t[o]);
            return n
        }, map: function (t, e, r) {
            var n, o, a = 0, i = [];
            if (isArrayLike(t)) for (n = t.length; a < n; a++) null != (o = e(t[a], a, r)) && i.push(o); else for (a in t) null != (o = e(t[a], a, r)) && i.push(o);
            return concat.apply([], i)
        }, guid: 1, support: support
    });
    var rnothtmlwhite = /[^\x20\t\r\n\f]+/g;

    function createOptions(t) {
        var r = {};
        return jq.each(t.match(rnothtmlwhite) || [], function (t, e) {
            r[e] = !0
        }), r
    }

    jq.Callbacks = function (n) {
        n = "string" == typeof n ? createOptions(n) : jq.extend({}, n);
        var r, t, e, o, a = [], i = [], s = -1, c = function () {
            for (o = o || n.once, e = r = !0; i.length; s = -1) for (t = i.shift(); ++s < a.length;) !1 === a[s].apply(t[0], t[1]) && n.stopOnFalse && (s = a.length, t = !1);
            n.memory || (t = !1), r = !1, o && (a = t ? [] : "")
        }, l = {
            add: function () {
                return a && (t && !r && (s = a.length - 1, i.push(t)), function r(t) {
                    jq.each(t, function (t, e) {
                        isFunction(e) ? n.unique && l.has(e) || a.push(e) : e && e.length && "string" !== toType(e) && r(e)
                    })
                }(arguments), t && !r && c()), this
            }, remove: function () {
                return jq.each(arguments, function (t, e) {
                    for (var r; -1 < (r = jq.inArray(e, a, r));) a.splice(r, 1), r <= s && s--
                }), this
            }, has: function (t) {
                return t ? -1 < jq.inArray(t, a) : 0 < a.length
            }, empty: function () {
                return a && (a = []), this
            }, disable: function () {
                return o = i = [], a = t = "", this
            }, disabled: function () {
                return !a
            }, lock: function () {
                return o = i = [], t || r || (a = t = ""), this
            }, locked: function () {
                return !!o
            }, fireWith: function (t, e) {
                return o || (e = [t, (e = e || []).slice ? e.slice() : e], i.push(e), r || c()), this
            }, fire: function () {
                return l.fireWith(this, arguments), this
            }, fired: function () {
                return !!e
            }
        };
        return l
    };
    var r20 = /%20/g, rhash = /#.*$/, rantiCache = /([?&])_=[^&]*/, rheaders = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, rnoContent = /^(?:GET|HEAD)$/,
        rprotocol = /^\/\//, prefilters = {}, transports = {}, allTypes = "*/".concat("*"),
        originAnchor = document.createElement("a");

    function addToPrefiltersOrTransports(a) {
        return function (t, e) {
            "string" != typeof t && (e = t, t = "*");
            var r, n = 0, o = t.toLowerCase().match(rnothtmlwhite) || [];
            if (isFunction(e)) for (; r = o[n++];) "+" === r[0] ? (r = r.slice(1) || "*", (a[r] = a[r] || []).unshift(e)) : (a[r] = a[r] || []).push(e)
        }
    }

    function inspectPrefiltersOrTransports(e, o, a, i) {
        var s = {}, c = e === transports;

        function l(t) {
            var n;
            return s[t] = !0, jq.each(e[t] || [], function (t, e) {
                var r = e(o, a, i);
                return "string" != typeof r || c || s[r] ? c ? !(n = r) : void 0 : (o.dataTypes.unshift(r), l(r), !1)
            }), n
        }

        return l(o.dataTypes[0]) || !s["*"] && l("*")
    }

    function ajaxExtend(t, e) {
        var r, n, o = jq.ajaxSettings.flatOptions || {};
        for (r in e) void 0 !== e[r] && ((o[r] ? t : n || (n = {}))[r] = e[r]);
        return n && jq.extend(!0, t, n), t
    }

    function ajaxHandleResponses(t, e, r) {
        for (var n, o, a, i, s = t.contents, c = t.dataTypes; "*" === c[0];) c.shift(), void 0 === n && (n = t.mimeType || e.getResponseHeader("Content-Type"));
        if (n) for (o in s) if (s[o] && s[o].test(n)) {
            c.unshift(o);
            break
        }
        if (c[0] in r) a = c[0]; else {
            for (o in r) {
                if (!c[0] || t.converters[o + " " + c[0]]) {
                    a = o;
                    break
                }
                i || (i = o)
            }
            a = a || i
        }
        if (a) return a !== c[0] && c.unshift(a), r[a]
    }

    function ajaxConvert(t, e, r, n) {
        var o, a, i, s, c, l = {}, u = t.dataTypes.slice();
        if (u[1]) for (i in t.converters) l[i.toLowerCase()] = t.converters[i];
        for (a = u.shift(); a;) if (t.responseFields[a] && (r[t.responseFields[a]] = e), !c && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), c = a, a = u.shift()) if ("*" === a) a = c; else if ("*" !== c && c !== a) {
            if (!(i = l[c + " " + a] || l["* " + a])) for (o in l) if ((s = o.split(" "))[1] === a && (i = l[c + " " + s[0]] || l["* " + s[0]])) {
                !0 === i ? i = l[o] : !0 !== l[o] && (a = s[0], u.unshift(s[1]));
                break
            }
            if (!0 !== i) if (i && t.throws) e = i(e); else try {
                e = i(e)
            } catch (t) {
                return {state: "parsererror", error: i ? t : "No conversion from " + c + " to " + a}
            }
        }
        return {state: "success", data: e}
    }

    originAnchor.href = location.href, jq.extend({
        Deferred: function (t) {
            var a = [["notify", "progress", jq.Callbacks("memory"), jq.Callbacks("memory"), 2], ["resolve", "done", jq.Callbacks("once memory"), jq.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", jq.Callbacks("once memory"), jq.Callbacks("once memory"), 1, "rejected"]],
                o = "pending", i = {
                    state: function () {
                        return o
                    }, always: function () {
                        return s.done(arguments).fail(arguments), this
                    }, catch: function (t) {
                        return i.then(null, t)
                    }, pipe: function () {
                        var o = arguments;
                        return jq.Deferred(function (n) {
                            jq.each(a, function (t, e) {
                                var r = isFunction(o[e[4]]) && o[e[4]];
                                s[e[1]](function () {
                                    var t = r && r.apply(this, arguments);
                                    t && isFunction(t.promise) ? t.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[e[0] + "With"](this, r ? [t] : arguments)
                                })
                            }), o = null
                        }).promise()
                    }, then: function (e, r, n) {
                        var c = 0;

                        function l(o, a, i, s) {
                            return function () {
                                var r = this, n = arguments, t = function () {
                                    var t, e;
                                    if (!(o < c)) {
                                        if ((t = i.apply(r, n)) === a.promise()) throw new TypeError("Thenable self-resolution");
                                        e = t && ("object" == typeof t || "function" == typeof t) && t.then, isFunction(e) ? s ? e.call(t, l(c, a, Identity, s), l(c, a, Thrower, s)) : (c++, e.call(t, l(c, a, Identity, s), l(c, a, Thrower, s), l(c, a, Identity, a.notifyWith))) : (i !== Identity && (r = void 0, n = [t]), (s || a.resolveWith)(r, n))
                                    }
                                }, e = s ? t : function () {
                                    try {
                                        t()
                                    } catch (t) {
                                        jq.Deferred.exceptionHook && jq.Deferred.exceptionHook(t, e.stackTrace), c <= o + 1 && (i !== Thrower && (r = void 0, n = [t]), a.rejectWith(r, n))
                                    }
                                };
                                o ? e() : (jq.Deferred.getStackHook && (e.stackTrace = jq.Deferred.getStackHook()), window.setTimeout(e))
                            }
                        }

                        return jq.Deferred(function (t) {
                            a[0][3].add(l(0, t, isFunction(n) ? n : Identity, t.notifyWith)), a[1][3].add(l(0, t, isFunction(e) ? e : Identity)), a[2][3].add(l(0, t, isFunction(r) ? r : Thrower))
                        }).promise()
                    }, promise: function (t) {
                        return null != t ? jq.extend(t, i) : i
                    }
                }, s = {};
            return jq.each(a, function (t, e) {
                var r = e[2], n = e[5];
                i[e[1]] = r.add, n && r.add(function () {
                    o = n
                }, a[3 - t][2].disable, a[3 - t][3].disable, a[0][2].lock, a[0][3].lock), r.add(e[3].fire), s[e[0]] = function () {
                    return s[e[0] + "With"](this === s ? void 0 : this, arguments), this
                }, s[e[0] + "With"] = r.fireWith
            }), i.promise(s), t && t.call(s, s), s
        },
        when: function (t) {
            var r = arguments.length, e = r, n = Array(e), o = slice.call(arguments), a = jq.Deferred(),
                i = function (e) {
                    return function (t) {
                        n[e] = this, o[e] = 1 < arguments.length ? slice.call(arguments) : t, --r || a.resolveWith(n, o)
                    }
                };
            if (r <= 1 && (adoptValue(t, a.done(i(e)).resolve, a.reject, !r), "pending" === a.state() || isFunction(o[e] && o[e].then))) return a.then();
            for (; e--;) adoptValue(o[e], i(e), a.reject);
            return a.promise()
        },
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: location.href,
            type: "GET",
            isLocal: rlocalProtocol.test(location.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": allTypes,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": jq.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (t, e) {
            return e ? ajaxExtend(ajaxExtend(t, jq.ajaxSettings), e) : ajaxExtend(jq.ajaxSettings, t)
        },
        ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
        ajaxTransport: addToPrefiltersOrTransports(transports),
        ajax: function (t, e) {
            "object" == typeof t && (e = t, t = void 0), e = e || {};
            var u, p, f, r, d, n, h, j, o, a, y = jq.ajaxSetup({}, e), m = y.context || y,
                x = y.context && (m.nodeType || m.jq) ? jq(m) : jq.event, g = jq.Deferred(),
                v = jq.Callbacks("once memory"), q = y.statusCode || {}, i = {}, s = {}, c = "canceled", b = {
                    readyState: 0, getResponseHeader: function (t) {
                        var e;
                        if (h) {
                            if (!r) for (r = {}; e = rheaders.exec(f);) r[e[1].toLowerCase()] = e[2];
                            e = r[t.toLowerCase()]
                        }
                        return null == e ? null : e
                    }, getAllResponseHeaders: function () {
                        return h ? f : null
                    }, setRequestHeader: function (t, e) {
                        return null == h && (t = s[t.toLowerCase()] = s[t.toLowerCase()] || t, i[t] = e), this
                    }, overrideMimeType: function (t) {
                        return null == h && (y.mimeType = t), this
                    }, statusCode: function (t) {
                        var e;
                        if (t) if (h) b.always(t[b.status]); else for (e in t) q[e] = [q[e], t[e]];
                        return this
                    }, abort: function (t) {
                        var e = t || c;
                        return u && u.abort(e), l(0, e), this
                    }
                };
            if (g.promise(b), y.url = ((t || y.url || location.href) + "").replace(rprotocol, location.protocol + "//"), y.type = e.method || e.type || y.method || y.type, y.dataTypes = (y.dataType || "*").toLowerCase().match(rnothtmlwhite) || [""], null == y.crossDomain) {
                n = document.createElement("a");
                try {
                    n.href = y.url, n.href = n.href, y.crossDomain = originAnchor.protocol + "//" + originAnchor.host != n.protocol + "//" + n.host
                } catch (t) {
                    y.crossDomain = !0
                }
            }
            if (y.data && y.processData && "string" != typeof y.data && (y.data = jq.param(y.data, y.traditional)), inspectPrefiltersOrTransports(prefilters, y, e, b), h) return b;
            for (o in(j = jq.event && y.global) && 0 == jq.active++ && jq.event.trigger("ajaxStart"), y.type = y.type.toUpperCase(), y.hasContent = !rnoContent.test(y.type), p = y.url.replace(rhash, ""), y.hasContent ? y.data && y.processData && 0 === (y.contentType || "").indexOf("application/x-www-form-urlencoded") && (y.data = y.data.replace(r20, "+")) : (a = y.url.slice(p.length), y.data && (y.processData || "string" == typeof y.data) && (p += (rquery.test(p) ? "&" : "?") + y.data, delete y.data), !1 === y.cache && (p = p.replace(rantiCache, "$1"), a = (rquery.test(p) ? "&" : "?") + "_=" + nonce++ + a), y.url = p + a), y.ifModified && (jq.lastModified[p] && b.setRequestHeader("If-Modified-Since", jq.lastModified[p]), jq.etag[p] && b.setRequestHeader("If-None-Match", jq.etag[p])), (y.data && y.hasContent && !1 !== y.contentType || e.contentType) && b.setRequestHeader("Content-Type", y.contentType), b.setRequestHeader("Accept", y.dataTypes[0] && y.accepts[y.dataTypes[0]] ? y.accepts[y.dataTypes[0]] + ("*" !== y.dataTypes[0] ? ", " + allTypes + "; q=0.01" : "") : y.accepts["*"]), y.headers) b.setRequestHeader(o, y.headers[o]);
            if (y.beforeSend && (!1 === y.beforeSend.call(m, b, y) || h)) return b.abort();
            if (c = "abort", v.add(y.complete), b.done(y.success), b.fail(y.error), u = inspectPrefiltersOrTransports(transports, y, e, b)) {
                if (b.readyState = 1, j && x.trigger("ajaxSend", [b, y]), h) return b;
                y.async && 0 < y.timeout && (d = window.setTimeout(function () {
                    b.abort("timeout")
                }, y.timeout));
                try {
                    h = !1, u.send(i, l)
                } catch (t) {
                    if (h) throw t;
                    l(-1, t)
                }
            } else l(-1, "No Transport");

            function l(t, e, r, n) {
                var o, a, i, s, c, l = e;
                h || (h = !0, d && window.clearTimeout(d), u = void 0, f = n || "", b.readyState = 0 < t ? 4 : 0, o = 200 <= t && t < 300 || 304 === t, r && (s = ajaxHandleResponses(y, b, r)), s = ajaxConvert(y, s, b, o), o ? (y.ifModified && ((c = b.getResponseHeader("Last-Modified")) && (jq.lastModified[p] = c), (c = b.getResponseHeader("etag")) && (jq.etag[p] = c)), 204 === t || "HEAD" === y.type ? l = "nocontent" : 304 === t ? l = "notmodified" : (l = s.state, a = s.data, o = !(i = s.error))) : (i = l, !t && l || (l = "error", t < 0 && (t = 0))), b.status = t, b.statusText = (e || l) + "", o ? g.resolveWith(m, [a, l, b]) : g.rejectWith(m, [b, l, i]), b.statusCode(q), q = void 0, j && x.trigger(o ? "ajaxSuccess" : "ajaxError", [b, y, o ? a : i]), v.fireWith(m, [b, l]), j && (x.trigger("ajaxComplete", [b, y]), --jq.active || jq.event.trigger("ajaxStop")))
            }

            return b
        },
        getJSON: function (t, e, r) {
            return jq.get(t, e, r, "json")
        },
        getScript: function (t, e) {
            return jq.get(t, void 0, e, "script")
        }
    }), jq.isNumeric = function (t) {
        var e = jq.type(t);
        return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t))
    }, jq.ajaxSettings.xhr = function () {
        try {
            return new window.XMLHttpRequest
        } catch (t) {
        }
    };
    var xhrSuccessStatus = {0: 200, 1223: 204}, xhrSupported = jq.ajaxSettings.xhr();
    support.cors = !!xhrSupported && "withCredentials" in xhrSupported, support.ajax = xhrSupported = !!xhrSupported, jq.ajaxTransport(function (o) {
        var a, i;
        if (support.cors || xhrSupported && !o.crossDomain) return {
            send: function (t, e) {
                var r, n = o.xhr();
                if (n.open(o.type, o.url, o.async, o.username, o.password), o.xhrFields) for (r in o.xhrFields) n[r] = o.xhrFields[r];
                for (r in o.mimeType && n.overrideMimeType && n.overrideMimeType(o.mimeType), o.crossDomain || t["X-Requested-With"] || (t["X-Requested-With"] = "XMLHttpRequest"), t) n.setRequestHeader(r, t[r]);
                a = function (t) {
                    return function () {
                        a && (a = i = n.onload = n.onerror = n.onabort = n.ontimeout = n.onreadystatechange = null, "abort" === t ? n.abort() : "error" === t ? "number" != typeof n.status ? e(0, "error") : e(n.status, n.statusText) : e(xhrSuccessStatus[n.status] || n.status, n.statusText, "text" !== (n.responseType || "text") || "string" != typeof n.responseText ? {binary: n.response} : {text: n.responseText}, n.getAllResponseHeaders()))
                    }
                }, n.onload = a(), i = n.onerror = n.ontimeout = a("error"), void 0 !== n.onabort ? n.onabort = i : n.onreadystatechange = function () {
                    4 === n.readyState && window.setTimeout(function () {
                        a && i()
                    })
                }, a = a("abort");
                try {
                    n.send(o.hasContent && o.data || null)
                } catch (t) {
                    if (a) throw t
                }
            }, abort: function () {
                a && a()
            }
        }
    }), jq.ajaxPrefilter(function (t) {
        t.crossDomain && (t.contents.script = !1)
    }), jq.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /\b(?:java|ecma)script\b/},
        converters: {
            "text script": function (t) {
                return jq.globalEval(t), t
            }
        }
    }), jq.ajaxPrefilter("script", function (t) {
        void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET")
    }), jq.ajaxTransport("script", function (r) {
        var n, o;
        if (r.crossDomain) return {
            send: function (t, e) {
                n = jq("<script>").prop({charset: r.scriptCharset, src: r.url}).on("load error", o = function (t) {
                    n.remove(), o = null, t && e("error" === t.type ? 404 : 200, t.type)
                }), document.head.appendChild(n[0])
            }, abort: function () {
                o && o()
            }
        }
    });
    var oldCallbacks = [], rjsonp = /(=)\?(?=&|$)|\?\?/;
    jq.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var t = oldCallbacks.pop() || jq.expando + "_" + nonce++;
            return this[t] = !0, t
        }
    }), jq.ajaxPrefilter("json jsonp", function (t, e, r) {
        var n, o, a,
            i = !1 !== t.jsonp && (rjsonp.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && rjsonp.test(t.data) && "data");
        if (i || "jsonp" === t.dataTypes[0]) return n = t.jsonpCallback = isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, i ? t[i] = t[i].replace(rjsonp, "$1" + n) : !1 !== t.jsonp && (t.url += (rquery.test(t.url) ? "&" : "?") + t.jsonp + "=" + n), t.converters["script json"] = function () {
            return a || jq.error(n + " was not called"), a[0]
        }, t.dataTypes[0] = "json", o = window[n], window[n] = function () {
            a = arguments
        }, r.always(function () {
            void 0 === o ? jq(window).removeProp(n) : window[n] = o, t[n] && (t.jsonpCallback = e.jsonpCallback, oldCallbacks.push(n)), a && isFunction(o) && o(a[0]), a = o = void 0
        }), "script"
    });


// Give the init function the jq prototype for later instantiation

    function on(elem, types, selector, data, fn, one) {
        var origFn, type;

        // Types can be a map of types/handlers
        if (typeof types === "object") {

            // ( types-Object, selector, data )
            if (typeof selector !== "string") {

                // ( types-Object, data )
                data = data || selector;
                selector = undefined;
            }
            for (type in types) {
                on(elem, type, selector, data, types[type], one);
            }
            return elem;
        }

        if (data == null && fn == null) {

            // ( types, fn )
            fn = selector;
            data = selector = undefined;
        } else if (fn == null) {
            if (typeof selector === "string") {

                // ( types, selector, fn )
                fn = data;
                data = undefined;
            } else {

                // ( types, data, fn )
                fn = data;
                data = selector;
                selector = undefined;
            }
        }
        if (fn === false) {
            fn = returnFalse;
        } else if (!fn) {
            return elem;
        }

        if (one === 1) {
            origFn = fn;
            fn = function (event) {

                // Can use an empty set, since event contains the info
                jq().off(event);
                return origFn.apply(this, arguments);
            };

            // Use same guid so caller can remove using origFn
            fn.guid = origFn.guid || (origFn.guid = jq.guid++);
        }
        return elem.each(function () {
            jq.event.add(this, types, fn, data, selector);
        });
    }

    jq.fn.extend({

        on: function (types, selector, data, fn) {
            return on(this, types, selector, data, fn);
        },
        one: function (types, selector, data, fn) {
            return on(this, types, selector, data, fn, 1);
        },
        off: function (types, selector, fn) {
            var handleObj, type;
            if (types && types.preventDefault && types.handleObj) {

                // ( event )  dispatched jq.Event
                handleObj = types.handleObj;
                jq(types.delegateTarget).off(
                    handleObj.namespace ?
                        handleObj.origType + "." + handleObj.namespace :
                        handleObj.origType,
                    handleObj.selector,
                    handleObj.handler
                );
                return this;
            }
            if (typeof types === "object") {

                // ( types-object [, selector] )
                for (type in types) {
                    this.off(type, selector, types[type]);
                }
                return this;
            }
            if (selector === false || typeof selector === "function") {

                // ( types [, fn] )
                fn = selector;
                selector = undefined;
            }
            if (fn === false) {
                fn = returnFalse;
            }
            return this.each(function () {
                jq.event.remove(this, types, fn, selector);
            });
        }
    });
    jq.each([
        "ajaxStart",
        "ajaxStop",
        "ajaxComplete",
        "ajaxError",
        "ajaxSuccess",
        "ajaxSend"
    ], function (i, type) {
        jq.fn[type] = function (fn) {
            return this.on(type, fn);
        };
    });

    (function ($) {
        'use strict';

        // TODO-me test POST  Shopping Bag
        // TODO-me check bagSource for other country
        API = function (options) {
            var default_options = {
                isEcom: true,
                Consumer: '...',
                Contract: '...',
                Country: 'ru',
                Language: 'ru'
            };
            var options = $.extend({}, default_options, options);


            this.Consumer = options.Consumer;
            this.Contract = options.Contract;
            this.Country = options.Country;
            this.Language = options.Language;
            this.LocationShop = this.Country + '/' + this.Language;

            this.ShoppingBagType = options.isEcom ? 'onlineshoppingcart' : 'onlineshoppinglist';
            this.Accept = '';
            this.headers = '';
            this.StatusCodes = {
                200: 'OK',
                403: 'Forbidden',
                401: 'Not Authorized'
            };
            this.timeoutRequest = 5000;
            /*
        var ajaxSetupSettings = {
            'timeout': timeoutRequest,
    
            'beforeSend': function (xhr, settings) {
    
            },
            'error': function (xhr, textstatus, message) {
                console && console.log(this, arguments );
                if (textstatus === 'timeout') {
                    console && console.log('timeout');
                } else {
                    if (xhr.status === '404') {
    
                    } else if (xhr.status === '403') {
    
                    } else if (xhr.status === '501') {
    
                    } else {
                        console && console.log(xhr.status, arguments);
                    }
                }
            }
        };
        $.ajaxSetup(ajaxSetupSettings);*/


            var _CategoryItem = function (itemID) {
                var category = "art";
                if (itemID.toString().toLowerCase().indexOf('s') > -1 || itemID.charAt(1) == '9'){
                    category = "spr";
                }

                return category;
            };

            var _GET = [];

            function _getUrlVar() {
                var urlVar = window.location.search;
                var arrayVar;
                var valueAndKey = [];
                var resultArray = [];
                arrayVar = (urlVar.substr(1)).split('&');
                if (arrayVar[0] === "")
                    return false;
                for (var i = 0; i < arrayVar.length; i++) {
                    valueAndKey = arrayVar[i].split('=');
                    resultArray[valueAndKey[0]] = valueAndKey[1];
                }
                return resultArray;
            }

            _GET = _getUrlVar();

            var that = this;
           
            return {

                'Authentication': (function () {


                    return {
                        'GetSessionContext': function (_callbackBeforeSend, _callbackSuccess, _callbackError) {
                            var Consumer = 'IRW';
                            var Contract = '26705';
                            var Accept = 'application/vnd.ikea.iows+json;version=1.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };
                            return $.ajax({
                                type: 'GET',

                                headers: headers,
                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/sessioncontext',

                                success: function (data, textStatus, jqXHR) {
                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(data);
                                    }


                                },


                                error: _callbackError || undefined


                            });
                        },
                        'GetNewSessionContext': function (_callbackBeforeSend, _callbackSuccess, _callbackError) {
                            var Consumer = Parameters.Consumer || 'IRW';
                            var Contract = Parameters.Contract || '26705';
                            var Accept = 'application/vnd.ikea.api+json;version=1.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };
                            return $.ajax({
                                type: 'GET',

                                headers: headers,
                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/sessioncontext?includeEmail=true',

                                success: function (data, textStatus, jqXHR) {
                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(data);
                                    }


                                },


                                error: _callbackError || undefined


                            });
                        }
                    }

                }()),
                'StockAvailability': (function () {


                    return {


                        'isAvailability': function (itemID, storeID, _callbackBeforeSend, _callbackSuccess) {
                            var Consumer = that.Consumer;
                            var Contract = that.Contract;
                            var Accept = 'application/vnd.ikea.iows+json;version=1.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };

                            var StatusCodes = {
                                200: 'OK',
                                204: 'An empty or void/null entity is returned in the response.',
                                404: 'The server has not found anything matching the Request-URI. No indication is given of whether the condition is temporary or permanent. The 410 (Gone) status code SHOULD be used if the server knows, through some internally configurable mechanism, that an old resource is permanently unavailable and has no forwarding address. that status code is commonly used when the server does not wish to reveal exactly why the request has been refused, or when no other response is applicable.',
                                500: 'The server encountered an unexpected condition which prevented it from fulfilling the request.',
                                503: 'The server is currently unable to handle the request due to a temporary overloading or maintenance of the server. The implication is that that is a temporary condition which will be alleviated after some delay. If known, the length of the delay MAY be indicated in a Retry-After header. If no Retry-After is given, the client SHOULD handle the response as it would for a 500 response.',
                                504: 'The server, while acting as a gateway or proxy, did not receive a timely response from the upstream server specified by the URI (e.g. HTTP, FTP, LDAP) or some other auxiliary server (e.g. DNS) it needed to access in attempting to complete the request.',
                                403: 'Forbidden bro'
                            };


                            var category;
                            category = _CategoryItem(itemID);
                            if (category === 'spr') {
                                itemID = itemID.substring(1);
                            }
                            return $.ajax({
                                type: 'GET',

                                headers: headers,
                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/stores/' + storeID + '/availability/' + category + ',' + itemID,
                                /*   statusCode: {
                                200: function(data, textStatus, jqXHR) {
                                    console && console.log('callbackTest');
                                    var howItemWeHave = data.StockAvailability.RetailItemAvailability.InStockProbabilityCode.$;
    
                                    if (typeof(_callback) === 'function') {
                                        _callback(howItemWeHave);
                                    }
                                    },
                                403: function(data, textStatus, jqXHR) {
                                    console && console.log(StatusCodes['403']);
                                }
                            },*/
                                success: function (data, textStatus, jqXHR) {

                                    var howItemWeHave = data.StockAvailability.RetailItemAvailability.InStockProbabilityCode.$;

                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(howItemWeHave);
                                    }
                                },


                                complete: function (xhr, textStatus) {

                                    /*console && console.log('complete', StatusCodes[xhr.status]);*/

                                }


                            });


                        }


                    }


                })(),
                'ShoppingBag': (function () {
                    var Consumer = that.Consumer;
                    var Contract = that.Contract;
                    var Accept = 'application/vnd.ikea.iows+json;version=2.0';
                    var headers = {
                        'Accept': Accept,
                        'Consumer': Consumer,
                        'Contract': Contract
                    };

                    return {
                        'GetShoppingBags': function (userID, _callbackBeforeSend, _callbackSuccess, _callbackError) {

                            return $.ajax({
                                type: 'GET',
                                headers: headers,

                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/' + userID + '/shoppingbags/' + that.ShoppingBagType,

                                success: function (data) {
                                    // получаем корзину

                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(data);
                                    }


                                },
                                error: _callbackError || undefined
                            });


                        },
                        'GetShoppingBag': function (userID, listID, _callbackBeforeSend, _callbackSuccess, _callbackError) {
                            return $.ajax({
                                type: 'GET',
                                headers: headers,

                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/' + userID + '/shoppingbags/' + that.ShoppingBagType + '/' + listID,

                                success: function (data) {
                                    // получаем корзину
                                    var items = [];
                                    var listItems = data.ShoppingBag.ShoppingBagSectionList.ShoppingBagSection.ShoppingBagItemList.ShoppingBagItem;

                                    function toArray(obj) {
                                        return [].slice.call(obj)
                                    }

                                    if (typeof (listItems) !== 'undefined') {
                                        if (listItems instanceof Array !== true) {

                                            listItems = [listItems];

                                        }

                                        listItems.forEach(function (item, i, arr) {

                                            if (item['ItemType'].$ === 'SPR') {
                                                item['ItemNo'].$ = 'S' + item['ItemNo'].$
                                            }
                                            ;items.push({
                                                key: item['ItemNo'].$,
                                                count: item['ItemQty'].$
                                            })
                                        })
                                    } else {
                                        items = 0;
                                    }

                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(data);
                                    }


                                },

                            });


                        },
                        'GetActiveShoppingBagID': function (userID, _callbackSuccess, _callbackError) {
                            var activeListID = 0;
                            activeListID = _GET['listId'];
                            if (
                                typeof(activeListID) !== 'undefined' && $.isNumeric(activeListID)
                            ) {
                                _callbackSuccess(+activeListID);
                                return +activeListID
                            } else {
                                that.GetShoppingBags(userID, undefined, function (data) {
                                    var lists = data;
                                    try {
                                        activeListID = lists.ShoppingBagRefList.ShoppingBagRef.BagId.$;
                                    } catch (e) {
                                        try {
                                            activeListID = lists.ShoppingBagRefList.ShoppingBagRef[0].BagId.$;
                                        } catch (ee) {
                                            try {
                                                if ($.isNumeric(js_fn_CURRENT_LISTID)) {

                                                    activeListID = js_fn_CURRENT_LISTID;
                                                } else {
                                                    throw "Cannot find active listId"
                                                }
                                            }
                                            catch (eee) {
                                                //console && console.log(eee);
                                                console && console.log('clearCache and update page', lists);
                                                if (typeof(_callbackError) === 'function') {
                                                    _callbackError(ee);
                                                }
                                            }

                                        }

                                    }
                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(activeListID, data);
                                    }
                                }, _callbackError)

                            }
                        },
                        'PutShoppingBagItems': function (userID, listID, items, _callbackBeforeSend, _callbackSuccess, _callbackError) {

//items for example [     {      "itemType":"ART","itemNo":"80376216","itemQty":"1"   }];


                            var Consumer = that.Consumer;
                            var Contract = that.Contract;
                            var Accept = 'application/vnd.ikea.iows+json;version=2.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };


                            return $.ajax({
                                type: 'PUT',


                                headers: headers,

                                xhrFields: {
                                    withCredentials: true
                                },
                                data: JSON.stringify(items),
                                timeout: 10000,
                                contentType: 'application/json',
                                dataType: 'text',
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/' + userID + '/shoppingbags/' + that.ShoppingBagType + '/' + listID + '/items',

                                success: function (data) {
                                    // получаем корзину
                                    try {
                                        if (data !== "") 	
                                            data = JSON.parse(data);
                                        if (typeof(_callbackSuccess) === 'function' && typeof (data.ErrorList) === 'undefined') {
                                            return _callbackSuccess(data);
                                        }
                                        if (typeof (data.ErrorList) !== 'undefined' && (typeof(_callbackError) === 'function')) {
                                            return _callbackError(data);
                                        }
                                    } catch (e) {
                                        if (typeof(_callbackError) === 'function') {
                                            return _callbackError(data);
                                        }
                                    }


                                },
                                error: _callbackError || undefined
                            })
                        },
                        'DeleteShoppingBagItems': function (userID, listID, items, _callbackBeforeSend, _callbackSuccess, _callbackError) {

//items for example [     {      "itemType":"ART","itemNo":"80376216","itemQty":"1"   }];


                            var Consumer = that.Consumer;
                            var Contract = that.Contract;
                            var Accept = 'application/vnd.ikea.iows+json;version=2.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };


                            return $.ajax({
                                type: 'DELETE',


                                headers: headers,

                                xhrFields: {
                                    withCredentials: true
                                },
                                data: JSON.stringify(items),
                                contentType: 'application/json',
                                timeout: 10000,
                                dataType: 'json',
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/' + userID + '/shoppingbags/' + that.ShoppingBagType + '/' + listID + '/items',

                                success: function (data) {
                                    // получаем корзину
                                    try {
										if (data !== "") 
                                        	data = JSON.parse(data);
                                        if (typeof(_callbackSuccess) === 'function' && typeof (data.ErrorList) === 'undefined') {
                                            return _callbackSuccess(data);
                                        }
                                        if (typeof (data.ErrorList) !== 'undefined' && (typeof(_callbackError) === 'function')) {
                                            return _callbackError(data);
                                        }
                                    } catch (e) {
                                        if (typeof(_callbackError) === 'function') {
                                            return _callbackError(data);
                                        }
                                    }


                                },
                                error: _callbackError || undefined
                            })


                        },
                        'PostShoppingBag': function (userID,nameList, items, _callbackBeforeSend, _callbackSuccess, _callbackError) {
                            //nameList can be CART
                            /*
                            Items is an array 
                            [
                                {
                                    "ItemType":{ $: "SPR" },
                                    "ItemNo":{ $: "39216754" },
                                    "ItemQty":{ $: "1" }
                                }
                            ]
                            */

                            var Consumer = that.Consumer;
                            var Contract = that.Contract;
                            var Accept = 'application/vnd.ikea.iows+json;version=2.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };
                            var dataSend = {
                                "bagType": "ONLINESHOPPINGLIST",
                                "bagSource": "IRW",
                                "bagName": nameList,
                                "customerSource": "IRW",
                                "customerId": userID,
                                "shoppingBagSectionList": [
                                    {
                                        "shoppingBagItemList": items
                                    }
                                ]
                            };
                            /*
                            "shoppingBagSectionList": [
                                {
                                    "shoppingBagSection": {
                                        "shoppingBagItemList": {
                                            "shoppingBagItem": items
                                        }
                                    }
                                }
                            ]
                            */

                            return $.ajax({
                                type: 'POST',


                                headers: headers,

                                xhrFields: {
                                    withCredentials: true
                                },
                                data: JSON.stringify(dataSend),
                                timeout: 10000,
                                contentType: 'application/json',
                                dataType: 'text',
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/' + userID + '/shoppingbags/' + that.ShoppingBagType,

                                success: function (data) {
                                    // получаем корзину
                                    try {
																				if (data !== "") 
																					data = JSON.parse(data);
                                        if (typeof(_callbackSuccess) === 'function' && typeof (data.ErrorList) === 'undefined') {
                                            return _callbackSuccess(data);
                                        }
                                        if (typeof (data.ErrorList) !== 'undefined' && (typeof(_callbackError) === 'function')) {
                                            return _callbackError(data);
                                        }
                                    } catch (e) {
                                        if (typeof(_callbackError) === 'function') {
                                            return _callbackError(data);
                                        }
                                    }
                                },
                                error: _callbackError || undefined
                            })
                        },
                        'AddToCart_old': function (item, _callbackBeforeSend, _callbackSuccess, _callbackError) {
                            var itemID = item['itemNo'];
                            var itemQ = item['itemQty'];
                            return $.ajax({
                                type: "POST",

                                url: 'https://www.ikea.com/webapp/wcs/stores/servlet/IrwInterestItemAddByPartNumber?partNumber=' + itemID + '&quantity=' + itemQ + '&storeId=' + '23' + '&langId=' + '-31',
                                success: function (data) {
                                    if (typeof(_callbackSuccess) === 'function') {
                                        return _callbackSuccess(data);
                                    }
                                },
                                error: _callbackError || undefined
                            });
                        }
                    }

                })(),
                'RetailItemCommunication': (function () {


                    var StatusCodes = {
                        200: 'An entity corresponding to the requested resource is sent in the response.',
                        301: 'If sending in a global part number where the Retail Unit uses a market specific part number the response will be a redirect to the market specific RetailItemComm URL. The client should follow the URL stated by the location header in the response.',
                        404: 'The requested items does not exist or is not published',
                        403: 'Forbidden bro'
                    };
                    var _TypeItemPrice = function (data) {

                        var RetailItemCommPriceList = data.RetailItemComm.RetailItemCommPriceList;


                        var type = '';
                        // is it BTI ?
                        if (data.RetailItemComm.BreathTakingItem.$ === true) {
                            return 'BTI';
                        }

                        //if is RetailItemCommPrice not array
                        if (RetailItemCommPriceList.RetailItemCommPrice.hasOwnProperty('RetailPriceType')) {
                            return "Normal";
                        } else {
                            // console && console.log(RetailItemCommPriceList);
                            var RetailPriceType = RetailItemCommPriceList.RetailItemCommPrice[0].RetailPriceType.$;

                            if (RetailPriceType === "RegularSalesUnitPrice") {


                                if (RetailItemCommPriceList.RetailItemCommPrice[1].RetailPriceType.$ === 'IKEAFamilySalesUnitPrice') {
                                    return 'Family';
                                }
                                if (RetailItemCommPriceList.RetailItemCommPrice[0].ReasonCode.$ === 'NewLowerPrice') {
                                    return 'NLP';
                                }
                                return 'Sale';
                            } else if (RetailPriceType === "NewLowerPrice") {
                                return 'NLP';
                            }


                        }

                    };

                    function createShortDataItem(data) {

                        try {
                            var info = data.RetailItemComm;

                            return {
                                'itemID': (typeof (info.ItemType.$) === 'undefined') ? '' : info.ItemType.$,
                                'name': info.ProductName.$,
                                'whatIsIt': info.ProductTypeName.$,
                                'design': (info.hasOwnProperty('ValidDesignText') === false) ? '' : info.ValidDesignText.$,
                                'price': info.RetailItemCommPriceList.RetailItemCommPrice,
                                'typePrice': _TypeItemPrice(data)
                            };


                        } catch (e) {
                            return false;
                        }

                    }

                    return {
                        'getInfoItem': function (itemID, _callbackBeforeSend, _callbackSuccess, _callbackError) {
                            var Consumer = that.Consumer;
                            var Contract = that.Contract;
                            var Accept = 'application/vnd.ikea.iows+json;version=2.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };

                            var itemID_array = itemID.split(',');
                            var itemID_list = "";

                            for (var i=0; i<itemID_array.length; i++){
                                if (itemID_list !== "") itemID_list += ";";
                                var itemID = itemID_array[i].replace(/[^sS0-9]/g, '');
                                var category = _CategoryItem(itemID);
                                itemID = itemID_array[i].replace(/[^0-9]/g, '');
                                itemID_list += category + "," + itemID;
                            }


                            return $.ajax({
                                type: 'GET',

                                headers: headers,
                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/catalog/items/' + itemID_list + '?IgnoreErrors=true',

                                success: function (data, textStatus, jqXHR) {


                                    var infoItem = {};
                                    /* var shortDatashortData = createShortDataItem(data);
                                */
                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(data);
                                    }


                                },
                                error: _callbackError || undefined,


                                complete: function (xhr, textStatus) {

                                    /*console && console.log('complete', StatusCodes[xhr.status]);*/

                                }


                            });

                        },
                        'getWeightItem': function (itemID, count, _callbackSuccess, _callbackError) {
                            var Consumer = that.Consumer;
                            var Contract = that.Contract;
                            var Accept = 'application/vnd.ikea.iows+json;version=2.0';
                            var headers = {
                                'Accept': Accept,
                                'Consumer': Consumer,
                                'Contract': Contract
                            };
                            var category;

                            category = _CategoryItem(itemID);

                            var weight = 0;

                            return that.getInfoItem(itemID, undefined,
                                function (data) {

                                    var object;
                                    if (category === 'art') {
                                        object = data.RetailItemComm.RetailItemCommPackageMeasureList.RetailItemCommPackageMeasure;
                                        object.forEach(function (item, i, arr) {

                                            if (item['PackageMeasureType'].$ === 'WEIGHT') {

                                                weight += parseFloat(item['PackageMeasureTextMetric'].$) * count;
                                            }
                                        })

                                    }
                                    else {
                                        object = data.RetailItemComm.RetailItemCommChildList.RetailItemCommChild;
                                        if (typeof (object) !== 'undefined') {
                                            if (object instanceof Array !== true) {

                                                object = [object];

                                            }
                                        }
                                        object.forEach(function (item, i, arr) {
                                            var arrWeight = item['RetailItemCommPackageMeasureList']['RetailItemCommPackageMeasure'];
                                            var quantity = item['Quantity'].$;

                                            arrWeight.forEach(function (item, i, arr) {

                                                if (item['PackageMeasureType'].$ === 'WEIGHT') {

                                                    weight += (parseFloat(item['PackageMeasureTextMetric'].$) * quantity) * count;
                                                }
                                            })

                                        })
                                    }

                                    _callbackSuccess(weight);

                                }, _callbackError)

                        }

                    }
                })(),
                'Customer': (function () {
                    var Consumer = that.Consumer;
                    var Contract = that.Contract;
                    var Accept = 'application/vnd.ikea.iows+json;version=2.0';
                    var headers = {
                        'Accept': Accept,
                        'Consumer': Consumer,
                        'Contract': Contract
                    };

                    return {
                        'GetCustomerProfile': function (userID, _callbackBeforeSend, _callbackSuccess, _callbackError) {

                            return $.ajax({
                                type: 'GET',
                                headers: headers,

                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/' + userID + '/profile',

                                success: function (data) {
                                    // получаем корзину

                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(data);
                                    }


                                },
                                error: _callbackError || undefined
                            });
                        },
                        'LogOutCustomer': function (_callbackBeforeSend, _callbackSuccess, _callbackError) {

                            return $.ajax({
                                type: 'DELETE',
                                headers: headers,

                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: that.timeoutRequest,
                                beforeSend: function (xhr, settings) {

                                    if (typeof(_callbackBeforeSend) === 'function') {
                                        _callbackBeforeSend(xhr, settings);
                                    }

                                },
                                url: 'https://iows.ikea.com/retail/iows/' + that.LocationShop + '/customer/irw/sessioncontext',

                                success: function (data) {
                                    // получаем корзину

                                    if (typeof(_callbackSuccess) === 'function') {
                                        _callbackSuccess(data);
                                    }


                                },
                                error: _callbackError || undefined
                            });
                        }
                    }
                })()
            }
        };
        //window.API = API;
    })(jq);

	}());

    var API = new API(Parameters);


	//*******************************************************************
	
	var transfer = {
        name: "API",
		objects: [
			{name: "API", fn: API}
		],
		dependencies: [],
		bootup: function(){
            
			Samuraj.API.Common = {
                Helper: {
                    
                    getPrices: function(input){
                        /*
                        EXAMPLE

                        Samuraj.API.Common.Helper.getPrices({
                            product: <RetailItemCommunication object>, 
                            success: function(data){
                                //data.response contains regular_price, reduced_price and/or family_price  

                            },
                            failure: function(data){

                            }
                        });
                        */
                        var c = Samuraj.Common;

                        if (input.product) {
                            
                            input.product.response = {};
                            
                            c.getProp({
                                object: input.product,
                                prop: 'RetailItemCommPriceList.RetailItemCommPrice',
                                success: function(pricedata) {
                                    pricedata = c.toArray(pricedata);
                                    for (var i=0; i<pricedata.length; i++){
            
                                        /*If the regular price has no reason code it is a regular price*/
                                        if (pricedata[i].RetailPriceType.$ == "RegularSalesUnitPrice" && !pricedata[i].ReasonCode)
                                            input.product.response.regular_price = pricedata[i].Price.$;
                                        
                                        /*If the regular price has a reason code it is a reduced price, test with: 80017389*/
                                        if (pricedata[i].RetailPriceType.$ == "RegularSalesUnitPrice" && pricedata[i].ReasonCode)
                                            input.product.response.reduced_price = pricedata[i].Price.$;
                                        
                                        /*Family price has another price type*/
                                        if (pricedata[i].RetailPriceType.$ == "IKEAFamilySalesUnitPrice")
                                            input.product.response.family_price = pricedata[i].Price.$;
                                        
                                    }

                                    if (c.isFunction(input.success))
                                        input.success(input.product);
                                },
                                failure: function() {
                                    /*there are no prices, for some wierd reason*/
                                    if (c.isFunction(input.failure))
                                        input.failure(input.product);
                                }
                            });
                
                        } else {
                            if (c.isFunction(input.failure))
                                input.failure(input.product);
                        }
                    }
                },
                LogOutCustomer: function(input){
                    /*
					EXAMPLE

					Samuraj.API.Common.LogOutCustomer({
						loading: function(){

						},
						success: function(){

						},
						failure: function(){

						}
					});
                    */
                    
                    var c = Samuraj.Common;
                    Samuraj.API.Customer.LogOutCustomer(
                        function beforeSend(jqXHR, settings) {},
                        function success(data) {
                            Samuraj.API.Authentication.GetNewSessionContext(
                                function beforeSend(jqXHR, settings) {},
                                function success(data) {
                                    if (c.isFunction(input.success))
                                        input.success(data);
                                },
                                function error( jqXHR, textStatus, errorThrown) {
                                    if (c.isFunction(input.failure))
                                        input.failure();
                                }
                            );
                        },
                        function error( jqXHR, textStatus, errorThrown) {
                            Samuraj.API.Authentication.GetNewSessionContext(
                                function beforeSend(jqXHR, settings) {},
                                function success(data) {
                                    if (c.isFunction(input.success))
                                        input.success(data);
                                },
                                function error( jqXHR, textStatus, errorThrown) {
                                    if (c.isFunction(input.failure))
                                        input.failure();
                                }
                            );
                        }
                    );
                    
                },
                GetCustomer: function(input){
                    /*
					EXAMPLE

					Samuraj.API.Common.GetCustomer({
						loading: function(){

						},
						success: function(){

						},
						failure: function(){

						}
					});
                    */
                    var c = Samuraj.Common;
                    Samuraj.API.Authentication.GetSessionContext(
						function beforeSend(){
							if (c.isFunction(input.loading))
								input.loading();
						}, 
						function success(data) {
							var userID = data.SessionContext.UserId.$;
							Samuraj.API.Customer.GetCustomerProfile(userID,
                                function beforeSend(jqXHR, settings) {},
                                function success(data) {
                                    if (c.isFunction(input.success))
                                        input.success(data);
                                },
                                function error( jqXHR, textStatus, errorThrown) {
                                    if (c.isFunction(input.failure))
                                        input.failure();
                                }
                            );
					    }, function error(jqXHR, textStatus, errorThrown) {
						    if (c.isFunction(input.exception))
							    input.exception(jqXHR, textStatus, errorThrown);
                        }   
                    );
                },
				GetItemInfo: function(input){
                    /*
					EXAMPLE

					Samuraj.API.Common.GetItemInfo({
						product: 's29011507',
						loading: function(){

						},
						success: function(){

						},
						failure: function(){

						}
					});
                    */
                   var c = Samuraj.Common;
                   Samuraj.API.RetailItemCommunication.getInfoItem(input.product,
                        function beforeSend(jqXHR, settings) {
                            if (c.isFunction(input.loading))
                                input.loading();
                        },
                        function success(data) {
                            if (c.isFunction(input.success))
                                input.success(data);
                        },
                        function error( jqXHR, textStatus, errorThrown) {
                            if (c.isFunction(input.failure))
                                input.failure(jqXHR, textStatus, errorThrown);
                    });

                },
				AddToCart: function(input){
					/*
					EXAMPLE

					Samuraj.API.Common.AddToCart({
						products: '80103344:1,s29011507:2',
						loading: function(){

						},
						success: function(){

						},
						failure: function(){

						},
						exception: function(){

						}
					});
					*/
					var c = Samuraj.Common;
					Samuraj.API.Authentication.GetSessionContext(
						function beforeSend(){
							if (c.isFunction(input.loading))
								input.loading();
						}, 
						function success(data) {
                            var userID = data.SessionContext.UserId.$;
                            
                            //the api server times out at 5 seconds, cancel this after 8 seconds
                            var canceled = false;
                            setTimeout(function(){
                                if (!canceled){
                                    canceled = true;
                                    if (c.isFunction(input.failure))
                                        input.failure();
                                }
                            }, 8000);

							Samuraj.API.ShoppingBag.GetShoppingBags(userID,
                                function beforeSend(jqXHR, settings) {},
                                function success(data) {
                                    if (!canceled){
                                        canceled = true;

                                        //convert input format to api format
                                        var payload = [];
                                        var pad = function(nr, size) {
                                            var s = String(nr);
                                            while (s.length < (size)) {s = "0" + s;}
                                            return s;
                                        }
                                        if (input.products){
                                            input.products = input.products.replace(/[^sS0-9,:]/g, "");;
                                            var products = input.products.split(',');
                                            for (var i=0; i<products.length; i++){
                                                var parts = products[i].split(':');
                                                if (parts[0] && parts[0].length > 3){
                                                    var itemNo = pad(parts[0], 8);
                                                    var itemQty = parts[1] || "1";
                                                    var itemType = "ART";
                                                    if (itemNo.toLowerCase().indexOf('s') > -1 || itemNo.charAt(1) == '9')
                                                        itemType = "SPR"
                                                    payload.push({"itemType": itemType,"itemNo": itemNo.replace(/[^0-9]/g, ""),"itemQty": itemQty});
                                                }
                                            } 
                                        }
                                    
                                        Samuraj.Common.getProp({
                                            object: data,
                                            prop: "ShoppingBagRefList.ShoppingBagRef.BagId.$",
                                            success: function(listID) {
                                                //add items to cart
                                                Samuraj.API.ShoppingBag.PutShoppingBagItems(userID, listID, payload,
                                                    function beforeSend(jqXHR, settings) {
                                                    },
                                                    function success(data) {
                                                        if (c.isFunction(input.success))
                                                            input.success(data);
                                                    },
                                                    function error(Errors) {
                                                        if (c.isFunction(input.failure))
                                                            input.failure(Errors);
                                                    }
                                                );
                                            },
                                            failure: function(Errors) {
                                                //no listID for the cart was found, create cart
                                                Samuraj.API.ShoppingBag.PostShoppingBag(userID, "CART", payload,
                                                    function beforeSend(jqXHR, settings) {},
                                                    function success(data) {
                                                        if (c.isFunction(input.success))
                                                            input.success(data);
                                                    },
                                                    function error(Errors) {
                                                        if (c.isFunction(input.failure))
                                                            input.failure(Errors);
                                                });
                                            }
                                        });
                                    }
                                },
                                function error( jqXHR, textStatus, errorThrown) {
                                    if (!canceled){
                                        canceled = true;
                                        if (c.isFunction(input.exception))
                                            input.exception(jqXHR, textStatus, errorThrown);
                                    }
                            });
                        }, function error(jqXHR, textStatus, errorThrown) {
                            if (c.isFunction(input.exception))
                                input.exception(jqXHR, textStatus, errorThrown);
					});
                },
                AddToCartLarge: function(input){
					/*
					EXAMPLE

					Samuraj.API.Common.AddToCartLarge({
						products: '80103344:1,s29011507:2',
						loading: function(){

						},
						success: function(){

						},
						failure: function(){

						},
						exception: function(){

						}
					});
					*/
					var c = Samuraj.Common;
					Samuraj.API.Authentication.GetSessionContext(
						function beforeSend(){
							if (c.isFunction(input.loading))
								input.loading();
						}, 
						function success(data) {
                            input.userID = data.SessionContext.UserId.$;
                            
                            
                            //the api server times out at 5 seconds, cancel this after 8 seconds
                            var canceled = false;
                            setTimeout(function(){
                                if (!canceled){
                                    canceled = true;
                                    if (c.isFunction(input.failure))
                                        input.failure();
                                }
                            }, 8000);

							Samuraj.API.ShoppingBag.GetShoppingBags(input.userID,
                                function beforeSend(jqXHR, settings) {},
                                function success(data) {
                                    if (!canceled){
                                        canceled = true;
                                        
                                        //convert input format to api format
                                        input.payload = [];
                                        var pad = function(nr, size) {
                                            var s = String(nr);
                                            while (s.length < (size)) {s = "0" + s;}
                                            return s;
                                        }
                                        if (input.products){
                                            input.products = input.products.replace(/[^sS0-9,:]/g, "");
                                            var products = input.products.split(',');
                                            for (var i=0; i<products.length; i++){
                                                var parts = products[i].split(':');
                                                if (parts[0] && parts[0].length > 3){
                                                    var itemNo = pad(parts[0], 8);
                                                    var itemQty = parts[1] || "1";
                                                    var itemType = "ART";
                                                    if (itemNo.toLowerCase().indexOf('s') > -1 || itemNo.charAt(1) == '9')
                                                        itemType = "SPR"
                                                    input.payload.push({"itemType": itemType,"itemNo": itemNo.replace(/[^0-9]/g, ""),"itemQty": itemQty});
                                                }
                                            } 
                                        }
                                        
                                        //Define the failed Array to be returned
                                        input.failed = [];

                                        var AddChunkToCart = function(input){
                                        
                                            //Divide product list into chucks of 50 products
                                            var payload_chunk = input.payload.splice(0, 50);

                                            //Create new cart
                                            var AddToNewCart = function(){
                                                Samuraj.API.ShoppingBag.PostShoppingBag(input.userID, "CART", payload_chunk,
                                                    function beforeSend(jqXHR, settings) {},
                                                    function success(data) {
                                                        data.failed = input.failed;
                                                        Samuraj.Common.getProp({
                                                            object: data,
                                                            prop: "ShoppingBag.BagId.$",
                                                            success: function(listID) {
                                                                input.listID = listID;
                                                                if (input.payload.length > 0){
                                                                    AddChunkToCart(input);
                                                                } else {
                                                                    if (c.isFunction(input.success))
                                                                        input.success(input);
                                                                }                                                                        
                                                            },
                                                            failure: function(){
                                                                if (c.isFunction(input.exception)){
                                                                    input.exception(input);
                                                                }
                                                            }
                                                        });
                                                    },
                                                    function error(data) {

                                                        Samuraj.Common.getProp({
                                                            object: data,
                                                            prop: "ErrorList.Error",
                                                            success: function(error) {

                                                                //removing all failed items from the list, collect them in errorprod array
                                                                error = Samuraj.Common.toArray(error);
                                                                for (var i=0; i<error.length; i++){
                                                                    if (error[i].ErrorAttributeList && error[i].ErrorAttributeList.ErrorAttribute){
                                                                        var errorattribute = error[i].ErrorAttributeList.ErrorAttribute;
                                                                        for (var j=0; j<errorattribute.length; j++){
                                                                            if (errorattribute[j].Value){
                                                                                for (var k=payload_chunk.length-1; k>-1; k--){
                                                                                    if (payload_chunk[k].itemNo.toLowerCase().indexOf(errorattribute[j].Value.$.toString().toLowerCase()) > -1){
                                                                                        //Remove from current chunk array
                                                                                        payload_chunk.splice(k, 1);
                                                                                        //Add error to failed array
                                                                                        input.failed.push(error[i]);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        });

                                                        //Add to cart again after removing the items that failed
                                                        Samuraj.API.ShoppingBag.PostShoppingBag(input.userID, "CART", payload_chunk,
                                                            function beforeSend(jqXHR, settings) {
                                                            },
                                                            function success(data) {
                                                                Samuraj.Common.getProp({
                                                                    object: data,
                                                                    prop: "ShoppingBag.BagId.$",
                                                                    success: function(listID) {
                                                                        input.listID = listID;
                                                                        if (input.payload.length > 0){
                                                                            AddChunkToCart(input);
                                                                        } else {
                                                                            if (c.isFunction(input.success))
                                                                                input.success(input);
                                                                        }                                                                        
                                                                    },
                                                                    failure: function(){
                                                                        if (c.isFunction(input.failure)){
                                                                            input.failure(input);
                                                                        }
                                                                    }
                                                                });
                                                                
                                                            },
                                                            function error() {
                                                                // this should not happen anymore
                                                                if (input.payload.length > 0){
                                                                    AddChunkToCart(input);
                                                                } else {
                                                                    if (c.isFunction(input.failure)){
                                                                        input.failure(input);
                                                                    }
                                                                }
                                                            }
                                                        );
                                                    }
                                                );
                                            };

                                            //Add products to existing cart
                                            var AddToExistingCart = function(){
                                                Samuraj.API.ShoppingBag.PutShoppingBagItems(input.userID, input.listID, payload_chunk,
                                                    function beforeSend(jqXHR, settings) {
                                                    },
                                                    function success() {
                                                        if (input.payload.length > 0){
                                                            AddChunkToCart(input);
                                                        } else {
                                                            if (c.isFunction(input.success))
                                                                input.success(input);
                                                        }
                                                    },
                                                    function error(data) {
                                                        
                                                        Samuraj.Common.getProp({
                                                            object: data,
                                                            prop: "ErrorList.Error",
                                                            success: function(error) {

                                                                //removing all failed items from the list, collect them in errorprod array
                                                                error = Samuraj.Common.toArray(error);
                                                                for (var i=0; i<error.length; i++){
                                                                    if (error[i].ErrorAttributeList && error[i].ErrorAttributeList.ErrorAttribute){
                                                                        var errorattribute = error[i].ErrorAttributeList.ErrorAttribute;
                                                                        for (var j=0; j<errorattribute.length; j++){
                                                                            if (errorattribute[j].Value){
                                                                                for (var k=payload_chunk.length-1; k>-1; k--){
                                                                                    if (payload_chunk[k].itemNo.toLowerCase().indexOf(errorattribute[j].Value.$.toString().toLowerCase()) > -1){
                                                                                        //Remove from current chunk array
                                                                                        payload_chunk.splice(k, 1);
                                                                                        //Add error to failed array
                                                                                        input.failed.push(error[i]);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        });
                                                        //Add to cart again after removing the items that failed
                                                        Samuraj.API.ShoppingBag.PutShoppingBagItems(input.userID, input.listID, payload_chunk,
                                                            function beforeSend(jqXHR, settings) {
                                                            },
                                                            function success() {
                                                                if (input.payload.length > 0){
                                                                    AddChunkToCart(input);
                                                                } else {
                                                                    if (c.isFunction(input.success))
                                                                        input.success(input);
                                                                }
                                                            },
                                                            function error(data) {
                                                                // this should not happen anymore
                                                                if (input.payload.length > 0){
                                                                    AddChunkToCart(input);
                                                                } else {
                                                                    if (c.isFunction(input.failure)){
                                                                        input.failure(input);
                                                                    }
                                                                }
                                                            }
                                                        );
                                                    }
                                                );
                                            }

                                            //Check if cart exist
                                            if (!input.followingchunks){
                                                input.followingchunks = true;
                                                Samuraj.Common.getProp({
                                                    object: data,
                                                    prop: "ShoppingBagRefList.ShoppingBagRef.BagId.$",
                                                    success: function(listID) {
                                                        //add items to cart
                                                        input.listID = listID;
                                                        AddToExistingCart();
                                                    },
                                                    failure: function() {
                                                        //no listID for the cart was found, create cart
                                                        AddToNewCart();
                                                    }
                                                });
                                            } else {
                                                //add items to cart
                                                AddToExistingCart();
                                            }
                                        };
                                        AddChunkToCart(input);
                                    }
                                },
                                function error( jqXHR, textStatus, errorThrown) {
                                    if (!canceled){
                                        canceled = true;
                                        if (c.isFunction(input.exception))
                                            input.exception(jqXHR, textStatus, errorThrown);
                                    }
                            });
                        }, function error(jqXHR, textStatus, errorThrown) {
                            if (c.isFunction(input.exception))
                                input.exception(jqXHR, textStatus, errorThrown);
					});
                },
                GetCart: function(input){
                    /*
					EXAMPLE

					Samuraj.API.Common.GetCart({
						loading: function(){

						},
						success: function(){

						},
						failure: function(){

						},
						exception: function(){

						}
					});
					*/
                    var c = Samuraj.Common;
                    Samuraj.API.Authentication.GetSessionContext(
						function beforeSend(){
							if (c.isFunction(input.loading))
								input.loading();
						}, 
						function success(data) {
                            var userID = data.SessionContext.UserId.$;
                            
                            //the api server times out at 5 seconds, cancel this request after 8 seconds
                            var canceled = false;
                            setTimeout(function(){
                                if (!canceled){
                                    canceled = true;
                                    if (c.isFunction(input.failure))
                                        input.failure();
                                }
                            }, 8000);

							Samuraj.API.ShoppingBag.GetShoppingBags(userID,
							    function beforeSend(jqXHR, settings) {},
							    function success(data) {
                                    if (!canceled){
                                        canceled = true;
                                        Samuraj.Common.getProp({
                                            object: data,
                                            prop: "ShoppingBagRefList.ShoppingBagRef.BagId.$",
                                            success: function(listID) {
                                                //listID for the cart was found, fetching the products from the cart
                                                Samuraj.API.ShoppingBag.GetShoppingBag(userID, listID,
                                                    function beforeSend(jqXHR, settings) {
                                                    },
                                                    function success(data) {
                                                        if (c.isFunction(input.success))
                                                            input.success(data);
                                                    },
                                                    function error(Errors) {
                                                        if (c.isFunction(input.failure))
                                                            input.failure(Errors);
                                                    }
                                                );
                                            },
                                            failure: function(Errors) {
                                                //no listID found, so cart is empty
                                                if (c.isFunction(input.failure))
                                                    input.failure(Errors);
                                            }
                                        });
                                    }
                                },
                                function error( jqXHR, textStatus, errorThrown) {
                                    if (!canceled){
                                        canceled = true;
                                        if (c.isFunction(input.exception))
                                            input.exception(jqXHR, textStatus, errorThrown);
                                    }
                            });

					    }, function error(jqXHR, textStatus, errorThrown) {
						    if (c.isFunction(input.exception))
							    input.exception(jqXHR, textStatus, errorThrown);
					});
				},
				DeleteCart: function(input){
                    /*
					EXAMPLE

					Samuraj.API.Common.DeleteCart({
						loading: function(){

						},
						success: function(){

						},
						failure: function(){

						},
						exception: function(){

						}
					});
					*/
                    var c = Samuraj.Common;
                    Samuraj.API.Authentication.GetSessionContext(
						function beforeSend(){
							if (c.isFunction(input.loading))
								input.loading();
						}, 
						function success(data) {
							var userID = data.SessionContext.UserId.$;
							Samuraj.API.ShoppingBag.GetShoppingBags(userID,
							function beforeSend(jqXHR, settings) {},
							function success(data) {
								var listID = data.ShoppingBagRefList.ShoppingBagRef.BagId.$;
								Samuraj.API.ShoppingBag.GetShoppingBag(userID, listID,
									function beforeSend(jqXHR, settings) {
									},
									function success(data) {
										if (c.isFunction(input.success))
											input.success(data);
									},
									function error(Errors) {
										if (c.isFunction(input.failure))
											input.failure(Errors);
									}
								);
							},
							function error( jqXHR, textStatus, errorThrown) {
								if (c.isFunction(input.exception))
									input.exception(jqXHR, textStatus, errorThrown);
						});
					}, function error(jqXHR, textStatus, errorThrown) {
						if (c.isFunction(input.exception))
							input.exception(jqXHR, textStatus, errorThrown);
					});
				}
			};

            Samuraj.Log.add({
                cat: "API",
                sub: "Timing",
                text: "Loading done",
                timing: performance.now() - SamurajTiming				
            });
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();
