"use strict";
    window.PostNordCommon = {
        /// Replace all instances of a substring from the string.
        //Function to create elements with type classlist id and innerHTML
        CreateElement: function CreateElement(type, classList, id, innnerHTML, child) {
            var element = document.createElement(type);
            element.innerHTML = innnerHTML;
            element.id = id;
            if (classList != "") classList.split(" ").forEach(function (className) {
                element.classList.add(className);
            });
            if (child != null) element.appendChild(child);
            return element;
        },
        ReplaceAll: function ReplaceAll(string, cut, paste) {
            while (string.indexOf(cut)!== -1) {
                string = string.replace(cut, paste);
            }
            return string;
        },
        ReplaceAllInInnerHtml: function ReplaceAllInInnerHtml(obj, cut, paste) {
            obj.innerHTML = PostNordCommon.ReplaceAll(obj.innerHTML, cut, paste);
        },
        CreateButton: function CreateButton(value, functionToExecute) {
            var button = PostNordCommon.CreateElement('input', '', '', 'postNordZipCodeInput', null);
            button.type = "button";
            button.onclick = functionToExecute;
            button.value = value;
            button.classList.add("ui-btn-inner");
            button.classList.add("ui-btn-corner-all");
            button.classList.add("ui-btn");
            button.classList.add("ui-btn-up-blueBtn");
            button.classList.add("ui-shadow");
            button.classList.add("ui-submit");

            var postNordButtonCaption = PostNordCommon.CreateElement('div', 'buttonCaption', '', '', button);
            return PostNordCommon.CreateElement('div', 'buttonContainer', '', '', postNordButtonCaption);
        },
        HideElement: function HideElement(id){
            document.getElementById(id).style.display = "none";
        },
    };
    
    ///Verify if DeliveryAddress form is visible or not.
    var postNordUrl = "https://t9s52o31a6.execute-api.eu-central-1.amazonaws.com/Prod/";
    var postNordFirstLoadOfMarkers = true;
    var postNordServicePoints = [];
    var postNordServicePointSelected;
    var postNordZipCode;
    var postNordCustomerType;
    var postNordOpeningHoursContainerCopy;
    var postNordServicePointListElement;
    /// Service Level. Function to be called from the Controller Level. This Level contains buisenss logic.
    /// Function to get and insert servicepoints into the list where the user will choice from.
    //window.Extension.
    window.PostNordCallBack = {
        /// The CallBackFunction when servicepoints shell remain on the map.
        CallBackClear: function CallBackClear(data) {
            return PostNord.ReadData(data);
        },
    };
    window.PostNord = {
        /// ServiceLevel
        /// All functions in here are functions called from within the javascript
        ///Function fetching serviepoints by postalcode.
        GetServicePointsByPostalCode: function GetServicePointsByPostalCode(postalCode) {
            if (postalCode > 9999 || postalCode < 1000) return;
            postNordZipCode = postalCode;
            PostNord.GetServicePoints(postNordUrl + "getbypostalcode?postalCode=" + postalCode + "&callback=PostNordCallBack.CallBackClear");
        },

        ///Function calling AWS 
        GetServicePoints: function GetServicePoints(url) {
            if (document.getElementById("PostNordAPIResponse")) document.getElementById("PostNordAPIResponse").parentElement.removeChild(document.getElementById("PostNordAPIResponse"));
            var script = PostNordCommon.CreateElement("script", "", "PostNordAPIResponse", "", null);
            script.src = url;
            document.body.appendChild(script);
        },
        ///Choice servicepoint by adding opening hours to the map and highlight on the list.
        ChoiceServicePoint: function ChoiceServicePoint(id, scroll) {
            if (postNordServicePointSelected == null || postNordServicePointSelected.servicePointId != id) PostNord.LoadOpeningHours(PostNord.GetServicePoint(id));
            postNordServicePointSelected = PostNord.GetServicePoint(id);
            PostNord.ChangeClassOnSelect(id, scroll);
        },
        ///On closing infowindow we unhighlight the servicepoint
        UnChoiceServicePoint: function UnChoiceServicePoint() {
            console.log("UnChoiceServicePoint");
            postNordServicePointSelected = null;
            PostNord.ChangeClassOnSelect(null);
        },
        ///Copy billingaddress and servicepoint information into deliveryaddress
        CopyBillingAddressToDeliveryAddress: function CopyBillingAddressToDeliveryAddress() {
            if (document.getElementById('SelectPickUpPointError')) {
                var element = document.getElementById("SelectPickUpPointError");
                element.parentNode.removeChild(element);
            }
            if (postNordServicePointSelected != null) {
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address1").value = postNordServicePointSelected.name.substring(0, 30);;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address2").value = postNordServicePointSelected.visitingAddress.streetName + " " + postNordServicePointSelected.visitingAddress.streetNumber;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value = postNordServicePointSelected.visitingAddress.postalCode;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_city").value = postNordServicePointSelected.visitingAddress.city;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_firstName").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_firstName").value;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_lastName").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_lastName").value;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_phone1").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_phone1").value;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_phone2").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_phone2").value;
                if (document.getElementById("signup_checkout_business_organizationName")) 
                    document.getElementById("signup_checkout_business_ship_organizationName").value = document.getElementById("signup_checkout_business_organizationName").value;
                return;
            }
            document.getElementById("PostNordElement").appendChild(PostNordCommon.CreateElement("div", "formError formErrorGoogleMaps", "SelectPickUpPointError", "Du mangler at vælge et posthus", null));
        },
        ///Changing the classes of selected servicepoint and unselected servicepoint
        ChangeClassOnSelect: function ChangeClassOnSelect(id, scroll) {
            Array.from(document.getElementsByClassName("PostNordRadio")).forEach(function (item) {
                return item.checked = false;
            });
            Array.from(document.getElementsByClassName("PostNord-ServicePoint")).forEach(function (item) {
                return item.classList.remove("PostNord-SelectedServicePoint");
            });
            if (id != null) {
                document.getElementById("PostNordServicePointRadio-" + id).checked = true;
                document.getElementById("PostNordServicePoint-" + id).classList.add("PostNord-SelectedServicePoint");
                if (scroll) document.getElementById('postNordServicePointListElement1').scrollTop = document.getElementById("PostNordServicePoint-" + id).offsetTop - document.getElementById('postNordServicePointListElement1').offsetTop;
            }
        },
        ///Converting the day from PostNord to readable text.
        ConvertDayInitToReadable: function ConvertDayInitToReadable(init, short) {
            switch (init) {
                case "MO":
                    return short ? "Man" : "Mandag";
                case "TU":
                    return short ? "Tir" : "Tirsdag";
                case "WE":
                    return short ? "Ons" : "Onsdag";
                case "TH":
                    return short ? "Tor" : "Torsdag";
                case "FR":
                    return short ? "Fre" : "Fredag";
                case "SA":
                    return short ? "Lør" : "Lørdag";
                case "SU":
                    return short ? "Søn" : "Søndag";
            }
        },
        /// Insert OpeningHours
        InsertOpeningHours: function InsertOpeningHours(startDay, endDay, startTime, endTime) {
            postNordOpeningHoursContainerCopy.appendChild(postNordOpeningHoursScope.cloneNode(true));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[StartDay]", startDay);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[EndDay]", endDay);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[StartTime]", startTime);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[EndTime]", endTime);
        },
        /// Insert servicepoints for the customer to choice.
        InsertServicePoint: function InsertServicePoint(servicePoint) {
            postNordOpeningHoursContainerCopy = postNordOpeningHoursContainer.cloneNode(true)
            var startDay;
            var endDay;
            var startTime;
            var endTime;
            servicePoint["openingHours"].forEach(function (openingDay) {
                var init = openingDay.day;
                if (startTime != PostNord.FormatClock(openingDay.from1) || endTime != PostNord.FormatClock(openingDay.to1)) {
                    if (startDay != undefined) {
                        PostNord.InsertOpeningHours(startDay, endDay, startTime, endTime);
                    }
                    startDay = PostNord.ConvertDayInitToReadable(init, true);
                    startTime = PostNord.FormatClock(openingDay.from1);
                    endTime = PostNord.FormatClock(openingDay.to1);
                };
                endDay = PostNord.ConvertDayInitToReadable(init, true);
            });
            PostNord.InsertOpeningHours(startDay, endDay, startTime, endTime);
            var postNordServicePointCopy = postNordServicePoint.cloneNode(true);;
            postNordServicePointCopy.setAttribute("onclick", "PostNord.ChoiceServicePoint(" + servicePoint.servicePointId + ",false)");
            postNordServicePointCopy.appendChild(postNordOpeningHoursContainerCopy);
            postNordServicePointListElement.appendChild(postNordServicePointCopy);

            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[servicePointId]", servicePoint.servicePointId);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[name]", servicePoint.name);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[streetName]", servicePoint.visitingAddress.streetName);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[streetNumber]", servicePoint.visitingAddress.streetNumber);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[postalCode]", servicePoint.visitingAddress.postalCode);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[city]", servicePoint.visitingAddress.city);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[countryCode]", servicePoint.visitingAddress.countryCode);
        },
        ///Load opening hours of servicepoint
        LoadOpeningHours: function LoadOpeningHours(servicePoint) {
            postNordOpeningHoursElement.innerHTML = "<strong>" + servicePoint.name + "</strong>" + "<div>" + servicePoint.visitingAddress.streetName + " " + servicePoint.visitingAddress.streetNumber + "</div>" + "<div>" + servicePoint.visitingAddress.postalCode + " " + servicePoint.visitingAddress.city + "</div>";
            postNordOpeningHoursElement.appendChild(postNordOpeningHoursTitleElement);

            PostNord.LoadOpeningHoursIndividualDay("MO", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("TU", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("WE", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("TH", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("FR", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("SA", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("SU", servicePoint);
            var postNordOpeningHoursElementDiv = document.createElement("div", "", "", "");
            postNordOpeningHoursElementDiv.appendChild(postNordOpeningHoursElement);
        },
        ///Load individual 
        LoadOpeningHoursIndividualDay: function LoadOpeningHoursIndividualDay(dayInit, servicePoint) {
            postNordOpeningHoursElement.appendChild(postNordOpeningHoursTemplate.cloneNode(true));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[Day]", PostNord.ConvertDayInitToReadable(dayInit, false));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[DayInit]", dayInit);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[Open]", PostNord.GetOpeningTime(dayInit, servicePoint));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[Close]", PostNord.GetClosingTime(dayInit, servicePoint));
        },
        /// get openingtime of a day
        GetOpeningTime: function GetOpeningTime(dayInit, servicePoint) {
            return servicePoint["openingHours"].some(function (o) {
                return o.day == dayInit;
            }) ? PostNord.FormatClock(servicePoint["openingHours"].find(function (o) {
                return o.day == dayInit;
            }).from1) : "";
        },
        /// get closingtime of a day
        GetClosingTime: function GetClosingTime(dayInit, servicePoint) {
            return servicePoint["openingHours"].some(function (o) {
                return o.day == dayInit;
            }) ? PostNord.FormatClock(servicePoint["openingHours"].find(function (o) {
                return o.day == dayInit;
            }).to1) : "";
        },
        /// Formating clock from "HHMM" to "HH:MM"
        FormatClock: function FormatClock(input) {
            return input.substring(0, input.length - 2) + ":" + input.slice(-2);
        },
        /// get the servicepoint previusly loaded and stored in the variable "servicePoints".
        GetServicePoint: function GetServicePoint(id) {
            return postNordServicePoints.find(function (x) {
                return x.servicePointId === String(id);
            });
        },
        /// Read the data from response
        ReadData: function ReadData(data) {
            postNordServicePointListElement.scrollTop = 0;
            var servicePointCounter = 1;
            document.getElementById("postNordServicePointListElement").innerHTML="";
            data.servicePointInformationResponse.servicePoints
                .filter(function (s) {
                    return (s.routeDistance < 30000 || s.visitingAddress.postalCode == postNordZipCode) && !s.name.includes("Pakkeboks");
                })
                .forEach(function (servicePoint) {
                    if (postNordZipCode == servicePoint.visitingAddress.postalCode || servicePointCounter < 10) 
                        PostNord.AddServicePoint(servicePoint);
                    servicePointCounter++;
                });
            setTimeout(function () {
                if (postNordFirstLoadOfMarkers == true) {
                    if (document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address1").value != "") {
                        Array.from(document.getElementsByClassName("PostNord-ServicePoint")).forEach(function (element) {
                            if (element.innerHTML.indexOf(document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address1").value)!== -1) {
                                element.click();
                            }
                        });
                    }
                    if (document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value != "")
                        document.getElementById("postNordZipCodeInput").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value;
                }
                if (postNordFirstLoadOfMarkers == true)
                    postNordFirstLoadOfMarkers = false;
                if (postNordServicePointSelected != null) 
                    PostNord.ChoiceServicePoint(postNordServicePointSelected.servicePointId, true);
            }, postNordFirstLoadOfMarkers ? 2000 : 0);
        },
        ///Add servicepoint to list
        AddServicePoint: function AddServicePoint(servicePoint) {
            PostNord.InsertServicePoint(servicePoint);
            postNordServicePoints.push(servicePoint);
        },
        ///Remove servicepoint from list
        RunPostNord: function RunPostNord() {
            if(!document.getElementById("signup_checkout_private_ship_firstName"))
            {
                var button = PostNordCommon.CreateButton("Vælg posthus", function () {
                    document.getElementById("accordionLink").firstElementChild.click();
                    return false;
                });
                button.style.marginTop = "20px";

                
                var userProfile = document.getElementById("accordionLink");
                userProfile.parentNode.insertBefore(button, userProfile.nextSibling);
                PostNordCommon.HideElement("privacyPolicyWrapper");
                PostNordCommon.HideElement("accordionLink");
                document.getElementById("errorMessageSubmit").nextElementSibling.style.display="none";
            }
            else
            {

                PostNordCommon.HideElement("accordionLink");
                PostNordCommon.HideElement("accordionContent");
                postNordServicePointListElement = PostNordCommon.CreateElement('div', 'PostNord-ServicePointContainer1', 'postNordServicePointListElement', '', null);

                //Creating the button to make the activate loading servicepoints
                var postNordButtonCaption = PostNordCommon.CreateElement('div', 'PostNord-width-50', '', '<div style="margin-right:5px;"><input type="number" id="postNordZipCodeInput" style="height: 49px;" onkeyup="PostNord.GetServicePointsByPostalCode(this.value);return false;" placeholder="Skriv postnummer" class="PostNord-PostalCode PostNord-width-100"/></div>', null);
                var postNordButtonContainer = PostNordCommon.CreateElement('div', 'PostNord-width-100', '', 'Postnummer:<br>', postNordButtonCaption);
                var switchPostalCode = PostNordCommon.CreateButton("Skift postnummer", function () {
                    PostNord.GetServicePointsByPostalCode(document.getElementById('postNordZipCodeInput').value);return false;
                })
                switchPostalCode.classList.add("PostNord-width-50");
                switchPostalCode.firstElementChild.style.marginLeft = "5px;"
                switchPostalCode.firstElementChild.firstElementChild.classList.add("SwitchPostalCode");
                postNordButtonContainer.appendChild(switchPostalCode);
                var PostNordModule = PostNordCommon.CreateElement('div', 'PostNord-width-100 PostNord-Module', 'PostNordModule', '<h2><span class="welcomeHeading">Vælg posthus</span></h2>', postNordButtonContainer);

                var test = document.getElementById("signup_checkout_private");
                test.parentNode.insertBefore(PostNordModule, test.nextSibling);    
                if (document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_firstName").value == "") 
                    document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_firstName").value = ".";

                if (document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value != "") 
                    document.getElementById("postNordZipCodeInput").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value;
                else 
                    document.getElementById("postNordZipCodeInput").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_zipCode").value;

                PostNord.GetServicePointsByPostalCode(document.getElementById("postNordZipCodeInput").value);
                document.getElementsByName("submitButton_IrwAddressDetails")[0].onclick = function () {
                    PostNord.CopyBillingAddressToDeliveryAddress();
                };

                PostNordModule.appendChild(postNordServicePointListElement);

                var copyAcceptPersonData = document.getElementById("privacyPolicyWrapper").cloneNode(true);
                if (document.getElementById("termsAndConditions_id").checked == true) 
                    copyAcceptPersonData.click();
                copyAcceptPersonData.id = "copyAcceptPersonData";
                copyAcceptPersonData.innerHTML = copyAcceptPersonData.innerHTML.replace("termsAndConditions_id", "copyTermsAndConditions_id");
                copyAcceptPersonData.onclick = function () {
                    document.getElementById("termsAndConditions_id").click();
                };
                PostNordCommon.HideElement("privacyPolicyWrapper");

                PostNordModule.appendChild(copyAcceptPersonData);
                document.getElementById("termsAndConditions_id").onclick = function () {
                    setTimeout(function () {
                        if (document.getElementById("termsAndConditions_id").checked == true) document.getElementById("copyTermsAndConditions_id").checked = true;else document.getElementById("copyTermsAndConditions_id").checked = false;
                    }, 10);
                };
                var button = PostNordCommon.CreateButton("Fortsæt til leveringsoplysninger", function () {
                    document.getElementById("errorMessageSubmit").nextElementSibling.firstElementChild.lastElementChild.click();
                });

                document.getElementById("errorMessageSubmit").nextElementSibling.style.display = "none";
                button.style.marginTop = "20px";
                PostNordModule.appendChild(button);

                PostNordModule.scrollIntoView(true);
                window.scrollBy(0, -30);
            }
        },
        VerifyDeliveryPrice: function VerifyDeliveryPrice() {
            if (!document.URL.indexOf("mCheckoutWelcomeView") && !document.URL.indexOf("mCheckoutAddressView")) 
                return false;
            if (!document.getElementById("privateCustomer") || document.getElementById("privateCustomer").checked) 
                postNordCustomerType = "private";
            else 
                postNordCustomerType = "business";
            var signup_checkout_private_savedaddress = document.getElementById("signup_checkout_"+ postNordCustomerType +"_savedaddress");
            if(signup_checkout_private_savedaddress && 0==signup_checkout_private_savedaddress.selectedIndex &&  signup_checkout_private_savedaddress.length >9)
                window.location = signup_checkout_private_savedaddress.lastElementChild.value;

            var zipcode = document.getElementById("signup_checkout_"+ postNordCustomerType +"_zipCode").value;
            if (zipcode == "")
                zipcode = "2632";
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "POST", "https://secure.ikea.com/webapp/wcs/stores/servlet/IrwWSCfbDeliveryDetail?zipCode=" + zipcode + "&state=null&storeId=14&langId=-12&priceexclvat=", false );
            xmlHttp.send( null );
            var jsonResponse = JSON.parse(xmlHttp.responseText);
            if(jsonResponse[1][0]["unFormatedPreShippingCost"] == "49.0")
            {
                PostNord.RunPostNord();
            } 
            return false;
        },
    };
    var postNordServicePoint = PostNordCommon.CreateElement('div', 'PostNord-ServicePoint PostNord-ClickAble PostNordRadio-container', 'PostNordServicePoint-[servicePointId]', '<input type="radio" id="PostNordServicePointRadio-[servicePointId]" class="PostNordRadio"/>' + '<div class="PostNord-width-90 PostNord-margin-left-10">' + ' <div><strong>[name]</strong></div>' + ' <div>[streetName] [streetNumber]</div>' + ' <div>[postalCode] [city], [countryCode]</div>' + '</div>', null);
    var postNordOpeningHoursContainer = PostNordCommon.CreateElement('div', 'PostNord-width-90 PostNord-margin-left-10 postNordOpeningHoursContainer', '', '', null);
    var postNordOpeningHoursScope = PostNordCommon.CreateElement('div', '', '', '[StartDay] - [EndDay]: [StartTime]-[EndTime]', null);
    var postNordOpeningHoursTemplate = PostNordCommon.CreateElement('div', '', '', '<div class="PostNord-Address">[Day]: </div>' + ' <div class="PostNord-Address">[Open]</div> - ' + ' <div class="PostNord-Address">[Close]</div></br>', null);

    var postNordOpeningHoursElement = PostNordCommon.CreateElement('div', 'PostNord-OpeningHoursInfoBox', 'postNordOpeningHoursElement', "", null);
    var postNordOpeningHoursTitleElement = PostNordCommon.CreateElement('div', '', '', '<strong>Åbningstider:</strong>', null);

    if(PostNord.VerifyDeliveryPrice()){
        var test = document.getElementById("userProfileField1Billing_field");
        test.parentNode.insertBefore(postNordOpeningHoursElement, test.nextSibling);    
    }
