function setM2Navigation(){
    //prevents that the header is loaded more than once
    setTimeout(function(){
                if(!document.querySelector('.ui-page-active #m2-navigation')) {
                    var URL = document.location.href.toLowerCase();
                   // console.log("Test")

                    document.querySelector("#ikea-homeheader").style.display = "none"; 

                    //create header
                    var header = document.createElement("header");
                    header.id ="m2-navigation";
                    header.setAttribute("class","header");
                    header.setAttribute("role","banner");
                    header.setAttribute("async","");
                    header.innerHTML =  "<ul class='top-menu'>" +
                                         "<li class='top-menu__item '>" +
                                            "<a href='https://m2.ikea.com/dk/da/ikea-family/' class='top-menu__link'>" +
                                                "<span class='top-menu__title header__h6'>IKEA FAMILY</span>" +
                                            "</a>" +
                                        "</li>" +
                                        "<li class='top-menu__item '>" +
                                            "<a href='https://m2.ikea.com/dk/da/customer-service/' class='top-menu__link'>" +
                                                "<span class='top-menu__title header__h6'>Kundeservice</span> " +
                                            "</a>" +
                                        "</li>" +
                                        "<li class='top-menu__item '>" +
                                            "<a href='https://m2.ikea.com/dk/da/stores/' class='top-menu__link'>"+
                                                 "<span class='top-menu__title header__h6'>Varehuse</span>" +
                                            "</a>" + 
                                        "</li>" +
                                        "<li class='top-menu__item top-menu__item--right'>" +
                                            "<a href='https://securem.ikea.com/dk/da/login/' class='top-menu__link'> <span class='top-menu__title header__h6'>Log ind</span> </a>" +
                                        "</li>" +
                                    "</ul>" +
                                    "<a href='http://m2.ikea.com/dk/da/' class='header__logo'> <img class='svg-icon' src='https://m2.ikea.com/dk/da/header-footer/static/ikea-logo.44494cf681bac0c153bd8c0a517421de.svg' alt='IKEA' title='IKEA'>" +
                                        "<span class='header__sr-only'>IKEA</span>" +
                                    "</a>" + 
                                    "<nav class='header__navigation' role='navigation'>" +
                                        "<ul class='desktop-menu'>" +
                                            "<li class='desktop-menu__item'>" +  
                                                "<a href='https://m2.ikea.com/dk/da/cat/functional-functional/' class='desktop-menu__link'> <span class='desktop-menu__title'>Produkter</span> </a>" +
                                            "</li>" +
                                            "<li class='desktop-menu__item'>" + 
                                                "<a href='https://m2.ikea.com/dk/da/ideas/' class='desktop-menu__link'> <span class='desktop-menu__title'>Inspiration</span></a>" + 
                                            "</li>" +
                                            "<li class='desktop-menu__item'> " +
                                                "<a href='https://m2.ikea.com/dk/da/news/' class='desktop-menu__link'> <span class='desktop-menu__title'>Nyt i IKEA</span> </a>" +
                                            "</li>" +
                                        "</ul>" + 
                                        "<ul class='header__icon-list header__icon-count-3'>" +
                                            "<li class='header__icon-list-item'>" +
                                                "<a href='https://m2.ikea.com/dk/da/search/'>" +
                                                    "<img src='https://m2.ikea.com/dk/da/header-footer/static/nav-search.be9b20e02c34370711bd3b3cc86702f9.svg' alt='Søg' title='Søg'>" +
                                                 "</a>" + 
                                            "</li>" +
                                            "<li class='header__icon-list-item'>" +
                                                "<a href='https://m2.ikea.com/dk/da/shop/wishlist/'>" + 
                                                    "<img src='https://m2.ikea.com/dk/da/header-footer/static/nav-shopping-list.cc02cd650d8c817b80edd65fb79f57e5.svg' alt='Huskeliste' title='Huskeliste'>" + 
                                                "</a>" + 
                                            "</li>" +
                                            "<li class='header__icon-list-item'>" +
                                                "<a class='header__shopping-cart js-shopping-cart-icon' href='http://www.ikea.com/dk/da/mcommerce/shoppingcart' data-market-code='dk-DA'>" +
                                                    "<img src='https://m2.ikea.com/dk/da/header-footer/static/nav-shopping-bag.6dda3f5f848587d5858a987a298ee635.svg' alt='Indkøbskurv' title='Indkøbskurv'>" +
                                                "</a>" + 
                                            "</li>" +
                                            "<li class='header__icon-list-item header__icon-list-item--menu'>" +
                                                "<a href='https://m2.ikea.com/dk/da/menu/' class=''> " +
                                                    "<img src='https://m2.ikea.com/dk/da/header-footer/static/nav-menu.2abb45a57649541b487c058051104b29.svg' alt='Menu' title='Menu'>" +
                                                "</a>" + 
                                            "</li>" +
                                        "</ul>" + 
                                    "</nav>";

                    //append the header
                    var body = document.querySelector('.ui-page-active');
                    body.insertBefore(header, body.firstChild);


                    //initilize shopping-cart counter
                    var value_counter = document.createElement("span");
                    value_counter.className += " header__shopping-bag-quantity";

                    var shopping_cart = document.getElementsByClassName("js-shopping-cart-icon")[0];
                    shopping_cart.appendChild(value_counter);
                    var number_of_items_in_cart = 0;

                    if(utag["data"]["cp.IRMW_CART_COUNT_DE"] != "undefined"){
                        number_of_items_in_cart = parseInt(utag["data"]["cp.IRMW_CART_COUNT_DE"]);
                        if(number_of_items_in_cart > 0){
                            value_counter.innerHTML = number_of_items_in_cart ;
                        }
                        else{
                            value_counter.style.display = "none";
                            number_of_items_in_cart = 0;      
                        }
                    }

                    //Calculate the cart-counter on a pip
                    if(URL.indexOf("products") != -1){

                        var addToCartButton = document.getElementById("addToCartButton");
                        addToCartButton.addEventListener("click",function(){
                            var dropdown_number_of_products = document.getElementById("quantity");
                            var number_of_products = parseInt(dropdown_number_of_products.options[dropdown_number_of_products.selectedIndex].value);
                            number_of_items_in_cart =number_of_items_in_cart + number_of_products; 

                            value_counter.innerHTML = number_of_items_in_cart;
                            value_counter.style.display = "block";
                        });
                    }

                //'Your basket' got also the m2 css class header - remove this backgroundcolor
                var your_basket = document.getElementsByClassName("header")[1];
                your_basket.style.backgroundColor = "rgb(241,241,241)";
        }

}, 50);

}
$(window).on('navigate', function(){
    setM2Navigation();
});

setM2Navigation();