(function(){
	var LIB = {
		Common: {
			toInt: function(str) {
				try {
					if (this.isInt(str)){
						try {
							str = parseFloat(str);
							return str;
						} catch (err){
							return str;
						}
					} else if (this.isFloat(str)){
							try {
								str = Math.round(str);
								return str;
							} catch (err){
								return str;
							}
						} else if (str.slice(0, 1) === '-') {
								str = str.split(',')[0];
								var t = parseInt(str.replace(/\D/g, ''));
								if (isNaN(t)) t = 0;
								return t - (t * 2);
							} else {
								str = str.split(',')[0];
								var t = parseInt(str.replace(/\D/g, ''));
								if (isNaN(t)) t = 0;
								return t;
							}
				} catch (err) {
					return -1;
				}
			},
			isInt: function(value) {
				if (isNaN(value)) {
					return false;
				}
				var x = parseFloat(value);
				var y = (x | 0) === x;
				return y;
			},
			isFloat: function(n) {
				return Number(n) === n && n % 1 !== 0;
			},
			isArray: function(obj) {
				return Object.prototype.toString.call(obj) === '[object Array]';
			},
			isNodeList: function(obj) {
				return Object.prototype.toString.call(obj) === '[object NodeList]';
			},
			isObject: function(obj) {
				return Object.prototype.toString.call(obj) === '[object Object]';
			},
			isFunction: function(obj) {
				return Object.prototype.toString.call(obj) === '[object Function]';
			},
		}, //Common
		Element: {
			get: function(input){
				if (!LIB.Common.isObject(input)) { input = {}; }
				input.baseelem = input.baseelem || document;
				if (input.selector) {
					var timeout = input.timeout || 5000;
					var interval = input.interval || 20;

					var elem = input.baseelem.querySelectorAll(input.selector);
					if (input.elementnumber){
						input.elementnumber = LIB.Common.toInt(input.elementnumber);
						if (elem.length >= LIB.Common.toInt(input.elementnumber)){
							elem = [elem[input.elementnumber]];
						} else elem = [];
					}
					if (elem.length > 0) {
						if (LIB.Common.isFunction(input.success)) { input.success(elem, input.context); }
					} else {
						window.setTimeout(() => {
							input.timeout = timeout - interval;
							if (input.timeout > 0) { LIB.Element.get(input); } else if (LIB.Common.isFunction(input.failure)) { input.failure(elem, input.context); }
						}, interval);
					}
				} else if (LIB.Common.isFunction(input.failure)) { input.failure(elem, input.context); }
			},
		}, //Element
	}; //LIB 

    var products = '20400144,60394179,60387070,80394178,00393776,10390678,30396189,80393777,40394793,60394792,10397694,10397731,10400842,30397725,30397730,40397701,60412036,50404485,10394167,10400149,20397453,30394171,40405800,50397545,80387074,90397614,20384988,40384954,40393779,50394165,60394136,70384962,20393780,60393783,90384536';
    var prod = products.split(',');
    for (var i = 0; i < prod.length; i++){
        if (document.URL.indexOf('/catalog/products/art/') > -1 && document.URL.indexOf(prod[i]) > -1){
            //This function will check for the existence of element with selector #salesArg every 20 millisecond for 5000 milliseconds. (5 seconds)
            //When the element is found the function success is triggered, if not the function failure is triggered (after timeout, not used here)
            LIB.Element.get({
				selector: '.summary-benifit', //Required
				baseelem: document, //Optional - Default = document
				timeout: 5000,			//Optional - Default = 5000
				interval: 20, //Optional - Default = 20
				success: function(salesArg){
					salesArg = salesArg[0];
					var newText = '<b>SÆRKOLLEKTION – BEGRÆNSET ANTAL</b><br>OMEDELBAR er en særkollektion, der kun sælges i varehusene i en kortere periode. <br>Kollektionen sælges fra 9. marts 2018';
					var content = document.createElement('div.pipCollections');
					var parentDiv = document.body.querySelector('.summary-benifit').parentNode;
					var sp2 = document.body.querySelector('.summary-benifit');
					content.innerHTML = newText;
					content.setAttribute('style', 'display: block; float: left; margin: 20px 0; padding: 20px; background-color: #484849; font-family: Verdana, Geneva, Tahoma, sans-serif; font-size: 13px; color: white; line-height: 19px;');
					parentDiv.insertBefore(content, sp2);
				},
            });
        }
    }
})();
