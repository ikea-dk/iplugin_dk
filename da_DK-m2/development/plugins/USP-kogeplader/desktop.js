'use strict';

(function () {
  var path = "css";
  var style = document.createElement('link');
  style.rel = 'stylesheet';
  style.type = 'text/css';
  style.href = 'https://www.ikea.com/ms/da_DK/XCLm55MBAS/USP-PLP-M2.css';
  document.getElementsByTagName('head')[0].appendChild(style);

  if (document.URL.indexOf("/cat/") > -1) {
    // Load the JSON file 
    var loadkogeplader = function loadkogeplader() {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          var kogeplader = JSON.parse(this.responseText);

          var allProducts = document.getElementsByClassName("product-compact");
          for (var i = 0; i < allProducts.length; i++) {

            var product = allProducts[i];
            for (var x = 0; x < kogeplader.length; x++) {

              //get productID in div
              var link = product.children[0].children[0].href;
              var substrings = link.split("-");
              var productID = substrings[substrings.length - 1].replace("/", "");

              //get productID in JSON
              var productID_json = kogeplader[x].Article.toString();

              if (productID === productID_json) {
                console.log("Product matches from div with JSON");
                console.log("ID from DIV:" + productID);
                console.log("ID from JSON:" + productID_json);

                var container = document.createElement("mark");

                container.innerHTML = '\n                     <ul.a>\n                          <li class = "usp">Type<span class="text-yes"> ' + kogeplader[x].Art + ' </span></li>\n                          <li class = "usp"> Antal kogezoner <span class ="text-yes"> ' + kogeplader[x].Kogezoner + ' </span></li>\n                          <li class = "usp"> ' + (kogeplader[x].Boosterfunktion ? 'Boosterfunktion<span class = "icon-yes"' + kogeplader[x].Boosterfunktion + '</span>' : '<div class = "text-no">Boosterfunktion</div><span class = "icon-no" ' + kogeplader[x].Boosterfunktion) + '</span></li>\n                          <li class = "usp"> ' + (kogeplader[x].Brofunktion ? 'Brofunktion<span class = "icon-yes"' + kogeplader[x].Brofunktion + '</span>' : '<div class = "text-no">Brofunktion</div><span class = "icon-no" ' + kogeplader[x].Brofunktion) + '</span></li>\n                          <li class = "usp"> ' + (kogeplader[x].Fzone ? 'Fleksibel zone<span class = "icon-yes"' + kogeplader[x].Fzone + '</span>' : '<div class = "text-no">Fleksibel zone</div><span class = "icon-no" ' + kogeplader[x].Fzone) + '</span></li>\n                          <li class = "usp"> Bredde <span class ="text-yes"> ' + kogeplader[x].Bredde + ' </span></li>\n                          <li class = "usp"> ' + (kogeplader[x].Garanti ? 'Garanti<span class = "text-yes">' + kogeplader[x].Garanti + '</span>' : '<div class = "text-no">Garanti</div> ' + kogeplader[x].Garanti) + '</li>\n                      </ul>';
                var _product = document.body.querySelector('.product-compact__type').parentNode;
                var sp2 = document.body.querySelector('.product-compact__type');
                _product.replaceChild(container, sp2);
              }
            }
          }
        }
      };
      //Get the JSON file
      xhttp.open('GET', 'https://m2.ikea.com/dk/da/data-sources/ee2efdf0c57611e88e36f73a205cf3a9.json', true);
      xhttp.send();
    };

    loadkogeplader();
  };
})();