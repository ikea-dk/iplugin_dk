/*

  _                     _          
 |_ ._ _  o  _  |_ _|_ /   _. |  _ 
 |  | (/_ | (_| | | |_ \_ (_| | (_ 
             _|                    

*/

(function(){

	var Settings = {
		country: "dk",
		cfbEnabled: true,								/* if iSOM is on, set this to true */
		calculatebutton: false,							/* show a calculation button or auto calculate when field input is correct */ 	
		zipcodedigits: 4,								/* how many digit is the zipcode */ 	
		forcezipcodeentry: true,	 					/* continue to checkout button disabled until zipcode is entered and calculated */ 	
		deliveryrestrictions: false,  					/* turns on and off check of delivery restrictions */  
		checkpickuppoints: false,
		express: true,									/* turns on and off express delivery */  
		clickcollect: true,			 					/* turns on and off click & collect */  
		pickuppoints: false, 		 					/* turns on and off pickuppoints */  
		parcelcheck: false,			 					/* only relevant if pickuppoints are on - DOES NOT WORK PERFECTLY */ 
		parcelcheckzipcode: 1396,    					/* only relevant if pickuppoints are on - DOES NOT WORK PERFECTLY */                         	
		parcelcheckwords: ["pakkeboks", "postkontor", "Parcel", "Paketlieferung","Colissimo"],  /* word that exist in the text key response from IRW when getting freight calculaiton, used to determine parcel or not */  
		expressontop: true,
		expressdefault: true,
		klarna:{
			country: "dk",
			submitUrl: 'https://ww8.ikea.com/checkout/pages/payment-no.php',
			submitUrlIRW: 'https://ww8.ikea.com/checkout/pages/paymentirw-no.php',
			//submitUrl: 'https://www.prougno.com/ikea/klarna/payment-no.php',
			//submitUrlIRW: 'https://www.prougno.com/ikea/klarna/paymentirw-no.php',
			defaults: {
				m2: {
					hs: false,
					ex: false,
					pp: false,
					cc: false
				},
				irw: {
					hs: false,
					ex: false,
					pp: false,
					cc: false
				}
			},
			switches:{}
		}
	}



	/*

	 ___  _   _  __
	  |  |_) |_ (_
	 _|_ |_) |_ __)


	*/

	var IBES = {
		cnc_configuration_url: "",
		exclude_prefix: "DO_NOT_USE_",

		// counts the list of collections points excluding "DO_NOT_USE_" prefix
		getFetchLocationsLength: function(fetchLocations){
			var count = Object.keys(fetchLocations).length;
			if(count > 0){
				var storeName;
				for (var prop in this.fetchLocations) {
					storeName = (this.isClosingTimeWindow) ? this.fetchLocations[prop].name : this.fetchLocations[prop];
					if(this.exclude_prefix != "" && storeName.indexOf(this.exclude_prefix) == 0) {
						count--;
					}
				}
			}
			return count;
		},

		// Fetches the list of collection points from the microsite
		getFetchLocations: function (input) {
			var version = (this.isClosingTimeWindow) ? '?version=2' : '';
			if (!window.cnc_configuration){
				Samuraj.IncludeScript.load(Samuraj.IBES.cnc_configuration_url, function(){
					Samuraj.IBES.getFetchLocations(input);
				});
			} else {

				var url = cnc_configuration.CFG_LIVE_PAYLOAD_URL + 'listfetchlocations' + version;

				if (Samuraj.Common.isFunction(input.loading))
					input.loading();
				var response = new Samuraj.Common.xhr();
				var DONE = 4;
				var OK = 200;
				response.overrideMimeType("application/x-www-form-urlencoded");
				response.open("get", url, true);
				response.onreadystatechange = function() {
					if (response.readyState === DONE) {
						try{
							if (response.status === OK) {
								response.responseJSON = JSON.parse(response.responseText);
								if(response.responseJSON){
									if (Samuraj.Common.isFunction(input.success))
										input.success(response);
								} else {
									if (Samuraj.Common.isFunction(input.failure))
										input.failure(response);
								}
							} else {
								if (Samuraj.Common.isFunction(input.failure))
									input.failure(response);
							}
						} catch (err) {
							if (Samuraj.Common.isFunction(input.exception))
								input.exception(err);
						}

						if (Samuraj.Common.isFunction(input.complete))
							input.complete(response);
					}
				}
				response.send(null);
			}

		},

		getPositionCCBtn: function(el) {
			var rect = el.getBoundingClientRect(),
				scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
				scrollTop = window.pageYOffset || document.documentElement.scrollTop;
			return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
		},

		//Current loaded page has to be the shoppingcart page
		fetchCart: function(){
			var form = document.querySelector('form#updateAllForm');
			var totalRow = form.querySelector('input#totalRow').getAttribute("value");
			var cart = {};
				cart.orderId = form.querySelector('input[name="orderId"]').getAttribute("value");
				cart.items = [];

			for (var i = 1; i <= totalRow; i++) {
				cart.items.push({
				  'count': form.querySelector('#order_qty_' + i).getAttribute("value"),
				  'articleNo': form.querySelector('#prodId_' + i).getAttribute("value")
				});
			}

			return cart;
		},

		//generates the payload for sending to Click&Collect. Iterates through all items on shoppinglist, counts and returns them with store-id & shoppinglist-id
		getPayload: function (input) {
			var input = input || {};
			input.system = input.system || "M2";
			input.service = input.service || "fetchlocation";
			input.selector = input.selector || "";
			input.selectorIRW = input.selectorIRW || "";
			input.selectorM2 = input.selectorM2 || "";

			var cart = this.fetchCart();
			var servicevalue = "";
			var device = "desktop";
			if (Samuraj.Common.checkurl({masks: "*/PromotionCodeManage?*", includeparams: true}) || Samuraj.Common.checkurl(["*/M2OrderItemDisplay", "*/OrderItemDisplayMobile", "*/mcommerce/shoppingcart"]))
				device = "mobile";

			var selector = "";
			if (device == "desktop"){
				selector = "select.storesList";
				if (Samuraj.Common.varExist(input.selectorIRW, true))
					selector = input.selectorIRW;

			} else {
				selector = "select.ccStoresList";
				if (Samuraj.Common.varExist(input.selectorM2, true))
					selector = input.selectorM2;
			}
			if (Samuraj.Common.varExist(input.selector, true))
				selector = input.selector;
			var elem = document.querySelectorAll(selector);
			for (var i=0; i<elem.length; i++){
				if (elem[i].value != "")
					servicevalue = elem[i].value;
			}
			return {
				selectedService: input.service,
				selectedServiceValue: servicevalue,
				slId: cart.orderId,
				articles: cart.items,
				locale: irwstats_locale,
				customerView: device,
				vis_id: utag.data["ut.visitor_id"],
				system: input.system
			};
		},

		//gerates the hash from payload with cryptojs and given password from Click&Collect and returns. Click&Collect needs the hash for checking the transported payload.
		getHashFromPayload: function (payload) {
			return Samuraj.CryptoJS.HmacSHA1(payload, cnc_configuration.CFG_HASHCODE).toString();
		},

		// returns error-message by given errorcode
		getErrorMessageByErrorCode: function (errorcode) {
			switch (errorcode) {
				case 1477918816:
                return {
                    'short' : 'closingTimeWindow',
                    'text'  : cnc_text.ALL_ERROR_CLOSING_TIME_WINDOW
                };
                break;
			case 1480409707:
                return {
                    'short' : 'noArticleinList',
                    'text'  : cnc_text.ALL_ERROR_NO_ITEMS_IN_SALESLIST
                };
                break;
            case 1470143968:
                return {
                    'short' : 'reachedCapacity',
                    'text'  : cnc_text.ALL_ERROR_REACHED_CAPACITY
                };
                break;
            case 1472636219:
                return {
                    'short' : 'reachedCapacity',
                    'text'  : cnc_text.ALL_ERROR_RESPONSEERROR
                };
                break;
            case 1472475118:
                return {
                    'short' : 'reachedCapacity',
                    'text'  : cnc_text.ALL_ERROR_HIGHER_DEMAND
                };
                break;
			case 1488979749:
                return {
                    'short' : 'onemanDelivery',
                    'text'  : cnc_text.ONE_MAN_BIG_ORDER_SIZE
                };
                break;
            case 100:
                return {
                    'short' : 'noStoreSelected',
                    'text'  : cnc_text.ALL_ERROR_NOSTORESELECTED
                };
                break;
            case 110:
                return {
                    'short' : 'noItemsInShoppinglist',
                    'text'  : cnc_text.ALL_ERROR_NOITEMSINSHOPPINGLIST
                };
                break;
            case 120:
                return {
                    'short' : 'responseError',
                    'text'  : cnc_text.ALL_ERROR_RESPONSEERROR
                };
                break;
            case 130:
                return {
                    'short' : 'responseError',
                    'text'  : cnc_text.ALL_ERROR_RESPONSEERROR
                };
                break;
            case 200:
                return {
                    'short' : 'responseError',
                    'text'  : cnc_text.ALL_ERROR_RESPONSEERROR
                };
                break;
            case 300:
                return {
                    'short' : 'ajaxError',
                    'text'  : cnc_text.ALL_ERROR_AJAXERROR
                };
                break;
            default:
                return {
                    'short' : 'responseError',
                    'text'  : cnc_text.ALL_ERROR_RESPONSEERROR
                };
                break;
			}
		},


		/**
		 * Actual function that validates shoppinglist, collect payload, generates hash and sends all to Click&Collect.
		 * If sending succeed and Click&Collect validates payload and hash correct, we redirect to the given target-url from Click&Collect-Response.
		 * If sending failed we display the error on page.
		 */
		sendPayloadJsonp: function (input) {

			if (!window.cnc_configuration){
				Samuraj.IncludeScript.load(Samuraj.IBES.cnc_configuration_url, function(){
					Samuraj.IBES.sendPayloadJsonp(input);
				});
			} else {
				input = input || {};
				input.service = input.service || "fetchlocation";

				if (Samuraj.Common.isFunction(input.loading))
					input.loading();

				var url = "";
				if (input.service=="express")
					url = cnc_configuration.CFG_LIVE_EXPRESS_DELIVERY_PAYLOAD_URL
				else
					url = cnc_configuration.CFG_LIVE_PAYLOAD_URL;

				if (url){
					// get payload
					var payload = JSON.stringify(Samuraj.IBES.getPayload(input));

					// get hash
					var hash = Samuraj.IBES.getHashFromPayload(payload);

					// send payload
					var params = {
						'payload': payload,
						'backUrl': document.location.href,
						'hmac': hash
					};
					params = JSON.stringify(params);
					params = encodeURIComponent(params);

					//var version = (this.isClosingTimeWindow) ? '?version=2' : '';
					//var version = ""; //"?version=2";
					//var url = cnc_configuration.CFG_LIVE_PAYLOAD_URL; // + 'listfetchlocations' + version;
					
					var active = true;
					var request_too_long = setTimeout(function(){
						active = false;
						var res = {
							code: 300,
							message: "Unresponsive",
							status: "ERROR"
						}
						if (Samuraj.Common.isFunction(input.failure))
							input.failure(res);
						if (Samuraj.Common.isFunction(input.complete))
							input.complete(res);
					}, 15000);

					var response = new Samuraj.Common.xhr();
					var DONE = 4;
					var OK = 200;

					response.open("post", url, true);
					response.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					response.onreadystatechange = function() {
						if (response.readyState === DONE) {
							if (active){
								window.clearTimeout(request_too_long);
								try{
									if (response.status === OK) {
										response.responseJSON = JSON.parse(response.responseText);
										if(response.responseJSON) {
											if (response.responseJSON.status && response.responseJSON.status == 'OK' && response.responseJSON.target && response.responseJSON.target != '') {
												if (Samuraj.Common.isFunction(input.success))
													input.success(response);
											} else {
												if (Samuraj.Common.isFunction(input.failure))
													input.failure(response);
											}
										}
									} else {
										if (Samuraj.Common.isFunction(input.failure))
											input.failure(response);
									}
								} catch (err) {
									if (Samuraj.Common.isFunction(input.exception))
										input.exception(err);
								}

								if (Samuraj.Common.isFunction(input.complete))
									input.complete(response);
							}
						}
					}
					response.send(params);
				} else {
					var errorResponse = {
						responseJSON:{
							status: "ERROR"
						}
					};
					if (Samuraj.Common.isFunction(input.failure))
						input.failure(errorResponse);
					if (Samuraj.Common.isFunction(input.complete))
						input.complete(errorResponse);
				}
			}
		}
	}


	/* CryptoJS v3.1.2 code.google.com/p/crypto-js */
	var CryptoJS = function (g, l) {
		var e = {},
			d = e.lib = {},
			m = function () {},
			k = d.Base = {
				extend: function (a) {
					m.prototype = this;
					var c = new m;
					a && c.mixIn(a);
					c.hasOwnProperty("init") || (c.init = function () {
						c.$super.init.apply(this, arguments)
					});
					c.init.prototype = c;
					c.$super = this;
					return c
				},
				create: function () {
					var a = this.extend();
					a.init.apply(a, arguments);
					return a
				},
				init: function () {},
				mixIn: function (a) {
					for (var c in a) a.hasOwnProperty(c) && (this[c] = a[c]);
					a.hasOwnProperty("toString") && (this.toString = a.toString)
				},
				clone: function () {
					return this.init.prototype.extend(this)
				}
			},
			p = d.WordArray = k.extend({
				init: function (a, c) {
					a = this.words = a || [];
					this.sigBytes = c != l ? c : 4 * a.length
				},
				toString: function (a) {
					return (a || n).stringify(this)
				},
				concat: function (a) {
					var c = this.words,
						q = a.words,
						f = this.sigBytes;
					a = a.sigBytes;
					this.clamp();
					if (f % 4)
						for (var b = 0; b < a; b++) c[f + b >>> 2] |= (q[b >>> 2] >>> 24 - 8 * (b % 4) & 255) << 24 - 8 * ((f + b) % 4);
					else if (65535 < q.length)
						for (b = 0; b < a; b += 4) c[f + b >>> 2] = q[b >>> 2];
					else c.push.apply(c, q);
					this.sigBytes += a;
					return this
				},
				clamp: function () {
					var a = this.words,
						c = this.sigBytes;
					a[c >>> 2] &= 4294967295 <<
						32 - 8 * (c % 4);
					a.length = g.ceil(c / 4)
				},
				clone: function () {
					var a = k.clone.call(this);
					a.words = this.words.slice(0);
					return a
				},
				random: function (a) {
					for (var c = [], b = 0; b < a; b += 4) c.push(4294967296 * g.random() | 0);
					return new p.init(c, a)
				}
			}),
			b = e.enc = {},
			n = b.Hex = {
				stringify: function (a) {
					var c = a.words;
					a = a.sigBytes;
					for (var b = [], f = 0; f < a; f++) {
						var d = c[f >>> 2] >>> 24 - 8 * (f % 4) & 255;
						b.push((d >>> 4).toString(16));
						b.push((d & 15).toString(16))
					}
					return b.join("")
				},
				parse: function (a) {
					for (var c = a.length, b = [], f = 0; f < c; f += 2) b[f >>> 3] |= parseInt(a.substr(f,
						2), 16) << 24 - 4 * (f % 8);
					return new p.init(b, c / 2)
				}
			},
			j = b.Latin1 = {
				stringify: function (a) {
					var c = a.words;
					a = a.sigBytes;
					for (var b = [], f = 0; f < a; f++) b.push(String.fromCharCode(c[f >>> 2] >>> 24 - 8 * (f % 4) & 255));
					return b.join("")
				},
				parse: function (a) {
					for (var c = a.length, b = [], f = 0; f < c; f++) b[f >>> 2] |= (a.charCodeAt(f) & 255) << 24 - 8 * (f % 4);
					return new p.init(b, c)
				}
			},
			h = b.Utf8 = {
				stringify: function (a) {
					try {
						return decodeURIComponent(escape(j.stringify(a)))
					} catch (c) {
						throw Error("Malformed UTF-8 data");
					}
				},
				parse: function (a) {
					return j.parse(unescape(encodeURIComponent(a)))
				}
			},
			r = d.BufferedBlockAlgorithm = k.extend({
				reset: function () {
					this._data = new p.init;
					this._nDataBytes = 0
				},
				_append: function (a) {
					"string" == typeof a && (a = h.parse(a));
					this._data.concat(a);
					this._nDataBytes += a.sigBytes
				},
				_process: function (a) {
					var c = this._data,
						b = c.words,
						f = c.sigBytes,
						d = this.blockSize,
						e = f / (4 * d),
						e = a ? g.ceil(e) : g.max((e | 0) - this._minBufferSize, 0);
					a = e * d;
					f = g.min(4 * a, f);
					if (a) {
						for (var k = 0; k < a; k += d) this._doProcessBlock(b, k);
						k = b.splice(0, a);
						c.sigBytes -= f
					}
					return new p.init(k, f)
				},
				clone: function () {
					var a = k.clone.call(this);
					a._data = this._data.clone();
					return a
				},
				_minBufferSize: 0
			});
		d.Hasher = r.extend({
			cfg: k.extend(),
			init: function (a) {
				this.cfg = this.cfg.extend(a);
				this.reset()
			},
			reset: function () {
				r.reset.call(this);
				this._doReset()
			},
			update: function (a) {
				this._append(a);
				this._process();
				return this
			},
			finalize: function (a) {
				a && this._append(a);
				return this._doFinalize()
			},
			blockSize: 16,
			_createHelper: function (a) {
				return function (b, d) {
					return (new a.init(d)).finalize(b)
				}
			},
			_createHmacHelper: function (a) {
				return function (b, d) {
					return (new s.HMAC.init(a,
						d)).finalize(b)
				}
			}
		});
		var s = e.algo = {};
		return e
	}(Math);

	var CryptoJSInit1 = function () {
		var g = Samuraj.CryptoJS,
			l = g.lib,
			e = l.WordArray,
			d = l.Hasher,
			m = [],
			l = g.algo.SHA1 = d.extend({
				_doReset: function () {
					this._hash = new e.init([1732584193, 4023233417, 2562383102, 271733878, 3285377520])
				},
				_doProcessBlock: function (d, e) {
					for (var b = this._hash.words, n = b[0], j = b[1], h = b[2], g = b[3], l = b[4], a = 0; 80 > a; a++) {
						if (16 > a) m[a] = d[e + a] | 0;
						else {
							var c = m[a - 3] ^ m[a - 8] ^ m[a - 14] ^ m[a - 16];
							m[a] = c << 1 | c >>> 31
						}
						c = (n << 5 | n >>> 27) + l + m[a];
						c = 20 > a ? c + ((j & h | ~j & g) + 1518500249) : 40 > a ? c + ((j ^ h ^ g) + 1859775393) : 60 > a ? c + ((j & h | j & g | h & g) - 1894007588) : c + ((j ^ h ^
							g) - 899497514);
						l = g;
						g = h;
						h = j << 30 | j >>> 2;
						j = n;
						n = c
					}
					b[0] = b[0] + n | 0;
					b[1] = b[1] + j | 0;
					b[2] = b[2] + h | 0;
					b[3] = b[3] + g | 0;
					b[4] = b[4] + l | 0
				},
				_doFinalize: function () {
					var d = this._data,
						e = d.words,
						b = 8 * this._nDataBytes,
						g = 8 * d.sigBytes;
					e[g >>> 5] |= 128 << 24 - g % 32;
					e[(g + 64 >>> 9 << 4) + 14] = Math.floor(b / 4294967296);
					e[(g + 64 >>> 9 << 4) + 15] = b;
					d.sigBytes = 4 * e.length;
					this._process();
					return this._hash
				},
				clone: function () {
					var e = d.clone.call(this);
					e._hash = this._hash.clone();
					return e
				}
			});
		g.SHA1 = d._createHelper(l);
		g.HmacSHA1 = d._createHmacHelper(l)
	}

	var CryptoJSInit2 = function () {
		var g = Samuraj.CryptoJS,
			l = g.enc.Utf8;
		g.algo.HMAC = g.lib.Base.extend({
			init: function (e, d) {
				e = this._hasher = new e.init;
				"string" == typeof d && (d = l.parse(d));
				var g = e.blockSize,
					k = 4 * g;
				d.sigBytes > k && (d = e.finalize(d));
				d.clamp();
				for (var p = this._oKey = d.clone(), b = this._iKey = d.clone(), n = p.words, j = b.words, h = 0; h < g; h++) n[h] ^= 1549556828, j[h] ^= 909522486;
				p.sigBytes = b.sigBytes = k;
				this.reset()
			},
			reset: function () {
				var e = this._hasher;
				e.reset();
				e.update(this._iKey)
			},
			update: function (e) {
				this._hasher.update(e);
				return this
			},
			finalize: function (e) {
				var d =
					this._hasher;
				e = d.finalize(e);
				d.reset();
				return d.finalize(this._oKey.clone().concat(e))
			}
		})
	}
	

	
	/*

	  _                     _          
	 |_ ._ _  o  _  |_ _|_ /   _. |  _ 
	 |  | (/_ | (_| | | |_ \_ (_| | (_ 
				 _|                    

	*/

	var FreightCalc = {
		settings: Settings,
		resultstore: {},
		mobileCheckout: function(){
			var c = Samuraj.Common,
				t = false;
			if (c.checkurl("*/servlet/M2*")) {
				t = true;
			} else if (c.checkurl("*/servlet/*PromotionCodeManage|IrwProceedFromCheckoutAddressView*")) {
				if (c.getStorage("freight_mobileplatform") == "true") t = true;
				var elem = document.querySelector("footer.footer");
				if (elem) t = true;
			} else if (c.checkurl("*/mcommerce/shoppingcart*")) {
				t = true;
			} else if (c.checkurl("*/servlet/*/OrderItemDisplayMobile")){
				t = true;
			}
			return t;
		},
		calcButtonValidate: function(input) {
			var result = false;
			input = input || {};
			input.state = document.getElementById("samuraj-hs-input-lower-field-state");
			input.zipcode = document.getElementById("samuraj-hs-input-lower-field");
			if (input.state && input.zipcode) {
				if (input.state.selectedIndex > 0) {
					if (input.zipcode.value.length == Samuraj.FreightCalc.settings.zipcodedigits)
						result = true;
				}
			} else if (input.zipcode) {
				if (input.zipcode.value.length == Samuraj.FreightCalc.settings.zipcodedigits)
					result = true;
			}
			
			if (result){
				if (Samuraj.FreightCalc.mobileCheckout()){
					//M2
					var elem = document.querySelector("#samuraj-hs-calculate-button");
					if (elem) {
						Samuraj.Common.removeClass(elem, "ui-disabled");
						Samuraj.Common.removeClass(elem, "samuraj-faded");
					}

				} else {
					//IRW
					var elem = document.querySelector("#samuraj-hs-calculate-button");
					if (elem) var inputelem = elem.querySelector("input");
					if (inputelem) {
						inputelem.removeAttribute('disabled');
						inputelem.style.setProperty('cursor', "pointer");
					}
					Samuraj.Common.removeClass(elem,"disabledButton");
				}
			} else {
				if (Samuraj.FreightCalc.mobileCheckout()){
					//M2
					var elem = document.querySelector("#samuraj-hs-calculate-button");
					if (elem) {
						Samuraj.Common.addClass(elem, "ui-disabled");
						Samuraj.Common.addClass(elem, "samuraj-faded");
					}
				} else {
					//IRW
					var elem = document.querySelector("#samuraj-hs-calculate-button");
					if (elem) var inputelem = elem.querySelector("input");
					if (inputelem) {
						inputelem.setAttribute('disabled', 'disabled');
						inputelem.style.setProperty('cursor', "default");
					}
					Samuraj.Common.addClass(elem,"disabledButton");
				}
			}
		},

		homeshippingButtonClick: function(){
			
			Samuraj.FreightCalc.toggleDeliveryType('hs', 'auto');
			var state = "";
			if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12"){
				var elem = document.getElementById("samuraj-hs-input-lower-field-state");
				if (elem) {
					if (elem.selectedIndex > 0)
						state = elem.value + "|";
				}
			}
			var zipcode = document.getElementById("samuraj-hs-input-lower-field").value;
			var value = state + zipcode;
			Samuraj.FreightCalc.calculateShipping({
				zc: value,
				na: '',
				dt: 'hs',
				ex: Samuraj.FreightCalc.cookie.ex
			});
			var elem = document.getElementById("samuraj-hs-calculate-button");
			if (elem) Samuraj.Common.removeClass(elem, "samuraj-show");
		},
		homeShippingInputClick: function(elem){
			Samuraj.FreightCalc.toggleDeliveryType('hs', 'auto');
			if(elem.value.substring(0,10-1) == Samuraj.Translation.data.freight.inputfieldplaceholder.substring(0,10-1))
				elem.value = "";
			var elem = document.getElementById("samuraj-hs-calculate-button");
			if (elem) Samuraj.Common.addClass(elem, "samuraj-show");
			Samuraj.FreightCalc.calcButtonValidate();
		},
		homeShippingInputKeyUp: function(elem){
			var c = Samuraj.Common;
			var valid = false;
			if (Samuraj.FreightCalc.settings.calculatebutton) {
				var elem = document.getElementById("samuraj-hs-calculate-button");
				if (elem) Samuraj.Common.addClass(elem, "samuraj-show");
				Samuraj.FreightCalc.calcButtonValidate();
				
				var elem = document.getElementById("samuraj-hs-input-lower-field");
				if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12"){
					var state = document.getElementById("samuraj-hs-input-lower-field-state");
					if (state) {
						if (state.selectedIndex > 0) {
							if (elem.value.length == Samuraj.FreightCalc.settings.zipcodedigits) 
								valid = true;
						}
					}
				} else if (elem.value.length == Samuraj.FreightCalc.settings.zipcodedigits) valid = true;
			} else {
				if (elem.value.length == Samuraj.FreightCalc.settings.zipcodedigits) {
					var state = ""
					if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12"){
						var elem = document.getElementById("samuraj-hs-input-lower-field-state");
						if (elem) {
							if (elem.selectedIndex > 0)
								state = elem.value + "|";
						}
					}
					var zipcode = document.getElementById("samuraj-hs-input-lower-field").value;
					var value = state + zipcode;
					Samuraj.FreightCalc.calculateShipping({
						zc: value,
						na: '',
						dt: 'hs',
						ex: Samuraj.FreightCalc.cookie.ex
					});
					valid = true;
				}
			}

			if(!valid){
				var elem = document.getElementById('samuraj-hs-result');
				if (elem) c.removeClass(elem, 'samuraj-show');
				var elem = document.getElementById('samuraj-ex-result');
				if (elem) Samuraj.Common.removeClass(elem, 'samuraj-show');
				c.removeClass(document.getElementById("samuraj-hs-result-restriction-cont"), "samuraj-show");

				Samuraj.Version.load('samurajinfotext', false, function() {
					c.removeClass(document.getElementById("samuraj-hs-result-checkpickuppoint-cont"), "samuraj-show");
				});
				
				c.removeClass(document.getElementById("samuraj-cont-pp"), "samuraj-show");

				Samuraj.FreightCalc.setInfoCookie('{"zc":"","na":"","dt":"hs","ex":"' + Samuraj.FreightCalc.cookie.ex + '"}');
				Samuraj.FreightCalc.resultstore.hs = undefined;
				Samuraj.FreightCalc.revertPrices();
				if (Samuraj.FreightCalc.settings.forcezipcodeentry){
					this.deactivateContinueButtons();
					c.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
					c.addClass(document.getElementById("samuraj-ex-button-text-error"), "samuraj-show");
					c.addClass(document.getElementById("samuraj-hs-button-text-error-klarna"), "samuraj-show");
					c.addClass(document.getElementById("samuraj-ex-button-text-error-klarna"), "samuraj-show");
				}
			}
		},
		setInfoCookie: function(value) {
			try {
				this.cookie = JSON.parse(value);
				this.setSaved('freight_info', value);
			} catch (err) {}
		},
		getInfoCookie: function() {
			var c = Samuraj.Common;
			try {
				var save = this.getSaved("freight_info");
				this.cookie = JSON.parse(save);
			} catch (err) {}
		},
		setSaved: function(key, value) {
			var c = Samuraj.Common;
			try {
				if (this.mobileCheckout() && !c.checkurl("*/mcommerce/|OrderItemDisplayMobile|mCheckoutAddressView|mDeliveryOptionsView*"))
					c.setStorage(key, c.uriencode(value));
					
				else
					c.setCookie(key, c.uriencode(value));
			} catch (err) {}
		},
		getSaved: function(key) {
			var c = Samuraj.Common;
			var save;
			try {
				if (this.mobileCheckout() && !c.checkurl("*/mcommerce/|OrderItemDisplayMobile|mCheckoutAddressView|mDeliveryOptionsView*"))
					save = c.uridecode(c.getStorage(key));
				else
					save = c.uridecode(c.getCookie(key));
			} catch (err) {}
			return save;
		},
		getHashObjectUrl: function() {
			try {
				return JSON.parse(this.URIdecode(window.location.hash));
			} catch (err) {
				return false
			}
		},
		setHashObjectUrl: function() {
			var cp = document.getElementById("cookParam");
			if (cookParam) {
				cookParam.value = this.URIencode(JSON.stringify(this.cookie));
				cookParam.value = "test";
			} else {
				var cookParam = document.createElement("input");
				cookParam.type = "hidden";
				cookParam.name = "cookParam";
				cookParam.id = "cookParam";
				cookParam.value = this.URIencode(JSON.stringify(this.cookie));
				cookParam.value = "test";
				var form = document.getElementById("shopRowBottom");
				if (form) {
					form.insertBefore(cookParam, form.childNodes[0]);
					form.appendChild(cookParam);
				}
			}
		},
		URIencode: function(value) {
			return encodeURIComponent(value).replace(/'/g, "%27").replace(/"/g, "%22");
		},
		URIdecode: function(value) {
			return decodeURIComponent(value.replace(/\+/g, " "));
		},
		setZipCodeForTransfer: function(zc) {
			var hzip = document.querySelector("#shopRowBottom input[name=zipCode]");
			var s = zc.split("|");
			if (s.length == 2){
				var state = s[0];
				zc = s[1];
			}
				
			if (hzip)
				hzip.value = zc;
			else {
				var czi = document.createElement("input");
				czi.type = "hidden";
				czi.name = "zipCode";
				czi.value = zc;
				var elem = document.getElementById("shopRowBottom");
				if (elem)
					elem.appendChild(czi);
			}
			hzip = document.querySelector("#shopRowTop input[name=zipCode]");
			if (hzip)
				hzip.value = zc;
			else {
				var czi = document.createElement("input");
				czi.type = "hidden";
				czi.name = "zipCode";
				czi.value = zc;
				var elem = document.getElementById("shopRowTop");
				if (elem)
					elem.appendChild(czi);
			}
		},
		clearZipCodeForTransfer: function() {
			var hzip = document.querySelector("#shopRowBottom input[name=zipCode]");
			if (hzip)
				hzip.value = "";
			hzip = document.querySelector("#shopRowTop input[name=zipCode]");
			if (hzip)
				hzip.value = "";
		},
		setdeliveryAddressDifferentForTransfer: function() {
			var hdad = document.querySelector("#shopRowBottom input[name=deliveryAddressDifferent]");
			if (hdad)
				hdad.value = "1";
			else {
				var hdadi = document.createElement("input");
				hdadi.type = "hidden";
				hdadi.name = "deliveryAddressDifferent";
				hdadi.value = "1";
				var elem = document.getElementById("shopRowBottom");
				if (elem)
					elem.appendChild(hdadi);
			}
			var hdad = document.querySelector("#shopRowTop input[name=deliveryAddressDifferent]");
			if (hdad)
				hdad.value = "1";
			else {
				var hdadi = document.createElement("input");
				hdadi.type = "hidden";
				hdadi.name = "deliveryAddressDifferent";
				hdadi.value = "1";
				var elem = document.getElementById("shopRowTop");
				if (elem)
					elem.appendChild(hdadi);
			}
		},
		cleardeliveryAddressDifferentForTransfer: function() {
			var hdad = document.querySelector("#shopRowBottom input[name=deliveryAddressDifferent]");
			if (hdad)
				hdad.parentNode.removeChild(hdad);
			hdad = document.querySelector("#shopRowTop input[name=deliveryAddressDifferent]");
			if (hdad)
				hdad.parentNode.removeChild(hdad);
		},
		expandAccordion: function() {
			var c = Samuraj.Common;
			this.accordionList = "false";
			var eab = document.getElementById("expandAccordion");
			eab.innerHTML = "-";
			eab.style.setProperty("padding-right", "4px");
			eab.setAttribute("onClick", "Samuraj.FreightCalc.retractAccordion()");

			var deltype = ["hs", "pp", "cc"];
			for (var i=0; i<deltype.length; i++){
				c.addClass(document.getElementById("samuraj-cont-" + deltype[i]), "samuraj-show");
				c.addClass(document.getElementById(deltype[i] + "-calc-heading-lower"), "samuraj-show");
			}
		},
		retractAccordion: function() {
			var c = Samuraj.Common;
			this.accordionList = "true";
			var eab = document.getElementById("expandAccordion");
			eab.innerHTML = "+";
			eab.style.setProperty("padding-right", "0");
			eab.setAttribute("onClick", "Samuraj.FreightCalc.expandAccordion()");

			var deltype = ["hs", "pp", "cc"];
			for (var i=0; i<deltype.length; i++){
				if (!document.getElementById("samuraj-cont-" + deltype[i] + "-radio-heading-input").getAttribute("checked")) {
					c.removeClass(document.getElementById("samuraj-cont-" + deltype[i]), "samuraj-show");
					c.removeClass(document.getElementById(deltype[i] + "-calc-heading-lower"), "samuraj-show");
				}
			}
		},
		addPrices: function(a, b) {
			var result = ""
			  , c = Samuraj.Common;
			var type = "int"
			if (SamurajSetting.Country.numeric.decimalsum)
				type = "float";
			try {
				if (type == "float")
					var a = c.toFloat({value: a, format: "price"})
					  , b = c.toFloat({value: b, format: "price"});
				else
					var a = c.toInt({value: a, format: "price"})
					  , b = c.toInt({value: b, format: "price"});
				result = a + b;
				if (result <= 0) {
					if (a >= 0)
						result = a;
					else if (b >= 0)
						result = b;
				}
			} catch (err) {}
			
			return c.formatNumber({value: result, type: type, format: "price"});
		},
		subtractPrices: function(a, b) {
			var result = ""
			  , c = Samuraj.Common;
			var type = "int"
			if (SamurajSetting.Country.numeric.decimalsum)
				type = "float";
			try {
				if (type == "float")
					var a = c.toFloat({value: a, format: "price"})
					  , b = c.toFloat({value: b, format: "price"});
				else
					var a = c.toInt({value: a, format: "price"})
					  , b = c.toInt({value: b, format: "price"});
				result = a - b;
				if (result <= 0) {
					if (a >= 0)
						result = a;
					else if (b >= 0)
						result = b;
				}
			} catch (err) {}
			
			return c.formatNumber({value: result, type: type, format: "price"});
		},
		changePrices: function(input){
			var elem,
				c = Samuraj.Common,
				e = Samuraj.Element;
			var type = "int"
			if (SamurajSetting.Country.numeric.decimalsum)
				type = "float";
			
			if(this.mobileCheckout()){

				//Freight and Goods
				var elemselectors = ["#txtNormaltotalTop", "#txtNormaltotalBottom"];
				for (var i=0; i<elemselectors.length; i++){
					elem = e.getChildNodes(document.querySelectorAll(elemselectors[i])[0])[0];
					if (elem) {elem.innerHTML = c.formatNumber({value: input.totalprice, type: type, format: "price"}); elem.textContent = c.formatNumber({value: input.totalprice, type: type, format: "price"});}
				}
				var elemselectors = ["#txtFamilyTotalTop", "#txtFamilyTotalBottom"];
				for (var i=0; i<elemselectors.length; i++){
					elem = e.getChildNodes(document.querySelectorAll(elemselectors[i])[0])[0];
					if (elem) {elem.innerHTML = c.formatNumber({value: input.totalfamilyprice, type: type, format: "price"}); elem.textContent = c.formatNumber({value: input.totalfamilyprice, type: type, format: "price"});}
				}

				//Freight
				var elemselectors = ["#txtTotalDeliveryTop span", "#txtTotalDeliveryBottom span", "#deliveryCalcResultTextBottom", "#deliveryCalcResultTextTop"];
				for (var i=0; i<elemselectors.length; i++){
					elem = document.querySelectorAll(elemselectors[i])[0];
					if (elem) elem.innerHTML = c.formatNumber({value: input.freightprice, type: type, format: "price"});
				}

			} else {
				if (this.checkPvatInput() == "true") {

					//Freight and Goods
					var elemselectors = ["#totalValueBottom span", "#totalValueTop span"];
					for (var i=0; i<elemselectors.length; i++){
						elem = document.querySelectorAll(elemselectors[i])[0];
						if (elem) elem.innerHTML = c.formatNumber({value: input.totalprice_exvat, type: type, format: "price"});
					}
					var elemselectors = ["#totalValueFamilyBottom", "#totalValueFamilyTop"];
					for (var i=0; i<elemselectors.length; i++){
						elem = document.querySelectorAll(elemselectors[i])[0];
						if (elem) elem.innerHTML = c.formatNumber({value: input.totalfamilyprice_exvat, type: type, format: "price"});
					}

					//Freight
					var elemselectors = ["#deliveryCalcResultTextBottom", "#deliveryCalcResultTextTop", "#txtTotalDeliveryBottom", "#txtTotalDeliveryTop"];
					for (var i=0; i<elemselectors.length; i++){
						elem = document.querySelectorAll(elemselectors[i])[0];
						if (elem) elem.innerHTML = c.formatNumber({value: input.freightprice_exvat, type: type, format: "price"});
					}

					//Price with VAT
					elem = document.getElementById("prodPriceWithVATInfo");
					if (elem) elem.innerHTML = c.formatNumber({value: input.totalprice, type: type, format: "price"});
					elem = document.getElementById("prodFamilyPriceWithVATInfo");
					if (elem) elem.innerHTML = c.formatNumber({value: input.totalfamilyprice, type: type, format: "price"});

				} else {

					//Freight and Goods
					var elemselectors = ["#totalValueBottom span", "#totalValueTop span"];
					for (var i=0; i<elemselectors.length; i++){
						elem = document.querySelectorAll(elemselectors[i])[0];
						if (elem) elem.innerHTML = c.formatNumber({value: input.totalprice, type: type, format: "price"});
					}
					var elemselectors = ["#totalValueFamilyBottom", "#totalValueFamilyTop"];
					for (var i=0; i<elemselectors.length; i++){
						elem = document.querySelectorAll(elemselectors[i])[0];
						if (elem) elem.innerHTML = c.formatNumber({value: input.totalfamilyprice, type: type, format: "price"});
					}

					//Freight
					var elemselectors = ["#deliveryCalcResultTextBottom", "#deliveryCalcResultTextTop", "#txtTotalDeliveryBottom", "#txtTotalDeliveryTop"];
					for (var i=0; i<elemselectors.length; i++){
						elem = document.querySelectorAll(elemselectors[i])[0];
						if (elem) elem.innerHTML = c.formatNumber({value: input.freightprice, type: type, format: "price"});
					}

					//Price with VAT
					elem = document.getElementById("prodPriceWithVATInfo");
					if (elem) elem.innerHTML = c.formatNumber({value: input.totalprice_exvat, type: type, format: "price"});
					elem = document.getElementById("prodFamilyPriceWithVATInfo");
					if (elem) elem.innerHTML = c.formatNumber({value: input.totalfamilyprice_exvat, type: type, format: "price"});

				}
			}
		},
		setPickingPrices: function() {
			this.revertPrices();
			var e = Samuraj.Element,
				topBottom = ["Top", "Bottom"];
			if(this.mobileCheckout()){
				for (var i=0; i<topBottom.length; i++){
					var txtTotalDelivery = document.querySelector('#txtTotalDelivery' + topBottom[i] + " span");
					if(txtTotalDelivery){
						txtTotalDelivery.innerHTML = Samuraj.Translation.data.freight.clickcollectprice;

						var elem;
						elem = document.getElementById('txtNormaltotal' + topBottom[i]);
						if(elem) elem.innerHTML = this.addPrices(elem.innerHTML, txtTotalDelivery.innerHTML);

						elem = document.getElementById('txtFamilyTotal' + topBottom[i]);
						if (elem) elem.innerHTML = this.addPrices(elem.innerHTML, txtTotalDelivery.innerHTML);
					}
				}
			} else {
				for (var i=0; i<topBottom.length; i++){
					var txtTotalDelivery = document.getElementById('txtTotalDelivery' + topBottom[i]);
					if(txtTotalDelivery){
						txtTotalDelivery.innerHTML = Samuraj.Translation.data.freight.clickcollectprice;

						var elem;
						elem = document.querySelector('#totalValue' + topBottom[i] + " span");
						if(elem){
							//elem = e.getChildNodes(elem)[0];
							//if (elem)
							elem.innerHTML = this.addPrices(elem.innerHTML, txtTotalDelivery.innerHTML);
						}
						elem = document.getElementById('totalValueFamily' + topBottom[i]);
						if (elem) elem.innerHTML = this.addPrices(elem.innerHTML, txtTotalDelivery.innerHTML);
					}
				}
			}
			var elem = document.querySelectorAll('.notYetCalc');
			for (var i=0; i<elem.length; i++){
				elem[i].style.setProperty("display", "none");
			}
		},
		revertPrices: function() {
			var e = Samuraj.Element,
				topBottom = ["Top", "Bottom"];
			if(this.mobileCheckout()){
				for (var i=0; i<topBottom.length; i++){
					var txtTotalDelivery = document.querySelector('#txtTotalDelivery' + topBottom[i] + " span");
					if (txtTotalDelivery){

						var elem;
						elem = document.getElementById('txtNormaltotal' + topBottom[i]);
						if(elem) elem.innerHTML = this.subtractPrices(elem.innerText, txtTotalDelivery.innerHTML);

						elem = document.getElementById('txtFamilyTotal' + topBottom[i]);
						if(elem) elem.innerHTML = this.subtractPrices(elem.innerHTML, txtTotalDelivery.innerHTML);

						txtTotalDelivery.innerHTML = "?";
					}
				}
			} else {
				if (this.activePage == "cart"){
					for (var i=0; i<topBottom.length; i++){
						var txtTotalDelivery = document.getElementById('txtTotalDelivery' + topBottom[i]);
						if(txtTotalDelivery){

							var elem;
							elem = document.querySelector('#totalValue' + topBottom[i] + " span");
							if(elem){
								//elem = e.getChildNodes(elem)[0];
								//if (elem)
								elem.innerHTML = this.subtractPrices(elem.innerHTML, txtTotalDelivery.innerHTML);
							}
							elem = document.getElementById('totalValueFamily' + topBottom[i]);
							if (elem) elem.innerHTML = this.subtractPrices(elem.innerHTML, txtTotalDelivery.innerHTML);

							txtTotalDelivery.innerHTML = "?";
						}
					}
				}
				if (this.activePage == "address"){
					var elemdel = document.querySelector("#txtTotalDelivery span");
					if (elemdel) {

						elem = document.querySelector("span#txtTotal") || document.querySelector("div#totalValue");
						if (elem) elem.innerHTML = this.subtractPrices(elem.innerHTML, elemdel.innerHTML);

						elem = document.querySelector("span#txtFamilyTotal") || document.querySelector("div#totalValueFamily");
						if (elem) elem.innerHTML = this.subtractPrices(elem.innerHTML, elemdel.innerHTML);

						elemdel.innerHTML = "?";
					}
				}
			}
			var elem = document.querySelectorAll('.notYetCalc');
			for (var i=0; i<elem.length; i++){
				elem[i].style.setProperty("display", "block");
			}
		},
		checkPvatInput: function() {
			var pvatInput = document.getElementById('pricevat');
			var result = "false";
			if ((pvatInput != null) && (pvatInput != undefined)) {
				if (pvatInput.checked) {
					result = "true";
				} else {
					result = "false";
				}
			} else {
				result = "false";
			}
			return result;
		},
		addingStates: function(callback) {
			var c = Samuraj.Common,
				data = this.data.states,
				statedropdown = document.getElementById("samuraj-hs-input-lower-field-state");
			for (var i=0; i<data.length; i++){
				var opt = document.createElement("option");
				if (c.varExist(data[i].code, true)) {
					opt.id = "samuraj-hs-option_" + data[i].code;
					opt.textContent = "\xA0\xA0\xA0" + data[i].name;
					opt.value = data[i].code;
					var statesplit = Samuraj.FreightCalc.cookie.zc.split("|");
					if (data[i].code == statesplit[0]){
						opt.selected = true;
					}
				}
				statedropdown.appendChild(opt);
			}
			callback(true);
		},
		addingPickupPoints: function(callback) {
			var c = Samuraj.Common,
				data = this.data.pickuppoints,
				pickuppointdropdown = document.getElementById("samuraj-pp-input-lower-field");
			for (var i=0; i<data.length; i++){
				var opt = document.createElement("option");
				if (c.varExist(data[i].zipcode, true)) {
					opt.id = "samuraj-pp-option_" + data[i].zipcode;
					opt.textContent = "\xA0\xA0\xA0" + data[i].name;
					opt.value = '{"zc":"' + data[i].zipcode + '",' +
						'"na":"' + data[i].name + '",' +
						'"dt":"pp"}';
					if (data[i].zipcode == Samuraj.FreightCalc.cookie.zc) {
						opt.selected = true;
						this.toggleDeliveryType("pp", "auto");
					}

				} else {
					opt.id = "samuraj-pp-option_cap_" + i;
					opt.textContent = data[i].caption;
					opt.disabled = true;
				}
				pickuppointdropdown.appendChild(opt);
			}
			callback(true);
		},
		addingClickCollects: function(callback) {
			var c = Samuraj.Common,
				clickcollectdropdown = document.getElementById("samuraj-cc-input-lower-field");

			Samuraj.IBES.getFetchLocations({
				success: function(data){
					var count = 1;
					for (var prop in data.responseJSON) {
						if (data.responseJSON[prop].indexOf(IBES.exclude_prefix) == -1){
							var opt = document.createElement("option");
							opt.id = "samuraj-cc-option_" + count;
							opt.textContent = "\xA0\xA0\xA0" + data.responseJSON[prop];
							opt.value = prop;
							if (Samuraj.FreightCalc.cookie.dt == "cc"){
								if (count == Samuraj.Common.toInt(Samuraj.FreightCalc.cookie.zc)) {
									opt.selected = true;
									Samuraj.FreightCalc.toggleDeliveryType("cc", "auto");
								}
							}
							clickcollectdropdown.appendChild(opt);
							count++;
						}
					}
					//sorting ikea stores in list
					var mobileCClist2 = document.getElementById("samuraj-cc-input-lower-field");
					for (i=1; i<mobileCClist2.children.length; i++) {
						for (c=i; c<mobileCClist2.children.length; c++) {
							if (mobileCClist2.children[i].textContent > mobileCClist2.children[c].textContent) {      
								mobileCClist2.insertBefore(mobileCClist2.children[c], mobileCClist2.children[i]);
							}
						}    
					}
				},
				failure: function(data){
					//Could not add click and collect locations to dropdown
				},
				complete: function(data){
					callback(true);
				}
			}, callback);
		},
		savedCookieImport: function(){
			if (!Samuraj.FreightCalc.imported){
				var c = Samuraj.Common,
					e = Samuraj.Element;

				if (this.cookie.dt == "" || this.cookie.dt == "pp" || this.cookie.dt == "cc") {
					if (Samuraj.FreightCalc.settings.expressdefault)
						this.cookie.ex = "true";
				}
				if (this.cookie.dt == "") {
					if (this.activePage == "cart"){
						this.cookie.dt = "hs";
					}
				}				
				if (this.cookie.dt == "hs") {
					if (this.activePage == "cart"){
						this.toggleDeliveryType("hs", "auto");
						if (c.varExist(this.cookie.zc, true)) {
							var zipcode = document.getElementById("samuraj-hs-input-lower-field");
							var state = document.getElementById("samuraj-hs-input-lower-field-state");
							if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12"){
								if (state){
									var s = this.cookie.zc.split("|");
									if (s.length == 2){
										state = document.getElementById("samuraj-hs-option_" + s[0]);
										if (state) state.selected = true;
										zipcode.value  = s[1];
									}
									var elem = document.getElementById("samuraj-hs-input-lower-field-state");
									if (elem) {

									}
								} else {
									zipcode.value = this.cookie.zc;
								}
							} else {
								zipcode.value = this.cookie.zc;
							}

							this.calculateShipping({
								zc: this.cookie.zc,
								na:  this.cookie.na,
								dt: this.cookie.dt,
								ex: this.cookie.ex
							});

							var elem = document.getElementById("samuraj-hs-calculate-button");
							if (elem) Samuraj.Common.removeClass(elem, "samuraj-show");
						}
					}
				}
				if (this.cookie.dt == "pp") {
					this.toggleDeliveryType("pp", "auto");
					if (c.varExist(this.cookie.zc, true)) {
						var elem = document.getElementById("samuraj-pp-option_" + this.cookie.zc)
						if (elem) elem.selected = true;
						this.calculateShipping({
							zc: this.cookie.zc,
							na:  this.cookie.na,
							dt: this.cookie.dt,
							ex: this.cookie.ex,
							system: "irw"
						});
					}
				}
				if (this.cookie.dt == "cc") {
					this.toggleDeliveryType("cc", "auto");
					this.clickCollectAdjustments("cc");
					if (c.varExist(this.cookie.zc, true)) {

						Samuraj.Element.get("#samuraj-cc-option_" + this.cookie.zc, function(selector, elem){
							elem = elem[0];
							elem.selected = true;

							var system = "M2";
							if (!Samuraj.FreightCalc.mobileCheckout())
								system = "IRW"

							Samuraj.IBES.sendPayloadJsonp({
								system: system,
								service: "fetchlocation",
								selector: "#samuraj-cc-input-lower-field",
								loading: function(){
									//Clear resultstate
									Samuraj.FreightCalc.resultstore.cc = undefined;

									//Deactivate all input and continue buttons
									Samuraj.FreightCalc.deactivateAll();
									Samuraj.FreightCalc.deactivateContinueButtons();
									Samuraj.FreightCalc.freightCalcLoaders.show("#samuraj-cc-input-lower-div .samuraj-freightcalc-loader");
									Samuraj.Common.addClass(document.getElementById("samuraj-cc-result"), "samuraj-faded");
									Samuraj.Common.removeClass(document.getElementById("samuraj-calc-cc-result-error-div"), "samuraj-show");
									Samuraj.Common.removeClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
								},
								success: function(response){

									Samuraj.FreightCalc.resultstore.cc = {data: Samuraj.FreightCalc.cookie, response: response};

									Samuraj.FreightCalc.clickcollectcarturl = response.responseJSON.target;
									Samuraj.Common.removeClass(document.getElementById("samuraj-cc-result"), "samuraj-faded");

									// Date and price
									document.getElementById("samuraj-calc-cc-result-delivery-date").innerHTML = "";
									document.getElementById("samuraj-calc-cc-result-price").innerHTML = Samuraj.Translation.data.freight.clickcollectprice;

									Samuraj.FreightCalc.setPickingPrices();

									setTimeout(function(){
										Samuraj.Common.addClass(document.getElementById("samuraj-cc-result"), "samuraj-show");
										Samuraj.Common.removeClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
										Samuraj.FreightCalc.activateContinueButtons();
									}, 600);
								},
								failure: function(response){
									Samuraj.FreightCalc.clickcollectcarturl = undefined;
									Samuraj.Common.removeClass(document.getElementById("samuraj-cc-result"), "samuraj-show");
									if (response.responseJSON){
										setTimeout(function(){
											Samuraj.Common.addClass(document.getElementById("samuraj-calc-cc-result-error-div"), "samuraj-show");
											Samuraj.Common.addClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
										}, 600);
										var elem = document.getElementById("samuraj-calc-cc-result-error-span");
										var error = Samuraj.IBES.getErrorMessageByErrorCode(response.responseJSON.code);
										if (elem && error) elem.innerHTML = error.text;
									}
									Samuraj.FreightCalc.revertPrices();

								},
								complete: function(response){
									Samuraj.FreightCalc.activateAll();
									Samuraj.FreightCalc.freightCalcLoaders.hide();
								}
							});
						});
					}
				}
				if (this.cookie.dt == "oa") {
					if (this.activePage == "address")
						this.toggleDeliveryType("oa", "auto");
				}

				Samuraj.FreightCalc.imported = true;
			}
		},
		checkKlarnaDefault: function(){
			Samuraj.FreightCalc.settings.klarna.switches = {
				m2: {
					hs: window.MaxymiserKlarnaM2hs || Samuraj.FreightCalc.settings.klarna.defaults.m2.hs,
					ex: window.MaxymiserKlarnaM2ex || Samuraj.FreightCalc.settings.klarna.defaults.m2.ex,
					pp: window.MaxymiserKlarnaM2pp || Samuraj.FreightCalc.settings.klarna.defaults.m2.pp,
					cc: window.MaxymiserKlarnaM2cc || Samuraj.FreightCalc.settings.klarna.defaults.m2.cc
				},
				irw: {
					hs: window.MaxymiserKlarnaIRWhs || Samuraj.FreightCalc.settings.klarna.defaults.irw.hs,
					ex: window.MaxymiserKlarnaIRWex || Samuraj.FreightCalc.settings.klarna.defaults.irw.ex,
					pp: window.MaxymiserKlarnaIRWpp || Samuraj.FreightCalc.settings.klarna.defaults.irw.pp,
					cc: window.MaxymiserKlarnaIRWcc || Samuraj.FreightCalc.settings.klarna.defaults.irw.cc
				}
			}
			var s = Samuraj.FreightCalc.settings.klarna.switches;
			
			if(Samuraj.FreightCalc.mobileCheckout()){
				if (s.m2.hs || s.m2.ex || s.m2.pp || s.m2.cc){
					var elem = document.getElementById("samuraj-pay-with-other-card-wrapper");
					if (elem) elem.style.setProperty("display", "block");
				}
			} else {
				if (s.irw.hs || s.irw.ex || s.irw.pp ||s.irw.cc){
					var elem = document.getElementById("samuraj-pay-with-other-card-wrapper");
					if (elem) elem.style.setProperty("display", "block");
				}
			}

			var elem = document.getElementById("samuraj-pay-with-other-card-input");
			if ((elem && elem.checked) || window.iamabot) {
				Samuraj.FreightCalc.settings.klarna.switches = {
					m2: {hs: false,	ex: false, pp: false, cc: false},
					irw: {hs: false, ex: false, pp: false, cc: false}
				}
			}
		},
		toggleShownButton: function(dt){
			var elem, current,
				dts = ["hs", "ex", "pp", "cc"];
			
			var klarna = "";
			this.checkKlarnaDefault();
			if(this.mobileCheckout()){
				if (Samuraj.FreightCalc.settings.klarna.switches.m2[dt])
					klarna = "-klarna";
			} else {
				if (Samuraj.FreightCalc.settings.klarna.switches.irw[dt])
					klarna = "-klarna";
			}

			for (var i=0; i<dts.length; i++){
				elem = document.querySelector("#samuraj-" + dts[i] + "-wrapper");
				if (elem) elem.style.setProperty("display", "none");
				elem = document.querySelector("#samuraj-" + dts[i] + "-wrapper-klarna");
				if (elem) elem.style.setProperty("display", "none");
			}
			current = dt;
			if (current == "pp" && klarna === "") current = "hs";
			elem = document.querySelector("#samuraj-" + current + "-wrapper" + klarna);
			if (elem) elem.style.setProperty("display", "block");

			if(dt == "hs" && klarna === ""){
				elem = document.querySelector("#samuraj-hs-button-caption-value");
				if (elem) elem.value = Samuraj.Translation.data.freight.regulardeliverybuttoncaption;
				elem = document.querySelector("#samuraj-hs-button-caption");
				if (elem) elem.innerHTML = Samuraj.Translation.data.freight.regulardeliverybuttoncaption;
				elem = document.querySelector("#samuraj-hs-button-text");
				if (elem) elem.innerHTML = Samuraj.Translation.data.freight.regulardeliverybuttontext;
				elem = document.querySelector("#samuraj-hs-button-text-error");
				if (elem) elem.innerHTML = Samuraj.Translation.data.freight.regulardeliverybuttontexterror;
			}
			if(dt == "pp" && klarna === ""){
				elem = document.querySelector("#samuraj-hs-button-caption-value");
				if (elem) elem.value = Samuraj.Translation.data.freight.pickuppointbuttoncaption;
				elem = document.querySelector("#samuraj-hs-button-caption");
				if (elem) elem.innerHTML = Samuraj.Translation.data.freight.pickuppointbuttoncaption;
				elem = document.querySelector("#samuraj-hs-button-text");
				if (elem) elem.innerHTML = Samuraj.Translation.data.freight.pickuppointbuttontext;
				elem = document.querySelector("#samuraj-hs-button-text-error");
				if (elem) elem.innerHTML = Samuraj.Translation.data.freight.pickuppointbuttontexterror;
			}
		},
		clickCollectAdjustments: function(dt) {
			if(this.mobileCheckout()){
				var elem;
				if (dt == "cc") {

					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetypepickingservice;
					}
					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span.notYetCalc");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.notcalculatedpickingservice;
					}
					elem = document.querySelectorAll(".row.shoppingSubTotalExlDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetotalbeforepickingservice;
					}

				} else {

					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetypefreight;
					}
					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span.notYetCalc");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.notcalculatedfreight;
					}
					elem = document.querySelectorAll(".row.shoppingSubTotalExlDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetotalbeforefreight;
					}
				}
			} else {
				if (dt == "cc") {

					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetypepickingservice;
					}
					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span.notYetCalc");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.notcalculatedpickingservice;
					}
					elem = document.querySelectorAll(".row.shoppingSubTotalExlDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetotalbeforepickingservice;
					}

				} else {

					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetypefreight;
					}
					elem = document.querySelectorAll(".row.shoppingListDelivery .column-1 span.notYetCalc");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetotalbeforefreight;
					}
					elem = document.querySelectorAll(".row.shoppingSubTotalExlDelivery .column-1 span");
					for(var i=0; i<elem.length; i++) {
						elem[i].innerHTML = Samuraj.Translation.data.freight.pricetotalbeforefreight;
					}
				}

			}
		},
		otherAddressAdjustments: function(dt){
			var c = Samuraj.Common,
				elem, srcelem,
				customerType = this.checkCustomerType(),
				fields = ["organizationName", "firstName", "lastName", "address1", "address2", "zipCode", "city", "phone2", "phone1"];
			if (dt == "oa") {
				for (var i=0; i<fields.length; i++){
					elem = document.querySelector("#signup_checkout_" + customerType + "_ship_" + fields[i]);
					if (elem){
						if (this.oafields){
							if (c.varExist(this.oafields[fields[i]])) {
								elem.value = this.oafields[fields[i]];
								if (window.irwDoFieldValidation) window.irwDoFieldValidation({target: elem});
							}
						} else elem.value = "";
						this.validateRemoveError(elem);
					}
				}
			} else if (dt == "pp") {
				this.oafields = this.oafields || {};
				for (var i=0; i<fields.length; i++){
					elem = document.querySelector("#signup_checkout_" + customerType + "_ship_" + fields[i]);
					if (elem) {
						if (!c.varExist(this.oafields[fields[i]])) this.oafields[fields[i]] = "";
						else this.oafields[fields[i]] = elem.value;
						elem.value = "";
						this.validateRemoveError(elem);
					}
				}
				var fields = ["organizationName", "firstName", "lastName", "phone2", "phone1"]
				for (var i=0; i<fields.length; i++){
					elem = document.querySelector("#signup_checkout_" + customerType + "_ship_" + fields[i]);
					srcelem = document.querySelector("#signup_checkout_" + customerType + "_" + fields[i]);
					if (elem && srcelem) {
						elem.value = srcelem.value;
						if (window.irwDoFieldValidation) window.irwDoFieldValidation({target: elem});
						this.validateRemoveError(elem);
					}

				}
			}
		},
		deactivateAll: function(){
			var c = Samuraj.Common,
				elem;
			elem = document.querySelectorAll(".samuraj-radio-heading-label");
			for (var i=0; i<elem.length; i++){
				elem[i].setAttribute('disabled', 'disabled');
			}
			elem = document.getElementById("samuraj-cont-hs-radio-heading-input");
			if (elem) elem.setAttribute('disabled', 'disabled');
			elem = document.getElementById("samuraj-hs-input-lower-field-state");
			if (elem) elem.setAttribute('disabled', 'disabled');
			elem = document.getElementById("samuraj-hs-input-lower-field");
			if (elem) elem.setAttribute('disabled', 'disabled');
			elem = document.getElementById("samuraj-hs-calculate-button");
			if (elem) elem.setAttribute('disabled', 'disabled');

			elem = document.getElementById("samuraj-cont-pp-radio-heading-input");
			if (elem) elem.setAttribute('disabled', 'disabled');
			elem = document.getElementById("samuraj-pp-input-lower-field");
			if (elem) elem.setAttribute('disabled', 'disabled');

			elem = document.getElementById("samuraj-cont-cc-radio-heading-input");
			if (elem) elem.setAttribute('disabled', 'disabled');
			elem = document.getElementById("samuraj-cc-input-lower-field");
			if (elem) elem.setAttribute('disabled', 'disabled');

			elem = document.getElementById("samuraj-cont-oa-radio-heading-input");
			if (elem) elem.setAttribute('disabled', 'disabled');
			
			//Deactivating all continue to checkout buttons.
			this.deactivateContinueButtons();
			if (this.deactivatebuttontimer)
				clearTimeout(this.deactivatebuttontimer);
			this.deactivatebuttontimer = setTimeout(function(){
				Samuraj.FreightCalc.activateContinueButtons();
			},10000);
		},

		activateAll: function(){
			var c = Samuraj.Common,
				elem;
			elem = document.querySelectorAll(".samuraj-radio-heading-label");
			for (var i=0; i<elem.length; i++){
				elem[i].removeAttribute('disabled');
			}

			elem = document.getElementById("samuraj-cont-hs-radio-heading-input");
			if (elem) elem.removeAttribute('disabled');
			elem = document.getElementById("samuraj-hs-input-lower-field-state");
			if (elem) elem.removeAttribute('disabled');
			elem = document.getElementById("samuraj-hs-input-lower-field");
			if (elem) elem.removeAttribute('disabled');
			elem = document.getElementById("samuraj-hs-calculate-button");
			if (elem) elem.removeAttribute('disabled');

			elem = document.getElementById("samuraj-cont-pp-radio-heading-input");
			if (elem) elem.removeAttribute('disabled');
			elem = document.getElementById("samuraj-pp-input-lower-field");
			if (elem) elem.removeAttribute('disabled');

			elem = document.getElementById("samuraj-cont-cc-radio-heading-input");
			if (elem) elem.removeAttribute('disabled');
			elem = document.getElementById("samuraj-cc-input-lower-field");
			if (elem) elem.removeAttribute('disabled');

			elem = document.getElementById("samuraj-cont-oa-radio-heading-input");
			if (elem) elem.removeAttribute('disabled');
		},
		activateContinueButtons: function(){
			var c = Samuraj.Common,
				elem;

			//M2
			elem = document.querySelector("#checkoutButtonBoxTop .ui-submit");
			if (elem) c.removeClass(elem,"ui-disabled");
			elem = document.querySelectorAll("#checkoutButtonBoxBottom .ui-submit");
			for (var i=0; i<elem.length; i++) c.removeClass(elem[i],"ui-disabled");
			elem = document.querySelectorAll(".buttonContainer .ui-submit");
			for (var i=0; i<elem.length; i++) c.removeClass(elem[i],"ui-disabled");

			//IRW
			elem = document.querySelectorAll(".boxContent.saveNContinueButton .buttonContainer");
			for (var i=0; i<elem.length; i++){
				var inputelem = elem[i].querySelector("input")
				if (inputelem) {
					inputelem.removeAttribute('disabled');
					inputelem.style.setProperty('cursor', "pointer");
				}
				c.removeClass(elem[i],"disabledButton");
			}
			//IRW
			elem = document.querySelectorAll("#shopRowBottom .buttonContainer");
			for (var i=0; i<elem.length; i++){
				var inputelem = elem[i].querySelector("input")
				if (inputelem) {
					inputelem.removeAttribute('disabled');
					inputelem.style.setProperty('cursor', "pointer");
				}
				c.removeClass(elem[i],"disabledButton");
			}
		},
		deactivateContinueButtons: function(){
			var c = Samuraj.Common,
				elem;
			if (this.deactivatebuttontimer)
				clearTimeout(this.deactivatebuttontimer);

			//M2
			elem = document.querySelector("#checkoutButtonBoxTop .ui-submit");
			if (elem) c.addClass(elem,"ui-disabled");

			elem = document.querySelectorAll("#checkoutButtonBoxBottom .ui-submit");
			for (var i=0; i<elem.length; i++) c.addClass(elem[i],"ui-disabled");

			elem = document.querySelectorAll(".buttonContainer .ui-submit");
			for (var i=0; i<elem.length; i++) c.addClass(elem[i],"ui-disabled");

			//IRW
			elem = document.querySelectorAll(".boxContent.saveNContinueButton .buttonContainer");
			for (var i=0; i<elem.length; i++){
				var inputelem = elem[i].querySelector("input");
				if (inputelem) {
					inputelem.setAttribute('disabled', 'disabled');
					inputelem.style.setProperty('cursor', "default");
				}
				c.addClass(elem[i],"disabledButton");
			}

			//IRW
			elem = document.querySelectorAll("#shopRowBottom .buttonContainer");
			for (var i=0; i<elem.length; i++){
				var inputelem = elem[i].querySelector("input")
				if (inputelem) {
					inputelem.setAttribute('disabled', 'disabled');
					inputelem.style.setProperty('cursor', "default");
				}
				c.addClass(elem[i],"disabledButton");
			}
		},
		toggleDeliveryType: function(dt, sender) {
			var c = Samuraj.Common,
				elem,
				deltype,
				runToggle = true;
			if (this.cookie.dt == dt) {
				elem = document.getElementById("samuraj-cont-" + dt + "-radio-heading-input");
				if (elem) var checked = elem.checked;
				if (checked) runToggle = false;
			}

			if (runToggle) {
				
				if (this.activePage == "cart") deltype = ["hs", "pp", "cc"];
				if (this.activePage == "address") deltype = ["pp", "oa"];
				
				for (var i=0; i<deltype.length; i++){
					//Show all info if not Accordion
					if (this.accordionList == "false"){
						c.addClass(document.getElementById("samuraj-cont-" + deltype[i]), "samuraj-show");
						c.addClass(document.getElementById("samuraj-" + deltype[i] + "-calc-heading-lower"), "samuraj-show");
					}

					//If not current deliverytype and not deliverytype other address
					if (deltype[i] !== dt && deltype[i] !== "oa"){
						c.removeClass(document.getElementById("samuraj-" + deltype[i] + "-result"), "samuraj-show");
					}

					//If deliverytype is any other than current
					if (deltype[i] !== dt){
						var elem = document.getElementById("samuraj-cont-" + deltype[i] + "-radio-heading-input");
						if (elem){
							var checked = elem.checked;
							if (checked) {
								this.setInfoCookie('{"zc":"","na":"","dt":"' + dt + '","ex": "' + Samuraj.FreightCalc.cookie.ex + '"}');
								if (this.activePage == "cart"){
									this.clearZipCodeForTransfer ();
									this.cleardeliveryAddressDifferentForTransfer();
									this.clickCollectAdjustments(dt);
								}
								if (this.activePage == "address"){
									this.otherAddressAdjustments(dt);
								}
							}
						}

						//Fade text of inactive selections
						var elem = document.getElementById("samuraj-" + deltype[i] + "-calc-heading-lower");
						if (elem) elem.style.setProperty("color", "#ccc");
						var elem = document.getElementById("samuraj-" + deltype[i] + "-result-restriction-cont");
						if (elem) elem.style.setProperty("color", "#ccc");
						var elem = document.getElementById("samuraj-" + deltype[i] + "-result-checkpickuppoint-cont");
						if (elem) elem.style.setProperty("color", "#ccc");
						if (this.activePage == "address"){
							var elem = document.getElementById("samuraj-cont-oa");
							if (elem) c.removeClass(elem, "samuraj-show")
							if (checked) this.otherAddressAdjustments(dt);
						}

						if (this.accordionList == "true"){
							c.removeClass(document.getElementById("samuraj-cont-" + deltype[i]), "samuraj-show");
							c.removeClass(document.getElementById("samuraj-" + deltype[i] + "-calc-heading-lower"), "samuraj-show");
						}
					}
				} //For
				
				//Toggle on off calculate button
				if (Samuraj.FreightCalc.settings.calculatebutton){
					this.calcButtonValidate();
					var elem = document.getElementById("samuraj-hs-calculate-button");
					if (elem) Samuraj.Common.removeClass(elem, "samuraj-show");
				}

				var elem = document.getElementById("samuraj-" + dt + "-calc-heading-lower");
				if (elem) elem.style.setProperty("color", "black");
				var elem = document.getElementById("samuraj-" + dt + "-result-restriction-cont");
				if (elem) elem.style.setProperty("color", "black");
				var elem = document.getElementById("samuraj-" + dt + "-result-checkpickuppoint-cont");
				if (elem) elem.style.setProperty("color", "black");
				if (this.activePage == "address" && dt == "oa"){
					var elem = document.getElementById("samuraj-cont-oa")
					if (elem) c.addClass(elem, "samuraj-show")
				}

				c.addClass(document.getElementById("samuraj-cont-" + dt), "samuraj-show");
				c.addClass(document.getElementById("samuraj-" + dt + "-calc-heading-lower"), "samuraj-show");

				if (sender == "manual") {
					var elem = document.getElementById("samuraj-cont-" + dt + "-radio-heading-input");
					if (elem) {
						var checked = elem.checked;
						if (checked) {
							this.setInfoCookie('{"zc":"","na":"","dt":"' + dt + '","ex":"' + Samuraj.FreightCalc.cookie.ex + '"}');
							if (this.activePage == "cart") {
								this.clearZipCodeForTransfer ();
								this.cleardeliveryAddressDifferentForTransfer();
								this.clickCollectAdjustments(dt);
							}
							if (this.activePage == "address") {
								var addressadjustment = dt;
							}
						}
					}
				}

				//Reset prices on change of delivery type
				this.revertPrices();

				if (this.activePage == "cart") {
					c.removeClass(document.getElementById("samuraj-ex-result"), "samuraj-show");
					c.removeClass(document.getElementById("samuraj-hs-result-restriction-cont"), "samuraj-show");
					
					Samuraj.Version.load('samurajinfotext', false, function() {
						c.removeClass(document.getElementById("samuraj-hs-result-checkpickuppoint-cont"), "samuraj-show");
					});

					//Deactivate continuebuttons for pickuppoints and click & collect locations until selection has been made
					if(dt == "cc" || dt == "pp")
						Samuraj.FreightCalc.deactivateContinueButtons();
					else {
						if (Samuraj.FreightCalc.settings.forcezipcodeentry)
							Samuraj.FreightCalc.deactivateContinueButtons();
						else
							Samuraj.FreightCalc.activateContinueButtons();
					}
					//Show red error text on deltype change
					if (dt == "pp"){
						Samuraj.Common.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
						Samuraj.Common.addClass(document.getElementById("samuraj-pp-button-text-error-klarna"), "samuraj-show");						
					}
					if (dt == "cc"){
						Samuraj.Common.addClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
						Samuraj.Common.addClass(document.getElementById("samuraj-cc-button-text-error-klarna"), "samuraj-show");
					}

					if (dt == "hs") {
						if (Samuraj.FreightCalc.settings.forcezipcodeentry){
							Samuraj.Common.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
							Samuraj.Common.addClass(document.getElementById("samuraj-ex-button-text-error"), "samuraj-show");
							Samuraj.Common.addClass(document.getElementById("samuraj-hs-button-text-error-klarna"), "samuraj-show");
							Samuraj.Common.addClass(document.getElementById("samuraj-ex-button-text-error-klarna"), "samuraj-show");
						}
					}

					//Show correct continue button
					this.toggleShownButton(dt);

					//Show stored result
					if (Samuraj.FreightCalc.resultstore[dt]) {
						Samuraj.FreightCalc.resultstore[dt].nodelay = true;
						if (dt == "hs" || dt == "pp"){
							//Reset prices before loading new prices
							Samuraj.FreightCalc.revertPrices();

							//Deactivate all input and continue buttons
							//Samuraj.FreightCalc.deactivateAll();

							if (Samuraj.FreightCalc.activePage == "cart"){
								//Setting up input to the address page
								Samuraj.FreightCalc.setZipCodeForTransfer(Samuraj.FreightCalc.resultstore[dt].data.zc);
								if (dt == "pp") Samuraj.FreightCalc.setdeliveryAddressDifferentForTransfer();
								else Samuraj.FreightCalc.cleardeliveryAddressDifferentForTransfer();
							}

							if (dt == "hs"){
								var delres = Samuraj.FreightCalc.checkDeliveryRestrictions(Samuraj.FreightCalc.resultstore[dt].data.zc);
								if (Samuraj.Common.varExist(delres)) {
									document.getElementById("samuraj-hs-result-restriction").innerHTML = delres.restriction;
									Samuraj.Common.addClass(document.getElementById("samuraj-hs-result-restriction-cont"), "samuraj-show");
								} else
									Samuraj.Common.removeClass(document.getElementById("samuraj-hs-result-restriction-cont"), "samuraj-show");
								
							}

							var elem = document.querySelectorAll(".ikea-product-pricetag-error");
							for (var i=elem.length-1; i>-1; i--)
								Samuraj.Element.remove(elem[i]);

							Samuraj.FreightCalc.loadDeliveryDetail(Samuraj.FreightCalc.resultstore[dt]);
						} else if (dt == "cc"){

							Samuraj.FreightCalc.clickcollectcarturl = Samuraj.FreightCalc.resultstore[dt].response.responseJSON.target;
							Samuraj.Common.removeClass(document.getElementById("samuraj-cc-result"), "samuraj-faded");

							// Date and price
							var elem = document.getElementById("samuraj-calc-cc-result-delivery-date");
							if (elem) elem.innerHTML = "";
							var elem = document.getElementById("samuraj-calc-cc-result-price");
							if (elem) elem.innerHTML = Samuraj.Translation.data.freight.clickcollectprice;

							Samuraj.FreightCalc.setPickingPrices();
							Samuraj.Common.addClass(document.getElementById("samuraj-cc-result"), "samuraj-show");
							Samuraj.Common.removeClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
							Samuraj.FreightCalc.activateContinueButtons();
							var data = Samuraj.FreightCalc.resultstore[dt].data;
							Samuraj.FreightCalc.setInfoCookie('{"zc":"' + data.zc + '","na":"' + data.na + '","dt":"' + data.dt + '","ex":"' + Samuraj.FreightCalc.cookie.ex + '"}');
						}
					}
				}


				//Hide errors when delivery type changes
				if (this.activePage == "cart") {
					c.removeClass(document.getElementById("samuraj-calc-hs-result-error-div"), "samuraj-show");
					c.removeClass(document.getElementById("samuraj-calc-cc-result-error-div"), "samuraj-show");
				}
				c.removeClass(document.getElementById("samuraj-calc-pp-result-error-div"), "samuraj-show");


				var elem = document.getElementById("samuraj-cont-" + Samuraj.FreightCalc.cookie.dt + "-radio-heading-input");
				if (elem) elem.checked = true;
			}

			if (addressadjustment)
				this.otherAddressAdjustments(addressadjustment);
		},
		checkPickupPoints: function(data, callback) {
			if (Samuraj.FreightCalc.settings.pickuppoints){
				var c = Samuraj.Common;
				
				if (Samuraj.FreightCalc.currentconnectedpp) delete Samuraj.FreightCalc.currentconnectedpp;
				if (data.na.length > 0) {
					callback(true);
				} else {
					var pp = this.data.pickuppoints;
					for (var i=0; i<pp.length; i++){
						if (c.varExist(pp[i].zipcode, true)){
							if (pp[i].zipcode == data.zc) {
								//stopShippingCalc = "true";
								document.getElementById("samuraj-hs-input-lower-field").value = "";
								document.getElementById("samuraj-pp-input-lower-field").focus();
								var elem = document.getElementById("samuraj-pp-option_" + pp[i].zipcode);
								if (elem) elem.selected = true;

								this.toggleDeliveryType("pp", "auto");
								this.calculateShipping({
									zc: pp[i].zipcode,
									na:  pp[i].name,
									dt: 'pp',
									ex: Samuraj.FreightCalc.cookie.ex,
									system: "irw"
								});
								return false;
							}
						}
					}
					var found = false;
					if (Samuraj.FreightCalc.settings.checkpickuppoints){
						for (var i=0; i<pp.length; i++){
							if (c.varExist(pp[i].zipcode, true) && c.varExist(pp[i].connectedzipcodes, true)){
								var zipcodes = pp[i].connectedzipcodes.replace(/[^0-9,]/g, "").split(",");
								for (var j=0; j<zipcodes.length; j++){
									if (zipcodes[j] == data.zc) {
										found = true;
										Samuraj.FreightCalc.priceCheck({
											name: pp[i].name,
											zipcode: pp[i].zipcode,
											loading: function(response){
												//Add faded to result
												Samuraj.Common.addClass(document.getElementById("samuraj-" + data.dt + "-result"), "samuraj-faded");
						
												//Hide error
												Samuraj.Common.removeClass(document.getElementById("samuraj-calc-" + data.dt + "-result-error-div"), "samuraj-show");
						
												//Reset prices before loading new prices
												Samuraj.FreightCalc.revertPrices();
						
												//Hide express delivery result
												Samuraj.Common.removeClass(document.getElementById("samuraj-ex-result"), "samuraj-show");
												//Deactivate all input and continue buttons
												Samuraj.FreightCalc.deactivateAll();
												if (data.dt == "pp" || Samuraj.FreightCalc.settings.forcezipcodeentry){
													Samuraj.Common.removeClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
													Samuraj.Common.removeClass(document.getElementById("samuraj-ex-button-text-error"), "samuraj-show");
													Samuraj.Common.removeClass(document.getElementById("samuraj-pp-button-text-error-klarna"), "samuraj-show");
												}
						
												//Setting up input to the address page
												Samuraj.FreightCalc.setZipCodeForTransfer(data.zc);
												
												//Hide all loading animations
												Samuraj.FreightCalc.freightCalcLoaders.hide();
												//Show current loading animation
												Samuraj.FreightCalc.freightCalcLoaders.show("#samuraj-" + data.dt + "-input-lower-div .samuraj-freightcalc-loader");
						
												var elem = document.querySelectorAll(".ikea-product-pricetag-error");
												for (var i=elem.length-1; i>-1; i--)
													Samuraj.Element.remove(elem[i]);
												if (data.dt == "hs")
													Samuraj.FreightCalc.expressdeliverycarturl = undefined;
						
											},
											success: function(response){
												
												/*
												var elem = document.querySelector("#samuraj-hs-result-checkpickuppoint-date");
												if (elem) elem.innerHTML = response.responseJSON[1][0].preDelTimeSlotStart;
												elem = document.querySelector("#samuraj-hs-result-checkpickuppoint-name");
												if (elem) elem.innerHTML = this.name;
												*/
												Samuraj.FreightCalc.currentconnectedpp = {};
												var currentconnectedpp = Samuraj.FreightCalc.currentconnectedpp;
												currentconnectedpp.date = response.responseJSON[1][0].preDelTimeSlotStart;
												currentconnectedpp.zipcode = this.zipcode;
												currentconnectedpp.name = this.name;
												
												var transfer = {
													data: {
														zc: this.zipcode,
														na: this.name,
														dt: 'pp',
														ex: Samuraj.FreightCalc.cookie.ex
													},
													response: {
														irw: response
													}
												};
												Samuraj.FreightCalc.resultstore.pp = transfer;
												Samuraj.FreightCalc.prepConnectedPickupPoint(transfer);
												callback(true);
											},
											failure: function(response){
												var transfer = {
													data: data,
													response: {
														irw: response
													} 
												};
												if (transfer.response.irw.responseJSON[0][0].code == -120606){
													Samuraj.FreightCalc.resultstore[data.dt] = undefined;
													Samuraj.FreightCalc.loadDeliveryError(transfer);
												} else {
													callback(false);
												}
											},
											exception: function(response){
												Samuraj.FreightCalc.activateAll();
												if (data.dt !== "pp" && data.dt !== "cc")
													Samuraj.FreightCalc.activateContinueButtons();
						
												Samuraj.FreightCalc.freightCalcLoaders.hide();
						
												var operationCode = -1;
												if (response.irw) response.irw = response.irw.responseText || "no response text";
						
												var errMsgDiv = document.getElementById('samuraj-calc-' + data.dt + '-result-error-span');
												errMsgDiv.innerHTML = Samuraj.Translation.data.freight.generalerrortext;
												Samuraj.Common.removeClass(document.getElementById("samuraj-" + data.dt + "-result"), "samuraj-faded");
												Samuraj.Common.removeClass(document.getElementById("samuraj-" + data.dt + "-result"), "samuraj-show");
												setTimeout(function(){
													Samuraj.Common.addClass(document.getElementById("samuraj-calc-" + data.dt + "-result-error-div"), "samuraj-show");
												}, 600);
												// for statistics
												if (Samuraj.Common.varExist(document.updateAllForm)) {
													document.updateAllForm.errCodeForCalculateBtn.value = operationCode;
						
													if (Samuraj.Common.isFunction(irwstatSend))
														irwstatSend();
												}
											},
											complete: function(data){
												//callback(true);
											}
										});
										break;
									}
								}
							}
						}
					}
					if (!pp.length || !found)
						callback(true);
				}
			} else callback(true);
		},
		prepConnectedPickupPoint: function(input){
			var c = Samuraj.Common,
				p = Samuraj.FreightCalc;

			var preShippingCost = "";
			var preDelTimeSlotStart = "";
			if (input.response.irw.responseJSON[0][0].code == 0) {

				var elem = document.getElementById("samuraj-pp-option_" + input.data.zc);
				if (elem) elem.selected = true;
				

				preDelTimeSlotStart = input.response.irw.responseJSON[1][0].preDelTimeSlotStart;

				//if (input.data.dt == "hs") document.getElementById("samuraj-calc-" + input.data.dt + "-result-zc").innerHTML = input.data.zc;
				//document.getElementById("samuraj-calc-" + input.data.dt + "-result-na").innerHTML = input.data.na;
				document.getElementById("samuraj-calc-pp-result-delivery-date").innerHTML = preDelTimeSlotStart;

				if (p.checkPvatInput() == "true") preShippingCost = input.response.irw.responseJSON[1][0].preShippingCostExclVAT;
				else preShippingCost = input.response.irw.responseJSON[1][0].preShippingCost;

				document.getElementById("samuraj-calc-pp-result-price").innerHTML = preShippingCost;

			}

			var pp = this.getPickupPoint(input.data.zc);
			if (pp) {
				if (c.varExist(pp.mapurl, true)){
					document.getElementById('samuraj-calc-pp-map-url').href = pp.mapurl;
				}
			}


			
		},
		checkDeliveryRestrictions: function(zc){
			if (Samuraj.FreightCalc.settings.deliveryrestrictions){
				var dr = Samuraj.FreightCalc.data.deliveryrestrictions;
				for (var i=0; i<dr.length; i++){
					if (zc == dr[i].zipcode) 
						return dr[i];
				}
			}
		},
		checkParcelFreightCalcRestriction: function() {
			if (Samuraj.FreightCalc.settings.parcelcheck){
				Samuraj.FreightCalc.priceCheck({
					zipcode: Samuraj.FreightCalc.settings.parcelcheckzipcode,
					success: function(data){
						var parcel = false,
							pcw = Samuraj.FreightCalc.settings.parcelcheckwords,
							pdm = data.responseJSON[1][0].preDelMethod;
						for (var i=0; i<pcw.length; i++){
							if (pdm.indexOf(pcw[i]) > -1)
								parcel = true;
						}
						if (parcel) {
							c.addClass(document.getElementById("samuraj-pp-result-restriction-cont"), "samuraj-show");
						}
					}
				});
			}
		},
		freightCalcLoaders: {
			show: function(selector){
				if (!selector) selector = ".samuraj-freightcalc-loader";
				var elem = document.querySelectorAll(selector);
				for (var i=0; i<elem.length; i++){
					Samuraj.Common.addClass(elem[i], "samuraj-show");
				}
			},
			hide: function(selector){
				if (!selector) selector = ".samuraj-freightcalc-loader";
				var elem = document.querySelectorAll(selector);
				for (var i=0; i<elem.length; i++){
					Samuraj.Common.removeClass(elem[i], "samuraj-show");
				}
			}
		},
		calculateShipping: function(data) {

			//Check the zipcode, if it is a pickuppoint reload function as that pickuppoint
			Samuraj.FreightCalc.checkPickupPoints(data, function() {

				Samuraj.FreightCalc.getFreightPrices({
					zipcode: data.zc,
					system: data.system || "all",
					loading: function(response){
						//Add faded to result
						Samuraj.Common.addClass(document.getElementById("samuraj-" + data.dt + "-result"), "samuraj-faded");

						//Hide error
						Samuraj.Common.removeClass(document.getElementById("samuraj-calc-" + data.dt + "-result-error-div"), "samuraj-show");

						//Reset prices before loading new prices
						Samuraj.FreightCalc.revertPrices();

						//Hide express delivery result
						Samuraj.Common.removeClass(document.getElementById("samuraj-ex-result"), "samuraj-show");
						//Deactivate all input and continue buttons
						Samuraj.FreightCalc.deactivateAll();
						if (data.dt == "pp" || Samuraj.FreightCalc.settings.forcezipcodeentry){
							Samuraj.Common.removeClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
							Samuraj.Common.removeClass(document.getElementById("samuraj-ex-button-text-error"), "samuraj-show");
							Samuraj.Common.removeClass(document.getElementById("samuraj-pp-button-text-error-klarna"), "samuraj-show");
						}

						if (Samuraj.FreightCalc.activePage == "cart"){
							if (Samuraj.Common.varExist(document.updateAllForm))
								document.updateAllForm.calculateClicked.value = "yes";

							//Setting up input to the address page
							Samuraj.FreightCalc.setZipCodeForTransfer(data.zc);
							if (data.dt == "pp") Samuraj.FreightCalc.setdeliveryAddressDifferentForTransfer();
							else Samuraj.FreightCalc.cleardeliveryAddressDifferentForTransfer();
						}

						//Hide all loading animations
						Samuraj.FreightCalc.freightCalcLoaders.hide();
						//Show current loading animation
						Samuraj.FreightCalc.freightCalcLoaders.show("#samuraj-" + data.dt + "-input-lower-div .samuraj-freightcalc-loader");

						var delres = Samuraj.FreightCalc.checkDeliveryRestrictions(data.zc);
						if (Samuraj.Common.varExist(delres)) {
							document.getElementById("samuraj-hs-result-restriction").innerHTML = delres.restriction;
							Samuraj.Common.addClass(document.getElementById("samuraj-hs-result-restriction-cont"), "samuraj-show");
						} else
							Samuraj.Common.removeClass(document.getElementById("samuraj-hs-result-restriction-cont"), "samuraj-show");

						var elem = document.querySelectorAll(".ikea-product-pricetag-error");
						for (var i=elem.length-1; i>-1; i--)
							Samuraj.Element.remove(elem[i]);
						if (data.dt == "hs")
							Samuraj.FreightCalc.expressdeliverycarturl = undefined;

					},
					success: function(response){
						var c = Samuraj.Common;
						setTimeout(function() {
							var transfer = {
								data: data,
								response: response
							};
							Samuraj.FreightCalc.resultstore[data.dt] = transfer;
							if (response.ibes.success)
								Samuraj.FreightCalc.expressdeliverycarturl = response.ibes.responseJSON.target;
							Samuraj.FreightCalc.loadDeliveryDetail(transfer);
							
							//CFB addition
							if (Samuraj.FreightCalc.settings.cfbEnabled) {
								if (stockCheck) 
									stockCheck(response.irw.responseJSON);
							}
							
							if (transfer.data.dt == "hs" && transfer.data.ex == "true")
								Samuraj.FreightCalc.toggleShownButton("ex");
							else 
								Samuraj.FreightCalc.toggleShownButton(transfer.data.dt);

							var currentconnectedpp = Samuraj.FreightCalc.currentconnectedpp;
							if (currentconnectedpp) {
								//new Date(yyyy,mm,dd,hh,mm,0,0)
								var pdts = transfer.response.irw.responseJSON[1][0].preDelTimeSlotStart;
								if (c.varExist(currentconnectedpp.date, true) && c.varExist(pdts, true)){
									var pp_y = parseInt(currentconnectedpp.date.slice(6, 10));
									var pp_m = parseInt(currentconnectedpp.date.slice(3, 5));
									var pp_d = parseInt(currentconnectedpp.date.slice(0, 2));									
									var ppdate = new Date(pp_y, pp_m, pp_d, 12, 0, 0, 0);

									var hs_y = parseInt(pdts.slice(6, 10));
									var hs_m = parseInt(pdts.slice(3, 5));
									var hs_d = parseInt(pdts.slice(0, 2));
									var hsdate = new Date(hs_y, hs_m, hs_d, 12, 0, 0, 0);
								
									if (ppdate < hsdate && !Samuraj.FreightCalc.parcel){
										c.addClass(document.querySelector("#samuraj-cont-pp"), "samuraj-show");
										
										Samuraj.Version.load('samurajinfotext', false, function() {
											c.addClass(document.querySelector("#samuraj-hs-result-checkpickuppoint-cont"), "samuraj-show");
										});

										c.removeClass(document.getElementById("samuraj-pp-result"), "samuraj-faded");
										setTimeout(function(){
											c.addClass(document.getElementById("samuraj-pp-result"), "samuraj-show");
										}, 600);
									} else {
										Samuraj.FreightCalc.resultstore.pp = undefined;
									}
								}
							}
						}, 1000);
					},
					failure: function(response){
						var transfer = {
							data: data,
							response: response
						};
						Samuraj.FreightCalc.resultstore[data.dt] = undefined;
						Samuraj.FreightCalc.loadDeliveryError(transfer);
					},
					exception: function(response){
						Samuraj.FreightCalc.activateAll();
						if (data.dt !== "pp" && data.dt !== "cc")
							Samuraj.FreightCalc.activateContinueButtons();

						Samuraj.FreightCalc.freightCalcLoaders.hide();

						var operationCode = -1;
						if (response.irw) response.irw = response.irw.responseText || "no response text";

						var errMsgDiv = document.getElementById('samuraj-calc-' + data.dt + '-result-error-span');
						errMsgDiv.innerHTML = Samuraj.Translation.data.freight.generalerrortext;
						Samuraj.Common.removeClass(document.getElementById("samuraj-" + data.dt + "-result"), "samuraj-faded");
						Samuraj.Common.removeClass(document.getElementById("samuraj-" + data.dt + "-result"), "samuraj-show");
						setTimeout(function(){
							Samuraj.Common.addClass(document.getElementById("samuraj-calc-" + data.dt + "-result-error-div"), "samuraj-show");
						}, 600);
						// for statistics
						if (Samuraj.Common.varExist(document.updateAllForm)) {
							document.updateAllForm.errCodeForCalculateBtn.value = operationCode;

							if (Samuraj.Common.isFunction(irwstatSend))
								irwstatSend();
						}
					}
				}, data);

			});
		},

		getPickupPoint: function(zc){
			var c = Samuraj.Common,
				pp = this.data.pickuppoints;
			for (var i=0; i<pp.length; i++){
				if (c.varExist(pp[i].zipcode)){
					if (pp[i].zipcode == zc)
						return pp[i];
				}
			}
		},
		toggleExpress: function(dt) {
			//Force click checkbox to stay selected
			var elem = document.getElementById("samuraj-" + dt + "-result-checkbox-input");
			if (elem)
				elem.checked = true;

			var dt_other = "ex",
				express = false;
			if(dt == "ex"){
				var dt_other = "hs";
				express = true;
			}
			this.setInfoCookie('{"zc":"' + this.cookie.zc + '","na":"' + this.cookie.na + '","dt":"' + this.cookie.dt + '","ex":"' + express + '"}');

			var elem = document.getElementById("samuraj-" + dt_other + "-result-checkbox-input");
			if (elem)
				elem.checked = false;


			if(dt == "ex"){
				this.resultstore.hs.data.ex = "true";
				this.changePrices(this.pricesexpress);
			} else {
				this.resultstore.hs.data.ex = "false";
				this.changePrices(this.pricesregular);
			}
			this.toggleShownButton(dt);
		},
		expressdeliveryCheckout: function(){
			//Send customer to express delivery checkout
			if (Samuraj.FreightCalc.expressdeliverycarturl){
				window.location.href = Samuraj.FreightCalc.expressdeliverycarturl;
			} else {
				//errorhandler
			}
		},
		clickcollectCheckout: function(){
			//Send customer to click & collect checkout
			if (Samuraj.FreightCalc.clickcollectcarturl){
				window.location.href = Samuraj.FreightCalc.clickcollectcarturl;
			} else {
				//errorhandler
			}
		},
		Klarna: {
			checkout: function(deliverytype){
				var deviceType = "desktop";
				if (Samuraj.FreightCalc.mobileCheckout()) deviceType = "mobile";
				
				//Add familycard popup here 
				var transfer = {
					familyCardStatus: "ok"
				} //override
				
				var payload = {
					country: Samuraj.FreightCalc.settings.klarna.country,
					deviceType: deviceType,
					cart: [],
					zip: document.getElementById("samuraj-hs-input-lower-field").value,
					pickupZip: "",
					shipping_options: [{
						id: "1",
						name: "",
						description: "",
						price: 0,
						tax_amount: 0,
						tax_rate: 0
					}]
				}

				if (deliverytype == "hs" && Samuraj.FreightCalc.parcel)
					deliverytype = "hs-parcel";
				
				var all_good = false;
				if (deliverytype == "hs"){
					//HOME SHIPPING - Truck

					payload.shipping_options[0].name = "Levering innenfor dør"

					//Add questionnaire popup here 
					payload.shippingDetails = {
						inburen: {
							type: "enebolig",
							1: "KLARNA",
							2: {
								selection: true,
								comment: "KLARNA"
							}
						}
					}
					all_good = true;

				} else if (deliverytype == "hs-parcel"){
					//HOME SHIPPING - Parcel

					payload.shipping_options[0].name = "Postpakke"
					all_good = true;

				} else if (deliverytype == "ex"){
					//EXPRESS DELIVERY

				} else if (deliverytype == "pp"){
					//PICKUP POINT

					var elem = document.getElementById("samuraj-pp-input-lower-field");
					if (elem && elem.selectedIndex > 1){
						payload.pickupZip = JSON.parse(elem.options[elem.selectedIndex].value).zc;
						payload.shippingDetails = {
							Pickup: {
								location: elem.options[elem.selectedIndex].text
							}
						}
						payload.shipping_options[0].name = "Pick-up Point" + payload.shippingDetails.Pickup.location;
						all_good = true;
					}

				} else if (deliverytype == "cc"){
					//CLICK & COLLECT

				}

				if (all_good){
					payload = this.createJson(payload, transfer);
					if (payload) this.sendPayload(payload);
					//else this.klarnaError;
				} //else this.sendToNormalCheckout;
			},
			
			createJson: function(payload, transfer) {
				var productsInCart = document.querySelectorAll('#updateAllForm .productRow'); //#updateAllForm .productRow
				if (productsInCart.length > -1) {
					var jsonProducts = [];
					var isFamilyPrice = false;
					
					for (var i = 0; i < productsInCart.length; i++) {
						var product = productsInCart[i];
						var j = i + 1;
						var unitPrice, totalAmount, elem;
						
						if (payload.deviceType == "desktop"){

							var productNumber = document.querySelector('#prodId_' + j).value;
							var productName = product.querySelector('.prodName a').innerHTML.replace(/\s{2,}/g, "");
							var quantity = document.querySelector('#order_qty_' + j).value;
							
							
							elem = product.querySelector('.prodInfoContainer .colPrice .familyPrice');
							if (elem && transfer.familyCardStatus == 'ok') {
								isFamilyPrice = true;
								unitPrice = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							} else if (elem) {
								elem = product.querySelector('.prodInfoContainer .colPrice .regularPrice')
								if (elem) unitPrice = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							} else {
								elem = product.querySelector('.prodInfoContainer .colPrice');
								if (elem) unitPrice = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							}
							
							elem = product.querySelector('.colTotalPrice .familyPrice');
							if (elem && transfer.familyCardStatus == 'ok') 
								totalAmount = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							else if (elem) {
								elem = product.querySelector('.colTotalPrice .regularPrice')
								if (elem) totalAmount = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							} else {
								elem = product.querySelector('.colTotalPrice');
								if (elem) totalAmount = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							}
							
							var productUrl = 'http://www.ikea.com/' + SamurajSetting.Country.homePath + '/catalog/products/' + productNumber;

						} else if (payload.deviceType == "mobile"){

							var productNumber = document.querySelector('#prodId_' + j).value;
							var productName = product.querySelector('.productName').innerHTML.replace(/\s{2,}/g, "");
							var quantity = document.querySelector('#order_qty_' + j).value;
							
							
							elem = product.querySelector('.ikea-product-pricetag-price .familyPrice');
							if (elem && transfer.familyCardStatus == 'ok') {
								isFamilyPrice = true;
								unitPrice = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							} else if (elem) {
								elem = product.querySelector('.ikea-product-pricetag-normal .productPrice')
								if (elem) totalAmount = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							} else {
								elem = product.querySelector('.ikea-product-pricetag-price .productPrice');
								if (elem) unitPrice = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							}
							
							elem = product.querySelector('.totalPrice .ikea-product-pricetag-family');
							if (elem && transfer.familyCardStatus == 'ok') 
								totalAmount = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							else {
								elem = product.querySelector('.totalPrice .ikea-product-pricetag-normal');
								if (elem) totalAmount = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"});
							}
							
							var productUrl = 'http://www.ikea.com/' + SamurajSetting.Country.homePath + '/catalog/products/' + productNumber;
						}

						jsonProducts.push({
							type: 'physical',
							reference: productNumber,
							name: productName,
							quantity: quantity,
							unit_price: this.adaptPriceForKlarna(unitPrice),
							tax_rate: 0,
							total_amount: this.adaptPriceForKlarna(totalAmount),
							total_discount_amount: 0,
							total_tax_amount: 0,
							product_url: productUrl
						});
					}
					payload.cart = jsonProducts;

					if (payload.deviceType == "desktop") var shippingPrice = Samuraj.Common.toInt({value: document.querySelector('#txtTotalDeliveryBottom').innerHTML, format: "price"});
					if (payload.deviceType == "mobile") var shippingPrice = Samuraj.Common.toInt({value: document.querySelector('#txtTotalDeliveryBottom span').innerHTML, format: "price"});
					payload.shipping_options[0].price = this.adaptPriceForKlarna(shippingPrice);
					
					return payload;
				}
			},
			sendPayload: function(payload){
				// Create form with JSON and send it to the payment page.
				var form = document.createElement('form');
				form.method = 'post';
				if (payload.deviceType == "desktop") form.action = Samuraj.FreightCalc.settings.klarna.submitUrlIRW;
				if (payload.deviceType == "mobile") form.action = Samuraj.FreightCalc.settings.klarna.submitUrl;
				
				// Encode JSON to be able to POST to the payment page.
				payload = JSON.stringify(payload);
				payload = encodeURIComponent(payload);

				var elempayload = document.createElement('input');
				elempayload.type = 'hidden';
				elempayload.value = payload;
				elempayload.name = 'klarna-json';
				
				form.appendChild(elempayload);
				document.body.appendChild(form);
				
				form.submit();
			},
			adaptPriceForKlarna: function(floatNr) {
				if (floatNr > 0) floatNr = floatNr * 100;
				return floatNr;
			}
		},
		priceCheck: function(input, context){
			input.state = input.state || "null";
			
			//CFB addition	
			if (Samuraj.FreightCalc.settings.cfbEnabled) {
				var baseDeliveryDetail = location.protocol + "//" + document.location.host + "/webapp/wcs/stores/servlet/IrwWSCfbDeliveryDetail?zipCode=" + input.zipcode + "&state=" + input.state + "&storeId=" + SamurajSetting.Country.storeId + "&langId=" + SamurajSetting.Country.langId + "&priceexclvat=";
			} else {
				var baseDeliveryDetail = location.protocol + "//" + document.location.host + "/webapp/wcs/stores/servlet/IrwWSDeliveryDetail?zipCode=" + input.zipcode + "&state=" + input.state + "&storeId=" + SamurajSetting.Country.storeId + "&langId=" + SamurajSetting.Country.langId + "&priceexclvat=";
			}

			if (this.checkPvatInput() == "true") baseDeliveryDetail = baseDeliveryDetail + this.checkPvatInput();

			if (Samuraj.Common.isFunction(input.loading))
				input.loading(context);

			var response = new Samuraj.Common.xhr();
			var DONE = 4;
			var OK = 200;
			response.context = context;
			response.overrideMimeType("application/json");
			response.open('POST', baseDeliveryDetail, true);
			response.onreadystatechange = function() {
				if (response.readyState === DONE) {
					try{
						if (response.status === OK) {
							response.responseJSON = JSON.parse(response.responseText);
							var operationCode = parseInt(response.responseJSON[0][0].code);
							var operationSuccess = (operationCode == 0);
							if (operationSuccess) {
								if (Samuraj.Common.isFunction(input.success))
									input.success(response, context);
							} else {
								if (Samuraj.Common.isFunction(input.failure))
									input.failure(response, context);
							}
						} else {
							if (Samuraj.Common.isFunction(input.failure))
								input.failure(response, context);
						}
					} catch (err) {
						if (Samuraj.Common.isFunction(input.exception))
							input.exception(response, context);
					}

					if (Samuraj.Common.isFunction(input.complete))
						input.complete(response, context);
				}
			}
			response.send(null);
		},
		getFreightPrices: function(input, context) {
			var combinedResponse = {};

			if (Samuraj.Common.isFunction(input.loading))
				input.loading(context);
			
			if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12"){
				var s = input.zipcode.split("|");
				if (s.length == 2){
					input.state = s[0];
					input.zipcode = s[1];
				}
			}
			input.system = input.system || "all"
			
			if (input.system == "irw" || input.system == "all"){
				Samuraj.FreightCalc.priceCheck({
					state: input.state,
					zipcode: input.zipcode,
					success: function(data){
						data.success = true;
						combinedResponse.irw = data;
					},
					failure: function(data){
						data.success = false;
						combinedResponse.irw = data;
					}
				});
			} else combinedResponse.irw = {success: false};

			if (Samuraj.FreightCalc.settings.express && (input.system == "ibes" || input.system == "all")){
				Samuraj.IBES.sendPayloadJsonp({
					service: "express",
					selector: "#samuraj-hs-input-lower-field",
					success: function(data){
						data.success = true;
						combinedResponse.ibes = data;
					},
					failure: function(data){
						data.success = false;
						combinedResponse.ibes = data;
					}
				});
			} else combinedResponse.ibes = {success: false};

			var toolate = false;
			var timeout = setTimeout(function(){
				var transfer = {
					data: Samuraj.FreightCalc.cookie,
					timeout: Samuraj.Translation.data.freight.timeouterrormsg || "This took to long. Please try again."
				};

				Samuraj.FreightCalc.resultstore[transfer.data.dt] = undefined;
				Samuraj.FreightCalc.loadDeliveryError(transfer);

				clearInterval(checkvar);
				toolate = true;
			},20000);

			var checkvar = setInterval(function(){
				if (Samuraj.Common.varExist(combinedResponse.irw) && Samuraj.Common.varExist(combinedResponse.ibes)){
					clearTimeout(timeout);
					clearInterval(checkvar);
					if (!toolate){
						if (combinedResponse.irw.success){ 
							//|| combinedResponse.ibes.success){
							if (Samuraj.Common.isFunction(input.success))
								input.success(combinedResponse, context);
						} else {
							if (Samuraj.Common.isFunction(input.failure))
								input.failure(combinedResponse, context);
						}
					} else {
						if (Samuraj.Common.isFunction(input.failure))
							input.failure(combinedResponse, context);
					}
					if (Samuraj.Common.isFunction(input.complete))
						input.complete(combinedResponse, context);
				}
			}, 20);


		},
		loadDeliveryDetail: function (input) {
			var c = Samuraj.Common,
				e = Samuraj.Element,
				p = Samuraj.FreightCalc,
				t = Samuraj.Translation;
			var elem;
			var type = "int"
			if (SamurajSetting.Country.numeric.decimalsum)
				type = "float";
			
			p.pricedetails = input.response.irw.responseJSON[1][0];
			p.pricedetails.clickcollectprice = t.data.freight.clickcollectprice;
			p.pricedetails.clickcollecttotalPrice = p.addPrices(p.pricedetails.clickcollectprice, p.pricedetails.totalPrice);
			p.pricedetails.clickcollecttotalFamilyPrice = p.addPrices(p.pricedetails.clickcollectprice, p.pricedetails.totalFamilyPrice);

			p.activateAll();
			p.activateContinueButtons();
			c.removeClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
			c.removeClass(document.getElementById("samuraj-ex-button-text-error"), "samuraj-show");
			c.removeClass(document.getElementById("samuraj-hs-button-text-error-klarna"), "samuraj-show");
			c.removeClass(document.getElementById("samuraj-ex-button-text-error-klarna"), "samuraj-show");
			c.removeClass(document.getElementById("samuraj-pp-button-text-error-klarna"), "samuraj-show");

			if (this.activePage == "cart"){
				elem = document.querySelectorAll('.tblShoppingCart td');
				for (var i=0; i<elem.length; i++) {
					if (c.varExist(elem[i], true)) e.remove(elem[i]);
					if (elem[i].className && elem[i].className == 'colProduct outOfStockItem') elem[i].className = 'colProduct';
					if (elem[i].className && elem[i].className == 'colOutOfStockErr') e.remove(elem[i]);
					if (elem[i].className && (elem[i].className == 'colPrice' ||  elem[i].className == 'colQuantity' ||  elem[i].className == 'colTotalPrice')) elem[i].style.setProperty("display", "block");
				}

				var errorMsg = document.getElementById("errorMsg");
				if (errorMsg) errorMsg.style.setProperty("display", "none");

			}

			if (this.activePage == "address"){
				var customerType = this.checkCustomerType();
				var fields= ["zipCode", "city", "address1", "address2"];
				var pp = this.getPickupPoint(input.data.zc);
				if (pp) {
					var pupdata = [pp.zipcode, pp.name, pp.address, pp.addressaddon];
					for (var i=0; i<fields.length; i++) {
						elem = document.querySelector("#signup_checkout_" + customerType + "_ship_" + fields[i]);
						if (elem) {
							elem.value = pupdata[i];
							if (window.irwDoFieldValidation) window.irwDoFieldValidation({target: elem});
						}
					}
				}
			}

			//Hide the loader animation
			p.freightCalcLoaders.hide();

			Samuraj.FreightCalc.parcel = false;
			var preShippingCost = "";
			var preDelTimeSlotStart = "";
			if (input.response.irw.responseJSON[0][0].code == 0) {
				preDelTimeSlotStart = input.response.irw.responseJSON[1][0].preDelTimeSlotStart;

				//if (input.data.dt == "hs") document.getElementById("samuraj-calc-" + input.data.dt + "-result-zc").innerHTML = input.data.zc;
				//document.getElementById("samuraj-calc-" + input.data.dt + "-result-na").innerHTML = input.data.na;
				document.getElementById("samuraj-calc-" + input.data.dt + "-result-delivery-date").innerHTML = preDelTimeSlotStart;
				
				//CFB addition
				if (Samuraj.FreightCalc.settings.cfbEnabled && input.data.dt == "hs") {
					var parcelDateElem = document.getElementById("samuraj-calc-hs-result-delivery-date");
					var truckDateElem = document.getElementById("samuraj-calc-hs-result-delivery-date-second");

					if (input.response.irw.responseJSON[1][0].parcelArticles.length > 0 && input.response.irw.responseJSON[1][0].truckArticles.length > 0){
						parcelDateElem.innerHTML = input.response.irw.responseJSON[1][0].parcelAvailableDate;
						truckDateElem.innerHTML = input.response.irw.responseJSON[1][0].truckAvailableDate;
					} else if (input.response.irw.responseJSON[1][0].parcelArticles.length > 0){
						parcelDateElem.innerHTML = input.response.irw.responseJSON[1][0].parcelAvailableDate;
						truckDateElem.innerHTML = "";
					} else if (input.response.irw.responseJSON[1][0].truckArticles.length > 0){
						parcelDateElem.innerHTML = input.response.irw.responseJSON[1][0].truckAvailableDate;
						truckDateElem.innerHTML = "";
					} else {
						parcelDateElem.innerHTML = "";
						truckDateElem.innerHTML = "";
					}
				}


				if (p.checkPvatInput() == "true") preShippingCost = input.response.irw.responseJSON[1][0].preShippingCostExclVAT;
				else preShippingCost = input.response.irw.responseJSON[1][0].preShippingCost;

				document.getElementById("samuraj-calc-" + input.data.dt + "-result-price").innerHTML = preShippingCost;

				//Check if delivery is parcel
				var parcel = false,
					pcw = Samuraj.FreightCalc.settings.parcelcheckwords,
					pdm = input.response.irw.responseJSON[1][0].preDelMethod;
				for (var i=0; i<pcw.length; i++){
					if (pdm.indexOf(pcw[i]) > -1)
						parcel = true;
				}
				Samuraj.FreightCalc.parcel = parcel;
				
				

				if (Samuraj.FreightCalc.activePage == "cart"){

					//CFB addition
					if (Samuraj.FreightCalc.settings.cfbEnabled && input.response.irw.responseJSON[1][0].parcelArticles.length > 0 && input.response.irw.responseJSON[1][0].truckArticles.length > 0) {
						document.getElementById("samuraj-hs-result-img").innerHTML = '<img class=\"samuraj-result-img\" src="' + t.data.freight.multipledeliveryresultimg + '" />';
						document.getElementById("samuraj-hs-result-text-1").innerHTML = t.data.freight.multipledeliveryresulttext1;
						document.getElementById("samuraj-hs-result-text-2").innerHTML = t.data.freight.multipledeliveryresulttext2;
						document.getElementById("samuraj-hs-result-text-3").innerHTML = t.data.freight.multipledeliveryresulttext3;
						document.getElementById("samuraj-hs-result-text-4").innerHTML = t.data.freight.multipledeliveryresulttext4;
						document.getElementById("samuraj-hs-result-text-5").innerHTML = t.data.freight.multipledeliveryresulttext5;
					} else if (parcel) {
						document.getElementById("samuraj-hs-result-img").innerHTML = '<img class=\"samuraj-result-img\" src="' + t.data.freight.parceldeliveryresultimg + '" />';
						document.getElementById("samuraj-hs-result-text-1").innerHTML = t.data.freight.parceldeliveryresulttext1;
						document.getElementById("samuraj-hs-result-text-2").innerHTML = t.data.freight.parceldeliveryresulttext2;
						document.getElementById("samuraj-hs-result-text-3").innerHTML = t.data.freight.parceldeliveryresulttext3;
						document.getElementById("samuraj-hs-result-text-4").innerHTML = t.data.freight.parceldeliveryresulttext4;
						document.getElementById("samuraj-hs-result-text-5").innerHTML = t.data.freight.parceldeliveryresulttext5;
					} else {
						document.getElementById("samuraj-hs-result-img").innerHTML = '<img class=\"samuraj-result-img\" src="' + t.data.freight.homeshippingresultimg + '" />';
						document.getElementById("samuraj-hs-result-text-1").innerHTML = t.data.freight.homeshippingresulttext1;
						document.getElementById("samuraj-hs-result-text-2").innerHTML = t.data.freight.homeshippingresulttext2;
						document.getElementById("samuraj-hs-result-text-3").innerHTML = t.data.freight.homeshippingresulttext3;
						document.getElementById("samuraj-hs-result-text-4").innerHTML = t.data.freight.homeshippingresulttext4;
						document.getElementById("samuraj-hs-result-text-5").innerHTML = t.data.freight.homeshippingresulttext5;
					}
					this.pricesregular = {
						freightprice: input.response.irw.responseJSON[1][0].preShippingCost,
						totalprice: input.response.irw.responseJSON[1][0].totalPrice,
						totalfamilyprice: input.response.irw.responseJSON[1][0].totalFamilyPrice,
						freightprice_exvat: input.response.irw.responseJSON[1][0].preShippingCostExclVAT,
						totalprice_exvat: input.response.irw.responseJSON[1][0].totalPriceExclVAT,
						totalfamilyprice_exvat: input.response.irw.responseJSON[1][0].totalFamilyPriceExclVat,
					}
				}

				c.removeClass(document.getElementById("samuraj-" + input.data.dt + "-result"), "samuraj-faded");
				if (input.nodelay)
					c.addClass(document.getElementById("samuraj-" + input.data.dt + "-result"), "samuraj-show");
				else {
					setTimeout(function(){
						c.addClass(document.getElementById("samuraj-" + input.data.dt + "-result"), "samuraj-show");
					}, 600);
				}

				//Express delivery

				if (this.activePage == "cart" && input.data.dt == "hs"){
					var addexpress = false,
						freightprice;

					if (input.response.ibes.success && !parcel){

						//Zone pricing
						if (this.data.lcdprices){
							if (this.data.lcdprices.activepricing == "fixed"){
								var fixed = this.data.lcdprices.fixed;
								if (fixed){
									freightprice = c.toInt({value: fixed, format: "price"});
									addexpress = true;
								}
							}
							if (this.data.lcdprices.activepricing == "zipcode_fixed"){
								var zipcode_fixed = this.data.lcdprices.zipcode_fixed;
								if (zipcode_fixed){
									for (var i=0; i<zipcode_fixed.length; i++){
										if (c.varExist(zipcode_fixed[i].zipcode, true) && c.varExist(zipcode_fixed[i].price, true)){
											var zipcode = zipcode_fixed[i].zipcode.split(",");
											for (var j=0; j<zipcode.length; j++){
												if (zipcode[j] == input.data.zc){
													freightprice = c.toInt({value: zipcode_fixed[i].price, format: "price"});
													addexpress = true;
												}
											}
										}
									}
								}
							}
							if (this.data.lcdprices.activepricing == "mirror_ccd"){
								//Mirror CCD pricing
								var mirror_ccd = this.data.lcdprices.mirror_ccd;
								if (mirror_ccd){
									freightprice = c.toInt({value: this.pricesregular.freightprice, format: "price"}) + c.toInt({value: mirror_ccd, format: "price"});
									addexpress = true;
								}
							}
							if (this.data.lcdprices.activepricing == "order_value"){
								//Mirror CCD pricing
								var order_value = this.data.lcdprices.order_value;
								
								if (order_value){
									freightprice = 0;
									var goodsvalue = c.toInt({value: this.pricesregular.totalprice, format: "price"}) - c.toInt({value: this.pricesregular.freightprice, format: "price"}) ;
									for (var i=order_value.length-1; i>-1; i--){
										if (!order_value[i].value || goodsvalue <= order_value[i].value)
											freightprice = order_value[i].price;
									}										
									addexpress = true;
								}
							}
						}

					}

					if (addexpress){
						//if (!t.data.freight.expressdeliveryresulttext2staticprice == "")
						c.removeClass(document.getElementById("samuraj-ex-result"), "samuraj-faded");
						if (input.nodelay)
							c.addClass(document.getElementById("samuraj-ex-result"), "samuraj-show");
						else {
							setTimeout(function(){
								c.addClass(document.getElementById("samuraj-ex-result"), "samuraj-show");
							}, 600);
						}

						//Set express delivery freight


						var totalprice = freightprice,
							totalfamilyprice = freightprice;
						elem = document.querySelector("#beginCheckoutBottom .shoppingSubTotalExlDelivery .nonfamilyPrice");
						if (!elem) elem = document.querySelector(".shoppingListPriceRow .shoppingSubTotalExlDelivery .nonfamilyPrice");
						if (!elem) elem = document.querySelector("#beginCheckoutFamilyBottom .shoppingSubTotalExlDelivery .nonfamilyPrice");
						if (elem) totalprice = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"}) + freightprice;
						elem = document.querySelector("#beginCheckoutBottom .shoppingSubTotalExlDelivery .familyPrice");
						if (!elem) elem = document.querySelector(".shoppingListPriceRow .shoppingSubTotalExlDelivery .familyPrice");
						if (!elem) elem = document.querySelector("#beginCheckoutFamilyBottom .shoppingSubTotalExlDelivery .familyPrice");
						if (elem) totalfamilyprice = Samuraj.Common.toInt({value: elem.innerHTML, format: "price"}) + freightprice;


						this.pricesexpress = {
							freightprice: freightprice,
							totalprice: totalprice,
							totalfamilyprice: totalfamilyprice,
							freightprice_exvat: freightprice / 1.25,
							totalprice_exvat: totalprice / 1.25,
							totalfamilyprice_exvat: totalfamilyprice / 1.25,
						}
						var currentfreightprice = this.pricesexpress.freightprice;
						if(!this.mobileCheckout()){
							if (this.checkPvatInput() == "true") {
								currentfreightprice = this.pricesexpress.freightprice_exvat;
							}
						}
						var elem = document.getElementById("samuraj-calc-ex-result-price")
						if (elem) elem.innerHTML = c.formatNumber({value: currentfreightprice, type: type, format: "price"});


						if (input.data.ex == "true")
							this.toggleExpress("ex");
						else
							this.toggleExpress("hs");


					} else {
						this.toggleExpress("hs");
						input.data.ex = "false";
					}
				}
			}



			if (input.data.dt == "pp") {
				var pp = this.getPickupPoint(input.data.zc);
				if (pp) {
					if (c.varExist(pp.mapurl, true)){
						document.getElementById('samuraj-calc-pp-map-url').href = pp.mapurl;
					}
				}
			} else {
				var selection = {
					address: "",
					addressaddon: "",
					mapurl: ""
				};
			}
			this.setInfoCookie('{"zc":"' + input.data.zc + '","na":"' + input.data.na + '","dt":"' + input.data.dt + '","ex":"' + input.data.ex + '"}');


			if (this.activePage == "cart"){
				var elem;

				if (input.data.dt == "pp")
					this.changePrices(this.pricesregular);

				elem = document.querySelectorAll(".notYetCalc");
				for (var i=0; i<elem.length; i++){
					elem[i].style.setProperty("display", "none");
				}

				//Enable Continue to checkout button
				//Samuraj.FreightCalc.activateContinueButtons();
				//elem = document.getElementById("samuraj-hs-button");
				//if (elem)
				//    c.removeClass(elem, "disabledButton");


				elem = document.querySelectorAll(".calcError");
				for (var i=0; i<elem.length; i++) {
					if (elem[i].style.display != "none") elem[i].style.display = "none";
				}

				// for statistics
				if (input.response.irw.responseJSON[1][0].milliSecDelTime != '') document.updateAllForm.milliSecDelTime.value = input.response.irw.responseJSON[1][0].milliSecDelTime;
				else document.updateAllForm.milliSecDelTime.value = "0";

				if (c.varExist(document.updateAllForm)){
					document.updateAllForm.milliSecDelTime.value = input.response.irw.responseJSON[1][0].milliSecDelTime;
					document.updateAllForm.unFormattedShippingCost.value = input.response.irw.responseJSON[1][0].unFormatedPreShippingCost;
					document.updateAllForm.errCodeForCalculateBtn.value = "0";
					// First time while calculating the freight using AJAX call, we should set the zipCode to form so that
					// stats script will pick up the value and send it.
					if (!document.updateAllForm.zipCode) {
						var zipCodeNode = document.createElement('input');
						zipCodeNode.setAttribute('name', 'zipCode');
						zipCodeNode.setAttribute('type', 'hidden');
						zipCodeNode.setAttribute('id', 'zipCode');
						zipCodeNode.setAttribute('value', input.data.zc);
						document.getElementById('updateAllForm').appendChild(zipCodeNode);
					}
					irwstatSend();
				}
			}

			if (this.activePage == "address"){
				var elem;
				if(!this.mobileCheckout()){
					var freightprice = input.response.irw.responseJSON[1][0].preShippingCost;
					if (this.checkPvatInput() == "true") freightprice = input.response.irw.responseJSON[1][0].preShippingCostExclVAT;

					var elem = document.querySelector("#txtTotalDelivery span");
					if (elem) elem.innerHTML = freightprice;

					elem = document.querySelector("span#txtTotal") || document.querySelector("div#totalValue");
					if (elem) elem.innerHTML = this.addPrices(freightprice, elem.innerHTML);

					elem = document.querySelector("span#txtFamilyTotal") || document.querySelector("div#totalValueFamily");
					if (elem) elem.innerHTML = this.addPrices(freightprice, elem.innerHTML);

				}
			}

			elem = document.querySelectorAll(".notYetCalc");
			for (var i=0; i<elem.length; i++){
				elem[i].style.setProperty("display", "none");
			}

		},

		loadDeliveryError: function(input) {
			var c = Samuraj.Common,
				p = Samuraj.FreightCalc;
			var elem;
			this.activateAll();
			if (input.data.dt == "pp" || input.data.dt == "cc"){
				this.deactivateContinueButtons();
				c.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
				c.addClass(document.getElementById("samuraj-pp-button-text-error-klarna"), "samuraj-show");
			} else {
				if (Samuraj.FreightCalc.settings.forcezipcodeentry){
					this.deactivateContinueButtons();
					c.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
					c.addClass(document.getElementById("samuraj-ex-button-text-error"), "samuraj-show");
					c.addClass(document.getElementById("samuraj-hs-button-text-error-klarna"), "samuraj-show");
					c.addClass(document.getElementById("samuraj-ex-button-text-error-klarna"), "samuraj-show");
				} else
					this.activateContinueButtons();
			}

			var calcResultErrorSpan = document.getElementById('samuraj-calc-' + input.data.dt + '-result-error-span');
			if (input.response) 
				calcResultErrorSpan.innerHTML = input.response.irw.responseJSON[0][0].msg;
			if (input.timeout)
				calcResultErrorSpan.innerHTML = input.timeout;

			setTimeout(function(){
				Samuraj.Common.addClass(document.getElementById("samuraj-calc-" + input.data.dt + "-result-error-div"), "samuraj-show");
			}, 600);
			//Hide result dropdowns
			c.removeClass(document.getElementById("samuraj-" + input.data.dt + "-result"), "samuraj-faded");
			c.removeClass(document.getElementById("samuraj-" + input.data.dt + "-result"), "samuraj-show");

			//Hide express delivery result dropdown
			c.removeClass(document.getElementById("samuraj-ex-result"), "samuraj-faded");
			c.removeClass(document.getElementById("samuraj-ex-result"), "samuraj-show");

			//Hide loader animation
			p.freightCalcLoaders.hide();

			var errorMsg = document.getElementById("errorMsg");
			if (errorMsg) errorMsg.style.setProperty("display", "none");

			if (this.activePage == "address"){
				var customerType = this.checkCustomerType();
				var fields= ["zipCode", "city", "address1", "address2"];
				for (var i=0; i<fields.length; i++) {
					elem = document.querySelector("#signup_checkout_" + customerType + "_ship_" + fields[i]);
					if (elem) elem.value = "";
				}
			}

			if (this.activePage == "cart"){

				this.clearZipCodeForTransfer();
				this.cleardeliveryAddressDifferentForTransfer();

				if (input.response){
					/* START ITEM LEVEL ERROR MESSAGE */
					// Populates the nonBuybaleArticle array
					
					var nonbuyable = input.response.irw.responseJSON[1][0].nonBuyableArticles || "";
					if (nonbuyable !== "") {
						var articles = "" + nonbuyable;
						var articlesarr = articles.split("_");
					}
					var errmsg = input.response.irw.responseJSON[1][0].itemLevelErrorMessage || "";
					var freightcalcmsg = input.response.irw.responseJSON[1][0].msg || "";
					
					
					if (articlesarr){
						if (this.mobileCheckout()){

							for (var i=0; i<articlesarr.length; i++){
								var selector = "li.itemNo" + articlesarr[i];
								var elems = document.querySelectorAll(selector);
								for (var j=0; j<elems.length; j++){
									elems[j].style.setProperty("border", "1px solid #ff0000");
									var cell = elems[j].querySelector(".ikea-product-pricetag-cell");
									if (cell){
										var elem = document.createElement("div");
										elem.className = "ikea-product-pricetag-error";
										elem.innerHTML = errmsg;
										cell.appendChild(elem);
									}
								}
							}
							
						} else {
														
							for (var i=0; i<articlesarr.length; i++){
								var selector = '.outsideProductRow[data-productid="' + articlesarr[i].toUpperCase() + '"]';
								var elems = document.querySelectorAll(selector);
								for (var j=0; j<elems.length; j++){
									var errordiv = elems[j].querySelector(".noError");
									if (errordiv){
										c.removeClass(errordiv, "noError");
										c.addClass(errordiv, "productError");
										var elem = document.createElement("div");
										elem.className = "colOutOfStockErr";
										elem.innerHTML = errmsg;
										errordiv.appendChild(elem);
									}
								}
							}
						}
					}
				
					/* END ITEM LEVEL ERROR MESSAGE */

					//deliveryDetailEntryDiv.style.display = 'none';
					// for statistics
					if (c.varExist(document.updateAllForm)){
						document.updateAllForm.errCodeForCalculateBtn.value = parseInt(input.response.irw.responseJSON[0][0].code);
						document.updateAllForm.milliSecDelTime.value = "0";
						irwstatSend();
					}
				}
			}
		},
		checkCustomerType: function() {
			var customerTypePrivateInput = document.getElementById("privateCustomer");
			if (customerTypePrivateInput) {
				if (customerTypePrivateInput.getAttribute("checked")) {
					return "private";
				} else {
					return "business";
				}
			} else {
				var customerTypePrivateZipcode = document.getElementById("signup_checkout_private_zipCode");
				if (customerTypePrivateZipcode) {
					return "private";
				} else {
					return "business";
				}
			}
		},
		addressLoadGeneralSettings: function(callback){
			var c = Samuraj.Common;
			var elem, zipcode, zipcode_oa;

			var ct = this.checkCustomerType();

			//Getting the saved zipcode and replacing the transferred zipcode,
			zipcode = Samuraj.FreightCalc.getSaved("freight_zipcode");

			elem = document.getElementById("signup_checkout_" + ct + "_zipCode");
			if (elem) {
				elem.setAttribute("size", "6");
				elem.setAttribute("maxlength", Samuraj.FreightCalc.settings.zipcodedigits);
				if (this.cookie.dt == "pp") elem.value = "";
				else elem.value = this.cookie.zc;
				if (c.varExist(zipcode)) elem.value = zipcode;
				if (this.cookie.dt == "hs") elem.value = this.cookie.zc;

				Samuraj.Common.addEvent(elem, "change", function(e){
					if (e.target.value.length == Samuraj.FreightCalc.settings.zipcodedigits){
						if (Samuraj.FreightCalc.cookie.dt == "hs"){
							Samuraj.FreightCalc.setInfoCookie('{"zc":"' + e.target.value + '","na":"","dt":"hs","ex":"false"}');
						}
						Samuraj.FreightCalc.setSaved("freight_zipcode", e.target.value);
					}
				});
			}

			//Setting fields as number for mobile
			var fields =["zipCode", "phone2", "userProfileField1", "phone1"];
			for (var i=0; i<fields.length; i++){
				elem = document.getElementById("signup_checkout_" + ct + "_" + fields[i]);
				if (elem) {
					elem.setAttribute("type", "number");
					elem.setAttribute("min", "0");
					elem.setAttribute("inputmode", "numeric");
					elem.setAttribute("pattern", "[0-9]*");
				}
			}


			//Changing the logout link
			elem = document.querySelector(".logoutWelcome a");
			if (elem) {
				elem.href = "#";
				if (this.mobileCheckout())
					elem.setAttribute("onclick", "Samuraj.User.logOut({redirecturl: 'm2cart'});");
				else
					elem.setAttribute("onclick", "Samuraj.User.logOut({redirecturl: 'irwcart', includecontentclass: 'samuraj-irw'});");
			}

			//Change the text of the delivery to other address
			elem = document.querySelector("div.contentAreaBox form div.accordionLink span.accordionText");
			if (elem) elem.innerHTML = Samuraj.Translation.data.checkout.deliveytootheraddressheading;

			callback();

		},
		addressLoadAdditionalSettings: function(callback){
			var c = Samuraj.Common;
			var elem, zipcode, zipcode_oa;

			var ct = this.checkCustomerType();

			//Getting the saved zipcode and replacing the transferred zipcode,
			zipcode_oa = Samuraj.FreightCalc.getSaved("freight_zipcode_oa");

			//Removing the errormessage at the bttom
			elem = document.querySelector("#errorMessageSubmit p");
			if (elem) elem.innerHTML = "";

			//Setting back the zipcode for shipping address
			elem = document.getElementById("signup_checkout_" + ct + "_ship_zipCode");
			if (elem) {
				elem.setAttribute("size", "6");
				elem.setAttribute("maxlength", Samuraj.FreightCalc.settings.zipcodedigits);
				if (this.cookie.dt == "pp") elem.value = "";
				else elem.value = this.cookie.zc;
				if (c.varExist(zipcode_oa)) elem.value = zipcode_oa;

				Samuraj.Common.addEvent(elem, "change", function(e){
					if (e.target.value.length == Samuraj.FreightCalc.settings.zipcodedigits){
					   Samuraj.FreightCalc.setSaved("freight_zipcode_oa", e.target.value);
					}
				});
			}

			//Setting fields as number for mobile
			var fields =["zipCode", "phone2", "phone1"];
			for (var i=0; i<fields.length; i++){
				elem = document.getElementById("signup_checkout_" + ct + "_ship_" + fields[i]);
				if (elem) {
					elem.setAttribute("type", "number");
					elem.setAttribute("min", "0");
					elem.setAttribute("inputmode", "numeric");
					elem.setAttribute("pattern", "[0-9]*");
				}
			}

			//Attaching change event to the input fields for transferring them to the delivery address fields if pickup point delivery option is selected
			var fields =["organizationName", "firstName", "lastName", "phone2", "phone1"];
			for (var i=0; i<fields.length; i++){
				elem = document.getElementById("signup_checkout_" + ct + "_" + fields[i]);
				if (elem) {
					elem.field = fields[i];
					Samuraj.Common.addEvent(elem, "blur", function(e){
						if (Samuraj.FreightCalc.cookie.dt == "pp"){
							var ct = Samuraj.FreightCalc.checkCustomerType()
							var elem = document.getElementById("signup_checkout_" + ct + "_ship_" + e.target.field);
							if (elem) {
								elem.value = e.target.value;
								if (window.irwDoFieldValidation) window.irwDoFieldValidation({target: elem});
							}
						}
					});
				}
			}

			//Hiding the savedaddress section
			elem = document.getElementById("signup_checkout_" + ct + "_savedaddress_field");
			if (elem) elem.style.setProperty("display", "none");
			elem = document.querySelector(".formField.formFieldTop");
			if (elem) elem.style.setProperty("display", "none");
			// IRW
			elem = document.getElementById("signup_checkout_" + ct + "_savedaddress");
			if (elem) elem.parentNode.style.setProperty("display", "none");

			//Checking for the savedaddress dropdown and selecting the first addres if it exists
			elem = document.getElementById("signup_checkout_" + ct + "_savedaddress");
			if (elem) elem = elem.options[1].value;
			if (elem) {
				var temp = elem.slice(elem.indexOf("addressIdDelivery"), elem.length);
				var value = temp.slice(18, temp.indexOf("&"));
				elem = document.getElementById("signup_checkout_" + ct);
				if (elem) {
					elem = elem.querySelector("input[name=addressIdDelivery]");
					if (elem) elem.value = value;
					else {
						elem = document.createElement("input");
						elem.type = "hidden";
						elem.name = "addressIdDelivery";
						elem.value = value;
						document.getElementById("signup_checkout_" + ct).appendChild(elem);
					}
				}
			}
			//Set deliveryAddressDifferent to 1
			elem = document.getElementById("orderItemDisplayForm");
			if (elem){
				elem = elem.querySelector("input[name=deliveryAddressDifferent]");
				if (elem) elem.value = "1";
				else {
					elem = document.createElement("input");
					elem.type = "hidden";
					elem.name = "deliveryAddressDifferent";
					elem.value = "1";
					document.getElementById("orderItemDisplayForm").appendChild(elem);
				}
			}
			elem = document.getElementsByName("login");
			if (elem) {
				if (elem[0]) {
					elem = elem[0].querySelector("input[name=deliveryAddressDifferent]");
					if (elem) elem.value = "1";
					else {
						elem = document.createElement("input");
						elem.type = "hidden";
						elem.name = "deliveryAddressDifferent";
						elem.value = "1";
						document.getElementsByName("login")[0].appendChild(elem);
					}
				}
			}

			//Setting back the other shipping fields
			var fields_ship = ["organizationName", "firstName", "lastName", "address1", "address2", "city", "phone2", "phone1"],
				jsonobj = {},
				freight_oafields = Samuraj.Common.getStorage("freight_oafields");
			if (c.varExist(freight_oafields)) {
				jsonobj = JSON.parse(freight_oafields);
				this.oafields = jsonobj;
			}
			for (var i=0; i<fields_ship.length; i++){
				elem = document.querySelector("#signup_checkout_" + ct + "_ship_" + fields_ship[i]);
				if (elem) {
					if (this.cookie.dt == "pp") elem.value = "";
					else if (c.varExist(jsonobj[fields_ship[i]])) elem.value = jsonobj[fields_ship[i]];

					Samuraj.Common.addEvent(elem, "change", function(e){
						var fields_ship = ["organizationName", "firstName", "lastName", "address1", "address2", "city", "phone2", "phone1"],
							jsonobj = {},
							elem,
							ct = Samuraj.FreightCalc.checkCustomerType();
						for (var j=0; j<fields_ship.length; j++){
							elem = document.querySelector("#signup_checkout_" + ct + "_ship_" + fields_ship[j]);
							if (elem) jsonobj[fields_ship[j]] = elem.value;
						}
						Samuraj.Common.setStorage("freight_oafields", JSON.stringify(jsonobj));
					});
				}
			}
		},

		validateRemoveError: function(elem){
			var c = Samuraj.Common;
			if (elem) {
				if (this.mobileCheckout()){
					c.removeClass(elem, "validateFormFail");
					elem = elem.parentNode.parentNode.querySelectorAll(".formError");
					if (elem)
						for (var i=0; i<elem.length; i++)
							elem[i].innerHTML = "";
				} else {
					c.removeClass(elem, "fieldError");
					//while (!c.hasClass("formField"))
					//  elem = elem.parentNode;
					elem = elem.parentNode.parentNode
					var elemtemp = elem.querySelectorAll(".formError .errorMessage");
					if (elemtemp.length>0){
						for (var i=0; i<elemtemp.length; i++)
							elemtemp[i].innerHTML = "";
					} else {
						elem = elem.parentNode;
						elemtemp = elem.querySelectorAll(".formError .errorMessage");
						if (elemtemp.length>0)
							for (var i=0; i<elemtemp.length; i++)
								elemtemp[i].innerHTML = "";
					}
				}
			}
		},
		validateAllAddressFields: function(){
			var c = Samuraj.Common,
				elem,
				customerType = this.checkCustomerType();

			var fields = ["organizationName", "firstName", "lastName", "address1", "address2", "zipCode", "city", "email1", "email1retype", "phone2", "userProfileField1", "phone1"];
			var fields_ship = ["organizationName", "firstName", "lastName", "address1", "address2", "zipCode", "city", "phone2", "phone1"];

			for (var i=0; i<fields.length; i++){
				elem = document.querySelector("#signup_checkout_" + customerType + "_" + fields[i]);
				if (elem) if (window.irwDoFieldValidation) window.irwDoFieldValidation({target: elem});
			}
			for (var i=0; i<fields_ship.length; i++){
				elem = document.querySelector("#signup_checkout_" + customerType + "_ship_" + fields_ship[i]);
				if (elem) if (window.irwDoFieldValidation) window.irwDoFieldValidation({target: elem});
			}
		},


		FreightCalcCart: function(callback) {
			var elem;
			elem = document.querySelectorAll(".titleHomeDelivery")
			for (var i=0; i<elem.length; i++) {
				elem[i].innerHTML = "<p><span>" + Samuraj.Translation.data.freight.topcalclinktext + "</span></p><br>";
			}


			if (!this.mobileCheckout()){
				//Top calculation adjustments on desktop
				/*

				var productsInCart = 0;
				elem = document.getElementsByClassName("outsideProductRow");
				if (elem) productsInCart = elem.length;
				if (productsInCart > 5){
					elem = document.querySelector("div.wrapDeliveryAndCostTop");
					if (elem) elem.style.setProperty("display", "block");

					var calculationTop = document.getElementById("deliveryAllWrapTop");
					if (calculationTop){
						elem = Samuraj.Element.getChildNodes(calculationTop);
						for (var i=0; i < elem.length;i++){
							if (elem[i].style) elem[i].style.setProperty("display", "none");
						}

						elem = document.createElement("style");
						elem.innerHTML = "#samuraj-top-calc-link img{position:absolute; height:70px}#samuraj-top-calc-link-text{position:absolute;left:150px;top:55px;font-size:16px}.samuraj-top-calc-link:hover{cursor:pointer; text-decoration: underline}div#deliveryAllWrapTop .ccWrapper{display: none !important;}";
						document.getElementsByTagName("head")[0].appendChild(elem);

						var elem = document.createElement("div");
						elem.innerHTML = '<a id="samuraj-top-calc-link" class="samuraj-top-calc-link" onclick="Samuraj.Anchor.go(\'#samuraj-pup-master-cont\', -100)">' +
							  '<img src="' + Samuraj.Translation.data.freight.homeshippingresultimg + '" style="left:40px; top:20px" />' +
							  '<span id="samuraj-top-calc-link-text" class="samuraj-top-calc-link">' + Samuraj.Translation.data.freight.topcalclinktext + '</span></a>';
						calculationTop.appendChild(elem);

						// Hide the top Continue button
						elem = document.getElementById("jsButton_beginCheckOut_01");
						if (elem) {
							elem = elem.parentNode;
							elem.style.setProperty("visibility", "hidden");
						}
					}
				}*/


				//Expand accordion link
				/*
				var elem = document.createElement("span");
				elem.id = "samuraj-expand-accordion";
				elem.setAttribute("style", "cursor: pointer; float: right; color: #ccc; font-size: 18px; font-weight: bold;");
				elem.innerHTML = "+";
				elem.setAttribute("onClick", "Samuraj.FreightCalc.expandAccordion()");
				document.querySelector("#deliveryCostCalculatorBottom .titleHomeDelivery p").appendChild(elem);
				*/
			}


			var htmlradiotemplate = function(){
				var display_pickuppoints = "";
				if (Samuraj.FreightCalc.settings.pickuppoints)
					display_pickuppoints = "" +
					"  <div id=\"samuraj-cont-pp-radio-heading\" class=\"samuraj-radio-heading-div\"><input id=\"samuraj-cont-pp-radio-heading-input\" class=\"samuraj-radio-heading-button\" type=\"radio\" name=\"shippingtype\" value=\"pp\" onclick=\"Samuraj.FreightCalc.toggleDeliveryType('pp', 'manual')\"><label class=\"samuraj-radio-heading-label\" for=\"samuraj-cont-pp-radio-heading-input\"><%this.pickuppoint%></label></div>" +
					"  <div id=\"samuraj-cont-pp\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
					"    <%this.pickuppointHTML%>" +
					"  </div>";
				var display_clickcollect = "";
				if (Samuraj.FreightCalc.settings.clickcollect)
					display_clickcollect = "" +
					"  <div id=\"samuraj-cont-cc-radio-heading\" class=\"samuraj-radio-heading-div\"><input id=\"samuraj-cont-cc-radio-heading-input\" class=\"samuraj-radio-heading-button\" type=\"radio\" name=\"shippingtype\" value=\"cc\" onclick=\"Samuraj.FreightCalc.toggleDeliveryType('cc', 'manual')\"><label class=\"samuraj-radio-heading-label\" for=\"samuraj-cont-cc-radio-heading-input\"><%this.clickcollect%></label></div>" +
					"  <div id=\"samuraj-cont-cc\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
					"    <%this.clickcollectHTML%>" +
					"  </div>";

				var html = "" +
					"<div id=\"samuraj-pup-master-cont\">" +
					"  <div class=\"samuraj-master-heading\"><%this.masterheading%></div>" +
					"  <div id=\"samuraj-cont-hs-radio-heading\" class=\"samuraj-radio-heading-div\"><input id=\"samuraj-cont-hs-radio-heading-input\" class=\"samuraj-radio-heading-button\" type=\"radio\" name=\"shippingtype\" value=\"hs\" onclick=\"Samuraj.FreightCalc.toggleDeliveryType('hs', 'manual')\"><label class=\"samuraj-radio-heading-label\" for=\"samuraj-cont-hs-radio-heading-input\"><%this.homeshipping%></label></div>" +
					"  <div id=\"samuraj-cont-hs\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
					"    <%this.homeshippingHTML%>" +
					"  </div>" +
					display_pickuppoints + 
					display_clickcollect + 
					"  </div>" +
					"</div>";

				return html;	
			}

			var htmlstateselector = function(){
				if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12") {
					return "" +
					"	   <select id=\"samuraj-hs-input-lower-field-state\" class=\"samuraj-hs-input-field-state\">" +
					"	     <option id=\"samuraj-hs-option-default\" value=\"\"><%this.statedropdownfieldplaceholder%></option>" +
					"	   </select>" +
					"	   <div id=\"samuraj-state-spacer\" style=\"height:5px\"></div>";
				} else return "";
			}

			var htmlcalculatebutton = function(){
				if (Samuraj.FreightCalc.settings.calculatebutton) {
					if (Samuraj.FreightCalc.mobileCheckout()){
						return "" +
						"	<div id=\"samuraj-calculation-button-spacer\" style=\"height:5px\"></div>" +
						"	<div id=\"samuraj-hs-calculate-button\" data-corners=\"true\" data-shadow=\"true\" data-iconshadow=\"true\" data-wrapperels=\"span\" data-icon=\"null\" data-iconpos=\"null\" data-theme=\"blueBtn\" class=\"ui-btn ui-btn-up-purchase ui-shadow ui-btn-corner-all ui-submit ui-btn-up-blueBtn ui-disabled samuraj-pup-viscon samuraj-show\" aria-disabled=\"false\">" +
						"		<span class=\"ui-btn-inner ui-btn-corner-all\">" +
						"			<span id=\"samuraj-hs-calculate-button-caption\" class=\"ui-btn-text\">" + Samuraj.Translation.data.freight.calculatebuttoncaption + "</span>" +
						"		</span>" +
						"		<input type=\"button\" data-theme=\"blueBtn\" id=\"samuraj-hs-calculate-button-caption-value\" value=\"" + Samuraj.Translation.data.freight.calculatebuttoncaption + "\" class=\"ui-btn-hidden\" aria-disabled=\"false\" onclick=\"Samuraj.FreightCalc.homeshippingButtonClick(); return false;\">" +
						"	</div>";
					} else {
						return "" +
						"	<div id=\"samuraj-calculation-button-spacer\" style=\"height:5px\"></div>" +
						"	<div class=\"buttonContainer disabledButton samuraj-pup-viscon samuraj-show\" id=\"samuraj-hs-calculate-button\" style=\"width: 200px;\">" +
						"		<a class=\"blueButton\" id=\"samuraj-hs-calculate-button-link\" href=\"#\" onclick=\"Samuraj.FreightCalc.homeshippingButtonClick(); return false;\">" +
						"			<div class=\"buttonLeft buttonLeftBlue\">&nbsp;</div>" +
						"			<div class=\"buttonCaption buttonCaptionBlue\">" +
						"				<input id=\"samuraj-hs-calculate-button-caption-value\" type=\"button\" value=\"" + Samuraj.Translation.data.freight.calculatebuttoncaption + "\" disabled=\"disabled\" style=\"cursor: default; width: 200px;\">" +
						"			</div>" +
						"			<div class=\"buttonRight buttonRightBlue\">&nbsp;</div>" +
						"		</a>" +
						"	</div>";
					}
				} else return "";
			}

			var input = {
				init: function() {
					this.htmloptions = this.htmloptionsfunction();
					return this;
				},

				htmloptionsfunction: function() {
					var options = {};

					options.homeshippingHTML = "" +
					"    <div id=\"samuraj-hs-calc-heading-lower\" class=\"samuraj-radio-heading samuraj-pup-viscon\"><%this.homeshippingtext%></div>" +
					"    <div id=\"samuraj-hs-result-restriction-cont\" class=\"samuraj-radio-heading samuraj-border-red samuraj-pup-viscon\">" +
					"       <span id=\"samuraj-hs-result-restriction-text\"><b><%this.deliveryrestrictiontext%></b></span>: <span id=\"samuraj-hs-result-restriction\"></span>" +
					"    </div>" +
					"    <div id=\"samuraj-hs-input-lower-div\" class=\"samuraj-hs-input-div\">" +
					
					htmlstateselector() +

					"      <input id=\"samuraj-hs-input-lower-field\" class=\"samuraj-hs-input-field\" placeholder=\"<%this.inputfieldplaceholder%>\" size=\"6\" maxlength=\"" + Samuraj.FreightCalc.settings.zipcodedigits +"\" type=\"number\" min=\"0\" inputmode=\"numeric\" pattern=\"[0-9]*\" onclick=\"Samuraj.FreightCalc.homeShippingInputClick(this);\" onkeyup=\"Samuraj.FreightCalc.homeShippingInputKeyUp(this);\">" +
					
					htmlcalculatebutton() +

					"      <%this.freightcalcloader%>" +
					//"      <span id=\"samuraj-hsGifLoadCalcBottom\" style=\"position: relative; left: 10px; top: 5px;\"></span>" +
					"    </div>" +
					"    <div id=\"samuraj-hs-result\" class=\"samuraj-calc-result-div samuraj-pup-viscon\"><div id=\"samuraj-hs-result-inner\" class=\"samuraj-calc-result-div-inner\">" +
					"      <table style=\"width: 100%\">" +
					"        <tbody>" +
					"          <tr>" +
					"            <td rowspan=\"4\" style=\"width: 90px; vertical-align: top; padding-right: 10px;\"><span id=\"samuraj-hs-result-img\"><img class=\"samuraj-result-img\" src=\"<%this.homeshippingresultimg%>\"></span></td>" +
					"            <td style=\"padding-bottom: 5px;\" valign=\"bottom\">" +
					"               <span id=\"samuraj-hs-result-text-1\"><%this.homeshippingresulttext1%></span>" +
					"            </td>" +
					"            <td rowspan=\"4\" style=\"vertical-align: center; width:25px; padding-right: 5px\"><input id=\"samuraj-hs-result-checkbox-input\" class=\"samuraj-result-checkbox\" style=\"height: 20px; width: 20px; display: block;\" type=\"checkbox\" onclick=\"Samuraj.FreightCalc.toggleExpress('hs', 'auto');\"></td>"+
					"          </tr>" +
					"          <tr>" +
					//"            <td><span id=\"samuraj-calc-hs-result-zc\"></span> <span id=\"samuraj-calc-hs-result-name\"></span>: <b><span id=\"samuraj-calc-hs-result-price\"></span></b></td>" +
					"            <td><span id=\"samuraj-hs-result-text-2\"><%this.homeshippingresulttext2%></span>: <b><span id=\"samuraj-calc-hs-result-price\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"padding-top: 5px\" valign=\"top\"><span id=\"samuraj-hs-result-text-3\"><%this.homeshippingresulttext3%></span> <b><span id=\"samuraj-calc-hs-result-delivery-date\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"padding-top: 5px\" valign=\"top\"><span id=\"samuraj-hs-result-text-4\"><%this.homeshippingresulttext4%></span> <b><span id=\"samuraj-calc-hs-result-delivery-date-second\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td colspan=\"2\" style=\"padding-top: 5px\" valign=\"bottom\"><span id=\"samuraj-hs-result-text-5\"><%this.homeshippingresulttext5%></span></td>" +
					"          </tr>" +
					"        </tbody>" +
					"      </table>" +
					"    </div></div>" +
					"    <div id=\"samuraj-ex-result\" class=\"samuraj-calc-result-div samuraj-pup-viscon\"><div id=\"samuraj-ex-result-inner\" class=\"samuraj-calc-result-div-inner\">" +
					"      <table style=\"width: 100%\">" +
					"        <tbody>" +
					"          <tr>" +
					"            <td rowspan=\"4\" style=\"width: 90px; vertical-align: top; padding-right: 10px;\"><span id=\"samuraj-ex-result-img\"><img class=\"samuraj-result-img\" src=\"<%this.expressdeliveryresultimg%>\"></span></td>" +
					"            <td style=\"padding-bottom: 5px;\" valign=\"bottom\">" +
					"               <span id=\"samuraj-ex-result-text-1\"><%this.expressdeliveryresulttext1%></span>" +
					"            </td>" +
					"            <td rowspan=\"4\" style=\"vertical-align: center; width:25px; padding-right: 5px\"><input id=\"samuraj-ex-result-checkbox-input\" class=\"samuraj-result-checkbox\" style=\"height: 20px; width: 20px; display: block;\" type=\"checkbox\" onclick=\"Samuraj.FreightCalc.toggleExpress('ex', 'auto');\"></td>"+
					"          </tr>" +
					"          <tr>" +
					"            <td><span id=\"samuraj-ex-result-text-2\"><%this.expressdeliveryresulttext2%></span>: <b><span id=\"samuraj-calc-ex-result-price\"><%this.expressdeliveryresulttext2staticprice%></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"padding-top: 5px\" valign=\"top\"><span id=\"samuraj-ex-result-text-3\"><%this.expressdeliveryresulttext3%></span> <b><span id=\"samuraj-calc-ex-result-delivery-date\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td colspan=\"2\" style=\"padding-top: 5px\" valign=\"bottom\"><span id=\"samuraj-ex-result-text-4\"><%this.expressdeliveryresulttext4%></span></td>" +
					"          </tr>" +
					"        </tbody>" +
					"      </table>" +
					"    </div></div>" +
					"    <div id=\"samuraj-calc-hs-result-error-div\" class=\"samuraj-calc-error samuraj-pup-viscon\"><span id=\"samuraj-calc-hs-result-error-span\"></span></div>" +

					"    <div id=\"samuraj-hs-result-checkpickuppoint-cont\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
					"       <div id=\"samuraj-hs-result-checkpickuppoint-text\"><b><%this.checkpickuppointtext1%></b></div>" +
					"		<div><span><%this.checkpickuppointtext2%></span></div>" +
					"    </div>" +
					/*
					"    <div id=\"samuraj-hs-result-checkpickuppoint-cont\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
					"       <div id=\"samuraj-hs-result-checkpickuppoint-text\"><b><%this.checkpickuppointtext1%></b></div><span><%this.checkpickuppointtext2%></span> <span id=\"samuraj-hs-result-checkpickuppoint-date\"></span> <span><%this.checkpickuppointtext3%></span> <span id=\"samuraj-hs-result-checkpickuppoint-name\"></span>" +
					"	    <div>" +
					"	       <div id=\"samuraj-hs-result-checkpickuppoint-click-button\" onclick=\"Samuraj.FreightCalc.changeToPickuppoint(this);\" style=\"cursor: pointer\"><span style=\"color:blue\" id=\"samuraj-hs-result-checkpickuppoint-click-text\"><%this.checkpickuppointtext4%></span></div>" +
					"	     </div>" +
					"    </div>" +
					*/
					"";

					options.pickuppointHTML = "" +
					"    <div id=\"samuraj-pp-calc-heading-lower\" class=\"samuraj-radio-heading samuraj-pup-viscon samuraj-show\"><%this.pickuppointtext%></div>" +
					"    <div id=\"samuraj-pp-result-restriction-cont\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
					"       <span id=\"samuraj-pp-result-restriction-text\"><%this.noparceldeliverytopuptext%></span>" +
					"    </div>" +
					"    <div id=\"samuraj-pp-input-lower-div\" class=\"samuraj-pp-input-div\">" +
					"      <select id=\"samuraj-pp-input-lower-field\" class=\"samuraj-pp-input-field\">" +
					"        <option id=\"samuraj-pp-option-default\" value=\"\"><%this.dropdownfieldplaceholder%></option>" +
					"      </select>" +
					"      <%this.freightcalcloader%>" +
					"    </div>" +
					"    <div id=\"samuraj-pp-result\" class=\"samuraj-calc-result-div samuraj-pup-viscon\"><div id=\"samuraj-pp-result-inner\" class=\"samuraj-calc-result-div-inner\">" +
					"      <table style=\"width: 100%\">" +
					"        <tbody>" +
					"          <tr>" +
					"            <td rowspan=\"5\" style=\"width: 90px; vertical-align: top; padding-right: 10px;\"><img class=\"samuraj-result-img\" src=\"<%this.pickuppointresultimg%>\"></td>" +
					"            <td colspan=\"2\" style=\"padding-bottom: 5px;\" valign=\"bottom\"><%this.pickuppointresulttext1%></td>" +
					"          </tr>" + "          <tr>" +
					"            <td><span id=\"samuraj-pp-result-text-2\"><%this.pickuppointresulttext2%></span>: <b><span id=\"samuraj-calc-pp-result-price\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"padding-top: 5px\" valign=\"bottom\"><%this.pickuppointresulttext3%> <b><span id=\"samuraj-calc-pp-result-delivery-date\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"border-top: 1px solid #ccc;\"></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"text-align: right;padding-top: 5px;\"> <span id=\"samuraj-calc-pp-map-text\"><a id=\"samuraj-calc-pp-map-url\" class=\"samuraj-map-tool-tip\" href=\"\" target=\"_blank\"><%this.pickuppointresultmaptextlink%></a></span></td>" +
					"          </tr>" +
					"        </tbody>" +
					"      </table>" +
					"    </div></div>" +
					"    <div id=\"samuraj-calc-pp-result-error-div\" class=\"samuraj-calc-error samuraj-pup-viscon\"><span id=\"samuraj-calc-pp-result-error-span\"></span></div>" +
					"";
					options.clickcollectHTML = "" +
					"    <div id=\"samuraj-cc-calc-heading-lower\" class=\"samuraj-radio-heading samuraj-pup-viscon\"><%this.clickcollecttext%></div>" +
					"    <div id=\"samuraj-cc-input-lower-div\" class=\"samuraj-cc-input-div\">" +
					"      <select id=\"samuraj-cc-input-lower-field\" class=\"samuraj-cc-input-field\">" +
					"        <option id=\"samuraj-cc-option-default\" value=\"\"><%this.clickcollectdropdownfieldplaceholder%></option>" +
					"      </select>" +
					"      <%this.freightcalcloader%>" +
					"    </div>" +
					"    <div id=\"samuraj-cc-result\" class=\"samuraj-calc-result-div samuraj-pup-viscon\"><div id=\"samuraj-cc-result-inner\" class=\"samuraj-calc-result-div-inner\">" +
					"      <table style=\"width: 100%\">" +
					"        <tbody>" +
					"          <tr>" +
					"            <td rowspan=\"5\" style=\"width: 90px; vertical-align: top; padding-right: 10px;\"><img class=\"samuraj-result-img\" src=\"<%this.clickcollectresultimg%>\"></td>" +
					"            <td colspan=\"2\" style=\"padding-bottom: 5px;\" valign=\"bottom\"><%this.clickcollectresulttext1%></td>" +
					"          </tr>" + "          <tr>" +
					"            <td><span id=\"samuraj-cc-result-text-2\"><%this.clickcollectresulttext2%></span>: <b><span id=\"samuraj-calc-cc-result-price\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"padding-top: 5px\" valign=\"bottom\"><%this.clickcollectresulttext3%> <b><span id=\"samuraj-calc-cc-result-delivery-date\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"\"></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"text-align: right;padding-top: 5px;\"> <span id=\"samuraj-calc-cc-map-text\"><a id=\"samuraj-calc-cc-map-url\" class=\"samuraj-map-tool-tip\" href=\"\" target=\"_blank\"><%this.clickcollectmaptextlink%></a></span></td>" +
					"          </tr>" +
					"        </tbody>" +
					"      </table>" +
					"    </div></div>" +
					"    <div id=\"samuraj-calc-cc-result-error-div\" class=\"samuraj-calc-error samuraj-pup-viscon\"><span id=\"samuraj-calc-cc-result-error-span\"></span></div>" +
					"";

					Samuraj.Merge.merge(options, Samuraj.Translation.data.freight)

					return options;
				},
				htmlcontent: htmlradiotemplate()
			}

			var containerhtml = Samuraj.Template.load(input.init());

			if (this.mobileCheckout()){
				Samuraj.Element.get("input#zipCodeBottom", function(selector, targetelem) {

					targetelem = targetelem[0].parentNode.parentNode.parentNode.parentNode;
					targetelem.insertAdjacentHTML("beforebegin", containerhtml);
					if (Samuraj.FreightCalc.settings.expressontop){
						var hselem = document.getElementById("samuraj-hs-result");
						var exelem = document.getElementById("samuraj-ex-result");
						if (hselem && exelem) hselem.parentNode.insertBefore(exelem, hselem); 
					}

					callback(true);
				}, containerhtml);
			} else {

				Samuraj.Element.get("input#zipCodeBottom", function(selector, targetelem) {

					targetelem = targetelem[0].parentNode.parentNode.parentNode.parentNode;
					targetelem.insertAdjacentHTML("beforebegin", containerhtml);
					if (Samuraj.FreightCalc.settings.expressontop){
						var hselem = document.getElementById("samuraj-hs-result");
						var exelem = document.getElementById("samuraj-ex-result");
						if (hselem && exelem) hselem.parentNode.insertBefore(exelem, hselem); 
					}

					Samuraj.Element.showall("pup");
					callback(true);
				}, containerhtml);
				
			}
		},
		FreightCalcButtons: function(callback){
			//Adding buttoncontainer and hidden buttons for express delivery and click & collect
			if (this.mobileCheckout()){
				var buttons = document.createElement("div");
				buttons.id = "samuraj-checkout-button-cont";
				buttons.innerHTML =  "" +
					"<div id=\"samuraj-pay-with-other-card-wrapper\" style=\"display: none\"><input type=\"checkbox\" id=\"samuraj-pay-with-other-card-input\"><label for=\"samuraj-pay-with-other-card-input\"> Ønsker du å betale med gavekort eller et IKEA Bedriftskort, huk av her før du går videre.</label></div>" +
					"<div id=\"samuraj-ex-wrapper\" class=\"samuraj-checkout-button-wrapper\" style=\"display: none\">" +
					"	<div id=\"samuraj-ex-button-text\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.expressdeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-ex-button-text-error\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.expressdeliverybuttontexterror + "</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowdown + "\"></div>" +
					"	<div id=\"samuraj-ex-button\" data-corners=\"true\" data-shadow=\"true\" data-iconshadow=\"true\" data-wrapperels=\"span\" data-icon=\"null\" data-iconpos=\"null\" data-theme=\"blueBtn\" class=\"ui-btn ui-btn-up-purchase ui-shadow ui-btn-corner-all ui-submit ui-btn-up-blueBtn\" aria-disabled=\"false\">" +
					"		<span class=\"ui-btn-inner ui-btn-corner-all\">" +
					"			<span id=\"samuraj-ex-button-caption\" class=\"ui-btn-text\">" + Samuraj.Translation.data.freight.expressdeliverybuttoncaption + "</span>" +
					"		</span>" +
					"		<input type=\"button\" data-theme=\"blueBtn\" id=\"samuraj-ex-button-caption-value\" value=\"" + Samuraj.Translation.data.freight.expressdeliverybuttoncaption + "\" class=\"ui-btn-hidden\" aria-disabled=\"false\" onclick=\"Samuraj.FreightCalc.expressdeliveryCheckout(); return false;\">" +
					"	</div>" +
					"</div>" +
					"<div id=\"samuraj-cc-wrapper\" class=\"samuraj-checkout-button-wrapper\" style=\"display: none\">" +
					"	<div id=\"samuraj-cc-button-text\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.clickcollectbuttontext + "</div>" +
					"	<div id=\"samuraj-cc-button-text-error\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.clickcollectbuttontexterror + "</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowdown + "\"></div>" +
					"	<div id=\"samuraj-cc-button\" data-corners=\"true\" data-shadow=\"true\" data-iconshadow=\"true\" data-wrapperels=\"span\" data-icon=\"null\" data-iconpos=\"null\" data-theme=\"blueBtn\" class=\"ui-btn ui-btn-up-purchase ui-shadow ui-btn-corner-all ui-submit ui-btn-up-blueBtn\" aria-disabled=\"false\">" +
					"		<span class=\"ui-btn-inner ui-btn-corner-all\">" +
					"			<span id=\"samuraj-cc-button-caption\" class=\"ui-btn-text\">" + Samuraj.Translation.data.freight.clickcollectbuttoncaption + "</span>" +
					"		</span>" +
					"		<input type=\"button\" data-theme=\"blueBtn\" id=\"samuraj-cc-button-caption-value\" value=\"" + Samuraj.Translation.data.freight.clickcollectbuttoncaption + "\" class=\"ui-btn-hidden\" aria-disabled=\"false\" onclick=\"Samuraj.FreightCalc.clickcollectCheckout(); return false;\">" +
					"	</div>" +
					"</div>" +
					"<div id=\"samuraj-hs-wrapper\" class=\"samuraj-checkout-button-wrapper\">" +
					"	<div id=\"samuraj-hs-button-text\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.regulardeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-hs-button-text-error\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.regulardeliverybuttontexterror + "</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowdown + "\"></div>" +
					"</div>" +

					"<div id=\"samuraj-ex-wrapper-klarna\" class=\"samuraj-checkout-button-wrapper\" style=\"display: none\">" +
					"	<div id=\"samuraj-ex-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.expressdeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-ex-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.expressdeliverybuttontexterror + "</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowdown + "\"></div>" +
					"	<div id=\"samuraj-ex-button-klarna\" data-corners=\"true\" data-shadow=\"true\" data-iconshadow=\"true\" data-wrapperels=\"span\" data-icon=\"null\" data-iconpos=\"null\" data-theme=\"blueBtn\" class=\"ui-btn ui-btn-up-purchase ui-shadow ui-btn-corner-all ui-submit ui-btn-up-blueBtn\" aria-disabled=\"false\">" +
					"		<span class=\"ui-btn-inner ui-btn-corner-all\">" +
					"			<span id=\"samuraj-ex-button-caption-klarna\" class=\"ui-btn-text\">" + Samuraj.Translation.data.freight.expressdeliverybuttoncaption + "</span>" +
					"		</span>" +
					"		<input type=\"button\" data-theme=\"blueBtn\" id=\"samuraj-ex-button-caption-value-klarna\" value=\"" + Samuraj.Translation.data.freight.expressdeliverybuttoncaption + "\" class=\"ui-btn-hidden\" aria-disabled=\"false\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('ex'); return false;\">" +
					"	</div>" +
					"</div>" +
					"<div id=\"samuraj-cc-wrapper-klarna\" class=\"samuraj-checkout-button-wrapper\" style=\"display: none\">" +
					"	<div id=\"samuraj-cc-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.clickcollectbuttontext + "</div>" +
					"	<div id=\"samuraj-cc-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.clickcollectbuttontexterror + "</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowdown + "\"></div>" +
					"	<div id=\"samuraj-cc-button-klarna\" data-corners=\"true\" data-shadow=\"true\" data-iconshadow=\"true\" data-wrapperels=\"span\" data-icon=\"null\" data-iconpos=\"null\" data-theme=\"blueBtn\" class=\"ui-btn ui-btn-up-purchase ui-shadow ui-btn-corner-all ui-submit ui-btn-up-blueBtn\" aria-disabled=\"false\">" +
					"		<span class=\"ui-btn-inner ui-btn-corner-all\">" +
					"			<span id=\"samuraj-cc-button-caption-klarna\" class=\"ui-btn-text\">" + Samuraj.Translation.data.freight.clickcollectbuttoncaption + "</span>" +
					"		</span>" +
					"		<input type=\"button\" data-theme=\"blueBtn\" id=\"samuraj-cc-button-caption-value-klarna\" value=\"" + Samuraj.Translation.data.freight.clickcollectbuttoncaption + "\" class=\"ui-btn-hidden\" aria-disabled=\"false\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('cc'); return false;\">" +
					"	</div>" +
					"</div>" +
					"<div id=\"samuraj-pp-wrapper-klarna\" class=\"samuraj-checkout-button-wrapper\" style=\"display: none\">" +					
					"	<div id=\"samuraj-pp-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.pickuppointbuttontext + "</div>" +
					"	<div id=\"samuraj-pp-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.pickuppointbuttontexterror + "</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowdown + "\"></div>" +
					"	<div id=\"samuraj-pp-button-klarna\" data-corners=\"true\" data-shadow=\"true\" data-iconshadow=\"true\" data-wrapperels=\"span\" data-icon=\"null\" data-iconpos=\"null\" data-theme=\"blueBtn\" class=\"ui-btn ui-btn-up-purchase ui-shadow ui-btn-corner-all ui-submit ui-btn-up-blueBtn\" aria-disabled=\"false\">" +
					"		<span class=\"ui-btn-inner ui-btn-corner-all\">" +
					"			<span id=\"samuraj-pp-button-caption-klarna\" class=\"ui-btn-text\">" + Samuraj.Translation.data.freight.pickuppointbuttoncaption + "</span>" +
					"		</span>" +
					"		<input type=\"button\" data-theme=\"blueBtn\" id=\"samuraj-pp-button-caption-value-klarna\" value=\"" + Samuraj.Translation.data.freight.pickuppointbuttoncaption + "\" class=\"ui-btn-hidden\" aria-disabled=\"false\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('pp'); return false;\">" +
					"	</div>" +
					"</div>" +
					"<div id=\"samuraj-hs-wrapper-klarna\" class=\"samuraj-checkout-button-wrapper\" style=\"display: none\">" +					
					"	<div id=\"samuraj-hs-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.regulardeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-hs-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.regulardeliverybuttontexterror + "</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowdown + "\"></div>" +
					"	<div id=\"samuraj-hs-button-klarna\" data-corners=\"true\" data-shadow=\"true\" data-iconshadow=\"true\" data-wrapperels=\"span\" data-icon=\"null\" data-iconpos=\"null\" data-theme=\"blueBtn\" class=\"ui-btn ui-btn-up-purchase ui-shadow ui-btn-corner-all ui-submit ui-btn-up-blueBtn\" aria-disabled=\"false\">" +
					"		<span class=\"ui-btn-inner ui-btn-corner-all\">" +
					"			<span id=\"samuraj-hs-button-caption-klarna\" class=\"ui-btn-text\">" + Samuraj.Translation.data.freight.regulardeliverybuttoncaption + "</span>" +
					"		</span>" +
					"		<input type=\"button\" data-theme=\"blueBtn\" id=\"samuraj-hs-button-caption-value-klarna\" value=\"" + Samuraj.Translation.data.freight.regulardeliverybuttoncaption + "\" class=\"ui-btn-hidden\" aria-disabled=\"false\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('hs'); return false;\">" +
					"	</div>" +
					"</div>" +

					"";
				Samuraj.Element.get("#checkoutButtonBoxBottom > div", function(selector, elem) {
					var elem = elem[0];
					elem.parentNode.insertBefore(buttons, elem);
					elem.id = "samuraj-hs-button";
					var inputelem = elem.querySelector(".ui-btn-text");
					if (inputelem) {
						inputelem.id = "samuraj-hs-button-caption";
						inputelem.innerHTML = Samuraj.Translation.data.freight.regulardeliverybuttoncaption;
					}
					var inputelem = elem.querySelector("input");
					if (inputelem) {
						inputelem.id = "samuraj-hs-button-caption-value";
						inputelem.value = Samuraj.Translation.data.freight.regulardeliverybuttoncaption;
					}
					var wrap = buttons.querySelector("#samuraj-hs-wrapper");
					wrap.appendChild(elem);
					
					//Remove disabled on the default continue button
                    elem = document.querySelector("input#samuraj-hs-button-caption-value");
                    if (elem) elem.removeAttribute('disabled');
					
					//Deactivate continue button
					if (Samuraj.FreightCalc.settings.forcezipcodeentry)
						Samuraj.FreightCalc.deactivateContinueButtons();

					
					elem = document.getElementById("samuraj-pay-with-other-card-input");
					if (elem) {
						Samuraj.Common.addEvent(elem, "change", function(e){
							var dt = Samuraj.FreightCalc.cookie.dt;
							if (Samuraj.FreightCalc.cookie.dt == "hs" & Samuraj.FreightCalc.cookie.ex == "true") dt = "ex";
							Samuraj.FreightCalc.toggleShownButton(dt);
						});
						//*** Test ***
						//elem.parentNode.style.setProperty("display", "block");
						//************
					}

					callback(true);
				}, buttons);
			} else {
				var buttons = document.createElement("div");
				buttons.id = "samuraj-checkout-button-cont";
				buttons.innerHTML =  "" +
					"<div id=\"samuraj-pay-with-other-card-wrapper\" style=\"display: none\"><div id=\"samuraj-pay-with-other-card\"><input type=\"checkbox\" id=\"samuraj-pay-with-other-card-input\"><label for=\"samuraj-pay-with-other-card-input\"> Ønsker du å betale med gavekort eller et IKEA Bedriftskort, huk av her før du går videre.</label></div></div>" +
					"<div id=\"samuraj-ex-wrapper\" style=\"display: none\">" +
					"	<div class=\"buttonContainer\" id=\"samuraj-ex-button\" style=\"width: 200px;\">" +
					"		<a class=\"blueButton\" id=\"samuraj-ex-link\" href=\"#\" onclick=\"Samuraj.FreightCalc.expressdeliveryCheckout(); return false;\">" +
					"			<div class=\"buttonLeft buttonLeftBlue\">&nbsp;</div>" +
					"			<div class=\"buttonCaption buttonCaptionBlue\">" +
					"				<input id=\"samuraj-ex-button-caption-value\" type=\"button\" value=\"" + Samuraj.Translation.data.freight.expressdeliverybuttoncaption + "\" style=\"cursor: pointer; width: 200px;\">" +
					"			</div>" +
					"			<div class=\"buttonRight buttonRightBlue\">&nbsp;</div>" +
					"		</a>" +
					"	</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowright + "\"></div>" +
					"	<div id=\"samuraj-ex-button-text\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.expressdeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-ex-button-text-error\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.expressdeliverybuttontexterror + "</div>" +
					"</div>" +
					"<div id=\"samuraj-cc-wrapper\" style=\"display: none\">" +
					"	<div class=\"buttonContainer\" id=\"samuraj-cc-button\" style=\"width: 200px;\">" +
					"		<a class=\"blueButton\" id=\"samuraj-cc-link\" href=\"#\" onclick=\"Samuraj.FreightCalc.clickcollectCheckout(); return false;\">" +
					"			<div class=\"buttonLeft buttonLeftBlue\">&nbsp;</div>" +
					"			<div class=\"buttonCaption buttonCaptionBlue\">" +
					"				<input id=\"samuraj-cc-button-caption-value\" type=\"button\" value=\"" + Samuraj.Translation.data.freight.clickcollectbuttoncaption + "\" style=\"cursor: pointer; width: 200px;\">" +
					"			</div>" +
					"			<div class=\"buttonRight buttonRightBlue\">&nbsp;</div>" +
					"		</a>" +
					"	</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowright + "\"></div>" +
					"	<div id=\"samuraj-cc-button-text\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.clickcollectbuttontext + "</div>" +
					"	<div id=\"samuraj-cc-button-text-error\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.clickcollectbuttontexterror + "</div>" +
					"</div>" +
					"<div id=\"samuraj-hs-wrapper\">" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowright + "\"></div>" +
					"	<div id=\"samuraj-hs-button-text\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.regulardeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-hs-button-text-error\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.regulardeliverybuttontexterror + "</div>" +
					"</div>" +

					"<div id=\"samuraj-ex-wrapper-klarna\" style=\"display: none\">" +
					"	<div class=\"buttonContainer\" id=\"samuraj-ex-button-klarna\" style=\"width: 200px;\">" +
					"		<a class=\"blueButton\" id=\"samuraj-ex-link-klarna\" href=\"#\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('ex'); return false;\">" +
					"			<div class=\"buttonLeft buttonLeftBlue\">&nbsp;</div>" +
					"			<div class=\"buttonCaption buttonCaptionBlue\">" +
					"				<input id=\"samuraj-ex-button-caption-value-klarna\" type=\"button\" value=\"" + Samuraj.Translation.data.freight.expressdeliverybuttoncaption + "\" style=\"cursor: pointer; width: 200px;\">" +
					"			</div>" +
					"			<div class=\"buttonRight buttonRightBlue\">&nbsp;</div>" +
					"		</a>" +
					"	</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowright + "\"></div>" +
					"	<div id=\"samuraj-ex-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.expressdeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-ex-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.expressdeliverybuttontexterror + "</div>" +
					"</div>" +
					"<div id=\"samuraj-cc-wrapper-klarna\" style=\"display: none\">" +
					"	<div class=\"buttonContainer\" id=\"samuraj-cc-button-klarna\" style=\"width: 200px;\">" +
					"		<a class=\"blueButton\" id=\"samuraj-cc-link-klarna\" href=\"#\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('cc'); return false;\">" +
					"			<div class=\"buttonLeft buttonLeftBlue\">&nbsp;</div>" +
					"			<div class=\"buttonCaption buttonCaptionBlue\">" +
					"				<input id=\"samuraj-cc-button-caption-value-klarna\" type=\"button\" value=\"" + Samuraj.Translation.data.freight.clickcollectbuttoncaption + "\" style=\"cursor: pointer; width: 200px;\">" +
					"			</div>" +
					"			<div class=\"buttonRight buttonRightBlue\">&nbsp;</div>" +
					"		</a>" +
					"	</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowright + "\"></div>" +
					"	<div id=\"samuraj-cc-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.clickcollectbuttontext + "</div>" +
					"	<div id=\"samuraj-cc-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.clickcollectbuttontexterror + "</div>" +
					"</div>" +
					"<div id=\"samuraj-pp-wrapper-klarna\" style=\"display: none\">" +
					"	<div class=\"buttonContainer\" id=\"samuraj-pp-button-klarna\" style=\"width: 200px;\">" +
					"		<a class=\"blueButton\" id=\"samuraj-pp-link-klarna\" href=\"#\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('pp'); return false;\">" +
					"			<div class=\"buttonLeft buttonLeftBlue\">&nbsp;</div>" +
					"			<div class=\"buttonCaption buttonCaptionBlue\">" +
					"				<input id=\"samuraj-pp-button-caption-value-klarna\" type=\"button\" value=\"" + Samuraj.Translation.data.freight.pickuppointbuttoncaption + "\" style=\"cursor: pointer; width: 200px;\">" +
					"			</div>" +
					"			<div class=\"buttonRight buttonRightBlue\">&nbsp;</div>" +
					"		</a>" +
					"	</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowright + "\"></div>" +
					"	<div id=\"samuraj-pp-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.pickuppointbuttontext + "</div>" +
					"	<div id=\"samuraj-pp-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.pickuppointbuttontexterror + "</div>" +
					"</div>" +
					"<div id=\"samuraj-hs-wrapper-klarna\" style=\"display: none\">" +
					"	<div class=\"buttonContainer\" id=\"samuraj-hs-button-klarna\" style=\"width: 200px;\">" +
					"		<a class=\"blueButton\" id=\"samuraj-hs-link-klarna\" href=\"#\" onclick=\"Samuraj.FreightCalc.Klarna.checkout('hs'); return false;\">" +
					"			<div class=\"buttonLeft buttonLeftBlue\">&nbsp;</div>" +
					"			<div class=\"buttonCaption buttonCaptionBlue\">" +
					"				<input id=\"samuraj-hs-button-caption-value-klarna\" type=\"button\" value=\"" + Samuraj.Translation.data.freight.regulardeliverybuttoncaption + "\" style=\"cursor: pointer; width: 200px;\">" +
					"			</div>" +
					"			<div class=\"buttonRight buttonRightBlue\">&nbsp;</div>" +
					"		</a>" +
					"	</div>" +
					"   <div class=\"samuraj-checkout-button-img\"><img src=\"" + Samuraj.Translation.data.freight.continuebuttonarrowright + "\"></div>" +
					"	<div id=\"samuraj-hs-button-text-klarna\" class=\"samuraj-checkout-button-text\">" + Samuraj.Translation.data.freight.regulardeliverybuttontext + "</div>" +
					"	<div id=\"samuraj-hs-button-text-error-klarna\" class=\"samuraj-checkout-button-text samuraj-red-text samuraj-pup-viscon\">" + Samuraj.Translation.data.freight.regulardeliverybuttontexterror + "</div>" +
					"</div>" +

					"";
				Samuraj.Element.get("form#shopRowBottom .buttonContainer", function(selector, elem) {
					var elem = elem[0];
					var parent = elem.parentNode;
					if (parent.id.indexOf("noShippingCalcButtonBottom") > -1){
						// For Denmark where the continue button is disabled by default
						parent.style.setProperty("display", "none");
						Samuraj.Element.get("form#shopRowBottom #shippingCalcButtonBottom .buttonContainer", function(selector, elem) {
							var elem = elem[0];
							var parent = elem.parentNode;
							parent.style.setProperty("display", "block");
							elem.parentNode.insertBefore(buttons, elem);
							elem.id = "samuraj-hs-button";
							elem.setAttribute("style", "width: 200px");
							var inputelem = elem.querySelector("input")
							if (inputelem) {
								inputelem.id = "samuraj-hs-button-caption-value";
								inputelem.style.setProperty("width", "200px")
								inputelem.value = Samuraj.Translation.data.freight.regulardeliverybuttoncaption;
							}
							var wrap = buttons.querySelector("#samuraj-hs-wrapper");
							wrap.insertBefore(elem, wrap.firstChild);

							//Deactivate continue button
							if (Samuraj.FreightCalc.settings.forcezipcodeentry)
							Samuraj.FreightCalc.deactivateContinueButtons();

							
							elem = document.getElementById("samuraj-pay-with-other-card-input");
							if (elem) {
								Samuraj.Common.addEvent(elem, "change", function(e){
									var dt = Samuraj.FreightCalc.cookie.dt;
									if (Samuraj.FreightCalc.cookie.dt == "hs" & Samuraj.FreightCalc.cookie.ex == "true") dt = "ex";
									Samuraj.FreightCalc.toggleShownButton(dt);
								});
								//*** Test ***
								//elem.parentNode.style.setProperty("display", "block");
								//************
							}

							callback(true);
						});
					} else {
						elem.parentNode.insertBefore(buttons, elem);
						elem.id = "samuraj-hs-button";
						elem.setAttribute("style", "width: 200px");
						var inputelem = elem.querySelector("input")
						if (inputelem) {
							inputelem.id = "samuraj-hs-button-caption-value";
							inputelem.style.setProperty("width", "200px")
							inputelem.value = Samuraj.Translation.data.freight.regulardeliverybuttoncaption;
						}
						var wrap = buttons.querySelector("#samuraj-hs-wrapper");
						wrap.insertBefore(elem, wrap.firstChild);

						elem = document.getElementById("samuraj-pay-with-other-card-input");
						if (elem) {
							Samuraj.Common.addEvent(elem, "change", function(e){
								var dt = Samuraj.FreightCalc.cookie.dt;
								if (Samuraj.FreightCalc.cookie.dt == "hs" & Samuraj.FreightCalc.cookie.ex == "true") dt = "ex";
								Samuraj.FreightCalc.toggleShownButton(dt);
							});
							//*** Test ***
							//elem.parentNode.style.setProperty("display", "block");
							//************
						}

						callback(true);
					}
				}, buttons);

			}
		},
		FreightCalcAddress: function(callback) {
			var c = Samuraj.Common;
			var input = {
				init: function() {
					this.htmloptions = this.htmloptionsfunction();
					return this;
				},
				htmloptionsfunction: function() {
					var options = {};
					options.pickuppointHTML = "" +
					"    <div id=\"samuraj-pp-calc-heading-lower\" class=\"samuraj-radio-heading samuraj-pup-viscon samuraj-show\"><%this.pickuppointtext%></div>" +
					"    <div id=\"samuraj-pp-result-restriction-cont\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
					"       <span id=\"samuraj-pp-result-restriction-text\"><%this.noparceldeliverytopuptext%></span>" +
					"    </div>" +
					"    <div id=\"samuraj-pp-input-lower-div\" class=\"samuraj-pp-input-div\">" +
					"      <select id=\"samuraj-pp-input-lower-field\" class=\"samuraj-pp-input-field\">" +
					"        <option id=\"samuraj-pp-option-default\" value=\"\"><%this.dropdownfieldplaceholder%></option>" +
					"      </select>" +
					"      <%this.freightcalcloader%>" +
					"    </div>" +
					"    <div id=\"samuraj-pp-result\" class=\"samuraj-calc-result-div samuraj-pup-viscon\">" +
					"      <table style=\"width: 100%\">" +
					"        <tbody>" +
					"          <tr>" +
					"            <td rowspan=\"5\" style=\"max-width: 70px; vertical-align: top\"><img class=\"samuraj-result-img\" src=\"<%this.pickuppointresultimg%>\"></td>" +
					"            <td colspan=\"2\" style=\"padding-bottom: 5px;\" valign=\"bottom\"><%this.pickuppointresulttext1%></td>" +
					"          </tr>" + "          <tr>" +
					"            <td><span id=\"samuraj-pp-result-text-2\"><%this.pickuppointresulttext2%></span>: <b><span id=\"samuraj-calc-pp-result-price\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"padding-top: 5px\" valign=\"bottom\"><%this.pickuppointresulttext3%> <b><span id=\"samuraj-calc-pp-result-delivery-date\"></span></b></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"border-top: 1px solid #ccc;\"></td>" +
					"          </tr>" +
					"          <tr>" +
					"            <td style=\"text-align: right;padding-top: 5px;\"> <span id=\"samuraj-calc-pp-map-text\"><a id=\"samuraj-calc-pp-map-url\" class=\"samuraj-map-tool-tip\" href=\"\" target=\"_blank\"><%this.pickuppointresultmaptextlink%></a></span></td>" +
					"          </tr>" +
					"        </tbody>" +
					"      </table>" +
					"    </div>" +
					"    <div id=\"samuraj-calc-pp-result-error-div\" class=\"samuraj-calc-error samuraj-pup-viscon\"><span id=\"samuraj-calc-pp-result-error-span\"></span></div>" +
					"";
					options.otheraddressHTML = "" +
					"    <div id=\"samuraj-oa-calc-heading-lower\" class=\"samuraj-radio-heading samuraj-pup-viscon\"><%this.otheraddresstext%></div>" +
					"    <div id=\"samuraj-oa-input-lower-div\" class=\"samuraj-oa-input-div\">" +

					"    </div>" +
					"";

					Samuraj.Merge.merge(options, Samuraj.Translation.data.freight)

					return options;
				},
				htmlcontent: "" +
				"<div id=\"samuraj-pup-master-cont\">" +
				//"  <div class=\"samuraj-master-heading\"><%this.masterheading%></div>" +
				"  <div class=\"samuraj-master-heading\"></div>" +
				"  <div id=\"samuraj-cont-pp-radio-heading\" class=\"samuraj-radio-heading-div\"><input id=\"samuraj-cont-pp-radio-heading-input\" class=\"samuraj-radio-heading-button\" type=\"radio\" name=\"shippingtype\" value=\"pp\" onclick=\"Samuraj.FreightCalc.toggleDeliveryType('pp', 'manual')\"><label class=\"samuraj-radio-heading-label\" for=\"samuraj-cont-pp-radio-heading-input\"><%this.pickuppoint%></label></div>" +
				"  <div id=\"samuraj-cont-pp\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
				"    <%this.pickuppointHTML%>" +
				"  </div>" +
				"  <div id=\"samuraj-cont-oa-radio-heading\" class=\"samuraj-radio-heading-div\"><input id=\"samuraj-cont-oa-radio-heading-input\" class=\"samuraj-radio-heading-button\" type=\"radio\" name=\"shippingtype\" value=\"oa\" onclick=\"Samuraj.FreightCalc.toggleDeliveryType('oa', 'manual')\"><label class=\"samuraj-radio-heading-label\" for=\"samuraj-cont-oa-radio-heading-input\"><%this.otheraddress%></label></div>" +
				"  <div id=\"samuraj-cont-oa\" class=\"samuraj-radio-heading samuraj-pup-viscon\">" +
				"    <%this.otheraddressHTML%>" +
				"  </div>" +
				"</div>"
			}

			var containerhtml = Samuraj.Template.load(input.init());

			var elem = document.createElement("div");
			elem.id = "accordionContent_master";
			elem.innerHTML = containerhtml;
			Samuraj.Element.get({selector:["#accordionContent","#deliveryBox"], timeout: 2000}, function(selector, targetelem) {
				targetelem = targetelem[0];
				targetelem.parentNode.insertBefore(elem, targetelem);

				if (Samuraj.FreightCalc.mobileCheckout()){
					targetelem.className = "samuraj-radio-heading";
					var targetoaelem = document.getElementById("samuraj-oa-input-lower-div");
					targetoaelem.appendChild(targetelem);
				} else {
					var elempmc = document.querySelector("#samuraj-pup-master-cont");
					if (elempmc) Samuraj.Common.addClass(elempmc, "samuraj-irw");
					targetelem.className = "samuraj-radio-heading";
					var targetoaelem = document.getElementById("samuraj-oa-input-lower-div");
					targetoaelem.appendChild(targetelem);
					var heading = targetelem.querySelector(".billing_address_heading2");
					if (heading) heading.style.setProperty("display", "none");
				}
				callback();
			});

		},
		init: function(data) {
			var c = Samuraj.Common
			this.data = data;
			//if (document.URL.indexOf("/webapp/wcs/stores/servlet/OrderItemDisplay") > -1 || document.URL.indexOf("/webapp/wcs/stores/servlet/IrwOrderItemDisplay") > -1 || document.URL.indexOf("/webapp/wcs/stores/servlet/PromotionCodeManage") > -1 || document.URL.indexOf("/webapp/wcs/stores/servlet/OrderItemAddByPartNumber") > -1) {
			this.getInfoCookie();
			if (!this.cookie) {
				this.setInfoCookie('{"zc":"","na":"","dt":"","ex":"false"}');
				this.clearZipCodeForTransfer();
			}
			if (c.checkurl(["*/stores/servlet/*","*/mcommerce/shoppingcart*"])){
				if (c.checkurl(["*OrderItemDisplay|PromotionCodeManage|OrderItemAddByPartNumber*","*/mcommerce/shoppingcart*"]))
					this.activePage = "cart";
				if (c.checkurl("*CheckoutWelcome|CheckoutAddress|IrwProceedFromCheckoutAddressView*"))
					this.activePage = "address";
				if (c.checkurl("*DeliveryOptions*"))
					this.activePage = "delivery";
			}
			c.setStorage("freight_mobileplatform", this.mobileCheckout());

			if (this.activePage == "cart"){
				if (this.mobileCheckout()){
					//M2 hide to checkout button
					Samuraj.Element.get("#checkoutButtonBoxTop", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("visibility", "hidden");
						}
					});

					//M2 hide calculation
					Samuraj.Element.get(".calculate", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "none");
						}
					});

					//M2 hide click & collect
					Samuraj.Element.get(".clickCollectBody", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].parentNode.style.setProperty("display", "none");
						}
					});

					//M2 hide click & collect
					Samuraj.Element.get(".clickCollectHeader", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].parentNode.style.setProperty("display", "none");
						}
					});
					//M2 hide mandatoryErrorMsgBottom
					Samuraj.Element.get("#mandatoryErrorMsgBottom", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "none");
						}
					});
				} else {

					//cosmetic fix for freight IRW
					Samuraj.Element.get("#checkoutOptimization .beginCheckout .shoppingListDelivery", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("overflow", "hidden");
						}
					});

					//M2 hide mandatoryErrorMsgBottom
					Samuraj.Element.get("#shippingInfoBottom", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "none");
						}
					});

					//Hide bottom calculation
					Samuraj.Element.get("#deliveryAllWrapBottom", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "none");
						}
					});

					//Making sure elements are visible
					Samuraj.Element.get("#cartFooter", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "block");
						}
					});
					Samuraj.Element.get(".SummaryWrapper", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "block");
						}
					});
					Samuraj.Element.get(".shoppingSubTotalExlDelivery", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "block");
						}
					});
					Samuraj.Element.get(".shoppingListDelivery", function(selector, elem){
						for (var i=0; i<elem.length; i++){
							if (elem[i]) elem[i].style.setProperty("display", "block");
						}
					});

				}
				this.FreightCalcCart(function(){
					Samuraj.FreightCalc.FreightCalcButtons(function(){
						Samuraj.FreightCalc.checkParcelFreightCalcRestriction();
						
						if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12"){
						
							Samuraj.FreightCalc.addingStates(function(){
								Samuraj.FreightCalc.savedCookieImport();
								
								var elem = document.getElementById("samuraj-hs-input-lower-field-state");
								if (Samuraj.FreightCalc.settings.calculatebutton){
									Samuraj.Common.addEvent(elem, "change", function (e) {
										Samuraj.FreightCalc.toggleDeliveryType('hs', 'auto');
										Samuraj.FreightCalc.calcButtonValidate();
										var elem = document.getElementById("samuraj-hs-calculate-button");
										if (elem) Samuraj.Common.addClass(elem, "samuraj-show");
										
										var valid = false;
										var elem = document.getElementById("samuraj-hs-input-lower-field");
										if (SamurajSetting.Country.storeId == "3" || SamurajSetting.Country.storeId == "12"){
											var state = document.getElementById("samuraj-hs-input-lower-field-state");
											if (state) {
												if (state.selectedIndex > 0) {
													if (elem.value.length == Samuraj.FreightCalc.settings.zipcodedigits) 
														valid = true;
												}
											}
										} else if (elem.value.length == Samuraj.FreightCalc.settings.zipcodedigits) valid = true;

										if(!valid){
											var elem = document.getElementById('samuraj-hs-result');
											if (elem) Samuraj.Common.removeClass(elem, 'samuraj-show');
											var elem = document.getElementById('samuraj-ex-result');
											if (elem) Samuraj.Common.removeClass(elem, 'samuraj-show');
											Samuraj.FreightCalc.setInfoCookie('{"zc":"","na":"","dt":"hs","ex":"' + Samuraj.FreightCalc.cookie.ex + '"}');
											Samuraj.FreightCalc.resultstore.hs = undefined;
											Samuraj.FreightCalc.revertPrices();
											if (Samuraj.FreightCalc.settings.forcezipcodeentry){
												Samuraj.FreightCalc.deactivateContinueButtons();
												Samuraj.Common.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
												Samuraj.Common.addClass(document.getElementById("samuraj-ex-button-text-error"), "samuraj-show");
												Samuraj.Common.addClass(document.getElementById("samuraj-hs-button-text-error-klarna"), "samuraj-show");
												Samuraj.Common.addClass(document.getElementById("samuraj-ex-button-text-error-klarna"), "samuraj-show");
											}
										}
									});

								} else {
									Samuraj.Common.addEvent(elem, "change", function (e) {
										Samuraj.FreightCalc.toggleDeliveryType('hs', 'auto');
										if (document.getElementById("samuraj-hs-option-default").selected !== true){
											var state = e.target.value + "|";
											var zipcode = document.getElementById("samuraj-hs-input-lower-field");
											if (zipcode) {
												if (zipcode.value.length == Samuraj.FreightCalc.settings.zipcodedigits) {
													var value = state + zipcode.value;
													Samuraj.FreightCalc.calculateShipping({
														zc: value,
														na: '',
														dt: 'hs',
														ex: Samuraj.FreightCalc.cookie.ex
													});
												}
											}

										} else {
											Samuraj.Common.removeClass(document.getElementById("samuraj-hs-result"), "samuraj-show");
											Samuraj.Common.removeClass(document.getElementById("samuraj-calc-hs-result-error-div"), "samuraj-show");
											//Samuraj.Common.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
											Samuraj.FreightCalc.deactivateContinueButtons();
										}
									});
								}
							});
						}
						if (Samuraj.FreightCalc.settings.pickuppoints){
							Samuraj.FreightCalc.addingPickupPoints(function(){
								Samuraj.FreightCalc.savedCookieImport();

								var elem = document.getElementById("samuraj-pp-input-lower-field");

								Samuraj.Common.addEvent(elem, "change", function (e) {
									Samuraj.FreightCalc.toggleDeliveryType('pp', 'auto');

									if(document.getElementById("samuraj-pp-option-default").selected !== true){
										var pupInfo = JSON.parse(this.value);
										Samuraj.FreightCalc.calculateShipping({
											zc: pupInfo.zc,
											na: pupInfo.na,
											dt: 'pp',
											ex: Samuraj.FreightCalc.cookie.ex,
											system: "irw"
										});
									} else {
										Samuraj.Common.removeClass(document.getElementById("samuraj-pp-result"), "samuraj-show");
										Samuraj.Common.removeClass(document.getElementById("samuraj-calc-pp-result-error-div"), "samuraj-show");
										Samuraj.Common.addClass(document.getElementById("samuraj-hs-button-text-error"), "samuraj-show");
										Samuraj.Common.addClass(document.getElementById("samuraj-pp-button-text-error-klarna"), "samuraj-show");
										Samuraj.FreightCalc.deactivateContinueButtons();
									}
								});
							});
						}
						if (Samuraj.FreightCalc.settings.clickcollect){
							Samuraj.FreightCalc.addingClickCollects(function(){
								Samuraj.FreightCalc.savedCookieImport();

								var elem = document.getElementById("samuraj-cc-input-lower-field");

								Samuraj.Common.addEvent(elem, "change", function (e) {
									Samuraj.FreightCalc.toggleDeliveryType('cc', 'auto');
									if(document.getElementById("samuraj-cc-option-default").selected !== true){
										var system = "M2";
										if (!Samuraj.FreightCalc.mobileCheckout())
											system = "IRW"
										
										Samuraj.IBES.sendPayloadJsonp({
											system: system,
											service: "fetchlocation",
											selector: "#samuraj-cc-input-lower-field",
											loading: function(){
												//Clear resultstate
												Samuraj.FreightCalc.resultstore.cc = undefined;
												
												//Deactivate all input and continue buttons
												Samuraj.FreightCalc.deactivateAll();
												Samuraj.FreightCalc.deactivateContinueButtons();
												Samuraj.FreightCalc.freightCalcLoaders.show("#samuraj-cc-input-lower-div .samuraj-freightcalc-loader");
												Samuraj.Common.addClass(document.getElementById("samuraj-cc-result"), "samuraj-faded");
												Samuraj.Common.removeClass(document.getElementById("samuraj-calc-cc-result-error-div"), "samuraj-show");
												Samuraj.Common.removeClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");

											},
											success: function(response){
												var data = {};
												var elem = e.target[e.target.selectedIndex];
												if (elem){
													var zc = elem.id.replace("samuraj-cc-option_", ""),
														na = elem.innerHTML.replace(/&nbsp;/g, "");
													data = {
														zc: zc,
														na: na,
														dt: "cc",
														ex: Samuraj.FreightCalc.cookie.ex
													}
													Samuraj.FreightCalc.setInfoCookie('{"zc":"' + data.zc + '","na":"' + data.na + '","dt":"' + data.dt + '","ex":"' + data.ex + '"}');
												}

												//Save result state

												Samuraj.FreightCalc.resultstore.cc = {data: data, response: response};

												Samuraj.FreightCalc.clickcollectcarturl = response.responseJSON.target;
												Samuraj.Common.removeClass(document.getElementById("samuraj-cc-result"), "samuraj-faded");

												// Date and price
												document.getElementById("samuraj-calc-cc-result-delivery-date").innerHTML = "";
												document.getElementById("samuraj-calc-cc-result-price").innerHTML = Samuraj.Translation.data.freight.clickcollectprice;

												Samuraj.FreightCalc.setPickingPrices();

												setTimeout(function(){
													Samuraj.Common.addClass(document.getElementById("samuraj-cc-result"), "samuraj-show");
													Samuraj.Common.removeClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
													Samuraj.FreightCalc.activateContinueButtons();
												}, 600);
											},
											failure: function(response){
												Samuraj.FreightCalc.clickcollectcarturl = undefined;
												Samuraj.Common.removeClass(document.getElementById("samuraj-cc-result"), "samuraj-show");
												if (response.responseJSON){
													setTimeout(function(){
														Samuraj.Common.addClass(document.getElementById("samuraj-calc-cc-result-error-div"), "samuraj-show");
														Samuraj.Common.addClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
													}, 600);
													var elem = document.getElementById("samuraj-calc-cc-result-error-span");
													var error = Samuraj.IBES.getErrorMessageByErrorCode(response.responseJSON.code);
													if (elem && error) elem.innerHTML = error.text;
												}
												Samuraj.FreightCalc.revertPrices();

											},
											exception: function(response){
												Samuraj.FreightCalc.clickcollectcarturl = undefined;
												Samuraj.Common.removeClass(document.getElementById("samuraj-cc-result"), "samuraj-show");
												if (response.responseJSON){
													setTimeout(function(){
														Samuraj.Common.addClass(document.getElementById("samuraj-calc-cc-result-error-div"), "samuraj-show");
														Samuraj.Common.addClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
													}, 600);
													var elem = document.getElementById("samuraj-calc-cc-result-error-span");
													var error = Samuraj.IBES.getErrorMessageByErrorCode(response.responseJSON.code);
													if (elem && error) elem.innerHTML = error.text;
												}
												Samuraj.FreightCalc.revertPrices();

											},
											complete: function(response){
												Samuraj.FreightCalc.activateAll();
												Samuraj.FreightCalc.freightCalcLoaders.hide();
											}
										});
									} else {
										//Clear resultstate
										Samuraj.FreightCalc.resultstore.cc = undefined;

										Samuraj.Common.removeClass(document.getElementById("samuraj-cc-result"), "samuraj-show");
										Samuraj.Common.removeClass(document.getElementById("samuraj-calc-cc-result-error-div"), "samuraj-show");
										Samuraj.Common.addClass(document.getElementById("samuraj-cc-button-text-error"), "samuraj-show");
										
										Samuraj.FreightCalc.activateAll();
										Samuraj.FreightCalc.deactivateContinueButtons();
										Samuraj.FreightCalc.revertPrices();
									}
								});
							});
						}
						Samuraj.FreightCalc.savedCookieImport();
					});
				});
			}

			if (this.activePage == "address"){
				Samuraj.FreightCalc.addressLoadGeneralSettings(function(){

					if (Samuraj.FreightCalc.settings.pickuppoints){
						Samuraj.Element.get({selector:["#accordionContent div", "#deliveryBox"], timeout: 2000, returnstatus: true}, function(selector, elem){
							//  "#accordionContent div",
							if (elem.status == "success"){
								Samuraj.FreightCalc.FreightCalcAddress(function(){
									Samuraj.FreightCalc.toggleDeliveryType(Samuraj.FreightCalc.cookie.dt);

									Samuraj.FreightCalc.addingPickupPoints(function(){
										Samuraj.FreightCalc.savedCookieImport();

										var elem = document.getElementById("samuraj-pp-input-lower-field");
										Samuraj.Common.addEvent(elem, "change", function (e) {
											Samuraj.FreightCalc.toggleDeliveryType('pp', 'auto');

											if(document.getElementById("samuraj-pp-option-default").selected !== true){
												var pupInfo = JSON.parse(this.value);
												Samuraj.FreightCalc.calculateShipping({
													zc: pupInfo.zc,
													na: pupInfo.na,
													dt: 'pp',
													ex: Samuraj.FreightCalc.cookie.ex,
													system: "irw"
												});
											} else {
												Samuraj.Common.removeClass(document.getElementById("samuraj-pp-result"), "samuraj-show");
												Samuraj.Common.removeClass(document.getElementById("samuraj-calc-pp-result-error-div"), "samuraj-show");
												Samuraj.FreightCalc.deactivateContinueButtons();
											}
										});

										Samuraj.FreightCalc.addressLoadAdditionalSettings();
										Samuraj.FreightCalc.otherAddressAdjustments(Samuraj.FreightCalc.cookie.dt);
									});
								});

							} else {
								var ct = Samuraj.FreightCalc.checkCustomerType(),
									zc = "",
									elem;
								elem = document.getElementById("signup_checkout_" + ct + "_zipCode");
								if (elem) zc = elem.value;
								Samuraj.FreightCalc.setInfoCookie('{"zc":"' + zc + '","na":"","dt":"hs","ex":"false"}');
							}
						});
					}
				});
			}

			if (this.activePage == "delivery"){

				if (Samuraj.FreightCalc.settings.deliveryrestrictions){
					var dr = this.checkDeliveryRestrictions(this.cookie.zc);
					if (c.varExist(dr)) {
						Samuraj.Element.get({selector:["#deliveryOptionsBox .paymentAddressBox", ".infoContainer.nameAddress"], timeout: 2000}, function(selector, targetelem){
							var elem = document.createElement("div");
							elem.id = "samuraj-delivery-restriction";
							elem.innerHTML = "" +
							"<div id=\"samuraj-cont\" class=\"infoContainer samuraj-border-red\" style=\"padding-bottom: 15px\">" +
							"   <div class=\"deliveryCol1 headerAddress\">" + Samuraj.Translation.data.freight.deliveryrestrictiontext + "</div>" +
							"   <div class=\"addressContainer last deliveryCol2\">" + dr.restriction +  "</div>" +
							"</div>";
							targetelem[0].parentNode.insertBefore(elem, targetelem[0].nextSibling);
						});
					}
				}
				if (Samuraj.FreightCalc.settings.country == "no"){
					if (Samuraj.FreightCalc.settings.pickuppoints){
						if (this.cookie.dt == "pp"){
							
							Samuraj.Element.get("#questions", function(selector, q_elem) {
								q_elem[0].style.setProperty("display", "none");

								if (Samuraj.FreightCalc.mobileCheckout()){

									Samuraj.Element.get("#typeOfLivingBusiness", function(selector, elem) {
										elem[0].checked = true;
										//Global updatefunction
										update();

										Samuraj.Element.get("#question_DEL_ADDRESS_PART_TIME_ACCESSIBLE_no", function(selector, elem) {
											elem[0].checked = true;
											Samuraj.FreightCalc.questionsAnswered = true;

										});
									});
								} else {
									var elem = q_elem[0].querySelectorAll(".question .questionField .radioAnswer");
									if (elem.length > 2) {
										elem = elem[2].querySelector("input");
										if (elem) elem.click();


									}

									Samuraj.Element.get("#question_DEL_ADDRESS_PART_TIME_ACCESSIBLE_no", function(selector, elem) {
										elem[0].checked = true;
										Samuraj.FreightCalc.questionsAnswered = true;

										// disse tre er lagt inn av erik for å reaktivere betaling
										document.getElementById('question_ADDITIONAL_INFO_TSP').value="N/A";
										document.getElementById('question_FLOOR_NO').value="N/A";
										document.getElementById("question_ELEVATOR_EXISTS_NA").checked = true;
										//for mobilcheckout
										document.getElementsByName('question_ADDITIONAL_INFO_TSP_freetext')[0].value="N/A";
										document.getElementsByName('question_FLOOR_NO_freetext')[0].value="N/A";


									});
								}


								setTimeout(function(){
									if (!Samuraj.FreightCalc.questionsAnswered) q_elem[0].style.setProperty("display", "block");
								}, 2000);
							});

							if (Samuraj.FreightCalc.mobileCheckout()){
								Samuraj.Element.get("#deliveryMethod span.spanRight", function(selector, elem, cookie) {
									elem[0].innerHTML = "Pick-up Point " + cookie.na;
								}, this.cookie);
							} else {
								Samuraj.Element.get("#restrictions h3", function(selector, elem) {
									elem[0].style.setProperty("display", "none");
								});
								Samuraj.Element.get("#deliveryMethod .deliveryCol2", function(selector, elem, cookie) {
									elem[0].innerHTML = "Pick-up Point " + cookie.na;
								}, this.cookie);
							}
						} else {
							if (Samuraj.FreightCalc.mobileCheckout()){
								Samuraj.Element.get("#deliveryMethod span.spanRight", function(selector, elem) {
									var parcel = false,
										pcw = Samuraj.FreightCalc.settings.parcelcheckwords;
									for (var i=0; i<pcw.length; i++){
										if (elem[0].innerHTML.indexOf(pcw[i]) > -1)
											parcel = true;
									}
									if (!parcel) {
										elem[0].innerHTML = "Hjemlevering";
										Samuraj.Element.get("#typeOfLivingBusiness", function(selector, elem) {
											elem[0].parentNode.querySelector("label").innerHTML = "Bedrift";
										});
									}
								});
							} else {
								Samuraj.Element.get("#deliveryMethod .deliveryCol2", function(selector, elem) {
									var parcel = false,
										pcw = Samuraj.FreightCalc.settings.parcelcheckwords;
									for (var i=0; i<pcw.length; i++){
										if (elem[0].innerHTML.indexOf(pcw[i]) > -1)
											parcel = true;
									}
									if (!parcel) {
										elem[0].innerHTML = "Hjemlevering";
										Samuraj.Element.get("#typeOfLiving3", function(selector, elem) {
											elem[0].parentNode.querySelector("label").innerHTML = "Bedrift";
										});
									}
								});
							}
						}
					}
				}
			}
		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "FreightCalc",
		objects: [
			{name: "IBES", fn: IBES},
			{name: "CryptoJS", fn: CryptoJS},
			{name: "CryptoJSInit1", fn: CryptoJSInit1},
			{name: "CryptoJSInit2", fn: CryptoJSInit2},
			{name: "FreightCalc", fn: FreightCalc}
		],
		dependencies: ["User"],
		bootup: function(){
			Samuraj.Version.load('samurajinfotext', false, function() {
				Samuraj.Log.add({
					cat: "FreightCalc",
					sub: "Info",
					text: "Infotext activated"
				});
			});

			//Initiate FreightCalc
			var mw = "";
			if (Samuraj.FreightCalc.mobileCheckout())
				mw = "mw_";
			var host = (location.hostname.indexOf("ikea.com") > -1) ? "secure.ikea.com" : location.hostname;
        	IBES.cnc_configuration_url = '//' + host + '/ms/' + SamurajSetting.Country.locale + '/js/clickcollect_' + mw + 'cart_2b_' + SamurajSetting.Country.locale + '.js';
			Samuraj.CryptoJSInit1();
			Samuraj.CryptoJSInit2();

			Samuraj.Version.load('samurajpup', true, function(e) {
				Samuraj.Source.load('Translation', function(data) {
					Samuraj.Translation.data = data.translation;

					if (Samuraj.Common.checkurl(['*stores/servlet*', "*/mcommerce/shoppingcart*"])) {
						Samuraj.Source.load('Freight LCDPrices', function(data) {
							Samuraj.FreightCalc.init(data);

							//Test setup for local testers
							Samuraj.Version.load("klarnatesting", false, function(){
								Settings.forcezipcodeentry = true;
								var elem = document.getElementById("samuraj-pay-with-other-card-wrapper");
								if (elem) elem.style.setProperty("display", "block");
								window.MaxymiserKlarnaIRWhs = true;
								window.MaxymiserKlarnaIRWpp = true;
								window.MaxymiserKlarnaM2hs = true;
								window.MaxymiserKlarnaM2pp = true;
							});
							//Klarna override
							Samuraj.Version.load("iamabot", false, function(){
								window.iamabot = true;
							});
							if (document.URL.match(/[?|&]klarna=disabled/g)){
								window.iamabot = true;
							}

							Samuraj.Log.add({
								cat: "FreightCalc",
								sub: "Timing",
								text: "Loading done",
								timing: performance.now() - SamurajTiming				
							});
						});
					}
				});
			});
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();
