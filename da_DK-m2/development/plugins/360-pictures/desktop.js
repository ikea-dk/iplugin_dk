(function () {
    "use strict";

    function ready(fn) {
        if (document.attachEvent ? document.readyState == "complete" : document.readyState !== "loading") {
            fn();
        } else {
            document.addEventListener("DOMContentLoaded", fn);
        }
    }

    function insertThreesixty() {
        var request = new XMLHttpRequest();
        // insert url to your localhost or iplugins pointing to the datafile below
        request.open("GET", "https://m2.ikea.com/dk/da/data-sources/da66dd80e7ed11e88b197772665f8a1b.json", true);

        request.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                var data = JSON.parse(this.response);
                data.forEach(function (index, i) {
                    var itemNumber = document.getElementById("itemNumber");
                    var thumbContainer = "<div style='background-image: url(https://ww9.ikea.com/ext/360/" + index.object + "/" + index.thumb + "); background-size: 90%; background-position: center center; width: 110px; height: 112px; border: 1px solid rgb(238, 238, 238);' class='imageThumb' id='threeThumb'> <a href='javascript:void(0);' id='threeThumb'> <img id='360iconThumb' class='threesixThumb' src='https://ww8.ikea.com/ext/iplugins/sv_SE/development/plugins/360-rotation-pip/images/360-icon.svg' onmouseover='addOpacityEffect(this.id);' onmouseout='rmvOpacityEffect(this.id);' id='threeThumb'></a> </div>";
                    if (!itemNumber == false) {
                        var splitID = index.id.split(",");
                        splitID.forEach(function (splitID, i) {
                            if ((itemNumber.textContent.split(".").join("").indexOf(splitID) > -1)) {
                                var imageThumbnail = document.getElementById("imageThumb_0");

                                imageThumbnail.insertAdjacentHTML("afterend", thumbContainer);
                                var threeThumb = document.getElementById("threeThumb");
                                var iframeLoaded = false;
                                console.log(iframeLoaded);
                                var productImage = document.getElementById("productImg");
                                // Make the product image invisible and the 360 model visible if the 360 thumbnail is clicked
                                threeThumb.addEventListener("click", function () {
                                    if (!iframeLoaded == true) {
                                        iframeLoaded = true;
                                        document.getElementById("productImg").insertAdjacentHTML("afterend", "<iframe id='360Frame' frameborder='0'  width='560px' height='560px' src='https://ww9.ikea.com/ext/360/index.html?itemID=" + index.object + "'></iframe>");
                                    }
                                    document.getElementById("360Frame").style.display = "inline";
                                    threeThumb.style.borderColor = "#3399fb";
                                    productImage.style.display = "none";
                                    var activeClass = document.querySelector("a.active");
                                    if (activeClass != undefined) {
                                        activeClass.classList.remove("active");
                                    }
                                    // Fix conflicts with Video PIP plugin
                                    if (document.getElementById("productVideo") != undefined) {
                                        var productVideo = document.getElementById("productVideo");
                                        productVideo.style.display = "none";
                                        document.getElementById("videoDiv_1").style.borderColor = "#eee";
                                        var frameWindow = document.getElementById("productVideo").contentWindow;
                                        frameWindow.postMessage("pause", "*");
                                        if (document.getElementsByClassName('firstThumb')[0] != undefined) {
                                            var thumbNode = document.getElementsByClassName('firstThumb')[0];
                                            thumbNode.classList.add("active");
                                        }
                                    }

                                });
                                // Hide the 360 model again and re-show the image after any other thumbnail is clicked
                                // Fixes for IE below
                                var isIE = /*@cc_on!@*/ false || !!document.documentMode;
                                var isEdge = !isIE && !!window.StyleMedia;
                                var imageThumbnails = document.querySelectorAll("[id^='imageThumb_']");

                                if ((isIE == true) || (isEdge == true) ) {
                                    var iconThumb = document.getElementById("360iconThumb");
                                    iconThumb.src = "https://ww8.ikea.com/ext/iplugins/sv_SE/development/plugins/360-rotation-pip/images/360-icon.png";
                                    iconThumb.style.height = "45%";
                                    iconThumb.style.paddingTop = "30px";

                                    function forEach(array, fn) {
                                        for (var i = 0; i < array.length; i++)
                                            fn(array[i], i);
                                    }
                                    forEach(imageThumbnails, function (arrayItem) {
                                        arrayItem.addEventListener("click", function () {
                                            document.getElementById("360Frame").style.display = "none";
                                            threeThumb.style.borderColor = "#eee";
                                            productImage.style.display = "inline";
                                        });
                                    });
                                } else {
                                    imageThumbnails.forEach(function (arrayItem) {
                                        arrayItem.addEventListener("click", function () {
                                            document.getElementById("360Frame").style.display = "none";
                                            threeThumb.style.borderColor = "#eee";
                                            productImage.style.display = "inline";
                                        });
                                    });
                                }
                                
                                
                                // Fix conflicts with Video PIP plugin
                                if (document.getElementById("videoDiv_1") != undefined) {
                                    document.getElementById("videoDiv_1").addEventListener("click", function () {
                                        document.getElementById("360Frame").style.display = "none";
                                        document.getElementById("threeThumb").style.borderColor = "#eee";
                                    });
                                }
                            }
                        });
                    }
                });
            } else {
                console.log(this.status);
            }
        };
        request.onerror = function () {
            console.log(this.status);
        };
        request.send();
    }

    var remove = function (element) {
        element.parentNode.removeChild(element) || element.srcElement.removeChild(element);
    };

    var updateThumbLocation = function () {
        remove(document.getElementById("360Frame"));
        remove(document.getElementById("threeThumb"));
        insertThreesixty();
        var productImage = document.getElementById("productImg");
        productImage.style.display = 'inline';
    };

    ready(insertThreesixty);
    window.addEventListener('hashchange', updateThumbLocation);
})();