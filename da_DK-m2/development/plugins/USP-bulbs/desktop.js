'use strict';

/* var path = "css";
var style = document.createElement('link');
style.rel = 'stylesheet';
style.type = 'text/css';
style.href = 'https://www.ikea.com/ms/da_DK/XCLm55MBAS/USP-PLP-M2.css';
document.getElementsByTagName('head')[0].appendChild(style); */

loadbulbs();
// Load the JSON file 
function loadbulbs() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var bulbs = JSON.parse(this.responseText);

      var allProducts = document.getElementsByClassName("product-compact");
      for (var i = 0; i < allProducts.length; i++) {

        var product = allProducts[i];
        for (var x = 0; x < bulbs.length; x++) {

          //get productID in div
          var link = product.children[0].children[0].href;
          var substrings = link.split("-");
          var productID = substrings[substrings.length - 1].replace("/", "");

          //get productID in JSON
          var productID_json = bulbs[x].Article.toString();

          if (productID === productID_json) {
            console.log("Product matches from div with JSON");
            console.log("ID from DIV:" + productID);
            console.log("ID from JSON:" + productID_json);

            var container = document.createElement("mathilde");

            container.innerHTML = '\n                      <ul.a>\n                          <li class = "usp"><span class="underrubrik"> ' + bulbs[x].Navn + ' </span></li> \n                          <li class = "usp">Levetid<span class="text-yes"> ' + bulbs[x].Levetid + ' </span></li> \n                          <li class = "usp">Lysstyrke<span class ="text-yes"> ' + bulbs[x].Lysstyrke + '  </span></li> \n                          <li class = "usp">Lysfarve<span class ="text-yes"> ' + bulbs[x].Lysfarve + '  </span></li> \n                          <li class = "usp">RA-v\xE6rdi<span class ="text-yes"> ' + bulbs[x].RAvardi + '  </span></li> \n                          <li class = "usp"> ' + (bulbs[x].Dampbar ? 'Kan d\xE6mpes<span class = "icon-yes"' + bulbs[x].Dampbar + '</span>' : '<div class = "text-no">Kan d\xE6mpes</div><span class = "icon-no" ' + bulbs[x].Dampbar) + '</span></li>\n                          <li class = "usp"> ' + (bulbs[x].Tradlos ? 'Kan styres tr\xE5dl\xF8st<span class = "icon-yes"' + bulbs[x].Tradlos + '</span>' : '<div class = "text-no">Kan styres tr\xE5dl\xF8st</div><span class = "icon-no" ' + bulbs[x].Tradlos) + '</span></li>\n                      </ul>';
            var produkt = document.body.querySelector('.product-compact__type').parentNode;
            var sp2 = document.body.querySelector('.product-compact__type');
            produkt.replaceChild(container, sp2);
          }
        }
      }
    }
  };
  //Get the JSON file
  xhttp.open('GET', 'https://m2.ikea.com/dk/da/data-sources/c6841c90f32011e88ab777e4adf12617.json', true);
  xhttp.send();
}
