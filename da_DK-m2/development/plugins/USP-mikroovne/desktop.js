'use strict';

(function () {
  var path = "css";
  var style = document.createElement('link');
  style.rel = 'stylesheet';
  style.type = 'text/css';
  style.href = 'https://www.ikea.com/ms/da_DK/XCLm55MBAS/USP-PLP-M2.css';
  document.getElementsByTagName('head')[0].appendChild(style);
  if (document.URL.indexOf("/cat/") > -1) {
    // Load the JSON file 
    var loadMikroovne = function loadMikroovne() {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          var mikroovne = JSON.parse(this.responseText);

          var allProducts = document.getElementsByClassName("product-compact");
          for (var i = 0; i < allProducts.length; i++) {

            var product = allProducts[i];
            for (var x = 0; x < mikroovne.length; x++) {

              //get productID in div
              var link = product.children[0].children[0].href;
              var substrings = link.split("-");
              var productID = substrings[substrings.length - 1].replace("/", "");

              //get productID in JSON
              var productID_json = mikroovne[x].Article.toString();

              if (productID === productID_json) {
                console.log("Product matches from div with JSON");
                console.log("ID from DIV:" + productID);
                console.log("ID from JSON:" + productID_json);

                var container = document.createElement("mark");

                container.innerHTML = '\n                      <ul.a>\n                          <li class = "usp">Ovnrum liter<span class="text-yes"> ' + mikroovne[x].Ovnrum + ' </span></li> \n                          <li class = "usp">Effekt maks.<span class ="text-yes">   ' + mikroovne[x].Effekt + '  </span></li> \n                          <li class = "usp">Programmer<span class ="text-yes">   ' + mikroovne[x].Programmer + '  </span></li> \n                          <li class = "usp"> ' + (mikroovne[x].Grillfunktion ? 'Grillfunktion<span class = "icon-yes"' + mikroovne[x].Grillfunktion + '</span>' : '<div class = "text-no">Grillfunktion</div><span class = "icon-no" ' + mikroovne[x].Grillfunktion) + '</span></li>\n                          <li class = "usp"> ' + (mikroovne[x].Varmluft ? 'Varmluft<span class = "icon-yes"' + mikroovne[x].Varmluft + '</span>' : '<div class = "text-no">Varmluft</div><span class = "icon-no" ' + mikroovne[x].Varmluft) + '</span></li>\n                          <li class = "usp"> ' + (mikroovne[x].Dampfunktion ? 'Dampfunktion<span class = "icon-yes"' + mikroovne[x].Dampfunktion + '</span>' : '<div class = "text-no">Dampfunktion</div><span class = "icon-no" ' + mikroovne[x].Dampfunktion) + '</span></li>\n                          <li class = "usp"> ' + (mikroovne[x].Garanti ? 'Garanti<span class = "text-yes">' + mikroovne[x].Garanti + '</span>' : '<div class = "text-no">Garanti</div><span class = "icon-no" ' + mikroovne[x].Garanti) + '</span></li>\n                      </ul>';
                var _product = document.body.querySelector('.product-compact__type').parentNode;
                var sp2 = document.body.querySelector('.product-compact__type');
                _product.replaceChild(container, sp2);
              }
            }
          }
        }
      };
      //Get the JSON file
      xhttp.open('GET', 'https://m2.ikea.com/dk/da/data-sources/877348d0b5ce11e8837eb18effe1b65c.json', true);
      xhttp.send();
    };

    loadMikroovne();
  };
})();
