(function(){
	var LIB = {
		Common: {
			toInt: function(str) {
				try {
					if (this.isInt(str)){
						try {
							str = parseFloat(str);
							return str;
						} catch (err){
							return str;
						}
					} else if (this.isFloat(str)){
							try {
								str = Math.round(str);
								return str;
							} catch (err){
								return str;
							}
						} else if (str.slice(0, 1) === '-') {
								str = str.split(',')[0];
								var t = parseInt(str.replace(/\D/g, ''));
								if (isNaN(t)) t = 0;
								return t - (t * 2);
							} else {
								str = str.split(',')[0];
								var t = parseInt(str.replace(/\D/g, ''));
								if (isNaN(t)) t = 0;
								return t;
							}
				} catch (err) {
					return -1;
				}
			},
			isInt: function(value) {
				if (isNaN(value)) {
					return false;
				}
				var x = parseFloat(value);
				var y = (x | 0) === x;
				return y;
			},
			isFloat: function(n) {
				return Number(n) === n && n % 1 !== 0;
			},
			isArray: function(obj) {
				return Object.prototype.toString.call(obj) === '[object Array]';
			},
			isNodeList: function(obj) {
				return Object.prototype.toString.call(obj) === '[object NodeList]';
			},
			isObject: function(obj) {
				return Object.prototype.toString.call(obj) === '[object Object]';
			},
			isFunction: function(obj) {
				return Object.prototype.toString.call(obj) === '[object Function]';
			},
		}, //Common
		Element: {
			get: function(input){
				if (!LIB.Common.isObject(input)) { input = {}; }
				input.baseelem = input.baseelem || document;
				if (input.selector) {
					var timeout = input.timeout || 5000;
					var interval = input.interval || 20;

					var elem = input.baseelem.querySelectorAll(input.selector);
					if (input.elementnumber){
						input.elementnumber = LIB.Common.toInt(input.elementnumber);
						if (elem.length >= LIB.Common.toInt(input.elementnumber)){
							elem = [elem[input.elementnumber]];
						} else elem = [];
					}
					if (elem.length > 0) {
						if (LIB.Common.isFunction(input.success)) { input.success(elem, input.context); }
					} else {
						window.setTimeout(() => {
							input.timeout = timeout - interval;
							if (input.timeout > 0) { LIB.Element.get(input); } else if (LIB.Common.isFunction(input.failure)) { input.failure(elem, input.context); }
						}, interval);
					}
				} else if (LIB.Common.isFunction(input.failure)) { input.failure(elem, input.context); }
			},
		}, //Element
	}; //LIB 

    var products = '20411185,70400839,90407953,90400838,10400837,90411672,80400829,50410245,10410247,410163,90410168,50411184,60405007,10404958,90411158,50411160,60416001,70410485,90408231,80411658,20411661,30408229,30411665,50406333,20406339,20406344,30406348,90406350,80402418,70402414,90402427,30404962,20394082';
    var prod = products.split(',');
    for (var i = 0; i < prod.length; i++){
        if (document.URL.indexOf('/catalog/products/art/') > -1 && document.URL.indexOf(prod[i]) > -1){
            //This function will check for the existence of element with selector #salesArg every 20 millisecond for 5000 milliseconds. (5 seconds)
            //When the element is found the function success is triggered, if not the function failure is triggered (after timeout, not used here)
            LIB.Element.get({
				selector: '.summary-benifit', //Required
				baseelem: document, //Optional - Default = document
				timeout: 5000,			//Optional - Default = 5000
				interval: 20, //Optional - Default = 20
				success: function(salesArg){
					salesArg = salesArg[0];
					var newText = '<b>SÆRKOLLEKTION – BEGRÆNSET ANTAL</b><br>HJÄRTELIG er en særkollektion, der kun sælges i varehusene i en kortere periode. <br>Kollektionen sælges fra 13. april 2018';
					var content = document.createElement('div.pipCollections');
					var parentDiv = document.body.querySelector('.summary-benifit').parentNode;
					var sp2 = document.body.querySelector('.summary-benifit');
					content.innerHTML = newText;
					content.setAttribute('style', 'display: block; float: left; margin: 20px 0; padding: 20px; background-color: #484849; font-family: Verdana, Geneva, Tahoma, sans-serif; font-size: 13px; color: white; line-height: 19px;');
					parentDiv.insertBefore(content, sp2);
				},
            });
        }
    }
})();
