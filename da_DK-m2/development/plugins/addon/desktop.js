/*

                      
  /\   _|  _|  _  ._  
 /--\ (_| (_| (_) | | 
                      

*/
(function(){

	var Addon = {

		activemodules: [],
		activeaddonproducts: [],
		settings: {
			uselocation: false,
			showunbuyable: false,
			translations: {
				popup_heading: "Et nyt vindue åbnes. Trykk på Esc knappen og vinduet vil lukkes.",
				popup_added_to_cart_text: "Din vare er nu lagt i indkøbskurven.",
				popup_to_cart_button: "Se din indkøbskurv",
				addon_heading: "",
				addon_product_buy_button: "Læg i indkøbskurv",
				addon_product_in_basked_button: "Lagt i indkøbskurv",
				addon_product_buy_error: "Hov, der opstod en teknisk fejl"
			},
			/*
			translations: {
				popup_heading: "A new window will open. Press the Esc button and the widow will close",
				popup_added_to_cart_text: "The product has been added to the shopping cart.",
				popup_to_cart_button: "Go to cart",
				addon_heading: "Addons",
				addon_product_buy_button: "Buy now",
				addon_product_buy_error: "We have technical difficulties right now"
			},
			*/
		},
		init: function(data){
			var c = Samuraj.Common;

			//Check if PIP page
			if (document.URL.indexOf("/catalog/products/") > -1 || document.URL.indexOf("/p/") > -1){

				Samuraj.AddonSettings = this.settings;
				Samuraj.Addon.repeat = false;
				Samuraj.Time.get(function(time, data){
					//Samuraj.AddonSettings.TimeNow = time;
					//Samuraj.AddonSettings.TimeSet = true;

					if (c.checkMobile()){

						var counter = data.addon.length;
						for (var i=0; i<data.addon.length; i++){
							data.addon[i].modulenr = i;
							var module = data.addon[i];		
							if (module.active){

								//WHEN - checking if it is with the timesettings				
								Samuraj.Validation.dateValidation({
									name: "Addon",
									module: module, 
									success: function(module){
										
										//WHERE - checking if the page is valid, and if so call validateCustomer and check customer location as well
										Samuraj.Validation.validatePage({
											name: "Addon",
											uselocation: Samuraj.Addon.settings.uselocation,
											module: module, 
											success: function(module){
												
											
												//DEVICE - checking if the device setting is valid
												Samuraj.Validation.validateDevice({
													name: "Addon",
													module: module, 
													success: function(module){
																
														//Check targeting and add product elements to module
														module.products = {};
														if (module.where && module.where.products){
															Samuraj.Targeting.products(module.where.products, function(products){
																module.products = products;

																//WHAT - Inject module content to page
																if (c.varExist(products.pagetype)){
																	switch(products.pagetype) {
																		case "pip":
																			if(products.dom.main.data.length > 0){
																				if (module.what && module.what.modules){
																					Samuraj.Addon.activemodules = Samuraj.Addon.activemodules.concat(module.what.modules);
																				}
																			}
																			break;
																		case "plp":
																			//future dev
																			break;
																		case "search":
																			//future dev
																			break;
																		case "availability":
																			//future dev
																			break;
																		case "wishlist":
																			//future dev
																			break;
																	}
																}
																counter--;
																if (counter==0)
																	Samuraj.Addon.injectAddon();
															});
														} else {
															counter--;
															if (counter==0)
																Samuraj.Addon.injectAddon();
														}

													},
													failure: function(module){
														counter--;
														if (counter==0)
															Samuraj.Addon.injectAddon();
													}
												});
											},
											failure: function(module){
												counter--;
												if (counter==0)
													Samuraj.Addon.injectAddon();
											}
										});
									},
									failure: function(module){
										counter--;
										if (counter==0)
											Samuraj.Addon.injectAddon();
									}
								});

							} else {
								counter--;
								if (counter==0)
									Samuraj.Addon.injectAddon();
							}
						}
						if (data.addon.length == 0){
							Samuraj.Addon.injectAddon();
						}
					} else {
						if (!Samuraj.Addon.repeat){
							Samuraj.Addon.repeat = true;
							Samuraj.Element.get("#footer", function(selector, elem){
								c.monitorUrlChange({
									data: data,
									onchange: function(e){
										//setTimeout(function(){
											Samuraj.Addon.activemodules = [];
											Samuraj.Addon.activeaddonproducts = [];
											if (Samuraj.Targeting.addonreload)
												Samuraj.Addon.reload = true;

											var counter = data.addon.length;
											for (var i=0; i<data.addon.length; i++){
												
												var module = JSON.parse(JSON.stringify(data.addon[i]));
												module.modulenr = i;

												if (module.active){

													//WHEN - checking if it is with the timesettings				
													Samuraj.Validation.dateValidation({
														name: "Addon",
														module: module, 
														success: function(module){
															
															//WHERE - checking if the page is valid, and if so call validateCustomer and check customer location as well
															Samuraj.Validation.validatePage({
																name: "Addon",
																module: module, 
																success: function(module){
																	
																	//DEVICE - checking if the device setting is valid
																	Samuraj.Validation.validateDevice({
																		name: "Addon",
																		module: module, 
																		success: function(module){
																											
																			//Check targeting and add product elements to module
																			module.products = {};
																			if (module.where && module.where.products){
																				Samuraj.Targeting.products(module.where.products, function(products){
																					module.products = products;

																					//WHAT - Inject module content to page
																					if (c.varExist(products.pagetype)){
																						switch(products.pagetype) {
																							case "pip":
																								if(products.dom.main.data.length > 0){
																									if (module.what && module.what.modules){
																										Samuraj.Addon.activemodules = Samuraj.Addon.activemodules.concat(module.what.modules);
																									}
																								}
																								break;
																							case "plp":
																								//future dev
																								break;
																							case "search":
																								//future dev
																								break;
																							case "availability":
																								//future dev
																								break;
																							case "wishlist":
																								//future dev
																								break;
																						}
																					}
																					counter--;
																					if (counter==0)
																						Samuraj.Addon.injectAddon();
																				});
																			}

																		},
																		failure: function(module){
																			counter--;
																			if (counter==0)
																				Samuraj.Addon.injectAddon();
																		}
																	});
																},
																failure: function(module){
																	counter--;
																	if (counter==0)
																		Samuraj.Addon.injectAddon();
																}
															});
														},
														failure: function(module){
															counter--;
															if (counter==0)
																Samuraj.Addon.injectAddon();
														}
													});

												} else {
													counter--;
													if (counter==0)
														Samuraj.Addon.injectAddon();
												}
											}
											if (data.addon.length == 0){
												Samuraj.Addon.injectAddon();
											}
										//}, 500);
									}
								});
							});
						}


					}				
				}, data);
			}
		},
		injectAddon: function(){
			var c = Samuraj.Common;
			var activemodules = Samuraj.Addon.activemodules;
			
			if (c.checkMobile()){
				for (var i=0; i<activemodules.length; i++){
					activemodules[i].id = i;
					activemodules[i].html = activemodules[i].html || {};
					activemodules[i].html.content = activemodules[i].html.content || "";
					activemodules[i].html.link = activemodules[i].html.link || "";
					if (activemodules[i].product && activemodules[i].product.artnr){
						activemodules[i].product.heading = activemodules[i].product.heading || "";
						activemodules[i].html.link = "https://m2.ikea.com/" + SamurajSetting.Country.homePath + "/p/-" + activemodules[i].product.artnr.toLowerCase() +"/";
						
						if (Samuraj.AddonSettings.showunbuyable){
							this.activeaddonproducts.push(activemodules[i].product.artnr);
						} else {
							Samuraj.API.Common.GetItemInfo({
								i: i,
								product: activemodules[i].product.artnr,	
								success: function(data){
									if (data && data.RetailItemComm && data.RetailItemComm.OnlineSellable.$){
										Samuraj.Addon.activeaddonproducts.push(Samuraj.Addon.activemodules[this.i].product.artnr);
									} else {
										Samuraj.Addon.activemodules[this.i] = {empty: true};
									}

								},
								failure: function(){
									Samuraj.Addon.activemodules[this.i] = {empty: true};
								}
							});
						}
					}
				}

				if (!Samuraj.Addon.injected){
					Samuraj.Addon.injected = true;

					c.addEvent(window, "resize", function(e){
						if (Samuraj.Addon.swiper)
							Samuraj.Addon.swiper.update();
					});
					
					//Click events for M2
					var buybutton = document.querySelector(".js-purchase-add-to-cart");
					if (buybutton){
						c.addEvent(buybutton, "click", function(e){
							Samuraj.Addon.addCarousel();
						});
					}
					/*
					var buybuttonbottom = document.querySelector(".bottom-bar-purchase button.button--primary");
					if (buybuttonbottom){
						c.addEvent(buybuttonbottom, "click", function(e){
							Samuraj.Addon.addCarousel();
						});
					}
					*/

					/*
					var hidepopup = document.createElement("style");
					hidepopup.innerHTML = ".range-popup-frame{visibility: hidden}";
					document.head.appendChild(hidepopup);
					*/
				}
			} else {
				Samuraj.Targeting.addonreload = true;

				for (var i=0; i<activemodules.length; i++){
					activemodules[i].id = i;
					activemodules[i].html = activemodules[i].html || {};
					activemodules[i].html.content = activemodules[i].html.content || "";
					activemodules[i].html.link = activemodules[i].html.link || "";
					if (activemodules[i].product && activemodules[i].product.artnr){
						activemodules[i].product.heading = activemodules[i].product.heading || "";
						activemodules[i].html.link = "https://www.ikea.com/" + SamurajSetting.Country.homePath + "/catalog/products/" + activemodules[i].product.artnr.toUpperCase() +"/";
						
						if (Samuraj.AddonSettings.showunbuyable){
							this.activeaddonproducts.push(activemodules[i].product.artnr);
						} else {
							Samuraj.API.Common.GetItemInfo({
								i: i,
								product: activemodules[i].product.artnr,	
								success: function(data){
									if (data && data.RetailItemComm && data.RetailItemComm.OnlineSellable.$){
										Samuraj.Addon.activeaddonproducts.push(Samuraj.Addon.activemodules[this.i].product.artnr);
									} else {
										Samuraj.Addon.activemodules[this.i] = {empty: true};
									}

								},
								failure: function(){
									Samuraj.Addon.activemodules[this.i] = {empty: true};
								}
							});
						}
					}
				}
	
				if (!Samuraj.Addon.injected){
					Samuraj.Addon.injected = true;
	
					c.addEvent(window, "resize", function(e){
						if (Samuraj.Addon.swiper)
							Samuraj.Addon.swiper.update();
					});
					
					//Click events for IRW
					Samuraj.Element.get("#jsButton_buyOnline_lnk", function(selector, elem){
						if (elem[0] && elem[0].parentNode){
							c.addEvent(elem[0].parentNode, "click", function(e){
								Samuraj.Addon.addCarousel();
							});
						}
					});
					Samuraj.Element.get("#sticky-product .svgCartButtonContainer button.button", function(selector, elem){
						if (elem[0] && elem[0].parentNode){
							c.addEvent(elem[0].parentNode, "click", function(e){
								Samuraj.Addon.addCarousel();
							});
						}
					});

					/*
					var hidepopup = document.createElement("style");
					hidepopup.innerHTML = ".range-popup-frame{display: none;}";
					document.head.appendChild(hidepopup);
					*/
				}
			}

		},
		addCarousel: function(){
			var c = Samuraj.Common;
			for (var i=Samuraj.Addon.activemodules.length-1; i>-1; i--){
				if (Samuraj.Addon.activemodules[i].empty)
					Samuraj.Addon.activemodules.splice(i, 1);
			}

			if(c.checkMobile()){
				Samuraj.Element.get(".range-popup-frame", function(selector, elem){
					var popupframe = elem[0];
					popupframe.style.setProperty("display", "none");
					
					var popup = popupframe.parentNode;
									
					c.addClass(popup, "samuraj-purchase-popup");
					
					
					//Add continue shopping button
					/*
					var rangepopup = document.querySelector(".samuraj-purchase-popup .range-popup-frame .range-popup");
					c.monitorDomChange({
						baseelem: rangepopup,
						onchange: function(e){
							var rangepopup = document.querySelector(".samuraj-purchase-popup .range-popup-frame .range-popup");
							if (rangepopup){
								var checkoutbutton = rangepopup.querySelector("a.button");
								if (checkoutbutton){
									c.removeClass(checkoutbutton, "fill");
									c.removeClass(checkoutbutton, "button--primary");
									c.addClass(checkoutbutton, "samuraj-button--primary");
									rangepopup.appendChild(checkoutbutton);

								}
							}
						}
					});
					*/
					//checkoutbutton.insertAdjacentHTML('beforebegin', '<button id="samuraj-continue-shopping" type="button" class="button button--secondary text--center" name="Legg til i handleliste"><span class="white-space">Fortsett å handle</span></button>');
					//checkoutbutton.insertAdjacentHTML('afterend', '<a href="https://www.ikea.com/' + SamurajSetting.Country.homePath + '/mcommerce/shoppingcart" class="button samuraj-button--primary  button__loader text--center "><span class="icon icon__shoppingbag"></span><span>' + Samuraj.Addon.settings.translations.popup_to_cart_button + '</span></a>');

					//Add close function to continue shopping
					/*
					var continueshoppingbutton = document.querySelector(".samuraj-purchase-popup #samuraj-continue-shopping");
					c.addEvent(continueshoppingbutton, "click", function(e){
						var closebutton = document.querySelector(".range-popup-frame__close-button");
						closebutton.click();						
					});
					*/

					//If there are addon modules, inject them.
					if (Samuraj.Addon.activemodules.length>0){
						Samuraj.Product.getProductsHtml(Samuraj.Addon.activeaddonproducts, function(producthtml){
							//Create slides
							var j=0, slides="";
							var activemodules = Samuraj.Addon.activemodules;
							for (var i=0; i<activemodules.length; i++){
								var slidestyle = 'style="width: 100%"';
								if (activemodules[i].product && activemodules[i].product.artnr){
									slidestyle = "";
									if (producthtml[j] && !producthtml[j].failed){
										activemodules[i].html.content = '<div class="samuraj-header">' + activemodules[i].product.heading + '</div><div class="samuraj-add-buy" data-artnr="' + activemodules[i].product.artnr + '">' + producthtml[j].html + '</div>';
									}
									j++;
								}
								if (activemodules[i].html && !activemodules[i].html.content){
									activemodules[i].html.content = "";
									activemodules[i].html.image = activemodules[i].html.image || {};
									
									activemodules[i].html.image.url = activemodules[i].html.image.url || "";
									if (activemodules[i].html.image.url !== ""){
										activemodules[i].html.image.location = activemodules[i].html.image.location || "left";
										activemodules[i].html.image.location = "samuraj-image-" +activemodules[i].html.image.location;
										activemodules[i].html.image.alt = activemodules[i].html.image.alt || "";
										activemodules[i].html.image.style = activemodules[i].html.image.style || "";
										activemodules[i].html.image.width = activemodules[i].html.image.width || "";
										
										var styleimage = "", stylewidth = "";
										if (activemodules[i].html.image.style !== ""){
											styleimage += ' style="' + activemodules[i].html.image.style + '"';
										}
										if (activemodules[i].html.image.width !== ""){
											stylewidth = ' style="width: ' + parseInt(activemodules[i].html.image.width) + '%;"';
										}
										activemodules[i].html.content += '<div class="samuraj-imagecontainer ' + activemodules[i].html.image.location + '"' + stylewidth + '><img src="' + activemodules[i].html.image.url + '" alt="' + activemodules[i].html.image.alt + '"' + styleimage + '></div>';
									}
									if (c.varExist(activemodules[i].html.text, true)){
										activemodules[i].html.content += '<div class="samuraj-textcontainer"><div>' + activemodules[i].html.text + '</div></div>';
									}
								}
								if (c.varExist(activemodules[i].html.content, true)){
									if (c.varExist(activemodules[i].html.link, true))
										slides += '    <div class="swiper-slide"><div id="samuraj-slide-' + i + '" ' + slidestyle + ' class="samuraj-slide samuraj-link" data-href="' + activemodules[i].html.link + '" onclick="document.location=\'' + activemodules[i].html.link + '\';">' + activemodules[i].html.content + '</div></div>';
									else
										slides += '    <div class="swiper-slide"><div id="samuraj-slide-' + i + '" ' + slidestyle + ' class="samuraj-slide">' + activemodules[i].html.content + '</div></div>';
								}

							}
							if (slides !== ""){
								var addonHtml = '' +
								'<div id="samuraj-swiper-header" class="samuraj-swiper-header">' + Samuraj.Addon.settings.translations.addon_heading + '</div>' +
								'<div id="swiper-container" class="swiper-container" >' +
								'  <div class="swiper-wrapper">' +
									slides +
								'  </div>' +
								'</div>';
								if (activemodules.length > 1){
									addonHtml += '' +
									'<div id="swiper-pagination" class="swiper-pagination"></div>' +
									'<div id="swiper-button-prev" class="swiper-button-prev"></div>' +
									'<div id="swiper-button-next" class="swiper-button-next"></div>';
								}
						
								var html = '' +
								'<div class="samuraj-addonproduct">' +
									addonHtml +
								'</div>';
						
								var rangepopup = document.querySelector(".samuraj-purchase-popup .range-popup")
								rangepopup.insertAdjacentHTML('beforeend', html);
								
								var products = document.querySelectorAll(".samuraj-purchase-popup .product-compact__image-container");
								for (var i=0; i<products.length; i++){
									var elem = document.createElement("div");
									elem.className = "product-details";
									var parent = products[i].parentNode;
									parent.appendChild(elem);
									for (var j=parent.childNodes.length-1; j>-1; j--){
										if (parent.childNodes[j].className !== "product-compact__image-container" && parent.childNodes[j].className !== "product-details"){
											elem.insertBefore(parent.childNodes[j], elem.firstChild);
										}
									}
									var energyflag = parent.parentNode.querySelector(".product-compact__energy-flag-link");
									if (energyflag) 
										elem.appendChild(energyflag);
									var dates = parent.parentNode.querySelector(".product-compact__valid-dates");
									if (dates) 
										elem.appendChild(dates);
									var price = parent.parentNode.querySelector(".product-compact__prev-price");
									if (price) 
										elem.appendChild(price);

								}
								var addbuy = rangepopup.querySelectorAll(".samuraj-add-buy");
								for (var i=0; i<addbuy.length; i++){

									Samuraj.API.Common.GetItemInfo({
										i: i,
										product: addbuy[i].getAttribute("data-artnr"),	
										success: function(data){
											var currentaddbuy = addbuy[this.i];
											if (data && data.RetailItemComm && data.RetailItemComm.OnlineSellable.$){
												currentaddbuy.insertAdjacentHTML('beforeend', '<div class="samuraj-addtocart-container"><a data-artnr="' + currentaddbuy.getAttribute("data-artnr") + '" class="button button--primary samuraj-addon-addtocart button__loader text--center "><span class="icon icon__shoppingbag"></span><span>' + Samuraj.Addon.settings.translations.addon_product_buy_button + '</span><span class="samuraj-circle"><span class="samuraj-checkmark"></span></span></a><div class="samuraj-addtocart-error">' + Samuraj.Addon.settings.translations.addon_product_buy_error + '</div></div>');
											
												var addtocart = currentaddbuy.querySelector(".samuraj-addtocart-container .samuraj-addon-addtocart");
												c.addEvent(addtocart, "click", function(e){
													e.preventDefault();
													e.stopPropagation();
													var button = e.currentTarget;
													Samuraj.API.Common.AddToCart({
														products: button.getAttribute("data-artnr"),
														loading: function(response){
															c.addClass(button, "button__loader--active");
															
															var circle = button.querySelector(".samuraj-circle");
															if (circle) {
																circle.style.setProperty("display", "none");
															}
															var checkmark = button.querySelector(".samuraj-checkmark");
															if (checkmark) {
																checkmark.style.setProperty("display", "none");
															}
															var addtocarterror = button.querySelector(".samuraj-addtocart-error");
															if (addtocarterror) {
																addtocarterror.style.setProperty("display", "none");
															}
														},
														success: function(response, test1, test2){
															var shoppingbagquantity = document.querySelectorAll(".header__shopping-bag-quantity");
															if (shoppingbagquantity.length > 0){
																shoppingbagquantity[shoppingbagquantity.length-1].innerHTML = parseInt(shoppingbagquantity[shoppingbagquantity.length-1].innerHTML) + 1;
															}
															c.removeClass(button, "button__loader--active");
															var circle = button.querySelector(".samuraj-circle");
															if (circle) {
																circle.style.setProperty("display", "inline");
															}
															var checkmark = button.querySelector(".samuraj-checkmark");
															if (checkmark) {
																setTimeout(function(){
																	checkmark.style.setProperty("display", "inline");
																	/*
																	setTimeout(function(){
																		checkmark.style.setProperty("display", "none");
																		
																	},3000);
																	*/
																}, 400);
															}
															
															/**
															 * Adding tracking when addon product is added to cart
															 */
															try {
																var product_artnr = button.getAttribute("data-artnr");
																var product_name = button.parentNode.parentNode.querySelector(".product-compact .product-details .product-compact__name").innerHTML;
																var product_price = button.parentNode.parentNode.querySelector(".product-compact .product-details .product-compact__price .product-compact__price__value").innerHTML;
																
																Samuraj.Tracking.send({
																	category: 'ecommerce',
																	action: 'add_to_cart',
																	label: product_name + " - " + product_artnr,
																	params: {
																		value: product_price,
																		currency: "NOK",
																		items: [
																			{
																				id: product_artnr,
																				name: product_name,
																				price: product_price,
																				quantity: 1
																			}
																		]
	
																	}
																});
															} catch (err){
																
															}
														},
														failure: function(response, test1, test2){
															c.removeClass(button, "button__loader--active");
															var addtocarterror = button.querySelector(".samuraj-addtocart-error");
															if (addtocarterror) {
																addtocarterror.style.setProperty("display", "block");
															}
														},
														exception: function(response, test1, test2){
															c.removeClass(button, "button__loader--active");
															var addtocarterror = button.querySelector(".samuraj-addtocart-error");
															if (addtocarterror) {
																addtocarterror.style.setProperty("display", "block");
															}
														}
													});
												});
											}										
										}
									});

								}

								var params = {
									slidesPerView: '1',
									spaceBetween: 5,
									grabCursor: true
								}
								if (activemodules.length > 1){
									params.navigation = {
										nextEl: '.samuraj-addonproduct .swiper-button-next',
										prevEl: '.samuraj-addonproduct .swiper-button-prev'
									}
									params.pagination = {
										el: '.samuraj-addonproduct .swiper-pagination',
										type: 'bullets'
									}
								}
								//Show popup
								popupframe.style.setProperty("display", "block");
								Samuraj.Addon.swiper = new Swiper(".samuraj-addonproduct .swiper-container", params);

								//Monitor addtocart process, and remove addon if it fails
								var monitoraddtocart = setInterval(function(){
									var firstdiv = document.querySelector(".range-popup-frame .range-popup > div");
									if (firstdiv) var button = firstdiv.querySelector("a.button");
										 
									if (button){
										if (button.className.indexOf("button__loader--active") == -1)
											clearInterval(monitoraddtocart);
									} else {
										clearInterval(monitoraddtocart);
										var addoncontainer = document.querySelector(".range-popup-frame .range-popup .samuraj-addonproduct");
										addoncontainer.style.setProperty("display", "none");
									}

								}, 50);

								/**
								 * Adding view tracking when product has addon
								 */
								Samuraj.Tracking.send({
									category: 'local_tracking_add_on',
									action: 'view',
									label: document.URL
								});

								/**
								 * Adding tracking to slide change
								 */
								Samuraj.Addon.swiper.on('slideChange', function () {
									var data_href = "";
									var elem = document.querySelector(".samuraj-addonproduct .swiper-slide-active .samuraj-slide.samuraj-link");
									if (elem) 
										data_href = elem.getAttribute("data-href") || "";
									Samuraj.Tracking.send({
										category: 'local_tracking_add_on',
										action: 'slide_change',
										label: data_href
									});
								});

								/**
								 * Adding tracking to slide click
								 */
								var elems = document.querySelectorAll(".samuraj-addonproduct .swiper-slide .samuraj-slide");
								for (var i=0; i<elems.length; i++){
									Samuraj.Common.addEvent(elems[i], "click", function(e){
										var data_href = e.currentTarget.getAttribute("data-href") || "";
										Samuraj.Tracking.send({
											category: 'local_tracking_add_on',
											action: 'slide_click',
											label: data_href
										});
									});
								}

							}

							//Samuraj.Addon.setSlideHeight(200);
						});
					} else {
						//Show popup
						popupframe.style.setProperty("display", "block");
					}
				});
			} else {
					
				Samuraj.Element.get(".slPopup", function(selector, elem){
					var slPopup = elem[0];
					slPopup.style.setProperty("display", "none");
					slPopup.insertAdjacentHTML("beforebegin", '<div class="samuraj-purchase-popup samuraj-irw"><div class="range-overlay"></div><div class="range-popup-frame" role="dialog" aria-describedby="popup__heading"><div id="addon_header_dk"><button type="button" data-button-action="close-popup" class="range-popup-frame__close-button"><span class="icon--big icon__close"></span></button><h3 class="sr-only" id="popup__heading">' + Samuraj.Addon.settings.translations.popup_heading + '</h3><p aria-live="assertive">' + Samuraj.Addon.settings.translations.popup_added_to_cart_text + '</p></div><div class="range-popup"><div><a href="/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=' + SamurajSetting.Country.storeId + '&amp;langId=' + SamurajSetting.Country.langId + '&amp;catalogId=11001&amp;orderId=.&amp;newLinks=true" class="button button--primary fill button__loader text--center button__loader--active"><span class="icon icon__shoppingbag"></span><span>' + Samuraj.Addon.settings.translations.popup_to_cart_button + '</span></a></div></div></div></div>');
					
					var popup = document.querySelector(".samuraj-purchase-popup");
					var popupframe = popup.querySelector(".range-popup-frame");
					//Hide popup
					popupframe.style.setProperty("display", "none");

					Samuraj.Element.get({timeout: 15000, baseelem: slPopup, selector: "#slPopupH1"}, function(selector, elem){
						elem = document.querySelector(".samuraj-purchase-popup .range-popup a.button--primary");
						if (elem){
							c.removeClass(elem, "button__loader--active");
							closePopup();
							addProductEvts();
							slPopup.parentNode.removeChild(slPopup);
						}
					});

					
					
					//Add continue shopping button
					//var checkoutbutton = document.querySelector(".samuraj-purchase-popup .range-popup-frame .range-popup a.button")
					//checkoutbutton.insertAdjacentHTML('beforebegin', '<button id="samuraj-continue-shopping" type="button" class="button button--secondary text--center" name="Legg til i handleliste"><span class="icon icon__checkmark icon__shoppinglist--secondary"></span><span class="white-space">Fortsett å handle</span></button>');
					//checkoutbutton.insertAdjacentHTML('beforebegin', '<button id="samuraj-continue-shopping" type="button" class="button button--secondary text--center" name="Legg til i handleliste"><span class="white-space">Fortsett å handle</span></button>');
					



					//Lock page from scrolling
					c.addClass(document.head.parentNode, "samuraj-noscroll");

					//Add close function to overlay
					var overlay = popup.querySelector(".samuraj-purchase-popup .range-overlay");
					c.addEvent(overlay, "click", function(e){
						c.removeClass(document.head.parentNode, "samuraj-noscroll");
						var popup = document.querySelector(".samuraj-purchase-popup");
						if (popup){
							popup.parentNode.removeChild(popup);
						}
					});

					//Add close function to close X
					var closebutton = popup.querySelector(".samuraj-purchase-popup .range-popup-frame__close-button");
					c.addEvent(closebutton, "click", function(e){
						c.removeClass(document.head.parentNode, "samuraj-noscroll");
						var popup = document.querySelector(".samuraj-purchase-popup");
						if (popup){
							popup.parentNode.removeChild(popup);
						}
					});

					//Add close function to continue shopping
					/*
					var continueshoppingbutton = document.querySelector(".samuraj-purchase-popup #samuraj-continue-shopping")
					c.addEvent(continueshoppingbutton, "click", function(e){
						var popup = document.querySelector(".samuraj-purchase-popup");
						if (popup){
							popup.parentNode.removeChild(popup);
						}
					});
					*/

					//If there are addon modules, inject them.
					if (Samuraj.Addon.activemodules.length>0){
						Samuraj.Product.getProductsHtml(Samuraj.Addon.activeaddonproducts, function(producthtml){
							//Create slides
							var j=0, slides="";
							var activemodules = Samuraj.Addon.activemodules;
							for (var i=0; i<activemodules.length; i++){
								
								if (activemodules[i].product && activemodules[i].product.artnr){
									if (producthtml[j] && !producthtml[j].failed){
										activemodules[i].html.content = '<div class="samuraj-header">' + activemodules[i].product.heading + '</div><div class="samuraj-add-buy" data-artnr="' + activemodules[i].product.artnr + '">' + producthtml[j].html + '</div>';
									}
									j++;
								}
								if (activemodules[i].html && !activemodules[i].html.content){
									activemodules[i].html.content = "";
									activemodules[i].html.image = activemodules[i].html.image || {};
									
									activemodules[i].html.image.url = activemodules[i].html.image.url || "";
									if (activemodules[i].html.image.url !== ""){
										activemodules[i].html.image.location = activemodules[i].html.image.location || "left";
										activemodules[i].html.image.location = "samuraj-image-" +activemodules[i].html.image.location;
										activemodules[i].html.image.alt = activemodules[i].html.image.alt || "";
										activemodules[i].html.image.style = activemodules[i].html.image.style || "";
										activemodules[i].html.image.width = activemodules[i].html.image.width || "";
										
										var styleimage = "", stylewidth = "";
										if (activemodules[i].html.image.style !== ""){
											styleimage += ' style="' + activemodules[i].html.image.style + '"';
										}
										if (activemodules[i].html.image.width !== ""){
											stylewidth = ' style="width: ' + parseInt(activemodules[i].html.image.width) + '%;"';
										}
										activemodules[i].html.content += '<div class="samuraj-imagecontainer ' + activemodules[i].html.image.location + '"' + stylewidth + '><img src="' + activemodules[i].html.image.url + '" alt="' + activemodules[i].html.image.alt + '"' + styleimage + '></div>';
									}
									if (activemodules[i].html.text !== ""){
										activemodules[i].html.content += '<div class="samuraj-textcontainer"><div>' + activemodules[i].html.text + '</div></div>';
									}
								}
								if (c.varExist(activemodules[i].html.content, true)){
									if (c.varExist(activemodules[i].html.link, true))
										slides += '    <div class="swiper-slide"><div id="samuraj-slide-' + i + '" class="samuraj-slide samuraj-link" data-href="' + activemodules[i].html.link + '" onclick="document.location=\'' + activemodules[i].html.link + '\';">' + activemodules[i].html.content + '</div></div>';
									else
										slides += '    <div class="swiper-slide"><div id="samuraj-slide-' + i + '" class="samuraj-slide">' + activemodules[i].html.content + '</div></div>';
								}
							}
							if (slides !== ""){
								var addonHtml = '' +
								'<div id="samuraj-swiper-header" class="samuraj-swiper-header">' + Samuraj.Addon.settings.translations.addon_heading + '</div>' +
								'<div id="swiper-container" class="swiper-container" >' +
								'  <div class="swiper-wrapper">' +
									slides +
								'  </div>' +
								'</div>';
								if (activemodules.length > 1){
									addonHtml += '' +
									'<div id="swiper-pagination" class="swiper-pagination"></div>' +
									'<div id="swiper-button-prev" class="swiper-button-prev"></div>' +
									'<div id="swiper-button-next" class="swiper-button-next"></div>';
								}
					
								var html = '' +
								'<div class="samuraj-addonproduct irw-list">' +
									addonHtml +
								'</div>';
						
								var rangepopup = document.querySelector(".samuraj-purchase-popup .range-popup")
								rangepopup.insertAdjacentHTML('beforeend', html);

								var addbuy = rangepopup.querySelectorAll(".samuraj-add-buy");
								for (var i=0; i<addbuy.length; i++){

									var productFicheSmall = addbuy[i].querySelector(".productFicheSmall");
									if (productFicheSmall && productFicheSmall.href) productFicheSmall.setAttribute("href", addbuy[i].parentNode.getAttribute("data-href"));

									var prodFamilyPrice = addbuy[i].querySelector(".prodFamilyPrice");
									if (prodFamilyPrice){
										var regularPrice = addbuy[i].querySelector(".regularPrice");
										if (regularPrice) 
											regularPrice.setAttribute("style", "font-size: 16px !important");
									}

									Samuraj.API.Common.GetItemInfo({
										i:i,
										product: addbuy[i].getAttribute("data-artnr"),	
										success: function(data){
											var currentaddbuy = addbuy[this.i];
											if (data && data.RetailItemComm && data.RetailItemComm.OnlineSellable.$){
												currentaddbuy.insertAdjacentHTML('beforeend', '<div class="samuraj-addtocart-container"><a data-artnr="' + currentaddbuy.getAttribute("data-artnr") + '" class="button button--primary samuraj-addon-addtocart button__loader text--center "><span class="icon icon__shoppingbag"></span><span id="buy_span_btn_samuraj">' + Samuraj.Addon.settings.translations.addon_product_buy_button + '</span><span class="samuraj-circle samuraj-irw" ><span class="samuraj-checkmark"></span></span></a><div class="samuraj-addtocart-error">' + Samuraj.Addon.settings.translations.addon_product_buy_error + '</div></div>');
												
												var addtocart = currentaddbuy.querySelector(".samuraj-addtocart-container .samuraj-addon-addtocart");
												c.addEvent(addtocart, "click", function(e){
													e.preventDefault();
													e.stopPropagation();
													var button = e.currentTarget;
													Samuraj.API.Common.AddToCart({
														products: button.getAttribute("data-artnr"),
														loading: function(response){
															c.addClass(button, "button__loader--active");
															
															var circle = button.querySelector(".samuraj-circle");
															if (circle) {
																circle.style.setProperty("display", "none");
															}
															var checkmark = button.querySelector(".samuraj-checkmark");
															if (checkmark) {
																checkmark.style.setProperty("display", "none");
															}
															var addtocarterror = button.querySelector(".samuraj-addtocart-error");
															if (addtocarterror) {
																addtocarterror.style.setProperty("display", "none");
															}
														},
														success: function(response, test1, test2){
															var shoppingbagquantity = document.querySelectorAll("#noOfCartItemsNew");
															if (shoppingbagquantity.length > 0){
																shoppingbagquantity[shoppingbagquantity.length-1].innerHTML = parseInt(shoppingbagquantity[shoppingbagquantity.length-1].innerHTML) + 1;
															}
															c.removeClass(button, "button__loader--active");
															var putInBarsket = Samuraj.Addon.settings.translations.addon_product_in_basked_button;
															var notInBarsket = document.getElementById('buy_span_btn_samuraj');
															var circle = button.querySelector(".samuraj-circle");
															if (circle) {
																circle.style.setProperty("display", "inline");
																notInBarsket.innerHTML = putInBarsket; 
															}
															var checkmark = button.querySelector(".samuraj-checkmark");
															if (checkmark) {
																setTimeout(function(){
																	checkmark.style.setProperty("display", "inline");
																	notInBarsket.innerHTML = putInBarsket;
																	/*
																	setTimeout(function(){
																		checkmark.style.setProperty("display", "none");
																		
																	},3000);
																	*/
																}, 400);
															}

															/**
															 * Adding tracking when addon product is added to cart
															 */
															try {
																var product_artnr = button.getAttribute("data-artnr");
																var product_name = button.parentNode.parentNode.querySelector(".productDetails .productTitle").innerHTML;
																var product_price = button.parentNode.parentNode.querySelector(".productDetails .regularPrice").innerHTML;
																
																Samuraj.Tracking.send({
																	category: 'ecommerce',
																	action: 'add_to_cart',
																	label: product_name + " - " + product_artnr,
																	params: {
																		value: product_price,
																		currency: "NOK",
																		items: [
																			{
																				id: product_artnr,
																				name: product_name,
																				price: product_price,
																				quantity: 1
																			}
																		]

																	}
																});
															} catch (err){

															}
														},
														failure: function(response, test1, test2){
															c.removeClass(button, "button__loader--active");
															var addtocarterror = button.querySelector(".samuraj-addtocart-error");
															if (addtocarterror) {
																addtocarterror.style.setProperty("display", "block");
															}
														},
														exception: function(response, test1, test2){
															c.removeClass(button, "button__loader--active");
															var addtocarterror = button.querySelector(".samuraj-addtocart-error");
															if (addtocarterror) {
																addtocarterror.style.setProperty("display", "block");
															}
														}
													});
												});
											}										
										}
									});
								}

								var params = {
									slidesPerView: '1',
									spaceBetween: 5,
									grabCursor: true
								}
								if (activemodules.length > 1){
									params.navigation = {
										nextEl: '.samuraj-addonproduct .swiper-button-next',
										prevEl: '.samuraj-addonproduct .swiper-button-prev'
									}
									params.pagination = {
										el: '.samuraj-addonproduct .swiper-pagination',
										type: 'bullets'
									}
								}
								//Show popup
								popupframe.style.setProperty("display", "block");
								Samuraj.Addon.swiper = new Swiper(".samuraj-addonproduct .swiper-container", params);
								
								//Monitor addtocart process, and remove addon if it fails
								var monitoraddtocart = setInterval(function(){
									var firstdiv = document.querySelector(".range-popup-frame .range-popup > div");
									if (firstdiv) var button = firstdiv.querySelector("a.button");
										
									if (button){
										if (button.className.indexOf("button__loader--active") == -1)
											clearInterval(monitoraddtocart);
									} else {
										clearInterval(monitoraddtocart);
										var addoncontainer = document.querySelector(".range-popup-frame .range-popup .samuraj-addonproduct");
										addoncontainer.style.setProperty("display", "none");
									}

								}, 50);


								/**
								 * Adding view tracking when product has addon
								 */
								Samuraj.Tracking.send({
									category: 'local_tracking_add_on',
									action: 'view',
									label: "sc: " + rangepopup.querySelectorAll(".swiper-slide").length + " origin_url: " + document.URL
								});

								/**
								 * Adding tracking to slide change
								 */
								Samuraj.Addon.swiper.on('slideChange', function () {
									var data_href = "";
									var elem = document.querySelector(".samuraj-addonproduct .swiper-slide-active .samuraj-slide.samuraj-link");
									if (elem) 
										data_href = elem.getAttribute("data-href") || "";
									Samuraj.Tracking.send({
										category: 'local_tracking_add_on',
										action: 'slide_change',
										label: "active_slide_target_url: " + data_href
									});
								});

								/**
								 * Adding tracking to slide click
								 */
								var elems = document.querySelectorAll(".samuraj-addonproduct .swiper-slide .samuraj-slide");
								for (var i=0; i<elems.length; i++){
									Samuraj.Common.addEvent(elems[i], "click", function(e){
										var data_href = e.currentTarget.getAttribute("data-href") || "";
										Samuraj.Tracking.send({
											category: 'local_tracking_add_on',
											action: 'slide_click',
											label: "target_url: " + data_href
										});
									});
								}
							}

							//Samuraj.Addon.setSlideHeight(200);
						});
					} else {
						//Show popup
						popupframe.style.setProperty("display", "block");
					}
				});
			}
		},
		setSlideHeight: function(timeout){
			var timeout = timeout || 200;
			setTimeout(function() {
				var c = Samuraj.Common;
				if(c.checkMobile()){
					var elems = document.querySelectorAll(".samuraj-addonproduct .samuraj-slide");
				} else {
					var elems = document.querySelectorAll(".samuraj-addonproduct .samuraj-slide");
				}

				
				Samuraj.Addon.slideHeight = Samuraj.Addon.slideHeight || 0;
				Samuraj.Addon.slideWidth = Samuraj.Addon.slideWidth || 0;
				var slideHeight = 0, slideWidth = 0, imgwidth = 0;
				
				for (var i = 0; i < elems.length; i++) {
					elems[i].style.setProperty("height", "auto");
					var imgelem = elems[i].querySelector(".image-claim-height");
					if (imgelem){
						imgwidth = imgelem.clientWidth + imgelem.offsetTop;
					}
					var imgelem = elems[i].querySelector(".image");
					if (imgelem){
						imgwidth = imgelem.clientWidth + imgelem.offsetTop;
					}
					if (elems[i].clientHeight > slideHeight){
						slideHeight = elems[i].clientHeight;
					}
					if (imgwidth > slideHeight){
						slideHeight = imgwidth;
					}
					//slideWidth = elems[i].clientWidth;
				}
				//if (Samuraj.Addon.slideHeight < slideHeight-15 || Samuraj.Addon.slideHeight > slideHeight+15){
				//if (Samuraj.Addon.slideHeight !== slideHeight){
					Samuraj.Addon.slideHeight = slideHeight;
					for (var i = 0; i < elems.length; i++) {
						elems[i].style.setProperty('height', slideHeight + 'px')
					}
				//}
				/*
				if (Samuraj.Addon.slideWidth !== slideWidth){
					Samuraj.Addon.slideWidth = slideWidth;
					Samuraj.Addon.swiper.update();
				}
				*/

			}, timeout);
		},
		injectInfo: function(Addon, loc, page_platform){
			var c = Samuraj.Common,
				e = Samuraj.Element;
			if (c.varExist(Addon.info)){
				if (c.varExist(Addon.info.content) && c.varExist(Addon.info.setting)){
					if (Addon.info.setting.pip){
						e.get({selector: loc.selector,baseelem: document}, function(selector, targetelem, context){
							var c = c,
								infotypes = ["text", "html"];
							for (var j=0; j<infotypes.length; j++){
								if (c.varExist(context.Addon.info.content[infotypes[j]], true)){
									var elem = document.getElementById("samuraj-Addon-info-" + page_platform + "-" + infotypes[j]);
									if (!elem){
										elem = document.createElement("div");
										elem.id = "samuraj-Addon-info-" + page_platform + "-" + infotypes[j];
										elem.innerHTML = context.Addon.info.content[infotypes[j]];
										Samuraj.Element.inject(elem, targetelem[0], context.placing);
									} else {
										elem.innerHTML = context.Addon.info.content[infotypes[j]];
									}
								}
							}
						}, {
							Addon: Addon,
							placing: loc.placing
						});
					}
				}
			}
		},
		pip: function(module){
			var c = Samuraj.Common;
			module = this.getProductLocation(module);

			if (c.checkMobile()){
				for (var i=0; i<module.products.dom.main.elems.length; i++){
					Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[i]}, function(selector, targetelem){
						var elem = module.elem.cloneNode(module.elem);
						Samuraj.DisplayTimer.injectElement({
							elem: elem, 
							targetelem: targetelem[0], 
							placing: module.productloc.placing,
							module: module
						});
						if (Samuraj.AddonSettings.testcookieset){
							setTimeout(function(){
								Samuraj.DisplayTimer.PreviewMenu.getContent(module, elem);
							},300);
						}
						c.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

						//Callback
						if (c.isFunction(module.complete))
							module.complete(module);
					});
				}
			} else {
				//if (!Samuraj.DisplayTimer.repeat){
				//	Samuraj.DisplayTimer.repeat = true;
					c.monitorUrlChange({
						module: module,
						onchange: function(input){
							
							Samuraj.Element.remove(document.querySelectorAll("span.samuraj-module-" + input.module.modulenr));
							if (Samuraj.AddonSettings.testcookieset){
								Samuraj.DisplayTimer.PreviewMenu.clearBorders([input.module]);
							}

							Samuraj.Targeting.products(input.module.where.products, function(prod){
								input.module.products = prod;
								for (var i=0; i<input.module.products.dom.main.elems.length; i++){
									Samuraj.Element.get({selector: input.module.productloc.selector, baseelem: input.module.products.dom.main.elems[i]}, function(selector, targetelem){
										var elem = module.elem.cloneNode(input.module.elem);
										Samuraj.DisplayTimer.injectElement({
											elem: elem,
											targetelem: targetelem[0], 
											placing: input.module.productloc.placing, 
											module: input.module
										});
										if (Samuraj.AddonSettings.testcookieset){
											setTimeout(function(){
												Samuraj.DisplayTimer.PreviewMenu.getContent(module, elem);
											},300);
										}
										c.addTimeoutClass(elem, "samuraj-effect-" + input.module.productloc.effect + " samuraj-effect-animated", 1000);

										//Callback
										if (c.isFunction(module.complete))
											module.complete(module);
									});
								}
							});
						}
					});
				//}
			}


		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "Addon",
		objects: [
			{name: "Addon", fn: Addon}
		],
		dependencies: [
			"Swiper",
			"Product",
			"API"
		],
		bootup: function(){
			//Initiate Addon
			Samuraj.Version.load('samurajaddon', true, function(e) {
				Samuraj.Source.load('Addon', function(data) {

					if (data.addon) Samuraj.Addon.init(data);
					Samuraj.Log.add({
						cat: "Addon",
						sub: "Timing",
						text: "Loading done",
						timing: performance.now() - SamurajTiming				
					});
				});
			});
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();