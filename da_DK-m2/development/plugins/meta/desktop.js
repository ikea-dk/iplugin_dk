/*


 |\/|  _ _|_  _.
 |  | (/_ |_ (_|


*/

(function(){

	var Meta = {
		getprod: function(data) {
			var product = {};
			if (document.URL.indexOf('/p/') > -1 || document.URL.indexOf('/catalog/products/') > -1) {
				var c = Samuraj.Common;
				product.urlprod = c.getProdNumUrl();
				if (c.varExist(product.urlprod, true)) {
					product.urlprod = product.urlprod.toLowerCase();
					for (var i = 0; i < data.length; i++) {
						if (c.varExist(data[i].artnr)) {
							var tempartnr = data[i].artnr.replace(/[^0-9|s|S|,]/g, "");
							tempartnr = tempartnr.split(',');
							for (var j = 0; j < tempartnr.length; j++) {
								if (product.urlprod == tempartnr[j].toLowerCase()) {
									product.found = true;
									var prop = Object.getOwnPropertyNames(data[i]);
									for (var k = 0; k < prop.length; k++)
										product[prop[k]] = data[i][prop[k]];

								}
							}

						}
					}
				}
			}
			return product;
		},
		blacklist: function(blacklist, product) {
			var c = Samuraj.Common;
			if (!product) {
				var product = {};
				product = this.getprod(blacklist);
			} else {
				var product2 = {};
				product2 = this.getprod(blacklist);
				product = Samuraj.Merge.merge(product, product2);
			}
			if (c.varExist(product)) {
				if (product.found) {
					product.meta_date = "";
					if (c.varExist(product.uppdt)) {
						product.meta_date = '<meta property="article:modified_time" content="' + product.uppdt + '"/>';
					}
					product.meta_blacklist = "";
					if (c.varExist(product.cxwids)) {
						product.meta_blacklist = '<meta property="cXenseParse:recs:ike-widgetBl" content="' + product.cxwids.replace(/\,/g, " ") + '"/>';
					} else {
						product.meta_blacklist = '<meta name="cXenseParse:recs:recommendable" content="false"/>';
					}
				}
				return product;
			} else
				return {};

		},
		forcecrawl: function(forcecrawl, product) {
			var c = Samuraj.Common;
			if (!product) {
				var product = {};
				product = this.getprod(forcecrawl);
			} else {
				product.found = false;
				var product2 = {};
				product2 = this.getprod(forcecrawl);
				product = Samuraj.Merge.merge(product, product2);
			}
			if (c.varExist(product)) {
				if (product.found) {
					product.meta_date = "";
					if (c.varExist(product.uppdt)) {
						product.meta_date = '<meta property="article:modified_time" content="' + product.uppdt + '"/>';
					}
				}
				return product;
			} else
				return {};
		},
		init: function(data) {
			var c = Samuraj.Common;
			var product = this.forcecrawl(data.forcecrawl, this.blacklist(data.blacklist)) || {};

			if (c.varExist(product.meta_date, true))
				document.head.insertAdjacentHTML('afterbegin', product.meta_date);
			if (c.varExist(product.meta_blacklist, true))
				document.head.insertAdjacentHTML('afterbegin', product.meta_blacklist);

		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "Meta",
		objects: [
			{name: "Meta", fn: Meta}
		],
		dependencies: [],
		bootup: function(){
			//Initiate Meta on PIP pages
			if (Samuraj.Common.checkurl(['*m2*/no/no/p|/no/n2/p*', '*www.|preview.*/catalog/products*'])) {
				Samuraj.Source.load('Meta', function(data) {
					Samuraj.Meta.init(data);
					Samuraj.Log.add({
						cat: "Meta",
						sub: "Timing",
						text: "Loading done",
						timing: performance.now() - SamurajTiming				
					});
				});
			}
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();