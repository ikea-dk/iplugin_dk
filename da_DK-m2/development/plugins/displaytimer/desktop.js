/*

  _                   ___
 | \ o  _ ._  |  _.    | o ._ _   _  ._
 |_/ | _> |_) | (_| \/ | | | | | (/_ |
		  |         /

*/

(function(){
	

	var DisplayTimerInjected,
		DisplayTimer = {
		settings: {
			carruselarrow: 363,
			ids: ["main", "mainPadding", "allContent"],
			nodeTypes: ["body", "html"]
		},
		injectStyleToPage: function(module) {
			var c = Samuraj.Common;

			//Adding styling to the head section of the page
			if (c.varExist(module.what.style, true)) {
				var style = {};
				if (c.isObject(module.what.style)) style = module.what.style;
				else style.content = module.what.style;

				if (c.varExist(style.inline)) module.inline = style.inline;
				if (c.varExist(style.clear)) module.clear = style.clear;

				
				if (c.varExist(style.content, true)){
					if (!style.options) style.options = {}
					style.options.modulenr = module.modulenr;
					style.content = Samuraj.Template.load({htmlcontent: style.content, htmloptions: style.options});
					if (!document.getElementById("samuraj-style-" + module.modulenr)){
						var css = document.createElement("style");
						css.id = "samuraj-style-" + module.modulenr;
						css.innerHTML = style.content;
						document.head.appendChild(css);
					}
				}
				if (style.pages){
					style.pages = c.toArray(style.pages);
					for (var i=0; i<style.pages.length; i++){
						if (style.pages[i].predefined){
							var predefurls = Samuraj.Validation.getPredefinedUrls(style.pages[i].predefined);
							if (style.pages[i].urls) {
								style.pages[i].urls = c.toArray(style.pages[i].urls);
								style.pages[i].urls = predefurls.concat(style.pages[i].urls);
							} else style.pages[i].urls = predefurls;
						}
						if (style.pages[i].urls){
							style.pages[i].urls = c.toArray(style.pages[i].urls);
							if (c.checkurl(style.pages[i].urls)){
								if (c.varExist(style.pages[i].content, true)){
									if (!style.pages[i].options) style.pages[i].options = {}
									style.pages[i].options.modulenr = module.modulenr;
									style.pages[i].content = Samuraj.Template.load({htmlcontent: style.pages[i].content, htmloptions: style.pages[i].options});
									if (!document.getElementById("samuraj-style-" + module.modulenr + "-" + i)){
										var css = document.createElement("style");
										css.id = "samuraj-style-" + module.modulenr + "-" + i;
										css.innerHTML = style.pages[i].content;
										document.head.appendChild(css);
									}
								}
							}
						}
					}
					
				}
				
			}
			module.what.style = style;
			return module;
		},
		injectScriptToPage: function(module) {
			var c = Samuraj.Common;

			//Adding scripts throu eval function
			if (c.varExist(module.what.script, true)) {
				var script = {};
				if (c.isObject(module.what.script)) script = module.what.script;
				else script.content = module.what.script;
				
				if (c.varExist(script.content, true)){
					if (!script.options) script.options = {}
					script.options.modulenr = module.modulenr;
					script.content = Samuraj.Template.load({htmlcontent: script.content, htmloptions: script.options});
					try {
						var source = "var foundmodulenr = " + module.modulenr + ", foundpageindex = " + module.foundpageindex + ", foundurlindex = " + module.foundurlindex + ", htmloptions = Samuraj.Template.alloptions;";
						eval(source + script.content);
					} catch (err) {
						Samuraj.Log.add({
							cat: "DisplayTimer",
							sub: "Error",
							text: "Catch - injectScriptToPage - " + err,
							err: err,
							module: module,
							script: source + script.content
						});
					}
				}
				if (script.pages){
					script.pages = c.toArray(script.pages);				
					if (c.isArray(script.pages)){
						for (var i=0; i<script.pages.length; i++){
							if (script.pages[i].predefined){
								var predefurls = Samuraj.Validation.getPredefinedUrls(script.pages[i].predefined);
								if (script.pages[i].urls) {
									script.pages[i].urls = c.toArray(script.pages[i].urls);
									script.pages[i].urls = predefurls.concat(script.pages[i].urls);
								} else script.pages[i].urls = predefurls;
							}
							if (script.pages[i].urls){
								script.pages[i].urls = c.toArray(script.pages[i].urls);
								if (c.checkurl(script.pages[i].urls)){
									if (c.varExist(script.pages[i].content, true)){
										if (script.pages[i].options)
											script.pages[i].content = Samuraj.Template.load({htmlcontent: script.pages[i].content, htmloptions: script.pages[i].options});
										try {
											var source = "var foundmodulenr = " + module.modulenr + ", foundpageindex = " + module.foundpageindex + ", foundurlindex = " + module.foundurlindex + ", htmloptions = Samuraj.Template.alloptions;";
											eval(source + script.pages[i].content);
										} catch (err) {
											Samuraj.Log.add({
												cat: "DisplayTimer",
												sub: "Error",
												text: "Catch - injectScriptToPage - " + err,
												err: err,
												module: module,
												script: source + script.content
											});
										}
									}
								}
							}
						}
					}
				}
			}
		},
		injectFileToPage: function(module) {
			var c = Samuraj.Common;

			//Including .js or .ccs files to page  
			if (module.what.file) {
				var file = {};
				if (c.isObject(module.what.file)) file = module.what.file;
				else file.content = module.what.file;

				if (file.pages){
					file.pages = c.toArray(file.pages)
					for (var i=0; i<file.pages.length; i++){
						if (file.pages[i].predefined){
							var predefurls = Samuraj.Validation.getPredefinedUrls(file.pages[i].predefined);
							if (file.pages[i].urls) {
								file.pages[i].urls = c.toArray(file.pages[i].urls);
								file.pages[i].urls = predefurls.concat(file.pages[i].urls);
							} else file.pages[i].urls = predefurls;
						}
						if (file.pages[i].urls){
							file.pages[i].urls = c.toArray(file.pages[i].urls)
							if (c.checkurl(file.pages[i].urls)){
								if (c.varExist(file.pages[i].content, true)){
									Samuraj.IncludeScript.load(file.pages[i].content);
								}
							}
						}
					}
				}
				if (c.varExist(file.content, true))  Samuraj.IncludeScript.load(file.content);
			}
		},
		loadElementInjectionTimeout: function(timeouts, data){
			var c = Samuraj.Common;
			var to = c.toArray(timeouts);
			for (var i=0; i<to.length; i++){
				//var data = JSON.parse(JSON.stringify(data));
				var activepage = data.module.activepage,
					locationnr = data.locationnr;
				setTimeout(function(){
					data.module.activepage = activepage;
					data.locationnr = locationnr;
					Samuraj.DisplayTimer.loadElementInjection(data);
				}, c.toInt(to[i]));
			}
		},
		injectElement: function(input){
			var c = Samuraj.Common;
			var module = input.module;

			//Inject style
			Samuraj.DisplayTimer.moduleStylesInjected = Samuraj.DisplayTimer.moduleStylesInjected || [];
			if (module && module.what && module.what.style){
				if (!Samuraj.DisplayTimer.moduleStylesInjected[module.modulenr]){
					Samuraj.DisplayTimer.moduleStylesInjected[module.modulenr] = true;
					Samuraj.DisplayTimer.injectStyleToPage(module);
				}
			}
			//Inject html
			if (module && module.what && module.what.html){
				if (input.elem && input.targetelem && input.placing){
					Samuraj.Element.inject (input.elem, input.targetelem, input.placing);
				}
			}
			//Inject script
			Samuraj.DisplayTimer.moduleScriptsInjected = Samuraj.DisplayTimer.moduleScriptsInjected || [];
			if (module && module.what && module.what.script){
				if (module.what.script.repeat && c.isString(module.what.script.repeat)){
					if (module.what.script.repeat == "1" || module.what.script.repeat == "true")
						module.what.script.repeat = true;
					else
						module.what.script.repeat = false;
				}
				if (!Samuraj.DisplayTimer.moduleScriptsInjected[module.modulenr] || module.what.script.repeat){
					Samuraj.DisplayTimer.moduleScriptsInjected[module.modulenr] = true;
					Samuraj.DisplayTimer.injectScriptToPage(module);
				}
			}
			//Inject file
			Samuraj.DisplayTimer.moduleFilesInjected = Samuraj.DisplayTimer.moduleFilesInjected || [];
			if (module && module.what && module.what.file){
				if (!Samuraj.DisplayTimer.moduleFilesInjected[module.modulenr]){
					Samuraj.DisplayTimer.moduleFilesInjected[module.modulenr] = true;
					Samuraj.DisplayTimer.injectFileToPage(module);
				}
			}

			if ((SamurajSetting.Common.testcookieset && !input.plp) || (SamurajSetting.Common.testcookieset && input.plp && input.targetelem.parentNode.id !== "landingPopup")){
				//setTimeout(function(){
				try{
					Samuraj.Tool.getDisplaytimerContent(input);
				} catch(err){
					Samuraj.Log.add({
						cat: "A_Test",
						sub: "GetDisplaytimerContent",
						text: "Log",
						err: err,
						input: input,
						timing: performance.now() - SamurajTiming				
					});
				}
				//}, 1000);
			}

		},
		loadElementInjection: function (module) {
			var c = Samuraj.Common;
			/*
			if (module.what){
				if (module.what.style)
					module = Samuraj.DisplayTimer.injectStyleToPage(module);
			}
			*/
			var style = {};
			if (c.isObject(module.what.style)) style = module.what.style;
			else style.content = module.what.style;

			if (c.varExist(style.inline)) module.inline = style.inline;
			if (c.varExist(style.clear)) module.clear = style.clear;

			var displayblock = "samuraj-display-block ";
			if (module.inline) displayblock = "";
			
			var clear = "";
			if (module.clear) clear = "samuraj-clear ";
			
			module.elem = document.createElement("span");
			module.elem.className = "samuraj-dt-injected " + displayblock + clear + "samuraj-hidden samuraj-module-" + module.modulenr;
			
			var hascontent = false;
			if (module.what && module.what.html){
				if (module.what.html.content && module.what.html.options)
					module.what.html.content = Samuraj.Template.load({htmlcontent: module.what.html.content, htmloptions: module.what.html.options});
				if (Samuraj.Common.varExist(module.what.html.content, true)){
					hascontent = true;
					module.elem.innerHTML = module.what.html.content;
				}
			}
			if (module.products.hascrit){
				if (module.products.pagetype){
					switch(module.products.pagetype) {
						case "pip":
							Samuraj.DisplayTimer.pip(module);
							break;
						case "plp":
							Samuraj.DisplayTimer.plp(module);
							break;
						case "search":
							Samuraj.DisplayTimer.search(module);
							break;
						case "availability":
							//Samuraj.DisplayTimer.availability(module);
							break;
						case "wishlist":
							//Samuraj.DisplayTimer.wishlist(module);
							break;
					}
				}
			} else {
				if (!module.locations || !module.locations.length > 0){
					module.locations = [{
						selector: "body",
						elementnr: 0,
						placing: "append",
						effect: "fade"
					}];
				}
				for (var i=0; i<module.locations.length; i++){
					module.locations[i].elementnr = module.locations[i].elementnr || 0;
					module.locations[i].placing = module.locations[i].placing || "after";
					module.locations[i].effect = module.locations[i].effect || "fade";
					module.currentlocation = i;

					Samuraj.Element.get(module.locations[i].selector, function(selector, targetelems, module) {
						
						var c = Samuraj.Common,
							targetelemarr = [],
							temptargetelemarr = [],
							elementnr = [],
							tempelementnr = [];

						targetelemarr = c.toArray(targetelems);
						elementnr = c.toArray(module.locations[module.currentlocation].elementnr.toString());

						for (var i=0; i<elementnr.length; i++){
							elementnr[i] = elementnr[i].replace(/[^0-9,]/g,"");
							var split_arr = elementnr[i].toString().split(",");
							for (var j=0; j<split_arr.length; j++){
								tempelementnr.push(c.toInt(split_arr[j]));
							}
						}
						/*
						var arrlength = targetelemarr.length;
						for (var i=0; i<tempelementnr.length; i++){
							if (tempelementnr[i] > -1 && tempelementnr[i] < arrlength)
								temptargetelemarr.push(targetelemarr[tempelementnr[i]]);
						}
						module.targetelemarr = temptargetelemarr;
						*/

						//special for main page www.ikea
						if (module.locations[module.currentlocation].placing == "before" && module.locations[module.currentlocation].selector == "#whatsection") {
							Samuraj.DisplayTimer.settings.carruselarrow += 170;
							var arrowcss = '.corrosilnavigatingarrow{top:' + Samuraj.DisplayTimer.settings.carruselarrow + 'px !important}';
							var csselem = document.createElement("style");
							csselem.innerHTML = csselem.innerHTML + arrowcss;
							document.head.appendChild(csselem);
						}
						
						//Inject element to page
						if (module.elem) {
							for (var i=0; i<tempelementnr.length; i++){

								Samuraj.Element.get({selector: module.locations[i].selector, elementnumber: tempelementnr[i]}, function(selector, targetelem, module) {
									var elem = module.elem.cloneNode(true);
									Samuraj.Log.add({
										cat: "A_Test",
										sub: "Inject",
										text: "Log",
										elem: elem, 
										targetelem: targetelem[0], 
										placing: module.locations[module.currentlocation].placing,
										module: module,
										timing: performance.now() - SamurajTiming				
									});
									
									Samuraj.DisplayTimer.injectElement({
										elem: elem, 
										targetelem: targetelem[0], 
										placing: module.locations[module.currentlocation].placing,
										module: module	
									});
									Samuraj.Log.add({
										cat: "A_Test",
										sub: "Inject",
										text: "Log",
										elem: elem,
										timing: performance.now() - SamurajTiming				
									});
									Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.locations[module.currentlocation].effect + " samuraj-effect-animated", 1000);
									
									//Callback
									if (Samuraj.Common.isFunction(module.complete))
										module.complete(module);
								}, module);
							}
						}
					}, module);
				}
			}
		},
		mergetemplates: function(){
			var templates = Samuraj.Template.displaytimertemplates;
			if (templates){
				for (var i=0; i<templates.length; i++){
					templates[i] = this.gettemplate(templates[i]);
				}
			}
		},
		gettemplateold: function(module){
			var c = Samuraj.Common;
			if (c.varExist(module.template, true)){
				var moduletemplates = c.toArray(module.template);
				module.run = false;
				var templates = Samuraj.Template.displaytimertemplates;
				if (templates){
					
					//Samuraj setup
					if(c.isInt(moduletemplates[0])){
						for(var i=0; i<moduletemplates.length; i++){
							for (var j=0; j<templates.length; j++){
								if (templates[j].id == moduletemplates[i]){
									var template_clone = JSON.parse(JSON.stringify(templates[j]));
									module = Samuraj.Merge.deepmerge(template_clone, module, [], false); 
									module.run = true;
									break;
								}
							}
						}
					} else {

						//Regular data source setup
						for(var i=0; i<moduletemplates.length; i++){
							for (var j=0; j<templates.length; j++){
								if (templates[j].name == moduletemplates[i]){
									var template_clone = JSON.parse(JSON.stringify(templates[j]));
									module = Samuraj.Merge.deepmerge(template_clone, module, [], false); 
									module.run = true;
									break;
								}
							}
						}
					}
				}
			}
			return module;
		},
		gettemplate: function(module){
			var c = Samuraj.Common;
			if (c.varExist(module.template, true)){
				var moduletemplates = c.toArray(module.template);
				module.run = false;
				var templates = Samuraj.Template.displaytimertemplates;
				if (templates){
					
					//Samuraj setup
					if(c.isInt(moduletemplates[0])){
						for(var i=0; i<moduletemplates.length; i++){
							for (var j=0; j<templates.length; j++){
								if (templates[j].id == moduletemplates[i]){
									var template_clone = JSON.parse(JSON.stringify(templates[j]));
									module = Samuraj.Merge.deepmerge(this.gettemplate(template_clone), module, [], false);
									module.run = true;
									break;
								}
							}
						}
					} else {

						//Regular data source setup
						for(var i=0; i<moduletemplates.length; i++){
							for (var j=0; j<templates.length; j++){
								if (templates[j].name == moduletemplates[i]){
									var template_clone = JSON.parse(JSON.stringify(templates[j]));
									module = Samuraj.Merge.deepmerge(this.gettemplate(template_clone), module, [], false);
									module.run = true;
									break;
								}
							}
						}
					}
				}
			}
			return module;
		},
		load: function(input) {
			
			var c = Samuraj.Common;
			if (!c.varExist(Samuraj.DisplayTimerInjected)) {
				Samuraj.DisplayTimerInjected = true;
				
				Samuraj.Time.get(function(time, input) {
					//Samuraj.Time.now = time;
					//Samuraj.Time.set = true;

					var c = Samuraj.Common;
					
						
					Samuraj.Log.add({
						cat: "DisplayTimer",
						sub: "Timing",
						text: "File loaded",
						timing: performance.now() - SamurajTiming				
					});

					// Merge templates referencing eachother
					//Samuraj.DisplayTimer.mergetemplates();

					// Loop through all modules in displaytimer
					var elemList = [];
										
					for (var i = 0; i < input.modules.length; i++) {

						input.modules[i].modulenr = i;
						var module = input.modules[i];

						//Get templates for this module
						module = Samuraj.DisplayTimer.gettemplate(module);

						if (module.active && c.isString(module.active)){
							if (module.active == "1" || module.active == "true")
								module.active = true;
							else
								module.active = false;
						}						

						//Excluding templates
						var template = module.category == "template" || false;

						//Checking if the module is active
						if (!template && (module.active || (Samuraj.Tool && Samuraj.Tool.settings.inactivechecked && Samuraj.Validation.settings.override))){
							Samuraj.Log.add({
								cat: "DisplayTimer",
								sub: "Module_" + (module.id || module.modulenr),
								text: "Active",
								module: module			
							});
							module.run = true;
							module = Samuraj.DisplayTimer.gettemplate(module);
							if (Samuraj.Common.isFunction(input.complete))
								module.complete = input.complete;
							
							if (module){
								if (module.run){
									
									//WHEN - checking if it is with the timesettings				
									//Samuraj.DisplayTimer.dateValidation(module, function(module){
									Samuraj.Validation.dateValidation({
										name: "DisplayTimer",
										module: module, 
										success: function(module){

											//WHERE - checking if the page is valid, and if so call validateCustomer and check customer location as well
											//Samuraj.DisplayTimer.validatePage(module, function(module){
											Samuraj.Validation.validatePage({
												name: "DisplayTimer",
												uselocation: SamurajSetting.Common.uselocation,
												module: module, 
												success: function(module){

													//DEVICE - checking if the device setting is valid
													//Samuraj.DisplayTimer.validateDevice(module, function(module){
													Samuraj.Validation.validateDevice({
														name: "DisplayTimer",
														module: module, 
														success: function(module){

															//Check targeting and add product elements to module
															module.products = {};
															if (module.where){
																if (module.where.products){
																	Samuraj.Targeting.products(module.where.products, function(prod){
																		module.products = prod;						
																		
																		//WHAT - Inject module content to page
																		Samuraj.DisplayTimer.loadElementInjection(module);
																	});
																} else {
																	//WHAT - Inject module content to page
																	Samuraj.DisplayTimer.loadElementInjection(module);
																}
															} else {
																//WHAT - Inject module content to page
																Samuraj.DisplayTimer.loadElementInjection(module);
															}
														},
														failure: function(module){}
													});
												},
												failure: function(module){}
											});
										},
										failure: function(module){}
									});
								}
							}
						} else {
							Samuraj.Log.add({
								cat: "DisplayTimer",
								sub: "Module_" + (module.id || module.modulenr),
								text: "Inactive",
								module: module			
							});
						}
					}

				}, input);
				// Time.Get
			}
		},
		validateElement: function(elem) {
			var c = Samuraj.Common;
			if (c.varExist(elem.id))
				for (var i = 0; i < this.settings.ids.length; i++)
					if (elem.id == this.settings.ids[i])
						return false;
			for (var i = 0; i < this.settings.nodeTypes.length; i++)
				if (elem.tagName.toLowerCase() == this.settings.nodeTypes[i])
					return false;
			return true;
		},
		checkParents: function(elem, search) {
			var c = Samuraj.Common;
			if (c.varExist(elem.id))
				if (elem.id.indexOf(search) > -1)
					return true;
			if (c.varExist(elem.className))
				if (elem.className.indexOf(search) > -1)
					return true;
			return false;
		},
		getProductLocation: function(module){
			var productloc = {},
				productlocnr = 0
			if (module.where.products.locations)
				productlocnr = module.where.products.locations.pip || 0;
			if (productlocnr >= module.products.dom.main.loc.length)
				productlocnr = module.products.dom.main.loc.length - 1;
			
			
			if (module.products.dom.main.loc[productlocnr].selector)
				productloc.selector = module.products.dom.main.loc[productlocnr].selector
			if (module.products.dom.main.loc[productlocnr].placing)
				productloc.placing = module.products.dom.main.loc[productlocnr].placing

			productloc.placing = productloc.placing || "after";
			productloc.effect = productloc.effect || "fade";	
			if (module.where.products.locations){
				if (module.where.products.locations.custom){
					if (Samuraj.Common.varExist(module.where.products.locations.custom.selector, true))
						productloc.selector = module.where.products.locations.custom.selector;
					if (Samuraj.Common.varExist(module.where.products.locations.custom.placing, true))
						productloc.placing = module.where.products.locations.custom.placing;
					if (Samuraj.Common.varExist(module.where.products.locations.custom.effect, true))
						productloc.effect = module.where.products.locations.custom.effect;	
				}
			}
			
			module.productloc = productloc;
			module.productlocnr = productlocnr;
			
			return module;
				
		},
		pip: function(module){
			module = this.getProductLocation(module);

			if (Samuraj.Common.checkMobile()){
				for (var i=0; i<module.products.dom.main.elems.length; i++){
					Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[i]}, function(selector, targetelem){
						var elem = module.elem.cloneNode(module.elem);
						Samuraj.DisplayTimer.injectElement({
							elem: elem, 
							targetelem: targetelem[0], 
							placing: module.productloc.placing,
							module: module
						});
						
						Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

						//Callback
						if (Samuraj.Common.isFunction(module.complete))
							module.complete(module);
					});
				}
			} else {
				//if (!Samuraj.DisplayTimer.repeat){
				//	Samuraj.DisplayTimer.repeat = true;
					Samuraj.Common.monitorUrlChange({
						module: module,
						onchange: function(input){
							var module = input.module;
							
							Samuraj.Element.remove(document.querySelectorAll("span.samuraj-module-" + module.modulenr));
							if (SamurajSetting.Common.testcookieset){
								Samuraj.Tool.clearBorders([module]);
							}

							Samuraj.Targeting.products(module.where.products, function(prod){
								module.products = prod;
								for (var i=0; i<module.products.dom.main.elems.length; i++){
									Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[i]}, function(selector, targetelem){
										var elem = module.elem.cloneNode(module.elem);
										Samuraj.DisplayTimer.injectElement({
											elem: elem,
											targetelem: targetelem[0], 
											placing: module.productloc.placing, 
											module: module
										});

										Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

										//Callback
										if (Samuraj.Common.isFunction(module.complete))
											module.complete(module);
									});
								}
							});
						}
					});
				//}
			}


		},
		plp: function(module){
			module = this.getProductLocation(module);
			this.activepagemodules = this.activepagemodules || [];
			this.activepagemodules.push(module);

			Samuraj.DisplayTimer.productinjectagainirw = function (){
				var c = Samuraj.Common;

				var activepagemodules = Samuraj.DisplayTimer.activepagemodules;
				if (SamurajSetting.Common.testcookieset){
					Samuraj.Tool.clearBorders(activepagemodules);
				}

				for (var i=0; i<activepagemodules.length; i++){
					Samuraj.Element.remove(document.querySelectorAll("span.samuraj-module-" + activepagemodules[i].modulenr));
					Samuraj.Targeting.products(activepagemodules[i].where.products, function(prod, module){
						module.products = prod;
						var landingpopups = [];
						for (var j=0; j<module.products.dom.main.elems.length; j++){
							if (module.products.dom.main.elems[j].id || module.products.dom.main.elems[j].parentNode.id == "landingPopup"){
								Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[j]}, function(selector, targetelem, module){
									var elem = module.elem.cloneNode(module.elem);
									Samuraj.DisplayTimer.injectElement({
										elem: elem, 
										targetelem: targetelem[0], 
										placing: module.productloc.placing,
										module: module,
										plp: true
									});
									
									Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-appear samuraj-effect-animated", 1000);

									//Callback
									if (Samuraj.Common.isFunction(module.complete))
										module.complete(module);

								}, activepagemodules[i]);
							}
						}
					}, activepagemodules[i]);
				}
			}
			Samuraj.DisplayTimer.productinjectagain = function (){
				var c = Samuraj.Common;

				var activepagemodules = Samuraj.DisplayTimer.activepagemodules;
				if (SamurajSetting.Common.testcookieset){
					Samuraj.Tool.clearBorders(activepagemodules);
				}

				for (var i=0; i<activepagemodules.length; i++){
					Samuraj.Element.remove(document.querySelectorAll("span.samuraj-module-" + activepagemodules[i].modulenr));
					Samuraj.Targeting.products(activepagemodules[i].where.products, function(prod, module){
						module.products = prod;
						for (var j=0; j<module.products.dom.main.elems.length; j++){
							Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[j]}, function(selector, targetelem, module){
								var elem = module.elem.cloneNode(module.elem);
								Samuraj.DisplayTimer.injectElement({
									elem: elem, 
									targetelem: targetelem[0], 
									placing: module.productloc.placing,
									module: module
								});

								Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

								//Callback
								if (Samuraj.Common.isFunction(module.complete))
									module.complete(module);

							}, activepagemodules[i]);
						}
					}, activepagemodules[i]);
				}
			}

			if (Samuraj.Common.checkMobile()){
				
				for (var i=0; i<module.products.dom.main.elems.length; i++){
					Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[i]}, function(selector, targetelem){
						var elem = module.elem.cloneNode(module.elem);
						Samuraj.DisplayTimer.injectElement({
							elem: elem, 
							targetelem: targetelem[0], 
							placing: module.productloc.placing,
							module: module
						});

						Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

						//Callback
						if (Samuraj.Common.isFunction(module.complete))
							module.complete(module);
					});
				}
			} else {

				for (var i=0; i<module.products.dom.main.elems.length; i++){
					Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[i]}, function(selector, targetelem){
						var elem = module.elem.cloneNode(module.elem);
						Samuraj.DisplayTimer.injectElement({
							elem: elem, 
							targetelem: targetelem[0], 
							placing: module.productloc.placing,
							module: module
						});

						Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

						//Callback
						if (Samuraj.Common.isFunction(module.complete))
							module.complete(module);
					});
				}
				if (!Samuraj.DisplayTimer.repeat){
					Samuraj.DisplayTimer.repeat = true;
					//Samuraj.Common.addEvent(document, "click", function(evt){
					Samuraj.DisplayTimer.monitorElementChange({
						selector: ".productLists .product",
						innerselector: ".image a",
						prop: "href",
						onchange: function(){
							Samuraj.Targeting.reload = true;
							Samuraj.DisplayTimer.productinjectagainirw();
						}
					});

				}
			}

		},
		search: function(module){
			if (Samuraj.Common.checkMobile()){
				var module = this.getProductLocation(module);
			
				this.activepagemodules = this.activepagemodules || [];
				this.activepagemodules.push(module);

				for (var i=0; i<module.products.dom.main.elems.length; i++){
					Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[i]}, function(selector, targetelem){
						var elem = module.elem.cloneNode(module.elem);
						Samuraj.DisplayTimer.injectElement({
							elem: elem, 
							targetelem: targetelem[0], 
							placing: module.productloc.placing,
							module: module
						});

						Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

						//Callback
						if (Samuraj.Common.isFunction(module.complete))
							module.complete(module);
					});
				}
				Samuraj.DisplayTimer.changeidle = false;
				setTimeout(function(){
					Samuraj.DisplayTimer.changeidle = true;
				}, 500);
				if (!Samuraj.DisplayTimer.repeat){
					Samuraj.DisplayTimer.repeat = true;
					//Samuraj.Common.addEvent(document, "click", function(evt){
					Samuraj.Common.monitorDomChange({
						onchange: function(){
							if (Samuraj.DisplayTimer.changeidle){
								Samuraj.DisplayTimer.changeidle = false;
								setTimeout(function(){
									Samuraj.DisplayTimer.changeidle = true;
								}, 500);

								var c = Samuraj.Common;
								Samuraj.Targeting.reload = true;

								var activepagemodules = Samuraj.DisplayTimer.activepagemodules;
								if (SamurajSetting.Common.testcookieset){
									Samuraj.Tool.clearBorders(activepagemodules);
								}
								setTimeout(function(){
									for (var i=0; i<activepagemodules.length; i++){
										Samuraj.Element.remove(document.querySelectorAll("span.samuraj-module-" + activepagemodules[i].modulenr));
										Samuraj.Targeting.products(activepagemodules[i].where.products, function(prod, module){
											module.products = prod;
											var landingpopups = [];
											for (var j=0; j<module.products.dom.main.elems.length; j++){
												Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[j]}, function(selector, targetelem, module){
													var elem = module.elem.cloneNode(module.elem);
													Samuraj.DisplayTimer.injectElement({
														elem: elem, 
														targetelem: targetelem[0], 
														placing: module.productloc.placing,
														module: module
													});

													Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-appear samuraj-effect-animated", 1000);

													//Callback
													if (Samuraj.Common.isFunction(module.complete))
														module.complete(module);

												}, activepagemodules[i]);
											}
										}, activepagemodules[i]);
									}
								}, 100);
							}
						}
					});

				}
			} else {
				module = this.getProductLocation(module);
			
				for (var i=0; i<module.products.dom.main.elems.length; i++){
					Samuraj.Element.get({selector: module.productloc.selector, baseelem: module.products.dom.main.elems[i]}, function(selector, targetelem){
						var elem = module.elem.cloneNode(module.elem);
						Samuraj.DisplayTimer.injectElement({
							elem: elem, 
							targetelem: targetelem[0], 
							placing: module.productloc.placing,
							module: module
						});

						Samuraj.Common.addTimeoutClass(elem, "samuraj-effect-" + module.productloc.effect + " samuraj-effect-animated", 1000);

						//Callback
						if (Samuraj.Common.isFunction(module.complete))
							module.complete(module);
					});
				}
			}

		},
		/*
		availability: function(products, buyability){
			if (!Samuraj.Common.checkMobile()){
				var c = Samuraj.Common,
					e = Samuraj.Element,
					elems = products.dom.main.elems;
				e.get({selector: 'form#product',baseelem: document}, function(selector, elem){
					elem = elem[0];
					if (elem){
						Samuraj.Common.addClass(elem, "samuraj-display-none");
					}
				});
				for (var i=0; i<elems.length; i++){

					if (c.varExist(buyability.currentbuyability.actions)){
						// Remove "add to cart" button.
						if (buyability.currentbuyability.actions.removeAddToCartButton){
							e.get({selector: '.actionBlock #shoppingCartButton',baseelem: elems[i]}, function(selector, elem){
								elem = elem[0];
								if (elem){
									Samuraj.Common.addClass(elem, "samuraj-display-none");
								}
							});
						}

						// Remove "add to list" button.
						if (buyability.currentbuyability.actions.removeAddToListButton){
							e.get({selector: '.actionBlock #buttonBorder3',baseelem: elems[i]}, function(selector, elem){
								elem = elem[0];
								if (elem)
									Samuraj.Common.addClass(elem, "samuraj-display-none");
							});
						}

						if (buyability.currentbuyability.actions.removeAddToCartButton && buyability.currentbuyability.actions.removeAddToListButton){
							e.get({selector: '.actionBlock',baseelem: elems[i]}, function(selector, elem){
								elem = elem[0];
								if (elem)
									elem.style.setProperty("border-bottom", "0");
							});
						}
					}
				}
			}
			Samuraj.Element.showall("buyability");

		},
		wishlist: function(products, buyability){

			if (Samuraj.Common.checkMobile()){
				if (!Samuraj.Buyability.repeat){
					Samuraj.Buyability.repeat = true;
					Samuraj.Common.monitorDomChange({
						products: products,
						buyability: buyability.buyability,
						onchange: function(data){
							for (var n=0; n<data.buyability.length; n++){
								if (Samuraj.Time.dateValidationTimes(data.buyability[n].times)){
									Samuraj.Targeting.reload = true;
									Samuraj.Targeting.products(data.buyability[n].targeting, function(products, buyability){
										var c = Samuraj.Common,
											e = Samuraj.Element,
											elems = products.dom.main.elems,
											found = false;
										for (var i=0; i<elems.length; i++){
											if (c.varExist(buyability.actions)){
												found = true;
												// Remove "add to cart" button.
												if (buyability.actions.removeAddToCartButton){
													e.get({selector: '._Rfxb_._Rfxd_ button',baseelem: elems[i]}, function(selector, elem){
														elem = elem[0];
														if (elem){
															Samuraj.Common.addClass(elem, "samuraj-display-none");
														}
													});
												}
											}
										}
										if (found){
											e.get({selector: '.addalltobag._Rfxg_',baseelem: elems[i]}, function(selector, elem){
												elem = elem[0];
												if (elem){
													Samuraj.Common.addClass(elem, "samuraj-display-none");
												}
											});
										}
									}, buyability.buyability[n]);
								}
							}
						}
					});
				}
			} else {
				var c = Samuraj.Common,
					e = Samuraj.Element,
					elems = products.dom.main.elems,
					found = false;
				for (var i=0; i<elems.length; i++){

					if (c.varExist(buyability.currentbuyability.actions)){
						found = true;
						// Remove "add to cart" button.
						if (buyability.currentbuyability.actions.removeAddToCartButton){
							e.get({selector: 'td.colBuyable .buttonContainer',baseelem: elems[i]}, function(selector, elem){
								elem = elem[0];
								if (elem){
									Samuraj.Common.addClass(elem, "samuraj-display-none");
								}
							});
						}
					}
				}
				if (found){
					e.get({selector: 'tr#addAllToCartButton',baseelem: elems[i]}, function(selector, elem){
						elem = elem[0];
						if (elem){
							Samuraj.Common.addClass(elem, "samuraj-display-none");
						}
					});
				}
			}
			Samuraj.Element.showall("buyability");

		},
		*/
		monitorElementChange: function(input){
			var c = Samuraj.Common;
			if (c.isObject(input)){
				if (c.isFunction(input.onchange)){
					Samuraj.DisplayTimer.elementonchange = input.onchange;
					if (input.selector && input.innerselector){
						var checkarr = [];
						input.prop = input.prop || "outerHTML";
						var elems = document.querySelectorAll(input.selector);
						for (var i=0; i<elems.length; i++){
							if (c.varExist(elems[i].id, true)){
								var item = {}
								item.id = elems[i].id || ""
								item.class = elems[i].className || "";
								var innerelem = elems[i].querySelector(input.innerselector);
								item.prop = "";
								if (innerelem)
									item.prop = innerelem[input.prop].toString();
								checkarr.push(item);
							}
						} 
						var changed = false;
						input.checkarr = input.checkarr || [];
						if (input.checkarr.length > 0 || checkarr.length > 0){
							for (var i=0; i<checkarr.length; i++){
								if (i < input.checkarr.length){
									if (checkarr[i].prop !== input.checkarr[i].prop){
										var returnitem = input.checkarr[i];
										changed = true;
									}
								} else {
									var returnitem = input.checkarr[i];	
									changed = true;
								}
							}
							if (input.checkarr.length > checkarr.length){
								var returnitem = input.checkarr[i];	
								changed = true;
							}
						}
								
						
						input.time = input.time || 100;

						if (changed) {
							input.onchange(input.checkarr[i]);
							input.checkarr = checkarr;
						}
						setTimeout(function() {
							Samuraj.DisplayTimer.monitorElementChange(input);
						}, input.time);
					}
				}
			}
		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "DisplayTimer",
		objects: [
			{name: "DisplayTimerInjected", fn: DisplayTimerInjected},
			{name: "DisplayTimer", fn: DisplayTimer}
		],
		dependencies: [
			"Product", 
			"ProductList", 
			"ProductFilter", 
			"SimpleTabs", 
			"Swiper",
			"API"
		],
		bootup: function(){
			Samuraj.Log.add({
				cat: "DisplayTimer",
				sub: "Timing",
				text: "Bootup",
				timing: performance.now() - SamurajTiming				
			});

			//Set DisplayTimer status
			Samuraj.DisplayTimer.status = "started";

			//Initiate DisplayTimer
			//Samuraj.Source.load('DisplayTimer DisplayTimerTemplates ~Templates ProductList ProductFilter ProductInfo ExternalLibraries', function(data) {
			

			Samuraj.Source.load('DisplayTimer DisplayTimerTemplates', function(data) {

					Samuraj.Log.add({
						cat: "DisplayTimer",
						sub: "Timing",
						text: "DataSources loaded",
						timing: performance.now() - SamurajTiming				
					});

					var c = Samuraj.Common;
					if (c.varExist(data.texttemplates)) {
						Samuraj.Template.texttemplates = data.texttemplates;
					}
					
					data.displaytimerdata = data.displaytimerdata || [];
					data.displaytimertemplates = data.displaytimertemplates || [];
					data.displaytimertemplates = Samuraj.Merge.merge(data.displaytimertemplates, data.displaytimerdata, true);
					Samuraj.Template.displaytimertemplates = data.displaytimertemplates;
					
					Samuraj.DisplayTimer.load({
						data: data,
						modules: data.displaytimerdata || [], 
						
						complete: function(context) {
							var c = Samuraj.Common;

							//Initiate Simpletabs
							if (c.varExist(Samuraj.SimpleTabs)) {
								Samuraj.SimpleTabs.init();
							}
							//Initiate ProductList
							if (c.varExist(Samuraj.ProductList)) {
								Samuraj.ProductList.init(data.productlists, function(context, status) {
									Samuraj.Log.add({
										cat: "DisplayTimer",
										sub: "Timing",
										text: "Loading done",
										timing: performance.now() - SamurajTiming				
									});

									//Initiate ProductFilter
									/*
									if (c.varExist(Samuraj.ProductFilter)) {
										//if (status == "started"){
										//    Samuraj.ProductFilter.init({status: status, productInfo: data.productInfo, productFilters: data.productFilters});
										//}
										if (status == "filled") {
											//Samuraj.ProductFilter.init({status: status});
											Samuraj.ProductFilter.init({
												productInfo: data.productInfo,
												productFilters: data.productFilters
											});
											
										}
									}
									*/
									if (status == "filled") { 
										if (Samuraj.Common.checkMobile()){
											if (Samuraj.DisplayTimer.productinjectagain){
												Samuraj.Targeting.reload = true;
												Samuraj.DisplayTimer.productinjectagain();
											}
										} else {
											if (Samuraj.DisplayTimer.productinjectagainirw){
												Samuraj.Targeting.reload = true;
												Samuraj.DisplayTimer.productinjectagainirw();
											}
										}
									}


									if (Samuraj.DisplayTimer.abstatus == "filled")
										Samuraj.DisplayTimer.abstatus = "completed";

								},context);
							}
						}	
					});

				});
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}
	
})();