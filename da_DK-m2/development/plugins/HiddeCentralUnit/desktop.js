(function () {
    window.HideCentralUnit = {
        HideCentralUnit: function HideCentralUnit(selectElement, value) {
            if(document.getElementById(selectElement)){
                var selectWarehouse = document.getElementById(selectElement)
                for (var i = 0; i < selectWarehouse.children.length; i++) {
                    if( selectWarehouse.children[i].value == value ||
                        selectWarehouse.children[i].getAttribute("data-value") == value || 
                        selectWarehouse.children[i].getAttribute("aria-label") == value
                    ){
                        selectWarehouse.children[i].style = "display: none; visibility: hidden; -moz-appearance: none;";
                    }
                }
            };
            
        }
    };
    setTimeout(function () {
        //Removes Central unit from "Huskelisten"
        HideCentralUnit.HideCentralUnit("storeselector", "591");
        //Removes Central unit from PIP
        HideCentralUnit.HideCentralUnit("dropdown-stockcheck", "Central Unit");
    }, 1000);

})();