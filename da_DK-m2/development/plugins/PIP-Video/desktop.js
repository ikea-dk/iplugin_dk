function ready(fn) {
    if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
// This is the function which will load our videos onto the page
function insertThumb() {
    // Loads play icon overlay for thumbnails
    var playIcon = "https://mmapi.ikea.com/player/ikea/img-btn/play_60px_2x.png";
    // Open video list
    var request = new XMLHttpRequest();
    request.open("GET", "https://mmapi.ikea.com/retail/motionmedia/v1/item", true);
    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            var videoList = JSON.parse(this.response);
            var productNum = document.body.querySelector('form.purchase').getAttribute('data-item-no');
            var thumbContainer = document.body.querySelector('.carousel-bullets');
            var imageContainer = document.body.querySelector('.carousel__image-container');
            if (productNum!==""){
                // Extract video list from array
                videoList.some(function (index, i) {
                    // Match meta data to video list and prevent function rom running if not on a PIP
                    if (productNum !== "" && productNum.indexOf(index.item_number) > -1) {
                        // Set variable containing the videos
                        var sourceUrl = "https://mmapi.ikea.com/embed/?item_number=" + index.item_number + "&jsapi=1&is_mobile=true";
                        // Set variables to get the images
                        var imgSrc = "https://mmapi.ikea.com/retail/motionmedia/v1/item/" + index.item_number + "?dataset=images";                        
                        var imgRequest = new XMLHttpRequest();
                        imgRequest.open("GET", imgSrc, true);
                        imgRequest.onload = function () {
                            if (this.status >= 200 && this.status < 400) {
                                var imageSource = JSON.parse(this.response);
                                // selects the size for the thumbnail
                                var imgURL = imageSource.s2;
                                // Create a container for the thumbnail and import a play icon for the video
                                if (thumbContainer.firstChild) {
                                    var imgThumbnail =  '<li class="carousel-bullets__bullet btnpipvideo" style="background-size: 100% 100% !important;background-image:url(\''+imgURL +'\')">'
                                    +'<button class="bullet-clickable" type="button" >'
                                    +'<img src="'+playIcon+'" style="width:80%;height:80%;top:10%;left:10%;border:none;position:absolute;" class="carousel-bullets__bullet__image" sizes="50vw" alt="">'
                                    +'</button>'
                                    +'</li>';
                                    thumbContainer.insertAdjacentHTML("beforeend",imgThumbnail);
                                }
                                imageContainer.insertAdjacentHTML("afterend", '<iframe id="pipvideo" frameborder="0" width="560px" height="315px" style="width:100%;height:100%;display:none;position:absolute;top:0;left:0;" allowfullscreen="" src="' + sourceUrl + '"></iframe></div>');

                                // Add onclick event to image thumb
                                [].forEach.call(document.querySelectorAll('.carousel-bullets .carousel-bullets__bullet'), function(el) {
                                    el.addEventListener('click', function() {
                                        if (this.classList.contains('btnpipvideo')){
                                            document.querySelector('.carousel-bullets__bullet.carousel-bullets__bullet--active').classList.remove('carousel-bullets__bullet--active');
                                            this.classList.add('carousel-bullets__bullet--active');
                                            document.querySelector('.carousel__image-container').style.display = "none";
                                            document.querySelector('#pipvideo').style.display = "block";
                                        }
                                        else{
                                            document.querySelector('.carousel__image-container').style.display = "block";
                                            document.querySelector('#pipvideo').style.display = "none";
                                        }
                                    })
                                });
                                
                            } else {
                                console.log("Something went wrong:", this.status);
                            }
                            imgRequest.onerror = function () {
                                console.log(imgRequest.status);
                            };
                        };
                        imgRequest.send();
                        return true;
                    }
                });
            }
        }
    };
    request.send();
}

if (document.body.querySelector('form.purchase')!==null && document.body.querySelector('form.purchase').getAttribute('data-item-no')!==null && location.href.indexOf('/p/')>=0){
    ready(insertThumb);
}