(function(){
    
    //Check ready state
    function ready(fn) {
        if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
            fn();
        } else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    }

    let run = function() {
        if (document.URL.indexOf('/series/') > -1) { 
            const topLayer = document.getElementsByClassName('gridRow')[0];
            topLayer.setAttribute('style', 'display: none');

        }
    }

    ready(run);
 })();