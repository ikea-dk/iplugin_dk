
(function () {
   /* var path = "css";
    var style = document.createElement('link');
    style.rel = 'stylesheet';
    style.type = 'text/css';
    style.href = 'https://www.ikea.com/ms/da_DK/XCLm55MBAS/USP-PLP-IRW.css';
    document.getElementsByTagName('head')[0].appendChild(style); */
    //console.log ("CSS loaded")

    if (document.URL.indexOf("/catalog/categories/departments/") > -1) {
        // Load the JSON file 

        var loadovne = function loadovne() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var ovne = JSON.parse(this.responseText);
                    //console.log(this.responseText)

                    var allProducts = document.getElementsByClassName("threeColumn product");
                    for (var i = 0; i < allProducts.length; i++) {

                        var Product = allProducts[i];
                        for (var x = 0; x < ovne.length; x++) {

                            //get productID in div
                            var link = Product.children[1].childNodes[1].pathname;
                            var substrings = link.split("/");
                            var productID = substrings[5];

                            //get productID in JSON
                            var productID_json = ovne[x].Article.toString();

                            if (productID === productID_json) {
                                console.log("Product matches from div with JSON");
                                console.log("ID from DIV:" + productID);
                                console.log("ID from JSON:" + productID_json);

                                var container = document.createElement("mark");

                                container.innerHTML = '\n                          <ul.a>\n                              <br>\n                              <li class = "usp">Ovnrum liter<span class="text-yes"> ' + ovne[x].Ovnrum + ' </span></li> \n                              <li class = "usp">Programmer<span class ="text-yes">   ' + ovne[x].Programmer + '  </span></li> \n                              <li class = "usp"> ' + (ovne[x].Pyrolyse ? 'Pyrolyse<span class = "icon-yes"' + ovne[x].Pyrolyse + '</span>' : '<div class = "text-no">Pyrolyse</div><span class = "icon-no" ' + ovne[x].Pyrolyse) + '</span></li>\n                              <li class = "usp"> ' + (ovne[x].Dampfunktion ? 'Dampfunktion<span class = "icon-yes"' + ovne[x].Dampfunktion + '</span>' : '<div class = "text-no">Dampfunktion</div><span class = "icon-no" ' + ovne[x].Dampfunktion) + '</span></li>\n                              <li class = "usp"> ' + (ovne[x].Teleskopskinner ? 'Teleskopskinner<span class = "icon-yes"' + ovne[x].Teleskopskinner + '</span>' : '<div class = "text-no">Teleskopskinner</div><span class = "icon-no" ' + ovne[x].Teleskopskinner) + '</span></li>\n                              <li class = "usp"> ' + (ovne[x].Stegetermometer ? 'Stegetermometer<span class = "icon-yes"' + ovne[x].Stegetermometer + '</span>' : '<div class = "text-no">Stegetermometer</div><span class = "icon-no" ' + ovne[x].Stegetermometer) + '</span></li>\n                              <li class = "usp"> ' + (ovne[x].Garanti ? 'Garanti<span class = "text-yes">' + ovne[x].Garanti + '</span>' : '<div class = "text-no">Garanti</div><span class = "icon-no" ' + ovne[x].Garanti) + '</span></li>\n                          </ul>';

                                var product = document.body.querySelector('span.productDesp').parentNode;
                                var sp2 = document.body.querySelector('span.productDesp');
                                product.replaceChild(container, sp2);
                            }
                        }
                    }
                }
            };
            //Get the JSON file
            xhttp.open('GET', 'https://m2.ikea.com/dk/da/data-sources/3ad15280a55511e8b3a40519d78e2f54.json', true);
            xhttp.send();
        };

        loadovne();
    };
})();