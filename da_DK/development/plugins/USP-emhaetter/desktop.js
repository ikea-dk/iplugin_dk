

(function () {

    if (document.URL.indexOf("/catalog/categories/departments/") > -1) {
        // Load the JSON file 

        var loadEmhaetter = function loadEmhaetter() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var emhaetter = JSON.parse(this.responseText);
                    // console.log(this.responseText)

                    var allProducts = document.getElementsByClassName("threeColumn product");
                    for (var i = 0; i < allProducts.length; i++) {

                        var Product = allProducts[i];
                        for (var x = 0; x < emhaetter.length; x++) {

                            // get productID in div
                            var link = Product.children[1].childNodes[1].pathname;
                            var substrings = link.split("/");
                            var productID = substrings[5];

                            // get productID in JSON
                            var productID_json = emhaetter[x].Article.toString();

                            if (productID === productID_json) {
                                // console.log("Product matches from div with JSON");
                                // console.log(`ID from DIV:${  productID}`);
                                // console.log(`ID from JSON:${  productID_json}`);

                                var container = document.createElement("mark");

                                container.innerHTML = '\n                          <ul>\n                              <br>\n                              <li class = "usp">Lydniveau max.<span class="text-yes"> ' + emhaetter[x].dB + ' </span></li> \n                              <li class = "usp">Lydniveau max. recirk.<span class ="text-yes">   ' + emhaetter[x].dbTouch + '  </span></li> \n                              <li class = "usp"> ' + (emhaetter[x].belysning ? 'LED belysning<span class = "icon-yes"' + emhaetter[x].belysning + '</span>' : '<div class = "text-no">Belysning</div><span class = "icon-no" ' + emhaetter[x].belysning) + '</span></li>\n                              <li class = "usp"> ' + (emhaetter[x].Boosterfunktion ? 'Boosterfunktion<span class = "icon-yes"' + emhaetter[x].Boosterfunktion + '</span>' : '<div class = "text-no">Boosterfunktion</div><span class = "icon-no" ' + emhaetter[x].Boosterfunktion) + '</span></li>\n                              <li class = "usp"> ' + (emhaetter[x].Lysdamper ? 'Lysd\xE6mper<span class = "icon-yes"' + emhaetter[x].Lysdamper + '</span>' : '<div class = "text-no">Lysd\xE6mper</div><span class = "icon-no" ' + emhaetter[x].Lysdamper) + '</span></li>\n                              <li class = "usp"> ' + (emhaetter[x].Touch ? 'Touch-panel<span class = "icon-yes"' + emhaetter[x].Touch + '</span>' : '<div class = "text-no">Touch-panel</div><span class = "icon-no" ' + emhaetter[x].Touch) + '</span></li>\n                              <li class = "usp"> ' + (emhaetter[x].Garanti ? 'Garanti<span class = "text-yes">' + emhaetter[x].Garanti + '</span>' : '<div class = "text-no">Garanti</div><span class = "icon-no" ' + emhaetter[x].Garanti) + '</span></li>\n                          </ul>';

                                var product = document.body.querySelector('span.productDesp').parentNode;
                                var sp2 = document.body.querySelector('span.productDesp');
                                product.replaceChild(container, sp2);
                            }
                        }
                    }
                }
            };
            //Get the JSON file
            xhttp.open('GET', 'https://m2.ikea.com/dk/da/data-sources/fa685650b5b511e8837eb18effe1b65c.json', true);
            xhttp.send();
        };

        loadEmhaetter();
    }
})();