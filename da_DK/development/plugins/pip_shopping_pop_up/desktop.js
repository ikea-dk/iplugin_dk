
(function () {
    // Check ready state
    function readyNow(fn1) {
        if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
            fn1();
        } else {
            document.addEventListener('DOMContentLoaded', fn1);
        }
    }
    var runNow = function runNow() {
        setTimeout(function () {
            if (document.URL.indexOf('https://www.ikea.com/dk/da/catalog/products/') > -1) {

                var slPopup = document.querySelector('#slPopup');
                slPopup.removeAttribute('style');
                slPopup.setAttribute('id', 'newSlPopUp');

                var slPopupH1 = document.querySelector('#slPopupH1');
                slPopupH1.innerHTML = '\n        <div class="row" id="pop_Up_Headline" style="fill: white;" >\n        <div class="divForWidthPadding"></div>\n        <div id="buy_on"></div>\n        <div class="divForWidthMarginLeft"></div>\n        <div class="headline" id="slPopupH1" tabindex="0">Din vare er nu lagt i indk\xF8bskurven</div>\n        <div id="close" onclick="closePopup();addProductEvts();return false;"></div>\n        </div>\n        <div class="addOnsFooter" >\n        <button class="btnKeepShopping" type="button" onclick="closePopup();addProductEvts();return false;" >\n        Shop videre</button>\n        <button class="btnThisIs" type="button" >\n        <a id="btnLookInCart" href="/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=14&amp;langId=-12&amp;catalogId=11001&amp;orderId=.&amp;newLinks=true">\n        Se din indk\xF8bskurv</a>\n        </button>\n        </div>';

                var lookIndYourCart = document.getElementsByClassName('shoppinglistStyle')[0];
                lookIndYourCart.children[0].setAttribute('style', 'display:none');

                var oldCloseBtn = document.getElementById('shoppingListClose');
                oldCloseBtn.setAttribute('style', 'display:none;');
            }
        }, 1000);
    };
    readyNow(runNow);
})();
