/*

  __                _      
 (_ _|_ o  _ |     |_ o    
 __) |_ | (_ |< \/ |  | >< 
                /          

*/

(function(){

	var StickyFix = {
		init: function(){

			if (Samuraj.Common.checkurl("*m2.*/p/*")){
				//Hide if height is less than 450px
				/*
				Samuraj.Tracking.send({
					category: 'local_tracking_sticky_fix',
					action: 'view',
					label: document.URL
				});
				*/
				Samuraj.Element.get(".bottom-bar__container", function(selector, elem) {
					elem = elem[0];
					window.addEventListener("resize", function(e) {
						if (window.innerHeight < 450) {
							/*
							Samuraj.Tracking.send({
								category: 'local_tracking_sticky_fix',
								action: 'hidden',
								label: document.URL
							});
							*/
							Samuraj.Common.addClass(this, "samuraj-bottom-bar__container--hide");
							var elem = this.querySelector(".bottom-bar-panel");
							if (elem) {
								if (Samuraj.Common.hasClass(elem, "bottom-bar-panel--open"))
									elem.samuraj_was_open = true;
								Samuraj.Common.removeClass(elem, "bottom-bar-panel--open");
								Samuraj.Common.addClass(elem, "bottom-bar-panel--collapsed");
							}
						} else {
							Samuraj.Common.removeClass(this, "samuraj-bottom-bar__container--hide");
							var elem = this.querySelector(".bottom-bar-panel");
							if (elem && elem.samuraj_was_open) {
								Samuraj.Common.addClass(elem, "bottom-bar-panel--open");
								Samuraj.Common.removeClass(elem, "bottom-bar-panel--collapsed");
							}
						}
					}.bind(elem));
				});

				//Add Slide dismiss function
				Samuraj.Element.get(".bottom-bar__container", function(selector, elem) {
					elem = elem[0];
					var dismiss = new Samuraj.Element.dismiss();
					dismiss.load(elem, [elem.querySelector(".bottom-bar"), elem.querySelector(".bottom-bar img"), elem.querySelector(".bottom-bar-purchase"), elem.querySelector(".bottom-bar-info"), elem.querySelector(".bottom-bar-info__name")]);
					dismiss.tracking = function(){
						/*
						Samuraj.Tracking.send({
							category: 'local_tracking_sticky_fix',
							action: 'dismissed',
							label: document.URL
						});
						*/
					}
				});
			}
		}

	};
	
	
	//*******************************************************************
	
	var transfer = {
		name: "StickyFix",
		objects: [
			{name: "StickyFix", fn: StickyFix}
		],
		dependencies: [],
		bootup: function(){
			//Initiate

			Samuraj.StickyFix.init();
			
			Samuraj.Log.add({
				cat: "StickyFix",
				sub: "Timing",
				text: "Loading done",
				timing: performance.now() - SamurajTiming				
			});
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();


