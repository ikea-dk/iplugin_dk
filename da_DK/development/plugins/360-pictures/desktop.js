(function (document) {
    "use strict";

    /* eslint no-console: ["error", { allow: ["log", "error"] }] */

    function getJSON(url, callback) {
        var request = new XMLHttpRequest();
        // insert url to your iplugins pointing to the datafile below
        request.open("GET", url, true);

        request.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                try {
                    callback(JSON.parse(this.response));
                } catch (error) {
                    console.error(error);
                }
            } else {
                console.error(this.status);
            }
            request.onerror = function () {
                console.error(this.status);
            };
        };
        request.send();
    }

    function ready(fn) {
        if (document.attachEvent ? document.readyState == "complete" : document.readyState !== "loading") {
            fn();
        } else {
            document.addEventListener("DOMContentLoaded", fn);
        }
    }

    function forEach(array, fn) {
        for (var i = 0; i < array.length; i++)
            fn(array[i], i);
    }

    function insertThreesixty() {
        var itemNumber = document.getElementById("itemNumber");
        var productImage = document.getElementById("productImg");
        
        getJSON("https://m2.ikea.com/dk/da/data-sources/da66dd80e7ed11e88b197772665f8a1b.json", function (data) {
            data.forEach(function (model) {
                var thumbContainer = "<div style='background-image: url(https://ww9.ikea.com/ext/360/" + model.object + "/" + model.thumb + "); background-size: 90%; background-position: center center; width: 110px; height: 112px; border: 1px solid rgb(238, 238, 238);' class='imageThumb' id='threeThumb'> <a href='javascript:void(0);' id='threeThumb'> <img id='360iconThumb' class='threesixThumb' src='https://ww9.ikea.com/ext/360/360-icon.svg' onmouseover='addOpacityEffect(this.id);' onmouseout='rmvOpacityEffect(this.id);' id='threeThumb'></a> </div>";
                if (!itemNumber == false) {
                    var idArray = model.id;
                    forEach(idArray, function (articleID) {
                        if ((itemNumber.textContent.split(".").join("").indexOf(articleID) > -1)) {
                            var imageThumbnail = document.getElementById("imageThumb_0");
                            imageThumbnail.insertAdjacentHTML("afterend", thumbContainer);
                            var threeThumb = document.getElementById("threeThumb");
                            var iframeLoaded = false;
                            // Make the product image invisible and the 360 model visible if the 360 thumbnail is clicked
                            threeThumb.addEventListener("click", function () {
                                if (!iframeLoaded) {
                                    iframeLoaded = true;
                                    document.getElementById("productImg").insertAdjacentHTML("afterend", "<iframe id='360Frame' frameborder='0'  width='560px' height='560px' src='https://ww9.ikea.com/ext/360/index.html?itemID=" + model.object + "'></iframe>");
                                }
                                document.getElementById("360Frame").style.display = "inline";
                                threeThumb.style.borderColor = "#3399fb";
                                productImage.style.display = "none";
                                var activeClass = document.querySelector("a.active");
                                if (activeClass != undefined) {
                                    activeClass.classList.remove("active");
                                }
                                // Fix conflicts with Video PIP plugin
                                if (document.getElementById("productVideo") != undefined) {
                                    var productVideo = document.getElementById("productVideo");
                                    productVideo.style.display = "none";
                                    document.getElementById("videoDiv_1").style.borderColor = "#eee";
                                    var frameWindow = document.getElementById("productVideo").contentWindow;
                                    frameWindow.postMessage("pause", "*");
                                    if (document.getElementsByClassName("firstThumb")[0] != undefined) {
                                        var thumbNode = document.getElementsByClassName("firstThumb")[0];
                                        thumbNode.classList.add("active");
                                    }
                                }
                            });
                            // Hide the 360 model again and re-show the image after any other thumbnail is clicked
                            // Fixes for IE below
                            var isIE = !!document.documentMode;
                            var isEdge = !isIE && !!window.StyleMedia;
                            var imageThumbnails = document.querySelectorAll("[id^='imageThumb_']");

                            if (isIE || isEdge) {
                                var iconThumb = document.getElementById("360iconThumb");
                                iconThumb.src = "https://ww9.ikea.com/ext/360/360-icon.png";
                                iconThumb.style.height = "45%";
                                iconThumb.style.width = "45%";
                                iconThumb.style.paddingTop = "30px";
                            }
                            forEach(imageThumbnails, function (thumbnail) {
                                thumbnail.addEventListener("click", function () {
                                    document.getElementById("360Frame").style.display = "none";
                                    threeThumb.style.borderColor = "#eee";
                                    productImage.style.display = "inline";
                                });
                            });

                            var videoDiv = document.getElementById("videoDiv_1");
                            // Fix conflicts with Video PIP plugin
                            if (videoDiv) {
                                videoDiv.addEventListener("click", function () {
                                    document.getElementById("360Frame").style.display = "none";
                                    document.getElementById("threeThumb").style.borderColor = "#eee";
                                });
                            }
                        }
                    });
                }
            });
        });

    }
    var remove = function (element) {
        element.parentNode.removeChild(element) || element.srcElement.removeChild(element);
    };

    var updateThumbLocation = function () {
        remove(document.getElementById("360Frame"));
        remove(document.getElementById("threeThumb"));
        insertThreesixty();
        var productImage = document.getElementById("productImg");
        productImage.style.display = "inline";
    };

    ready(insertThreesixty);
    window.addEventListener("hashchange", updateThumbLocation);
})(document);