(function () {
    "use strict";

    function ready(fn) {
        if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
            fn();
        } else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    }
    function insertServiceHeader() {
        if (document.getElementById("topMenu")) {
            var node = document.getElementById("topMenu");
            node.insertAdjacentHTML("beforebegin", "<div id='serviceHeader'></div>");
            var request = new XMLHttpRequest();
            
            request.open('GET', 'https://m2.ikea.com/dk/da/data-sources/2c199310811511e8b1f23dc81bf76a31.json', true);

            request.onload = function () {
                if (this.status >= 200 && this.status < 400) {
                    var data = JSON.parse(this.response);
                    var serviceHeader = document.getElementById("serviceHeader");
                    serviceHeader.innerHTML = "<ul>" +
                        "<li><a href='" + data[0].link + "'><img src='" + data[0].image + "'/>" + data[0].text + "</a></li>" +
                        "<li><a href='" + data[1].link + "'><img src='" + data[1].image + "'/>" + data[1].text + "</a></li>" +
                        "<li><a href='" + data[2].link + "'><img src='" + data[2].image + "'/>" + data[2].text + "</a></li>" +
                        "</ul>";
                } else {
                    console.log("Something went wrong:", this.status);
                }
            };

            request.onerror = function(){
                console.log(this.status);
            };
        request.send();
        }
    };
    ready(insertServiceHeader);
}());