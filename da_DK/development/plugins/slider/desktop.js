/* eslint-disable no-undef */
/* eslint-disable func-names */
function loadSwiper() {
    var xhttp = new XMLHttpRequest();
     xhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200) {

function firstSlider() {
 let container = document.createElement("slider");

                   container.innerHTML = '<div class="samuraj-image-carousel">\n' +
                   ' <div class="swiper-container">\n' +
                   '  <div class="swiper-wrapper">\n' +
                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/70423082">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/70423082.jpg">\n' +
                   '		</div>\n' +
                     '		<div class="samuraj-headlinetext">\n' +
                   '			KUNGSBLOMMA\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Dynebetræk og 2 pudebetræk, hvid, grå\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			289.-\n' +
                   '		</div>\n' +
                   '  </a>\n' +
                   '	</div>\n' +

                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/00349849">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/00349849.jpg">\n' +
                   '		</div>\n' +
                   '		<div class="samuraj-headlinetext">\n' +
                   '			NORDLI\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Sengestel med opbevaring, hvid\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			2.699.-\n' +
                   '		</div>\n' +
                   '   </a>\n' +
                   '	</div>\n' +
                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/00313195">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/00313195.jpg">\n' +
                   '		</div>\n' +
                   '		<div class="samuraj-headlinetext">\n' +
                   '			FRÄJEN\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Badehåndklæde, blågrøn\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			49.-\n' +
                   '		</div>\n' +
                   '  </a>\n' +
                   '	</div>\n' +
                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/40391879">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/40391879.jpg">\n' +
                   '		</div>\n' +
                   '		<div class="samuraj-headlinetext">\n' +
                   '			SAXBORGA\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Krukke med låg og bakke, 5 dele, glas kork\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			99.-\n' +
                   '		</div>\n' +
                   '</a>\n' +
                   '	</div>\n' +
                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/70373591">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/70373591.jpg">\n' +
                   '		</div>\n' +
                   '		<div class="samuraj-headlinetext">\n' +
                   '			JÄTTESTOR\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Tøjdyr, elefant, grå\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			139.-\n' +
                   '		</div>\n' +
                   '</a>\n' +
                   '	</div>\n' +
                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/10435219">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/10435219.jpg">\n' +
                   '		</div>\n' +
                   '		<div class="samuraj-headlinetext">\n' +
                   '			KONGSTRUP\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Tæppe, lang luv, lyseblå, grøn\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			679.-\n' +
                   '		</div>\n' +
                   '</a>\n' +
                   '	</div>\n' +
                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/80371520">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/80371520.jpg">\n' +
                   '		</div>\n' +
                   '		<div class="samuraj-headlinetext">\n' +
                   '			TROGEN\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Køkkenstige/børnetaburet, gul\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			129.-\n' +
                   '		</div>\n' +
                   '</a>\n' +
                   '	</div>\n' +
                   '	<div class="swiper-slide">\n' +
                   '		<a href="https://www.ikea.com/dk/da/catalog/products/30358941">\n' +
                   '		<div class="samuraj-image">\n' +
                   '			<img src="https://s3.eu-central-1.amazonaws.com/ikea-development/images/30358941.jpg">\n' +
                   '		</div>\n' +
                   '		<div class="samuraj-headlinetext">\n' +
                   '			KATTEVIK\n' +
                   '		</div>\n' +
                            '<div class="samuraj-text">\n' +
                   '			Vask til bordplade, hvid\n' +
                   '		</div>\n' +
                            '<div class="samuraj-pris">\n' +
                   '			800.-\n' +
                   '		</div>\n' +
                   '</a>\n' +
                   '	</div>\n' +
                   '  </div>\n' +
                   '  <div class="swiper-pagination"></div>\n' +
                   '  <div class="swiper-button-next"></div>\n' +
                   '  <div class="swiper-button-prev"></div>\n' +
                   ' </div>\n' +
                   '</div>\n';
               const productList = document.getElementById('whatsection');
               const sp2 = document.getElementById('whatsection');
               productList.appendChild(container, sp2);
              }

            firstSlider();
            Swiper();
var params = {
	slidesPerView: '4',
	centeredSlides: false,
	spaceBetween: 30,
	
	pagination: {
		el: '.samuraj-image-carousel .swiper-pagination',
		clickable: true,
	},
	navigation: {
		nextEl: '.samuraj-image-carousel .swiper-button-next',
		prevEl: '.samuraj-image-carousel .swiper-button-prev',
	},
	autoplay: {
		delay: 5000,
		disableOnInteraction: true,
	}
};
var mySwiper = new Swiper(".samuraj-image-carousel .swiper-container", params);

xhttp.open('GET', 'https://ww8.ikea.com/ext/iplugins/da_DK/development/plugins/swiper/desktop.js', true);
xhttp.send();

whatsection