'use strict';

(function () {
    // Check ready state
    function readyNow(fn1) {
        if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
            fn1();
        } else {
            document.addEventListener('DOMContentLoaded', fn1);
        }
    }
    var runNow = function runNow() {
        if (document.URL.indexOf('https://secure.ikea.com/webapp/wcs/stores/servlet') > -1 && document.URL.includes('IrwCheckoutAddressView')) {
            if (document.getElementById('businessCustomer').checked && document.getElementById('deliveryAdress').checked == false) {
                var postionOfBusinessCartBox = document.getElementById('billingBox').children[0];

                var divBusinessCartBox = document.createElement("div");
                divBusinessCartBox.setAttribute('id', 'businessCartBox');
                divBusinessCartBox.innerHTML = "<p class='collapsible_IKEABUSINESSCARD'>IKEA BUSINESS Kort - klik her</p><div class='content_IKEABUSINESSCARD'><p class='boxBoby' >Du kan ikke betale online med IKEA BUSINESS kort i øjeblikket.<br> Kontakt <a target='_blank' href='https://www.ikea.com/dk/da/catalog/categories/business/ikea_business/?preferedui=desktop#businesslokal'>dit nærmeste IKEA BUSINESS</a>, hvis du ønsker at bruge kortet.</p></div><br>";
                postionOfBusinessCartBox.appendChild(divBusinessCartBox);

                var coll = document.getElementsByClassName("collapsible_IKEABUSINESSCARD");
                var i;

                for (i = 0; i < coll.length; i++) {
                    coll[i].addEventListener("click", function () {
                        var content = this.nextElementSibling;
                        if (content.style.display === "block") {
                            content.style.display = "none";
                        } else {
                            content.style.display = "block";
                        }
                    });
                }
            }
            if (document.getElementById('businessCustomer').checked && document.getElementById('deliveryAdress').checked == true){
                var postionOfBusinessCartBox = document.getElementById('billingBox').children[0];

                var divBusinessCartBox = document.createElement("div");
                divBusinessCartBox.setAttribute('id', 'businessCartBoxDA');
                divBusinessCartBox.innerHTML = "<p class='collapsible_IKEABUSINESSCARD'>IKEA BUSINESS Kort - klik her</p><div class='content_IKEABUSINESSCARD'><p class='boxBoby' >Du kan ikke betale online med IKEA BUSINESS kort i øjeblikket.<br> Kontakt <a target='_blank' href='https://www.ikea.com/dk/da/catalog/categories/business/ikea_business/?preferedui=desktop#businesslokal'>dit nærmeste IKEA BUSINESS</a>, hvis du ønsker at bruge kortet.</p></div><br>";
                postionOfBusinessCartBox.before(divBusinessCartBox);

                var coll = document.getElementsByClassName("collapsibleDA");
                var i;

                for (i = 0; i < coll.length; i++) {
                    coll[i].addEventListener("click", function () {
                        var content = this.nextElementSibling;
                        if (content.style.display === "block") {
                            content.style.display = "none";
                        } else {
                            content.style.display = "block";
                        }
                    });
                }
            }
        }
    };
    readyNow(runNow);
})();