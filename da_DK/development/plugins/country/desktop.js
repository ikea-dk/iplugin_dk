'use strict';

(function () {
    // Check ready state
    function readyNow(fn1) {
        if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
            fn1();
        } else {
            document.addEventListener('DOMContentLoaded', fn1);
        }
    }
    var runNow = function runNow() {
        if (document.URL.indexOf('https://secure.ikea.com/webapp/wcs/stores/servlet') > -1 && document.URL.includes('IrwCheckoutAddressView') || document.URL.includes('IrwCheckoutWelcomeView')) {
        if (document.getElementById('privateCustomer').checked == true && document.getElementById('deliveryAdress').checked == false) {
                var _insertInfoSpanToFrom = document.getElementById("billingBox");

                // take use of a existing country element
                var _country = document.getElementById('billingBox').children[5];
                _country.removeAttribute('type'); // do not hide 
                _country.setAttribute('value', 'Danmark');
                _country.setAttribute('id', 'countryDenmark');
                _country.setAttribute('name', 'basket[invoiceAddress][country]');
                _country.disabled = true;

                // tooltip div
                var _tooltip_div = document.createElement("div");
                _tooltip_div.setAttribute('id', 'myTooltipDiv');
                _tooltip_div.setAttribute('class', 'tooltip');
                _insertInfoSpanToFrom.appendChild(_tooltip_div);

                // Title on country
                var _para = document.createElement("p");
                var _p = document.createTextNode("Land");
                _para.appendChild(_p);
                _para.setAttribute('id', 'myTitle');
                _para.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
                _para.onclick = function openCountry() {
                    document.getElementById('myModalCountry').style.display = "block";
                    document.getElementById('lightboxCountry').style.display = "block";
                };
                _insertInfoSpanToFrom.appendChild(_para);

                // modal
                var _modalLightbox = document.createElement('div');
                _modalLightbox.setAttribute('id', 'lightboxCountry');
                _modalLightbox.setAttribute('class', 'lightbox-country');
                _modalLightbox.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                _insertInfoSpanToFrom.appendChild(_modalLightbox);

                var _modalPopUp = document.createElement('div');
                _modalPopUp.setAttribute('id', 'myModalCountry');
                _modalPopUp.setAttribute('class', 'modalCountry');
                _modalPopUp.innerHTML = '<div class=\'countryPopup\' >\n                                                <a id=\'lightbox-close-btn\' href=\'#\'>x</a>\n                                                <h3>Land</h3>\n                                                <p>Vi leverer til hele Danmark.<br>\n                                                Hvis du vil have leveret til udlandet, kan du l\xE6se mere                                                 \n                                                <a href=\'https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering\'>her.</a></p>\n                                            </div>';
                _modalPopUp.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                _insertInfoSpanToFrom.appendChild(_modalPopUp);

                // modal inster div in order
                _modalLightbox.insertBefore(_modalPopUp, _modalLightbox.childNodes[0]);

                // inster child in order
                _tooltip_div.insertBefore(_para, _tooltip_div.childNodes[0]);
                _tooltip_div.insertBefore(_country, _tooltip_div.childNodes[1]);

                // place before e-mail field
                var _mailElement = document.getElementById('email1_field');
                _mailElement.before(myTooltipDiv);
        }
            // deliveryAdress true - if coustomer needs delivery to a other adrresse. 
            if (document.getElementById('privateCustomer').checked && document.getElementById('deliveryAdress').checked == true) {
                var insertInfoSpanToFrom = document.getElementById("billingBox");

                // take use of a existing country element
                var country = document.getElementById('billingBox').children[4];
                country.removeAttribute('type'); // do not hide
                country.setAttribute('value', 'Danmark');
                country.setAttribute('id', 'countryDenmark');
                country.setAttribute('name', 'basket[invoiceAddress][country]');
                country.disabled = true;

                // tooltip div
                var tooltip_div = document.createElement("div");
                tooltip_div.setAttribute('id', 'myTooltipDiv');
                tooltip_div.setAttribute('class', 'tooltip');
                insertInfoSpanToFrom.appendChild(tooltip_div);

                // Title on country
                var para = document.createElement("p");
                var p = document.createTextNode("Land");
                para.appendChild(p);
                para.setAttribute('id', 'myTitle');
                para.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
                para.onclick = function openCountry() {
                    document.getElementById('myModalCountry').style.display = "block";
                    document.getElementById('lightboxCountry').style.display = "block";
                };
                insertInfoSpanToFrom.appendChild(para);

                // modal
                var modalLightbox = document.createElement('div');
                modalLightbox.setAttribute('id', 'lightboxCountry');
                modalLightbox.setAttribute('class', 'lightbox-country');
                modalLightbox.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                insertInfoSpanToFrom.appendChild(modalLightbox);

                var modalPopUp = document.createElement('div');
                modalPopUp.setAttribute('id', 'myModalCountry');
                modalPopUp.setAttribute('class', 'modalCountry');
                modalPopUp.innerHTML = '<div class=\'countryPopup\' >\n                                                <a id=\'lightbox-close-btn\' href=\'#\'>x</a>\n                                                <h3>Land</h3>\n                                                <p>Vi leverer til hele Danmark.<br>\n                                                Hvis du vil have leveret til udlandet, kan du l\xE6se mere\n                                                <a href=\'https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering\'>her.</a></p>\n                                            </div>';
                modalPopUp.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                insertInfoSpanToFrom.appendChild(modalPopUp);

                // modal inster div in order
                modalLightbox.insertBefore(modalPopUp, modalLightbox.childNodes[0]);

                // inster child in order
                tooltip_div.insertBefore(para, tooltip_div.childNodes[0]);
                tooltip_div.insertBefore(country, tooltip_div.childNodes[1]);
                console.log('deliveryAdress is true');

                // place before e-mail field
                var mailElement = document.getElementById('email1_field');
                mailElement.before(myTooltipDiv);
            }
            if (document.getElementById('businessCustomer').checked && document.getElementById('deliveryAdress').checked == false) {
                var _insertInfoSpanToFrom2 = document.getElementById("billingBox");

                // take use of a existing country element
                var _country2 = document.getElementById('billingBox').children[6];
                _country2.removeAttribute('type'); // do not hide 
                _country2.setAttribute('value', 'Danmark');
                _country2.setAttribute('id', 'countryDenmark');
                _country2.disabled = true;

                // tooltip div
                var _tooltip_div2 = document.createElement("div");
                _tooltip_div2.setAttribute('id', 'myTooltipDiv');
                _tooltip_div2.setAttribute('class', 'tooltip');
                _insertInfoSpanToFrom2.appendChild(_tooltip_div2);

                // Title on country
                var _para2 = document.createElement("p");
                var _p2 = document.createTextNode("Land");
                _para2.appendChild(_p2);
                _para2.setAttribute('id', 'myTitle');
                _para2.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
                _para2.onclick = function openCountry() {
                    document.getElementById('myModalCountry').style.display = "block";
                    document.getElementById('lightboxCountry').style.display = "block";
                };
                _insertInfoSpanToFrom2.appendChild(_para2);

                // modal
                var _modalLightbox2 = document.createElement('div');
                _modalLightbox2.setAttribute('id', 'lightboxCountry');
                _modalLightbox2.setAttribute('class', 'lightbox-country');
                _modalLightbox2.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                _insertInfoSpanToFrom2.appendChild(_modalLightbox2);

                var _modalPopUp2 = document.createElement('div');
                _modalPopUp2.setAttribute('id', 'myModalCountry');
                _modalPopUp2.setAttribute('class', 'modalCountry');
                _modalPopUp2.innerHTML = '<div class=\'countryPopup\' >\n                                                <a id=\'lightbox-close-btn\' href=\'#\'>x</a>\n                                                <h3>Land</h3>\n                                                <p>Vi leverer til hele Danmark.<br>\n                                                Hvis du vil have leveret til udlandet, kan du l\xE6se mere                                                 \n                                                <a href=\'https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering\'>her.</a></p>\n                                            </div>';
                _modalPopUp2.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                _insertInfoSpanToFrom2.appendChild(_modalPopUp2);

                // modal inster div in order
                _modalLightbox2.insertBefore(_modalPopUp2, _modalLightbox2.childNodes[0]);

                // inster child in order
                _tooltip_div2.insertBefore(_para2, _tooltip_div2.childNodes[0]);
                _tooltip_div2.insertBefore(_country2, _tooltip_div2.childNodes[1]);

                // place before e-mail field
                var _mailElement2 = document.getElementById('email1_field');
                _mailElement2.before(myTooltipDiv);
            }
            // deliveryAdress true - if coustomer needs delivery to a other adrresse. 
            if (document.getElementById('businessCustomer').checked && document.getElementById('deliveryAdress').checked == true) {
                var _insertInfoSpanToFrom3 = document.getElementById("billingBox");

                // take use of a existing country element
                var _country3 = document.getElementById('billingBox').children[5];
                _country3.removeAttribute('type'); // do not hide 
                _country3.setAttribute('value', 'Danmark');
                _country3.setAttribute('id', 'countryDenmark');
                _country3.disabled = true;

                // tooltip div
                var _tooltip_div3 = document.createElement("div");
                _tooltip_div3.setAttribute('id', 'myTooltipDiv');
                _tooltip_div3.setAttribute('class', 'tooltip');
                _insertInfoSpanToFrom3.appendChild(_tooltip_div3);

                // Title on country
                var _para3 = document.createElement("p");
                var _p3 = document.createTextNode("Land");
                _para3.appendChild(_p3);
                _para3.setAttribute('id', 'myTitle');
                _para3.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
                _para3.onclick = function openCountry() {
                    document.getElementById('myModalCountry').style.display = "block";
                    document.getElementById('lightboxCountry').style.display = "block";
                };
                _insertInfoSpanToFrom3.appendChild(_para3);

                // modal
                var _modalLightbox3 = document.createElement('div');
                _modalLightbox3.setAttribute('id', 'lightboxCountry');
                _modalLightbox3.setAttribute('class', 'lightbox-country');
                _modalLightbox3.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                _insertInfoSpanToFrom3.appendChild(_modalLightbox3);

                var _modalPopUp3 = document.createElement('div');
                _modalPopUp3.setAttribute('id', 'myModalCountry');
                _modalPopUp3.setAttribute('class', 'modalCountry');
                _modalPopUp3.innerHTML = '<div class=\'countryPopup\' >\n                                                <a id=\'lightbox-close-btn\' href=\'#\'>x</a>\n                                                <h3>Land</h3>\n                                                <p>Vi leverer til hele Danmark.<br>\n                                                Hvis du vil have leveret til udlandet, kan du l\xE6se mere                                                 \n                                                <a href=\'https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering\'>her.</a></p>\n                                            </div>';
                _modalPopUp3.onclick = function closeCountry() {
                    document.getElementById('myModalCountry').style.display = "none";
                    document.getElementById('lightboxCountry').style.display = "none";
                };

                _insertInfoSpanToFrom3.appendChild(_modalPopUp3);

                // modal inster div in order
                _modalLightbox3.insertBefore(_modalPopUp3, _modalLightbox3.childNodes[0]);

                // inster child in order
                _tooltip_div3.insertBefore(_para3, _tooltip_div3.childNodes[0]);
                _tooltip_div3.insertBefore(_country3, _tooltip_div3.childNodes[1]);

                // place before e-mail field
                var _mailElement3 = document.getElementById('email1_field');
                _mailElement3.before(myTooltipDiv);
            }
        }
        
    };
    readyNow(runNow);
})();