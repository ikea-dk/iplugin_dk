(function() {
	'use strict';
	if (document.URL.indexOf('dk/da/catalog/products') > -1) {
	var xmlhttp = new XMLHttpRequest(),
		jsonUrl = "https://m2.ikea.com/dk/da/data-sources/cd661fc0d14911e8bab71bf69e3f0133.json",
		showOnArticles,
		showMainCont = false;
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var jsonData = JSON.parse(this.responseText);
			createProductWrappers(jsonData);
		}
	}
	xmlhttp.open("GET", jsonUrl, true);
	xmlhttp.send();
	// Create main-div for product package for each package
	var mainProductWrapper = document.createElement('div');
	mainProductWrapper.id = 'mainProductWrapper';

	function createProductWrappers(jsonData) {
		for (var a = 0; a < jsonData.completePackages.length; a++) {
			var addPackages = false;
			for (var b = 0; b < jsonData.completePackages[a].showOnArticles.length; b++) {
				showOnArticles = jsonData.completePackages[a].showOnArticles[b];
				// Show on product URL or product name
				if (document.URL.indexOf(showOnArticles) > -1 || document.querySelector('meta[name="product_name"]').getAttribute('content').contains(showOnArticles)) {
					addPackages = true;
					showMainCont = true;
				}
			} // end of showOnArticles loop
			if (addPackages) {
				var artNumArray = [],
					quantNumArray = [],
					packageNameArray = [];
				packageNameArray.push(jsonData.completePackages[a].packageName);
				// }
				for (var c = 0; c < jsonData.completePackages[a].artNumbersInPackage.length; c++) {
					// Quantity
					//console.log(jsonData.completePackages[a].artNumbersInPackage[c].quantity);
					artNumArray.push(jsonData.completePackages[a].artNumbersInPackage[c].artNumber);
					quantNumArray.push(jsonData.completePackages[a].artNumbersInPackage[c].quantity);
				} // end of artNumbersInPackage
				mainProductWrapper.insertAdjacentHTML('beforeend', '<div class="compPackProductContainer" data-pl-template="pl-komplett" data-pl-articles-row="7" data-custom-articleid="' + artNumArray + '" data-custom-article-quantity="' + quantNumArray + '"><div class="product-list-waiting-text iplugin-show" style="display:none;"></div><div class="product-list-fallback-text iplugin-hide">Du skal aktivere javascript for at se dette</div>');
			} // if addPackages
		} // end of completepackages loop
	} // end of function createProductWrappers(jsonData)
	window.addEventListener("load", function() {
		if (showMainCont) {
			// create main div
			var compPackMainContainer = document.createElement('div');
			compPackMainContainer.id = 'compPackMainContainer';
			compPackMainContainer.innerHTML = '<h2 id="compPackMainHeadline" class="moreHead">Få hele pakken ' + document.querySelector('meta[name="product_name"]').getAttribute('content') + '</h2>';
			// Insert main-div on PIP
			var pipElement = document.getElementById("complementaryProductContainer");
			pipElement.parentNode.insertBefore(compPackMainContainer, pipElement);
			compPackMainContainer.appendChild(mainProductWrapper);
			// Create product list
			ipluginsCommon.isIpluginsStandardReady(function() {
				var apptusDiv = document.getElementsByClassName("compPackProductContainer");
				Array.from(apptusDiv).forEach(function(appdiv) {
					ipluginsStandard.productList(appdiv);
				});
			});
			// Add + and =
			var findPlp = setInterval(function() {
				if (document.querySelectorAll('.totalContainer')[0].innerHTML.length > 0) {
					console.log('Jeg fandt den');
					// Remove not buyable products
					if (document.querySelectorAll('#compPackMainContainer .disabled')) {
						var notBuyable = Array.from(document.querySelectorAll('#compPackMainContainer .disabled'));
						notBuyable.forEach(function(eachNotBuy) {
							console.log('Kan ikke købes');
							console.log(eachNotBuy);
							eachNotBuy.parentNode.parentNode.parentNode.parentNode.style.display = "none";
						});
					}
					// Add =
					var totCont = Array.from(document.querySelectorAll('.totalContainer'));
					totCont.forEach(function(eachTotCont) {
						eachTotCont.insertAdjacentHTML('beforebegin', '<div class="plusDiv">=</div>');
					});
					// Add +
					var prodCont = Array.from(document.querySelectorAll('#compPackMainContainer .product-list-product'));
					prodCont.forEach(function(eachProdCont) {
						if (eachProdCont.innerHTML.contains('disabled')) {} else {
							eachProdCont.parentNode.insertAdjacentHTML('beforebegin', '<div class="plusDiv">+</div>');
						}
					});
					// Hide IoS div "Kompletterande produkter"
					if (document.getElementById('complementaryProductContainer') && document.getElementById('compPackMainContainer')) {
						document.getElementById('complementaryProductContainer').style.display = 'none';
					}
					clearInterval(findPlp);
				} else {
					console.log('fandt intet');
				}
			}, 700); // setInterval

			


		} // show main container
		// } // if

		setTimeout(function(){ 

			if(document.getElementById('compPackMainContainer')){
			eventTrackingCtaBundlesPip('bundleExist');
			}

				// Tracking "Køp hele pakken" buy button
				var clkCountBuyBtnInBundle = Array.from(document.querySelectorAll('#compPackMainContainer #mainProductWrapper .totalContainer .product-list-cta a'));
				clkCountBuyBtnInBundle.forEach(function(eachClkBuyBtnBundle) {
					eachClkBuyBtnBundle.addEventListener("click", function(e) {
						eventTrackingCtaBundlesPip('clkBundlePackageBuyBtn');
					});
				});

				// Tracking "Køp hele pakken" buy button
				var clkCountEachBuyBtn = Array.from(document.querySelectorAll('#compPackMainContainer #mainProductWrapper .product-list-cta a'));
				clkCountEachBuyBtn.forEach(function(eachClkCountEachBuyBtn) {
					eachClkCountEachBuyBtn.addEventListener("click", function(e) {
						if(e.target.innerHTML == 'Læg i indkøbskurv'){
						eventTrackingCtaBundlesPip('clkEachBuyBtn');
						}
					});
				});


				// Tracking call to action click
				function eventTrackingCtaBundlesPip(eventvalue) {
					var senddata = {
						category: "bundlesPip",
						value: eventvalue,
						currency_code: "DKK",
						site_platform: "web",
						visit_country: "dk",
						visit_language: "da"
					};
					utag.link(senddata, null, [114]);
				}
				

		}, 1000);

	}); // end of window load
	
} // if document url
})(); // main function