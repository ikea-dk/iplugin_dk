(function () {
    ///Verify the user is at the correct page
    if (!document.URL.indexOf("IrwCheckoutWelcomeView") && !document.URL.indexOf("IrwCheckoutAddressView")) 
    return;
    window.Recalculation = {
        DeliveryPrice: function DeliveryPrice() {
            //Test if price is visible
            if(document.getElementById("txtTotalDelivery").innerHTML.match(/\d+/g)==null)
            {
                document.getElementById("jsButton_calculateDeliveryCost").click();
                window.setTimeout(function(){Recalculation.DeliveryPrice()},100);
                console.log("Click on recalc");
            }    
        },
        WaitForElement: function WaitForElement(elementId, callBack) {
            window.setTimeout(function () {
                if (document.getElementById(elementId)) {
                    callBack(elementId);
                } else {
                    PostNordCommon.WaitForElement(elementId, callBack);
                }
            }, 10);
        },
    };
    
    Recalculation.WaitForElement("txtTotalDelivery",function(){Recalculation.WaitForElement("jsButton_calculateDeliveryCost",function(){Recalculation.DeliveryPrice()})});
    Recalculation.WaitForElement("signup_checkout_private_zipCode",function(){document.getElementById("signup_checkout_private_zipCode").addEventListener("change", function() {Recalculation.DeliveryPrice()})});
})();