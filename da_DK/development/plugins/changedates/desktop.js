/*

  _                      _
 /  |_   _. ._   _   _  | \  _. _|_  _   _
 \_ | | (_| | | (_| (/_ |_/ (_|  |_ (/_ _>
				 _|

*/
(function(){
	
	var ChangeDates = {
		settings:{
			valid_to_text: "så længe lager haves",  // "så länge lagret räcker"
			valid_to_text_cart: "Gyldig til"
		},
		
		init: function(data) {
			for (var i = 0; i < data.length; i++) {
				data[i] = this.formatDate(data[i]);

				//Initiate IRW
				if (document.URL.toLowerCase().indexOf("www.ikea.com") > -1) {
					if (document.URL.toLowerCase().indexOf("/catalog/products/") > -1) {
						this.changePIP(data[i]);
						this.changePIP_other(data[i]);
					} else if (document.URL.toLowerCase().indexOf("/search/") > -1) {
						this.changeSearch(data[i]);
					} else if (document.URL.toLowerCase().indexOf("/availability/") > -1) {
						this.changeAvailability(data[i]);
					} else if (document.URL.toLowerCase().indexOf("/catalog/") > -1) {
						this.changePLP(data[i]);
					} else if (document.URL.toLowerCase().indexOf("/webapp/wcs/stores/servlet") > -1) {
						this.changeCheckout(data[i]);
					}
				}

				//Initiate M2
				if (document.URL.indexOf("m2.ikea.com") > -1) {
					if (document.URL.toLowerCase().indexOf("/p/") > -1) {
						this.changePIP_M2(data[i]);
					} else {
						this.changePLP_M2(data[i]);
					}
				}
			}

			Samuraj.Element.showall("changdates");

		},

		//Change on PIP pages
		changePIP: function(data) {
			Samuraj.Page.loaded(function(){
				if (data.lower) {
					Samuraj.Common.monitorElementChange({
						selector: "#troPrice",
						timeout: 10000,
						onchange: function(input){
							Samuraj.Element.get("#troPrice", function(selector, elem){
								for (var i = 0; i < data.products.length; i++) {
									var artnr = /\/[sS]?(\d{8})(?!.*\/[sS]?\d{8})/gm.exec(document.URL)[1];
									if (artnr && artnr && artnr.toLowerCase().indexOf(data.products[i].toLowerCase()) > -1){
										elem[0].innerHTML = data.fromDateText + " - " + data.toDateText + " <span> " + Samuraj.ChangeDates.settings.valid_to_text + "</span>";
									}
								}
							});
						}
					});
				}
				if (data.family) {
					Samuraj.Common.monitorElementChange({
						selector: "#familyDate",
						timeout: 10000,
						onchange: function(input){
							Samuraj.Element.get("#familyDate", function(selector, elem){
								for (var i = 0; i < data.products.length; i++) {
									var artnr = /\/[sS]?(\d{8})(?!.*\/[sS]?\d{8})/gm.exec(document.URL)[1];
									if (artnr && artnr.toLowerCase().indexOf(data.products[i].toLowerCase()) > -1){
										elem[0].innerHTML = data.fromDateText + " - " + data.toDateText + " <span> " + Samuraj.ChangeDates.settings.valid_to_text + "</span>";
									}
								}
							});
						}
					});
				}
			});
		},

		//Change other products on PIP
		changePIP_other: function(data) {
			Samuraj.Page.loaded(function(){
				if (data.lower) {
					Samuraj.Element.get(".priceNote", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode',
								success: function(parent){
									var parentId = parent.getAttribute("id");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "<div class='date'>" + data.fromDateText + " -" + data.toDateText + "</div><div class='detail'>" + Samuraj.ChangeDates.settings.valid_to_text + "</div>";
											}
										}
									}
								}
							});
						}
					});
				}
				if (data.family) {
					Samuraj.Element.get("#familyOfferDateMini", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode.parentNode.parentNode',
								success: function(parent){
									var parentId = parent.getAttribute("id");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "<div class='date'>" + data.fromDateText + " -" + data.toDateText + "</div><span>" + Samuraj.ChangeDates.settings.valid_to_text + "</span";
											}
										}
									}
								}
							});
						}
					});
				}
			});
		},

		//Change on search pages
		changeSearch: function(data) {
			Samuraj.Page.loaded(function(){
				if (data.lower) {
					Samuraj.Element.get("#troPrice", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode.parentNode',
								success: function(parent){
									var parentId = parent.getAttribute("id");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "<br>" + data.fromDateText + " -<div class='troDateWidth'>" + data.toDateText + "<span><br>" + Samuraj.ChangeDates.settings.valid_to_text + "</span></div>";
											}
										}
									} else {
										Samuraj.Common.getProp({
											object: this.originobject,
											prop: 'parentNode.parentNode.parentNode.parentNode',
											success: function(parent){
												//Else check href of SEO hero image
												var parentId = parent.getAttribute("id");
												if (parentId){
													for (var j = 0; j < data.products.length; j++) {
														if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
															this.originobject.innerHTML = "<br>" + data.fromDateText + " -<div class='troDateWidth'>" + data.toDateText + "<span><br>" + Samuraj.ChangeDates.settings.valid_to_text + "</span></div>";
														}
													}
												}
											}
										});
									}
								}
							});
						}
					});
				}
				if (data.family) {
					Samuraj.Element.get("#familyOfferDateSearch", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode.parentNode',
								success: function(parent){
									var parentId = parent.getAttribute("id");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "<br>" + data.fromDateText + " -<div class='troDateWidth'>" + data.toDateText + "<span><br>" + Samuraj.ChangeDates.settings.valid_to_text + "</span></div>";
											}
										}
									} else {
										Samuraj.Common.getProp({
											object: this.originobject,
											prop: 'parentNode.parentNode.parentNode.parentNode',
											success: function(parent){
												//Else check href of SEO hero image
												var parentId = parent.getAttribute("id");
												if (parentId){
													for (var j = 0; j < data.products.length; j++) {
														if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
															this.originobject.innerHTML = "<br>" + data.fromDateText + " -<div class='troDateWidth'>" + data.toDateText + "<span><br>" + Samuraj.ChangeDates.settings.valid_to_text + "</span></div>";
														}
													}
												}
											}
										});
									}
								}
							});
						}
					});
				}
			});
		},

		//Change on PLP pages
		changePLP: function(data) {
			Samuraj.Page.loaded(function(){
				if (data.lower) {
					Samuraj.Element.get(".priceNote", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode',
								success: function(parent){
									//Check all products in the productlist
									var parentId = parent.getAttribute("id");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "<div class='date'>" + data.fromDateText + " -" + data.toDateText + "</div><div class='detail'>" + Samuraj.ChangeDates.settings.valid_to_text + "</div>";
											}
										}
									} else {
										Samuraj.Common.getProp({
											object: this.originobject,
											prop: 'parentNode.parentNode.parentNode',
											success: function(parent){
												//Else check href of SEO hero image
												var parentId = parent.getAttribute("href");
												if (parentId){
													for (var j = 0; j < data.products.length; j++) {
														if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
															this.originobject.innerHTML = "<div class='date'>" + data.fromDateText + " -" + data.toDateText + "</div><div class='detail'>" + Samuraj.ChangeDates.settings.valid_to_text + "</div>";
														}
													}
												}
											}
										});
									}
								}
							});
						}
					});
				}
				if (data.family) {
					Samuraj.Element.get(".familyOfferDate", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode.parentNode.parentNode',
								success: function(parent){
									var parentId = parent.getAttribute("id");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "<div class='date'>" + data.fromDateText + " -" + data.toDateText + "</div><span>" + Samuraj.ChangeDates.settings.valid_to_text + "</span";
											}
										}
									} else {
										Samuraj.Common.getProp({
											object: this.originobject,
											prop: 'parentNode.parentNode.parentNode.parentNode.parentNode',
											success: function(parent){
												//Else check href of SEO hero image
												var parentId = parent.getAttribute("href");
												if (parentId){
													for (var j = 0; j < data.products.length; j++) {
														if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
															this.originobject.innerHTML = "<div class='date'>" + data.fromDateText + " -" + data.toDateText + "</div><span>" + Samuraj.ChangeDates.settings.valid_to_text + "</span";
														}
													}
												}
											}
										});
									}
								}
							});
						}
					});
				}
			});
		},

		//Change on Availability pages
		changeAvailability: function(data) {
			Samuraj.Page.loaded(function(){
				if (data.lower) {
					Samuraj.Element.get(".troDate", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode',
								success: function(parent){
									//Check all products in the productlist
									var parentId = parent.querySelector("a").getAttribute("href");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "\n" + data.fromDateText + " -" + data.toDateText + "\n<span>" + Samuraj.ChangeDates.settings.valid_to_text + "</span";
											}
										}
									}
								}
							});
						}
					});
				}
				if (data.family) {
					Samuraj.Element.get("#familyOfferDateStock", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode',
								success: function(parent){
									var parentId = parent.getAttribute("href");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = "<div class='date'>" + data.fromDateText + " -" + data.toDateText + "</div><span>" + Samuraj.ChangeDates.settings.valid_to_text + "</span";
											}
										}
									}
								}
							});
						}
					});
				}
			});
		},

		//Change valid to date in checkout
		changeCheckout: function(data) {
			Samuraj.Page.loaded(function(){
				if (data.lower) {
					Samuraj.Element.get(".colPrice .regular", function(selector, elem){
						for (var i = 0; i < elem.length; i++) {
							Samuraj.Common.getProp({
								object: elem[i],
								prop: 'parentNode.parentNode',
								success: function(parent){
									var parentId = parent.getAttribute("id");
									if (parentId){
										for (var j = 0; j < data.products.length; j++) {
											if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
												this.originobject.innerHTML = Samuraj.ChangeDates.settings.valid_to_text_cart + " " + data.toDateNumber;
											}
										}
									}
								}
							});
						}
					});
				}
			});
		},

		//Change on PIP for M2
		changePIP_M2: function(data) {
			Samuraj.Page.loaded(function(){
				if (data.lower || data.family) {
					var artnr = /([^sS\/]*)\/?$/g.exec(document.URL)[1];
					for (var i = 0; i < data.products.length; i++) {
						if (artnr && artnr.toLowerCase().indexOf(data.products[i].toLowerCase()) > -1){
							Samuraj.Element.get(".product-pip__price-package .price-package .no-margin", function(selector, elem){
								for (var j = 0; j < elem.length; j++) {
									if (elem[j].innerHTML.indexOf(Samuraj.ChangeDates.settings.valid_to_text) > -1) {
										elem[j].innerHTML = "(" + data.fromDateTextM2 + " - " + data.toDateTextM2 + " " + Samuraj.ChangeDates.settings.valid_to_text + ")";
									}
								}
							});
						}
					}
					Samuraj.ChangeDates.changePLP_M2(data);
				}
			});
		},

		//Change on Search pages on M2
		changePLP_M2: function(data) {
			if (data.lower || data.family) {
				Samuraj.Common.monitorDomChange({
					onchange: function(){
						setTimeout(function(){
							Samuraj.Element.get(".product-compact__valid-dates", function(selector, elem){
								for (var i = 0; i < elem.length; i++) {
									Samuraj.Common.getProp({
										object: elem[i],
										prop: 'parentNode.parentNode',
										success: function(parent){
											var parentId = parent.querySelector("a").getAttribute("href");
											if (parentId){
												for (var j = 0; j < data.products.length; j++) {
													if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
														if (this.originobject.innerHTML !== "(" + data.fromDateTextM2 + " - " + data.toDateTextM2 + " " + Samuraj.ChangeDates.settings.valid_to_text + ")") {
															this.originobject.innerHTML = "(" + data.fromDateTextM2 + " - " + data.toDateTextM2 + " " + Samuraj.ChangeDates.settings.valid_to_text + ")";
														}
													}
												}
											}
										}
									});
								}
							});
						}, 100);
					}
				});
			}
		},

		//Change on PLP pages on M2
		changePLP_OLD_M2: function(data) {
			if (data.lower) {
				var elems = document.querySelectorAll(".product-compact__valid-dates");

				for (var i = 0; i < elems.length; i++) {
					try {
						var parent = elems[i].parentNode;
						var parentId = parent.querySelector("a").getAttribute("href");
						for (var j = 0; j < data.products.length; j++) {
							if (parentId.toLowerCase().indexOf(data.products[j].toLowerCase()) > -1) {
								elems[i].innerHTML = "(" + data.fromDateTextM2 + " - " + data.toDateTextM2 + " " + Samuraj.ChangeDates.settings.valid_to_text + ")";
							}
						}
					} catch (e) {}
				}
			}
		},

		formatDate: function(data) {
			try {
				var fromDateSplit = data.fromDate.split(".");
				var toDateSplit = data.toDate.split(".");
				data.fromDateNumber = fromDateSplit[2] + "-" + fromDateSplit[1] + "-" + fromDateSplit[0];
				data.toDateNumber = toDateSplit[2] + "-" + toDateSplit[1] + "-" + toDateSplit[0];
				data.fromDateText = parseInt(fromDateSplit[0]) + " " + this.getMonthText(fromDateSplit[1]) + ", " + fromDateSplit[2];
				data.toDateText = parseInt(toDateSplit[0]) + " " + this.getMonthText(toDateSplit[1]) + ", " + toDateSplit[2];
				data.fromDateTextM2 = parseInt(fromDateSplit[0]) + ". " + this.getMonthText(fromDateSplit[1]) + ". " + fromDateSplit[2];
				data.toDateTextM2 = parseInt(toDateSplit[0]) + ". " + this.getMonthText(toDateSplit[1]) + ". " + toDateSplit[2];

			} catch (e) {}
			return data;
		},

		getMonthText: function(monthNumber) {
			var m = parseInt(monthNumber);
			switch (m) {
				case 1:
					return "jan";
				case 2:
					return "feb";
				case 3:
					return "mar";
				case 4:
					return "apr";
				case 5:
					return "mai";
				case 6:
					return "jun";
				case 7:
					return "jul";
				case 8:
					return "aug";
				case 9:
					return "sep";
				case 10:
					return "okt";
				case 11:
					return "nov";
				case 12:
					return "des";
				default:
					return "";
			}
		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "ChangeDates",
		objects: [
			{name: "ChangeDates", fn: ChangeDates}
		],
		dependencies: [],
		bootup: function(){
			//Initiate ChangeDates
			Samuraj.Source.load('ChangeDates', function(data) {
				//console.log(JSON.stringify(data.changedates));
				//override for testing
				/*
				data.changedates = [
					{
						products:[
							"80329791","S39216810","S49216701","S59216809", "80341175"
						],
						fromDate:"28.07.2017",
						toDate:"05.09.2017",
						lower:false,
						family:true
					}
				];
				*/
				
				Samuraj.ChangeDates.init(data.changedates || [] );
				Samuraj.Log.add({
					cat: "ChangeDates",
					sub: "Timing",
					text: "Loading done",
					timing: performance.now() - SamurajTiming				
				});
			});
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();