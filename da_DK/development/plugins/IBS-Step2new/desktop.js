(function (){

        // Check ready state
        function readyNow(fn1) {
            if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
                fn1();
            } else {
                document.addEventListener('DOMContentLoaded', fn1);
            }
        }
    let runNow = function () {
        if (document.URL.indexOf('https://ww8.ikea.com/clickandcollect/dk/collect/customerdata') > -1 || document.URL.indexOf('https://ww8.ikea.com/clickandcollect/dk/basketupdate/updatebasketaddresses/redirect') > - 1) {
            
            // remove login link. 
            const removeLogin = document.getElementsByClassName("loginContainer")[0];
            removeLogin.children[0].setAttribute("style", "display:none");


            /* START CLICK AND COLLECT CHANGES */

            const yourInfo = document.getElementById("addresses");
            if (yourInfo.children[0].innerText  === "Dine oplysninger") {
                yourInfo.classList.add("col-xs-5");
                yourInfo.setAttribute("style", "padding-left:12px");

                const btnLink = document.getElementsByClassName("col-sm-6")[0];
                const btnLinkText = "<a href='https://www.ikea.com/webapp/wcs/stores/servlet/OrderItemDisplay'>tilbage</a>"; 
                btnLink.innerHTML = btnLinkText;
    
            }

            /* END CLICK AND COLLECT CHANGES */

            // changhe back button to textlink...
            const btnLink = document.getElementsByClassName("col-sm-6")[0];
            const btnLinkText = "<a href='https://www.ikea.com/webapp/wcs/stores/servlet/OrderItemDisplay'>tilbage</a>"; 
            btnLink.innerHTML = btnLinkText;

            // move elment delivery information to the top of the from. 
            const HeadlineDelivery = document.getElementsByClassName("row")[5];
            HeadlineDelivery.append(addresses);
            HeadlineDelivery.setAttribute("style", "margin-left: -57px;");
            
            // text and drob down
            const TexAndDropDown = document.getElementsByClassName("row")[6];
            TexAndDropDown.append(addresses);
            TexAndDropDown.setAttribute("style", "margin-left: -42px");

            // Format the text below delivery date 
            const deliveryText2 = document.getElementsByClassName("col-xs-12")[2];
            const newText2  = "<b>Kantstenslevering 599.- </b><br> Du bærer selv dine varer ind.<br><br><b> Levering med fuld indbæring 799.- </b><br> Vi bærer dine varer helt ind i et rum, som du vælger.<br><hr><br>"; 
            deliveryText2.innerHTML = newText2; 

            // style formating hr delivery information.
            const hrOne = document.getElementsByTagName("hr")[0];
            hrOne.setAttribute("style", "margin-left:-42px; border-color:#cccccc");
            const hrsecond = document.getElementsByTagName("hr")[1];
            hrsecond.setAttribute("style", "margin-left:-18px; border-color:#cccccc");

            // hide survery
              const customerSurvery = document.getElementsByClassName("customerConsentSurvey")[0]; 
              customerSurvery.setAttribute("style", "display:none"); 

              // checked the radio button to hous
                const checkboxWhereDoYouLive = document.getElementsByName('basket[invoiceAddress][voQuestionnaireAnswerSet][QuestionnaireType]')[0]; 
                checkboxWhereDoYouLive.setAttribute("checked", "checked");
                checkboxWhereDoYouLive.setAttribute("value", "HOUSE");

                // show the hidden mandatory elements
                const expressData = document.getElementsByClassName('expressData row')[0];
                expressData.children[0].children[2].removeAttribute('style');

                // the dom is changin postions.. 
                // so some times form-group [28] is a text field and some time it is a checkbox.
                // the same gos for form-group [29]
                const mandatoryNumberOne = document.getElementsByClassName('form-group')[28];
                const mandatoryNumberTwo = document.getElementsByClassName('form-group')[29];

                if (mandatoryNumberOne.children[1].children[3].type === "text" ) {
                    mandatoryNumberOne.children[1].children[3].removeAttribute("style");
                    mandatoryNumberOne.children[1].children[3].removeAttribute("disabled");
                    mandatoryNumberOne.children[1].children[3].setAttribute("value", "000");

                    mandatoryNumberOne.children[1].children[0].removeAttribute("disabled");
                    mandatoryNumberOne.children[1].children[1].removeAttribute("disabled");
                }
                else if (mandatoryNumberOne.children[1].children[2].type === "checkbox" ) {
                    mandatoryNumberOne.children[1].children[2].removeAttribute("style");
                    mandatoryNumberOne.children[1].children[2].removeAttribute("disabled");
                    mandatoryNumberOne.children[1].children[2].setAttribute("checked", "checked");

                    mandatoryNumberOne.children[1].children[0].removeAttribute("disabled");
                    mandatoryNumberOne.children[1].children[1].removeAttribute("disabled");
                }

                if(mandatoryNumberTwo.children[1].children[3].type === "text") {
                    mandatoryNumberTwo.children[1].children[3].removeAttribute('style');
                    mandatoryNumberTwo.children[1].children[3].removeAttribute("disabled");
                    mandatoryNumberTwo.children[1].children[3].setAttribute("value", "000");

                    mandatoryNumberTwo.children[1].children[0].removeAttribute("disabled");
                    mandatoryNumberTwo.children[1].children[1].removeAttribute("disabled");
                }
                else if (mandatoryNumberTwo.children[1].children[2].type === "checkbox" ) {
                    mandatoryNumberTwo.children[1].children[2].removeAttribute('style');
                    mandatoryNumberTwo.children[1].children[2].removeAttribute("disabled");
                    mandatoryNumberTwo.children[1].children[2].setAttribute("checked", "checked");

                    mandatoryNumberTwo.children[1].children[0].removeAttribute("disabled");
                    mandatoryNumberTwo.children[1].children[1].removeAttribute("disabled");
                }

            // hide deliverly infomation. 
             const deliverlyInfomation = document.getElementById("questionnaireBlock");
             deliverlyInfomation.setAttribute("style", "display:none");

             const mandatoryField = document.getElementsByClassName("mandatory_hint")[0];
             mandatoryField.setAttribute("style", "display:none"); 
        }



    }
    readyNow(runNow);
})();