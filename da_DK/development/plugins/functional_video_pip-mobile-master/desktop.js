//////////////////////////////
/*  Functional Videos script for IRW mobile */
/*  Updated 20/03/2018   */
/*  yijun.ma@ikea.com  */
//////////////////////////////
//For use with Iplugins

"use strict";
//Check ready state
function ready(fn) {
    if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
// This is the function which will load our videos onto the page
function insertThumb() {
    // Loads play icon overlay for thumbnails
    var playIcon = "https://mmapi.ikea.com/player/ikea/img-btn/play_60px_2x.png";
    
    // Open video list
    var request = new XMLHttpRequest();
    request.open("GET", "https://mmapi.ikea.com/retail/motionmedia/v1/item", true);
    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            var videoList = JSON.parse(this.response);

            /////////////////////////
            // define variables (different for different platform)
            var productNum = "";
            var thumbContainer;
            var imageContainer;
            var prodRegexp = /\('([^']+)','([\d]+)',/g;
            // IRW
            if (document.body.querySelector('.ui-page-active #local_store')!==null){
                var matchprod = prodRegexp.exec(document.body.querySelector('.ui-page-active #local_store').getAttribute('onchange'));
                if (matchprod != null){
                    productNum = matchprod[2];
                    thumbContainer = document.body.querySelector('.ui-page-active .ikea-pip-thumbColumn');
                    imageContainer = document.body.querySelector('.ui-page-active .ikea-pip-fullImageWrapper');
                }
            }
            else if (!document.head.querySelector("[name=mpn]") == false){
                // NEW WEB
                productNum = document.head.querySelector("[name=mpn]").content;
            }
            else{
                // M2 ??
            }
            /////////////////////////

            if (productNum!=="" && thumbContainer !== null){
                // Extract video list from array
                videoList.some(function (index, i) {
                    // Match meta data to video list and prevent function rom running if not on a PIP
                    if (productNum !== "" && productNum.indexOf(index.item_number) > -1) {
                        // Set variable containing the videos
                        var sourceUrl = "https://mmapi.ikea.com/embed/?item_number=" + index.item_number + "&jsapi=1&is_mobile=true";
                        // Set variables to get the images
                        var imgSrc = "https://mmapi.ikea.com/retail/motionmedia/v1/item/" + index.item_number + "?dataset=images";                        
                        var imgRequest = new XMLHttpRequest();
                        imgRequest.open("GET", imgSrc, true);
                        imgRequest.onload = function () {
                            if (this.status >= 200 && this.status < 400) {
                                var imageSource = JSON.parse(this.response);
                                // selects the size for the thumbnail
                                var imgURL = imageSource.s2;
                                // Create a container for the thumbnail and import a play icon for the video
                                if (thumbContainer.firstChild) {
                                    var imgThumbnail =  '<div class="imageThumb" id="videoDiv_m" '+
                                    ' style="width:40px;height:40px;border: 1px solid #ccc;margin: 1px;background-size: 100% 100% !important;background-image:url(\''+imgURL +'?imPolicy=thumbnail\')">' +
                                    '<img src="' + playIcon + '" onclick="irwStatThumbImgClickedFromPIP();" id="videoThumb_m" style="width:25px;height:25px;margin:7px;border:none;">' +
                                    "</div>";

                                    thumbContainer.insertAdjacentHTML("beforeend",imgThumbnail);
                                }
                                imageContainer.insertAdjacentHTML("beforeend", '<iframe frameborder="0" width="560px" height="315px" style="width:100%;height:auto;display:none;" allowfullscreen="" src="' + sourceUrl + '"></iframe>');

                                // Add onclick event to image thumb
                                var thumbContainers = document.body.querySelectorAll('.ui-page-active .ikea-pip-thumbColumn');
                                for (var j = 0; j < thumbContainers.length; j++) {
                                    var imgThumbClick = thumbContainers[j].getElementsByTagName('img');
                                    for (var i = 0; i < imgThumbClick.length; i++) {
                                        imgThumbClick[i].addEventListener('click',function(){irwmobileshowvideo(false)});
                                    }    
                                }
                                thumbContainer.querySelector("#videoDiv_m").addEventListener('click',function(){irwmobileshowvideo(true)});

                            } else {
                                console.log("Something went wrong:", this.status);
                            }

                            imgRequest.onerror = function () {
                                console.log(imgRequest.status);
                            };
                        };
                        imgRequest.send();

                        return true;
                    }
                });
            }
        }
    };
    request.send();
}

function irwmobileshowvideo(istrue){
    var imageContainer = document.body.querySelector('.ui-page-active .ikea-pip-fullImageWrapper');
    if (imageContainer!== null){
        var imageContainer_img = imageContainer.getElementsByTagName('img');
        var imageContainer_iframe = imageContainer.getElementsByTagName('iframe');
        for (i = 0; i < imageContainer_img.length;i++ ) {
            imageContainer_img[i].style.display = (istrue)?"none":"block";
        }
        for (i = 0; i < imageContainer_iframe.length;i++ ) {
            imageContainer_iframe[i].style.display = (istrue)?"block":"none";
        } 
    }

}
if (location.href.indexOf('catalog/products')>-1){
    ready(insertThumb);
}



