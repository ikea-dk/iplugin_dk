# functional_video_pip-mobile
#### Developer: jelmer.timmer@ikea.com
#### Contributer: yijun.ma@ikea.com
#### Updated: 20/03/2018

Javascript for display product videos on mobile pip pages
> Current support IRW only

Example: https://m.ikea.com/au/en/catalog/products/spr/S09216755/ 

#### Notes
- Vanilla JavaScript
- Script only execute on pip pages
- No css file required