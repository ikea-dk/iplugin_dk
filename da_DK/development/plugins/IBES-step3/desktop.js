/* eslint-disable no-undef */
/* eslint-disable func-names */

(function expressDelivery(){
    // Check ready state
            function ready(fn) {
                if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
                    fn();
                } else {
                    document.addEventListener('DOMContentLoaded', fn);
                }
            }
    
        
let run = function () {
                if (document.URL.indexOf('/ww8.ikea.com/clickandcollect/dk/') > -1) {
    // Remove blue "Tilbage til indkøbskurv" button           
            const btnStep3 = document.getElementsByClassName('processButtons')[0];
            btnStep3.setAttribute('style', 'display: none');
        
    // change wording "Vælg levering"     
            const pickupHeadline = document.getElementsByClassName('headline pickUpDateHeadline')[1];
            const pickupText = 'Du har valgt levering den: ';
            pickupHeadline.innerHTML = pickupText;
    
    // Format the text below delivery date
            const deliveryTxt = document.getElementsByClassName('col-xs-12')[6];
            const newText = '<b>Kantstenslevering til 599.-</b><br>skal du selv bærer dine varer ind<br><br><b>Levering med fuld indbæring for 799.-</b><br>bærer vi dine varer helt ind i ét rum, som du selv vælger';
            deliveryTxt.innerHTML = newText;
            deliveryTxt.setAttribute("style", "display: block; padding-top: -25px");
    
    // Make sure col-xs-12 is empty on Click&Collect    
            const CandC = document.getElementsByClassName("col-xs-6")[0].children[0].children[3];
            CandC.setAttribute("style", "display: none");
        
    // Change wording for "Den valgte service"
            const yourSelectionTxt = document.getElementsByClassName('pickUpDateHeadline headline col-xs-12')[0];
            const newSelectionTxt = 'Hvis du har valgt:';
            yourSelectionTxt.innerHTML = newSelectionTxt;
            yourSelectionTxt.setAttribute('style', 'display: block; padding-top: 15px; font-size: 14px; margin-bottom: -7px');
    
    // Move IKEA BUSINESS card inputfield up
        let variabelNavn = setInterval(() => {
            const inputfield = document.getElementsByClassName('col-xs-7')[2];
            // check if the element is there
            if (inputfield != null){
                inputfield.setAttribute('style', 'display: block; margin-top: -120px; padding-left: 13px');
                clearInterval(variabelNavn);
            }
        }, 10); 
    
            const movebusinessCard = document.getElementsByClassName("col-xs-7")[0];
            movebusinessCard.setAttribute('style', 'display: block; margin-top: -120px; padding-left: 13px; z-index:1');


    // move payment chooser to the left
           // const paymentchooser = document.getElementById('paymentChooser');
           // paymentchooser.setAttribute('style', 'display: block; margin-left: 50px; margin-bottom: 25px');
                
    
    // Move payment window up
           const movePayment = document.getElementsByClassName('col-xs-7 paymentProviders aci_creditcard adapterContent')[0]; 
           movePayment.setAttribute('style', 'display: block; margin-top: -160px; margin-left: -49px; padding-left: 0px');
    
    // move C&C payment window
           const moveCCPayment = document.getElementById("paymentBoxAci"); 
           moveCCPayment.setAttribute('style', 'display: block; margin-top: 10%; margin-left: -49px; padding-left: 0px');
    
    // move headline 60px to the right 
                    let interval = setInterval(() => {
                        const newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0];
                        // check if the element is there
                        if (newHeadline != null){
                            newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 110px; margin-top: -45px; margin-bottom: 60px; z-index: 2');
                            clearInterval(interval);
                        }
                    }, 10);
            }
           
            let bcInputfield = setInterval(() => {
                const businesscardInput = document.getElementsByClassName('col-md-12')[0];
    // check if the element is there
                if (businesscardInput != null){
                    businesscardInput.setAttribute('style', 'display: none');
                    clearInterval(bcInputfield);
                }
            }, 10);
                function check() {
                    if (document.getElementById('payment-IkeaBusinessPayCardAdapter').checked === true) {
    // Ikea Business
                        const moveIkeaBusiness = document.getElementsByClassName('col-xs-7')[0]; 
                        moveIkeaBusiness.setAttribute('style', 'display: block; margin-top: -257px; margin-left: -2px; z-index:1');
            
                        const moveIkeaBusinesInput = document.getElementsByClassName('col-md-12 col-xs-11 ikeaBusinessPayCardMobileBlock')[0];
                        moveIkeaBusinesInput.setAttribute('style', 'margin-top: -188px; margin-left: -160px'); 
    
                        const businesscardInput = document.getElementsByClassName("col-md-12")[0];
                        businesscardInput.setAttribute("style", "display:show; padding-top: 115px");
    
                        const newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0];
                        newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 110px; margin-top: -45px; margin-bottom: 60px; z-index: 2');
                 } else if (document.getElementById('payment-aci_creditcard').checked === true) {
                        const newHeadline2 = document.getElementsByClassName('paymentHeadingBlockHead')[0];
                        newHeadline2.setAttribute('style', 'display: block; font-size: 18px; margin-left: 110px; margin-top: -45px; margin-bottom: 60px; z-index: 2');
    // Move payment card 
                        const movebusinessCard1 = document.getElementsByClassName("col-xs-7")[0];
                        movebusinessCard1.setAttribute('style', 'display: block; margin-top: -120px; padding-left: 13px; z-index:1');
                    
    // Move payment window up
                        const movePayment = document.getElementsByClassName('col-xs-7 paymentProviders aci_creditcard adapterContent')[0]; 
                        movePayment.setAttribute('style', 'display: block; margin-top: -160px; margin-left: -49px; padding-left: 0px');
                    }
                   }
    
    // Calling check radio button
                        document.getElementById('payment-IkeaBusinessPayCardAdapter').onclick = check;
                        document.getElementById('payment-aci_creditcard').onclick = check;
            };  
// Click & Collect

// Fjerner den blå knap, placerer betalingselementet og formaterer overskriften------------------------------------------
function clickandcollect() {
    if (document.URL.indexOf('/clickandcollect/dk/basketupdate/updatebasketaddresses/') > -1 && document.getElementsByClassName("col-xs-4 pickUpLocationOpeningHours")[0]) {
        const btnStep3 = document.getElementsByClassName('processButtons')[0];
        btnStep3.setAttribute('style', 'display: none');
        let interval = setInterval(() => {
            const newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0];
            if (newHeadline != null) {
                newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 90px; margin-bottom: 10px; z-index: 2');
                clearInterval(interval);
            }
        }, 10);
        const CandCPayment = document.getElementById("paymentBoxAci");
        CandCPayment.setAttribute('style', 'display: block; margin-left: -105px');
        // Placere IKEA BUSINESS radio buttons
        const CaCRadioButtons = document.getElementsByClassName("col-xs-7")[0];
        CaCRadioButtons.setAttribute('style', 'display: block; margin-top: 30px');
        if (document.getElementById('payment-IkeaBusinessPayCardAdapter').checked === true) {
            // Ikea Business
            const moveIkeaBusiness = document.getElementsByClassName('col-xs-7')[0];
            moveIkeaBusiness.setAttribute('style', 'display: block; margin-top: 30px; margin-left: -2px; z-index:1');

            const moveIkeaBusinesInput = document.getElementsByClassName('col-md-12 col-xs-11 ikeaBusinessPayCardMobileBlock')[0];
            moveIkeaBusinesInput.setAttribute('style', 'margin-top: 10px; margin-left: -160px');

            const businesscardInput = document.getElementsByClassName("col-md-12")[0];
            businesscardInput.setAttribute("style", "display:show; padding-top: 115px");

            const newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0];
            newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 60px; margin-bottom: 60px');
        } else if (document.getElementById('payment-aci_creditcard').checked === true) {
            // Move payment card 
            const movebusinessCard1 = document.getElementsByClassName("col-xs-7")[0];
            movebusinessCard1.setAttribute('style', 'display: block; margin-top: 30px; margin-left: -2px; z-index:1');
            // Move payment window up
            const movePayment = document.getElementsByClassName('col-xs-7 paymentProviders aci_creditcard adapterContent')[0];
            movePayment.setAttribute('style', 'display: block; margin-top: -160px; margin-left: -49px; padding-left: 0px');
        }
        // Calling check radio button
        document.getElementById('payment-IkeaBusinessPayCardAdapter').onclick = check;
        document.getElementById('payment-aci_creditcard').onclick = check;
    }
}
clickandcollect();
ready(run);
})();
