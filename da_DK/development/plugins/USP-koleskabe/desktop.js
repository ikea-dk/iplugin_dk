

(function () {
  /*var path = "css";
  var style = document.createElement('link');
  style.rel = 'stylesheet';
  style.type = 'text/css';
  style.href = 'https://www.ikea.com/ms/da_DK/XCLm55MBAS/USP-PLP-IRW.css';
  document.getElementsByTagName('head')[0].appendChild(style); */
  //console.log ("CSS loaded")

  if (document.URL.indexOf("/catalog/categories/departments/") > -1) {
    // Load the JSON file 

    var loadkoleskabe = function loadkoleskabe() {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
          var koleskabe = JSON.parse(this.responseText);
          //console.log(this.responseText)

          var allProducts = document.getElementsByClassName("threeColumn product");
          for (var i = 0; i < allProducts.length; i++) {

            var Product = allProducts[i];
            for (var x = 0; x < koleskabe.length; x++) {

              //get productID in div
              var link = Product.children[1].childNodes[1].pathname;
              var substrings = link.split("/");
              var productID = substrings[5];

              //get productID in JSON
              var productID_json = koleskabe[x].Article.toString();

              if (productID === productID_json) {
                console.log("Product matches from div with JSON");
                console.log("ID from DIV:" + productID);
                console.log("ID from JSON:" + productID_json);

                var container = document.createElement("mark");

                container.innerHTML = '\n                          <ul.a>\n                              <br>\n                              <li class = "usp">K\xF8lekapacitet liter<span class="text-yes"> ' + koleskabe[x].Kolekapacitet + ' </span></li> \n                          <li class = "usp">Frysekapacitet liter<span class ="text-yes">   ' + koleskabe[x].Frysekapacitet + ' </span></li> \n                          <li class = "usp">Lydniveau<span class ="text-yes">   ' + koleskabe[x].Lydniveau + '  </span></li> \n                          <li class = "usp"> ' + (koleskabe[x].Frost ? 'No frost funktion<span class = "icon-yes"' + koleskabe[x].Frost + '</span>' : '<div class = "text-no">No frost funktion</div><span class = "icon-no" ' + koleskabe[x].Frost) + '</span></li>\n                          <li class = "usp"> ' + (koleskabe[x].Feriefunktion ? 'Feriefunktion<span class = "icon-yes"' + koleskabe[x].Feriefunktion + '</span>' : '<div class = "text-no">Feriefunktion</div><span class = "icon-no" ' + koleskabe[x].Feriefunktion) + '</span></li>\n                          <li class = "usp"> ' + (koleskabe[x].Belysning ? 'LED belysning<span class = "icon-yes"' + koleskabe[x].Belysning + '</span>' : '<div class = "text-no">LED belysning</div><span class = "icon-no" ' + koleskabe[x].Belysning) + '</span></li>\n                          <li class = "usp"> ' + (koleskabe[x].Garanti ? 'Garanti<span class = "text-yes">' + koleskabe[x].Garanti + '</span>' : '<div class = "text-no">Garanti</div><span class = "icon-no" ' + koleskabe[x].Garanti) + '</span></li>\n                          </ul>';

                var product = document.body.querySelector('span.productDesp').parentNode;
                var sp2 = document.body.querySelector('span.productDesp');
                product.replaceChild(container, sp2);
              }
            }
          }
        }
      };
      //Get the JSON file
      xhttp.open('GET', 'https://m2.ikea.com/dk/da/data-sources/ae0337d0b5d811e8837eb18effe1b65c.json', true);
      xhttp.send();
    };

    loadkoleskabe();
  };
})();