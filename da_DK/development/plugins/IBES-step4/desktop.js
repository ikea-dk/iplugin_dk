const pickupHeadline = document.getElementsByClassName('headline pickUpDateHeadline')[1];
const pickupText = 'Du har valgt levering den: ';
pickupHeadline.innerHTML = pickupText;

// Format the text below delivery date
const deliveryTxt = document.getElementsByClassName('col-xs-12')[6];
const newText = '<b>Kantstenslevering til 599.-</b><br>skal du selv bærer dine varer ind<br><br><b>Levering med fuld indbæring for 799.-</b><br>bærer vi dine varer helt ind i ét rum, som du selv vælger';
deliveryTxt.innerHTML = newText;
deliveryTxt.setAttribute("style", "display: block; padding-top: -25px")

// Change wording for "Den valgte service"
const yourSelectionTxt = document.getElementsByClassName('pickUpDateHeadline headline col-xs-12')[0];
const newSelectionTxt = 'Hvis du har valgt:';
yourSelectionTxt.innerHTML = newSelectionTxt;
yourSelectionTxt.setAttribute('style', 'display: block; padding-top: 15px; font-size: 14px; margin-bottom: -7px');