/* New Header */
    'use strict';

    // Generate new header for all non checkout pages
    if (!isCheckout()){
        
        document.addEventListener("DOMContentLoaded", function() {
            var mainPadding = document.getElementById('mainPadding');
            if (mainPadding) {
                
                /*if(ipluginsCommon.getCookieValue('client_showlogoff_link_14')){
                    mainPadding.classList.add('loggedIn');
                }*/
                
                // Create new header.
                var newHeader = document.createElement('div');
                newHeader.id = 'fip-mh--main-header';
                newHeader.className = 'fip-mh--main-header';
                mainPadding.insertBefore(newHeader, mainPadding.childNodes[6]);

                // Create logo.
                var logoContainer = document.createElement('div');
                logoContainer.className = 'fip-mh--logo-container';
                logoContainer.innerHTML = createLogo();
                newHeader.appendChild(logoContainer);

                // Create search form container and move the original one inside.
                var searchFormContainer = document.createElement('div');
                searchFormContainer.className = 'searchForm';
                searchFormContainer.appendChild(searchForm);
                newHeader.appendChild(searchFormContainer);
                //Set placeholder on input search
                searchForm.firstElementChild.firstElementChild.setAttribute("placeholder","Søg...");

                // Create right section with links and icons.
                var rightSectionContainer = document.createElement('div');
                rightSectionContainer.className = 'fip-mh--right-section';
                rightSectionContainer.innerHTML = createRightSection();
                newHeader.appendChild(rightSectionContainer);
                styleSearchFormButton(searchForm);

                // Uppdate number of cart items inside yellow circle in the ehader each time a user click on the buy button.
                setNrOfCartItems('fip-mh--noOfCartItems');

                // To keep the height and avoid that our new header jumps uppwords,
                // first set topMenu to "vissible: hidden" in css and then remove it here.
                // In TeamSite Preview, topMenu id is not present so we need to hide the topMenu with it's class name.
                var topMenu;
                var topMenuById = document.getElementById('topMenu');
                var topMenuByClass = document.getElementsByClassName('topMenu');

                if (topMenuById) topMenu = topMenuById;
                else if (topMenuByClass) {
                    if (topMenuByClass[0]) topMenu = topMenuByClass[0];
                }

                if (topMenu) {

                   /*
                    Change default search forms class name.
                    How it works?
                    1. searchForm moves from the topMenu to the newHeader.
                    2. topMenu hides.
                    3. On scroll down, when MM-menu becomes sticky, searchForm moves to the MM-menu.
                    4. On scroll to the top, when MM-menu becomes non-sticky, searchForm moves back to the newHeader.
                       MM-menu script uses searchForms container with class name "searchForm" to return the searchForm to it's place.
                       And because this container exist in both newHeader and topMenu, we need to replace containers class name in the topMenu to "defaultSearchForm".
                    */
                    var oldSearchForm = topMenu.getElementsByClassName('searchForm');
                    if (oldSearchForm.length == 1) {
                        oldSearchForm[0].className = 'defaultSearchForm';
                    }

                    topMenu.style.display = 'none';
                }
            }
        });
    }

    /**
     * Create logo.
     *
     * @return string
     */
    function createLogo() {
        var output =
            '<a href="https://www.ikea.com/dk/da/" class="logoLink">'
                + '<img src="/ms/img/header/ikea-logo.svg" alt="IKEA Danmark" class="logo" width="100" height="38" />'
            + '</a>'
            + '<div class="fip-mh--tagline"></div>';
        return output;
    }

    /**
     * Style search box.
     *
     * @param object searchForm
     * @return void
     */
    function styleSearchFormButton(searchForm) {
        var formBtn = document.getElementsByClassName('formBtn', searchForm);
        if (formBtn && formBtn[0]) {
            formBtn[0].parentNode.removeChild(formBtn[0]);

            var newFormBtn  = document.createElement('div');
            newFormBtn.className = 'fip-mh--search-form-button-container';
            newFormBtn.innerHTML = '<a href="javascript:void(0);"><input id="lnkSearchBtnHeader" class="fip-mh--search-form-button" type="submit" value=""/></a>';
            searchForm.appendChild(newFormBtn);
        }
        // remove inline style from searh input
        var removeSearchStyle = document.getElementById('search');
        removeSearchStyle.removeAttribute('style');
    }
  
    /**
     * Create links.
     *
     * @return string
     */
    function createRightSection() {

        // Set links if user is not logged in.
        var myProfileLink = 'https://secure.ikea.com/webapp/wcs/stores/servlet/LogonForm?storeId=14&langId=-12&catalogId=11001';
        var logoutLink = '';
        var myProfileText = 'Log ind';

        // Change profile and login link if user is logged in.
        if (haslogoffURL('14')) {
            myProfileLink = 'https://secure.ikea.com/webapp/wcs/stores/servlet/MyProfile?langId=-12&storeId=14&ddkey=https%3AUpdateUser';
            myProfileText = ''
            logoutLink = '<a class="arrowLink" href="/webapp/wcs/stores/servlet/Logoff?langId=-12&amp;storeId=14&forgetMe=true&amp;flow=LogoutFlow">Log ud</a>';
        }

        var output =
            '<ul class="fip-mh--links-group">'
                + '<li>'
                    + '<a href="/dk/da/campaigns/varehuse.html?itm_campaign=header&itm_element=textlink&itm_content=findvarehus" class="arrowLink fip-mh--blacklink">Find varehus</a>'
                + '</li>'
                + '<li>'
                    + '<a href="/ms/da_DK/kundeservice/" class="arrowLink fip-mh--blacklink">Kundeservice</a>'
                + '</li>'
            + '</ul>'

            + '<ul class="fip-mh--links-group">'
                + '<li>'
                    + '<a href="/ms/da_DK/ikea_family/" id="link_ikea_FandB">IKEA FAMILY</a>'
                + '</li>'
                + '<li>'
                    + '<a href="/dk/da/catalog/categories/business/ikea_business" id="link_ikea_FandB">IKEA BUSINESS</a>'
                + '</li>'
            + '</ul>'

            + '<ul class="fip-mh--links-group">'
            + '<li>'
                + '<a href="' + myProfileLink + '" class="arrowLink fip-mh--blacklink" id="link_header_update_user" rel="nofollow">'+ myProfileText +'</a>'
                + logoutLink
            + '</li>'
            + '<li>'
                + '<a href="https://secure.ikea.com/webapp/wcs/stores/servlet/MyProfile?langId=-12&storeId=14&ddkey=https%3AUpdateUser" class="arrowLink fip-mh--blacklink">Min Profil</a>'
            + '</li>'
            + '</ul>'

            +'<div class="fip-mh--icons">'
               +'<div class="fip-mh--shopping-list">'
                +'<a href="/webapp/wcs/stores/servlet/InterestItemDisplay?storeId=14&amp;langId=-12" class="arrowLink" id="link_header_shopping_list" title="Huskeliste" rel="nofollow"></a>'
                +'</div>'
                + '<div class="fip-mh--basket">'
                    +'<a href="/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=14&langId=-12&catalogId=11001&orderId=.&priceexclvat=&newLinks=true" class="arrowLink" id="link_header_shopping_carts" title="Indkøbskurv" rel="nofollow"></a>'
                    +'</div>'
                        +'<a href="/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=14&langId=-12&catalogId=11001&orderId=.&priceexclvat=&newLinks=true" class="arrowLink arrowLinkInactive" title="Indkøbskurv" rel="nofollow">'
                        + '<span id="fip-mh--noOfCartItems">0</span>'
                        +'</a>'
            + '</div>';
            return output;
    }

    
    /**
     * Set correct number of cart items in the headers cart icon.
     * This process executes when user clicks on some buy button.
     *
     * @param string elemId
     * @return void
     */
    function setNrOfCartItems(elemId) {
        
        updateInElement(elemId);

        function getNrOfCartItemsFormIkea() {
            var userInfo = getUserInfo('14', 'normal');

            if (userInfo != null) {
                var nrOfCartItems = userInfo.numberShopCartItems.escapeHTML();
                nrOfCartItems = nrOfCartItems.replace(/\D/g, '');
                nrOfCartItems = parseInt(nrOfCartItems);

                if (nrOfCartItems >= 0) return nrOfCartItems;
                else return 0;
            } else {
                return 0;
            }
        }

        function updateInElement(elemId) {
            var elem = document.getElementById(elemId);
            if (elem) {
                elem.innerHTML = getNrOfCartItemsFormIkea();
            }
        }

        // Run each time a user clicks on the body.
        var bodyElement = document.getElementsByTagName('body')[0];
        if (bodyElement) {
            bodyElement.addEventListener('click', function(e) {
                var buyButtonClassNames = ['buttonLeft', 'buttonCaption', 'buttonRight', 'blueBtn', 'ctools-buy-button'];

                // Get clicked elements and it's parents element class name.
                var clickedElementsClassName = e.target.className;
                var clickedElementsParentClassName = e.target.parentNode.className;

                // Loop through all possible button classes.
                for (var i = 0; i < buyButtonClassNames.length; i++) {

                    // Check if clicked element is a button,
                    // and if yes, update the number of cart items in the headers cart icon.
                    if ((clickedElementsClassName.indexOf(buyButtonClassNames[i]) > -1) || (clickedElementsParentClassName.indexOf(buyButtonClassNames[i]) > -1)) {

                        // Delay is added to be able to get the right nr of cart items from the IKEAs system, otherwise the number is not correct. (Maybe the system has some delay???)
                        setTimeout(function() {
                            updateInElement(elemId);
                        }, 800);
                        break;
                    }
                }
            }, false);
        }
        
    }

    /**
     * Check if the page is a part of the checkout process.
     *
     * @return string
     */
    function isCheckout() {
        var checkoutUrls = [
            'webapp/wcs/stores/servlet/IrwCheckoutWelcomeView',
            'webapp/wcs/stores/servlet/IrwCheckoutAddressView',
            'webapp/wcs/stores/servlet/IrwDeliveryOptionsView',
            'webapp/wcs/stores/servlet/IrwProceedFromCheckoutAddressView'
        ];

        var nrOfUrls = checkoutUrls.length;
        for (var i = 0; i < nrOfUrls; i++) {
            if (document.URL.indexOf(checkoutUrls[i]) > -1) {
                return true;
                break;
            }
        }
        return false;
    }
;
