/*

  __                  ___
 (_  o ._ _  ._  |  _  |  _. |_   _
 __) | | | | |_) | (/_ | (_| |_) _>
			 |

*/

(function(){

	var SimpleTabs = {
		store: [],
		init: function(callback) {
			var c = Samuraj.Common;
			var elems = document.querySelectorAll(".simple-tabs-container");
			for (var n = 0; n < elems.length; n++) {
				if (!elems[n].loaded) {
					elems[n].loaded = true;
					elems[n].id = "simple-tabs-container_" + n;

					var tabs = elems[n].querySelectorAll("ul.simple-tabs");
					for (var i = 0; i < tabs.length; i++) {
						this.store[i] = new this.obj(tabs[i]);
					}
					var minheight = 333;
					if (c.checkMobile()) {
						minheight = 422;
					}
					var tabpage = elems[n].querySelectorAll("div.tab-page");
					for (var i = 0; i < tabpage.length; i++) {
						tabpage[i].style.setProperty("min-height", minheight + "px");
					}
				}
			}
		},

		obj: function(elem) {
			//get tab objects and store as pane + tab
			var activeTabObject;

			var TabObject = function() {
				var self = this;
				this.tab;
				//element
				this.pane;
				//element
				this.setClick = function() {
					self.tab.addEventListener('click', self.showThisTab)
				}

				this.showThisTab = function() {
					if (self !== activeTabObject && self.parentNode == activeTabObject.parentNode) {
						var c = Samuraj.Common;
						//change the tab page and update the active tab
						c.removeClass(activeTabObject.pane, 'active-page');
						c.removeClass(activeTabObject.tab, 'active');
						c.addClass(self.pane, 'active-page');
						c.addClass(self.tab, 'active');
						activeTabObject = self;
						Samuraj.ProductList.updateAllCarusels();
						if (Samuraj.DisplayTimer){
							if (Samuraj.Common.checkMobile()){
								if (Samuraj.DisplayTimer.productinjectagain){
									Samuraj.Targeting.reload = true;
									Samuraj.DisplayTimer.productinjectagain();
								}
							} else {
								if (Samuraj.DisplayTimer.productinjectagainirw){
									Samuraj.Targeting.reload = true;
									Samuraj.DisplayTimer.productinjectagainirw();
								}
							}
						}
						//if (activeTabObject.pane.getAttribute('data-type') == 'carusel') Samuraj.ProductList.setProductHeightCarusel(activeTabObject.pane);
						//if (activeTabObject.pane.getAttribute('data-type') == 'list') Samuraj.ProductList.setProductHeightList(activeTabObject.pane);
					}
				}

			};

			var ul = elem;
			var i;
			var items = ul.getElementsByTagName("li");
			for (i = 0; i < items.length; ++i) {
				var tab = new TabObject();
				tab.tab = items[i];
				var classString = items[i].className;
				var className = classString.split(' ')[0];
				tab.pane = ul.parentNode.querySelector("#" + className);
				tab.setClick();
				if (classString.indexOf('active') > -1) {
					activeTabObject = tab;
				}
			}
		}
	}
	
	//*******************************************************************

	var transfer = {
		name: "SimpleTabs",
		objects: [
			{name: "SimpleTabs", fn: SimpleTabs}
		],
		dependencies: [],
		bootup: function(){

			Samuraj.Log.add({
				cat: "SimpleTabs",
				sub: "Timing",
				text: "Loading done",
				timing: performance.now() - SamurajTiming				
			});
		}
	}
	
	if (window.Samuraj){window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer); if (Samuraj.Queue) Samuraj.Queue.dump("SamurajQueue");} 
	else { window.SamurajQueue = window.SamurajQueue || []; window.SamurajQueue.push(transfer);}

})();