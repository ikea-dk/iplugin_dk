(function () {
    // Check ready state
    function readyNow(fn1) {
        if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
            fn1();
        } else {
            document.addEventListener('DOMContentLoaded', fn1);
        }
    }
    var runNow = function runNow() {
        if (document.URL.indexOf('https://secure.ikea.com/webapp/wcs/stores/servlet') > -1 && document.getElementsByClassName('buttonCaption')[1].children[0].click) {
            const insertInfoSpanToFrom = document.getElementById("billingBox");
            
            const country = document.getElementById('billingBox').children[4]; 
            country.removeAttribute('type'); // do not hide 
            country.setAttribute('value', 'Danmark');
            country.setAttribute('id', 'countryDenmark');
            country.setAttribute('name', 'basket[invoiceAddress][country]'); 
            country.disabled = true; 

            // tooltip div
            let tooltip_div = document.createElement("div");
            tooltip_div.setAttribute('id','myTooltipDiv');
            tooltip_div.setAttribute('class','tooltip');
            insertInfoSpanToFrom.appendChild(tooltip_div);
            
            // Title on country
            let para = document.createElement("p");
            let p = document.createTextNode("Land");
            para.appendChild(p);
            para.setAttribute('id','myTitle');
            para.innerHTML = "<label for='invoiceAddress-country' class='control-label'>Land</label> <a href='#'>(?)</a>";
            para.onclick = function openCountry() {
                document.getElementById('myModalCountry').style.display = "block";
                document.getElementById('lightboxCountry').style.display = "block";
            }
            insertInfoSpanToFrom.appendChild(para);

            // modal
            let modalLightbox = document.createElement('div');
            modalLightbox.setAttribute('id', 'lightboxCountry');
            modalLightbox.setAttribute('class','lightbox-country');
            modalLightbox.onclick = function closeCountry() {
                document.getElementById('myModalCountry').style.display = "none";
                document.getElementById('lightboxCountry').style.display = "none";
            }

            insertInfoSpanToFrom.appendChild(modalLightbox);

            let modalPopUp = document.createElement('div');
            modalPopUp.setAttribute('id', 'myModalCountry');
            modalPopUp.setAttribute('class','modalCountry');
            modalPopUp.innerHTML = `<div class='countryPopup' >
                                        <a id='lightbox-close-btn' href='#'>x</a>
                                        <h3>Land</h3>
                                        <p>Vi leverer til hele Danmark.<br>
                                        Hvis du vil have leveret til udlandet, kan du læse mere                                                 
                                        <a href='https://www.ikea.com/ms/da_DK/kundeservice/IKEA-online.html#levering'>her.</a></p>
                                    </div>`;
            modalPopUp.onclick = function closeCountry() {
                document.getElementById('myModalCountry').style.display = "none";
                document.getElementById('lightboxCountry').style.display = "none";
            }

            insertInfoSpanToFrom.appendChild(modalPopUp);

            // modal inster div in order
            modalLightbox.insertBefore(modalPopUp, modalLightbox.childNodes[0]);

            // inster child in order
            tooltip_div.insertBefore(para, tooltip_div.childNodes[0]);
            tooltip_div.insertBefore(country, tooltip_div.childNodes[1]);

            // place before e-mail field
            const mailElement = document.getElementById('email1_field');
            mailElement.before(myTooltipDiv);
        }
    };
    readyNow(runNow);
})();


