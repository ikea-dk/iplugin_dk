"use strict";

(function () {
    ///Verify the user is at the correct page
    if (!document.URL.indexOf("IrwCheckoutWelcomeView") && !document.URL.indexOf("IrwCheckoutAddressView")) 
    return;

    /// THE FOLLOWING FUNCTION ARE GENERAL FUNCTIONS AND SHOULD BE MOVED TO OTHER FILES CONTAINING GENERAL FUNCTIONS IF THIS EXIST.
    //window.Extension.
    window.PostNordCommon = {
        WaitForElement: function WaitForElement(elementId, callBack) {
            window.setTimeout(function () {
                if (document.getElementById(elementId)) {
                    callBack(elementId);
                } else {
                    PostNordCommon.WaitForElement(elementId, callBack);
                }
            }, 10);
        },

        /// Replace all instances of a substring from the string.
        //Function to create elements with type classlist id and innerHTML
        CreateElement: function CreateElement(type, classList, id, innnerHTML, child) {
            var element = document.createElement(type);
            element.innerHTML = innnerHTML;
            element.id = id;
            if (classList != "") classList.split(" ").forEach(function (className) {
                element.classList.add(className);
            });
            if (child != null) element.appendChild(child);
            return element;
        },
        RemoveFromArray: function RemoveFromArray(array, value) {
            for (var i = array.length - 1; i >= 0; i--) {
                if (array[i] === value) {
                    array.splice(i, 1);
                }
            }
        },
        ReplaceAll: function ReplaceAll(string, cut, paste) {
            while (string.indexOf(cut)!== -1) {
                string = string.replace(cut, paste);
            }
            return string;
        },
        ReplaceAllInInnerHtml: function ReplaceAllInInnerHtml(obj, cut, paste) {
            obj.innerHTML = PostNordCommon.ReplaceAll(obj.innerHTML, cut, paste);
        },
        CreateButton: function CreateButton(value, functionToExecute) {
            var button = PostNordCommon.CreateElement('input', '', '', 'postNordZipCodeInput', null);
            button.type = "button";
            button.onclick = functionToExecute;
            button.value = value;
            var postNordButtonCaption = PostNordCommon.CreateElement('div', 'buttonCaption', '', '', button);
            return PostNordCommon.CreateElement('div', 'buttonContainer', '', '', postNordButtonCaption);
        }
    };

    ///Verify if DeliveryAddress form is visible or not.
    var postNordUrl = "https://t9s52o31a6.execute-api.eu-central-1.amazonaws.com/Prod/";
    var postNordFirstLoadOfMarkers = true;
    var postNordServicePoints = [];
    var postNordMap;
    var postNordGeoCoder;
    var postNordMarkers = [];
    var postNordPostalCodeByUser = false;
    var postNordServicePointSelected;
    var postNordInfoWindow;
    var postNordZipCode;
    var postNordGoogleMapsZoomEventListner;
    var postNordCoolDown;
    var postNordCoolTime = 500;
    var postNordGoogleIsReady = false;
    var postNordCustomerType;
    var postNordOpeningHoursContainerCopy;
    /// Service Level. Function to be called from the Controller Level. This Level contains buisenss logic.
    /// Function to get and insert servicepoints into the list where the user will choice from.
    //window.Extension.
    window.PostNordCallBack = {
        /// The CallBackFunction when servicepoints shell remain on the map.
        CallBack: function CallBack(data) {
            return PostNord.ReadData(data, false);
        },
        /// The CallBackFunction when servicepoints must be removed from the map.
        CallBackClear: function CallBackClear(data) {
            return PostNord.ReadData(data, true);
        }
    };

    window.PostNord = {
        /// ControllerLevel
        /// All functions in here are functions called from user interaction or callback.
        CallServicePointsByPostalCode: function CallServicePointsByPostalCode(postalCode, typedByUser) {
            return PostNord.GetServicePointsByPostalCode(postalCode, typedByUser);
        },
        CallServicePointsByCoordinates: function CallServicePointsByCoordinates() {
            return PostNord.GetServicePointsByCoordinates();
        },

        /// ServiceLevel
        /// All functions in here are functions called from within the javascript
        ///Function fetching serviepoints by postalcode.
        GetServicePointsByPostalCode: function GetServicePointsByPostalCode(postalCode, typedByUser) {
            postNordPostalCodeByUser = typedByUser;
            if (postalCode > 9999 || postalCode < 1000) return;
            postNordZipCode = postalCode;
            PostNord.GetServicePoints(postNordUrl + "getbypostalcode?postalCode=" + postalCode + "&callback=PostNordCallBack.CallBackClear");
        },
        ///Function fetching serviepoints by coordinates.
        GetServicePointsByCoordinates: function GetServicePointsByCoordinates() {
            postNordCoolDown = Date.now();
            setTimeout(function () {
                if (Date.now() < postNordCoolDown + postNordCoolTime) return;

                postNordGeoCoder.geocode({ 'latLng': postNordMap.getCenter() }, function (results, status) {
                    if (status == 'OK') {
                        var address = results[0].address_components;

                        if (address.some(function (a) {
                            return a.types[0] == "postal_code";
                        })) PostNord.GetServicePointsByPostalCode(address.find(function (a) {
                            return a.types[0] == "postal_code";
                        }).long_name, false);

                        PostNord.GetServicePoints(postNordUrl + "getbyaddress?city=" + address.find(function (a) {
                            return a.types[0] == "locality";
                        }).long_name + "&callback=PostNordCallBack.CallBack" + (address.some(function (a) {
                            return a.types[0] == "route";
                        }) ? "&streetName=" + address.find(function (a) {
                            return a.types[0] == "route";
                        }).long_name : "") + (address.some(function (a) {
                            return a.types[0] == "street_number";
                        }) ? "&streetNumber=" + address.find(function (a) {
                            return a.types[0] == "street_number";
                        }).long_name : "") + (address.some(function (a) {
                            return a.types[0] == "postal_code";
                        }) ? "&postalCode=" + address.find(function (a) {
                            return a.types[0] == "postal_code";
                        }).long_name : ""));
                    }
                });
            }, postNordCoolTime);
        },
        ///Function calling AWS 
        GetServicePoints: function GetServicePoints(url) {
            if (postNordGoogleMapsZoomEventListner != null) google.maps.event.removeListener(postNordGoogleMapsZoomEventListner);

            if (document.getElementById("PostNordAPIResponse")) document.getElementById("PostNordAPIResponse").parentElement.removeChild(document.getElementById("PostNordAPIResponse"));

            var script = PostNordCommon.CreateElement("script", "", "PostNordAPIResponse", "", null);
            script.src = url;
            document.body.appendChild(script);
        },
        ///Loading the Google Maps
        InitMap: function InitMap() {
            try {
                postNordGeoCoder = new google.maps.Geocoder();
                postNordInfoWindow = new google.maps.InfoWindow();
                postNordMap = new google.maps.Map(document.getElementById('map'), {
                    center: { lat: 55.6592223, lng: 12.2864592 },
                    zoom: 15,
                    clickableIcons: false,
                    gestureHandling: 'greedy'
                });
                google.maps.event.addListener(postNordMap, 'dragend', function () {
                    PostNord.GetServicePointsByCoordinates();
                });
                google.maps.event.addListener(postNordInfoWindow, 'closeclick', function () {
                    PostNord.UnChoiceServicePoint();
                });
                postNordGoogleIsReady = true;
            } catch (err) {
                setTimeout(function () {
                    PostNord.InitMap();
                }, 10);
            }
        },
        ///Add ServicePointTo The Map
        AddPointToMap: function AddPointToMap(servicePoint) {
            if (postNordGoogleIsReady) {
                var marker = new google.maps.Marker({
                    map: postNordMap,
                    position: { lat: servicePoint.coordinate.northing, lng: servicePoint.coordinate.easting },
                    title: servicePoint.name
                });
                marker.set("id", "GoogleMapsMarker" + servicePoint.servicePointId);
                marker.set("postalCode", servicePoint.visitingAddress.postalCode);
                google.maps.event.addListener(marker, 'click', function () {
                    PostNord.LoadOpeningHours(servicePoint);
                    PostNord.ChoiceServicePoint(servicePoint.servicePointId, true);
                });
                postNordMarkers.push(marker);
            } else {
                setTimeout(function () {
                    PostNord.AddPointToMap(servicePoint);
                }, 10);
            }
        },
        ///Zoom map to fit the markers
        ZoomMap: function ZoomMap(markerArray) {
            var bounds = new google.maps.LatLngBounds();
            markerArray.forEach(function (marker) {
                if (postNordZipCode == marker.postalCode) {
                    bounds.extend(marker.getPosition());
                }
            });
            if (bounds.ga.j == 180 || bounds.ga.l == 180) bounds.extend(markerArray[0].getPosition());
            postNordMap.fitBounds(bounds);
            if (bounds.ga.j == bounds.ga.l || bounds.ma.l == bounds.ma.j) postNordMap.setZoom(15);
        },
        ///Choice servicepoint by adding opening hours to the map and highlight on the list.
        ChoiceServicePoint: function ChoiceServicePoint(id, scroll) {
            if (postNordServicePointSelected == null || postNordServicePointSelected.servicePointId != id) PostNord.LoadOpeningHours(PostNord.GetServicePoint(id));
            postNordServicePointSelected = PostNord.GetServicePoint(id);
            PostNord.ChangeClassOnSelect(id, scroll);
        },
        ///On closing infowindow we unhighlight the servicepoint
        UnChoiceServicePoint: function UnChoiceServicePoint() {
            postNordServicePointSelected = null;
            PostNord.ChangeClassOnSelect(null);
        },
        ///Copy billingaddress and servicepoint information into deliveryaddress
        CopyBillingAddressToDeliveryAddress: function CopyBillingAddressToDeliveryAddress() {
            if (document.getElementById('copyTermsAndConditions_id').checked != true) document.getElementById("copyAcceptPersonData").getElementsByClassName("errorMsg")[0].style.display = "";else document.getElementById("copyAcceptPersonData").getElementsByClassName("errorMsg")[0].style.display = "none";

            if (document.getElementById('SelectPickUpPointError')) {
                var element = document.getElementById("SelectPickUpPointError");
                element.parentNode.removeChild(element);
            }
            if (postNordServicePointSelected != null) {
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address1").value = postNordServicePointSelected.name.substring(0, 30);;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address2").value = postNordServicePointSelected.visitingAddress.streetName + " " + postNordServicePointSelected.visitingAddress.streetNumber;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value = postNordServicePointSelected.visitingAddress.postalCode;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_city").value = postNordServicePointSelected.visitingAddress.city;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_firstName").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_firstName").value;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_lastName").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_lastName").value;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_phone1").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_phone1").value;
                document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_phone2").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_phone2").value;
                if (document.getElementById("signup_checkout_business_organizationName")) document.getElementById("signup_checkout_business_ship_organizationName").value = document.getElementById("signup_checkout_business_organizationName").value;

                return;
            }
            document.getElementById("PostNordElement").appendChild(PostNordCommon.CreateElement("div", "formError formErrorGoogleMaps", "SelectPickUpPointError", "Du mangler at vælge et posthus", null));
        },
        ///Changing the classes of selected servicepoint and unselected servicepoint
        ChangeClassOnSelect: function ChangeClassOnSelect(id, scroll) {
            Array.from(document.getElementsByClassName("PostNordRadio")).forEach(function (item) {
                return item.checked = false;
            });
            Array.from(document.getElementsByClassName("PostNord-ServicePoint")).forEach(function (item) {
                return item.classList.remove("PostNord-SelectedServicePoint");
            });
            if (id != null) {
                document.getElementById("PostNordServicePointRadio-" + id).checked = true;
                document.getElementById("PostNordServicePoint-" + id).classList.add("PostNord-SelectedServicePoint");
                if (scroll) document.getElementById('postNordServicePointListElement1').scrollTop = document.getElementById("PostNordServicePoint-" + id).offsetTop - document.getElementById('postNordServicePointListElement1').offsetTop;
            }
        },
        ///Converting the day from PostNord to readable text.
        ConvertDayInitToReadable: function ConvertDayInitToReadable(init, short) {
            switch (init) {
                case "MO":
                    return short ? "Man" : "Mandag";
                case "TU":
                    return short ? "Tir" : "Tirsdag";
                case "WE":
                    return short ? "Ons" : "Onsdag";
                case "TH":
                    return short ? "Tor" : "Torsdag";
                case "FR":
                    return short ? "Fre" : "Fredag";
                case "SA":
                    return short ? "Lør" : "Lørdag";
                case "SU":
                    return short ? "Søn" : "Søndag";
            }
        },
        /// Insert OpeningHours
        InsertOpeningHours: function InsertOpeningHours(startDay, endDay, startTime, endTime) {
            postNordOpeningHoursContainerCopy.appendChild(postNordOpeningHoursScope.cloneNode(true));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[StartDay]", startDay);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[EndDay]", endDay);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[StartTime]", startTime);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursContainerCopy, "[EndTime]", endTime);
        },
        /// Insert servicepoints for the customer to choice.
        InsertServicePoint: function InsertServicePoint(servicePoint) {
            postNordOpeningHoursContainerCopy = postNordOpeningHoursContainer.cloneNode(true)
            var startDay;
            var endDay;
            var startTime;
            var endTime;
            servicePoint["openingHours"].forEach(function (openingDay) {
                var init = openingDay.day;
                if (startTime != PostNord.FormatClock(openingDay.from1) || endTime != PostNord.FormatClock(openingDay.to1)) {
                    if (startDay != undefined) {
                        PostNord.InsertOpeningHours(startDay, endDay, startTime, endTime);
                    }
                    startDay = PostNord.ConvertDayInitToReadable(init, true);
                    startTime = PostNord.FormatClock(openingDay.from1);
                    endTime = PostNord.FormatClock(openingDay.to1);
                };
                endDay = PostNord.ConvertDayInitToReadable(init, true);
            });
            PostNord.InsertOpeningHours(startDay, endDay, startTime, endTime);
            var postNordServicePointCopy = postNordServicePoint.cloneNode(true);;
            postNordServicePointCopy.setAttribute("onclick", "PostNord.ChoiceServicePoint(" + servicePoint.servicePointId + ",false)");
            postNordServicePointCopy.appendChild(postNordOpeningHoursContainerCopy);
            postNordServicePointListElement.appendChild(postNordServicePointCopy);

            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[servicePointId]", servicePoint.servicePointId);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[name]", servicePoint.name);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[streetName]", servicePoint.visitingAddress.streetName);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[streetNumber]", servicePoint.visitingAddress.streetNumber);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[postalCode]", servicePoint.visitingAddress.postalCode);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[city]", servicePoint.visitingAddress.city);
            PostNordCommon.ReplaceAllInInnerHtml(postNordServicePointListElement, "[countryCode]", servicePoint.visitingAddress.countryCode);
        },
        ///Load opening hours of servicepoint
        LoadOpeningHours: function LoadOpeningHours(servicePoint) {
            postNordOpeningHoursElement.innerHTML = "<strong>" + servicePoint.name + "</strong>" + "<div>" + servicePoint.visitingAddress.streetName + " " + servicePoint.visitingAddress.streetNumber + "</div>" + "<div>" + servicePoint.visitingAddress.postalCode + " " + servicePoint.visitingAddress.city + "</div>";
            postNordOpeningHoursElement.appendChild(postNordOpeningHoursTitleElement);

            PostNord.LoadOpeningHoursIndividualDay("MO", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("TU", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("WE", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("TH", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("FR", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("SA", servicePoint);
            PostNord.LoadOpeningHoursIndividualDay("SU", servicePoint);
            var postNordOpeningHoursElementDiv = document.createElement("div", "", "", "");
            postNordOpeningHoursElementDiv.appendChild(postNordOpeningHoursElement);
            postNordInfoWindow.setContent(postNordOpeningHoursElementDiv.innerHTML);

            postNordInfoWindow.open(postNordMap, postNordMarkers.find(function (m) {
                return m.id == "GoogleMapsMarker" + servicePoint.servicePointId;
            }));
        },
        ///Load individual 
        LoadOpeningHoursIndividualDay: function LoadOpeningHoursIndividualDay(dayInit, servicePoint) {
            postNordOpeningHoursElement.appendChild(postNordOpeningHoursTemplate.cloneNode(true));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[Day]", PostNord.ConvertDayInitToReadable(dayInit, false));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[DayInit]", dayInit);
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[Open]", PostNord.GetOpeningTime(dayInit, servicePoint));
            PostNordCommon.ReplaceAllInInnerHtml(postNordOpeningHoursElement, "[Close]", PostNord.GetClosingTime(dayInit, servicePoint));
        },
        /// get openingtime of a day
        GetOpeningTime: function GetOpeningTime(dayInit, servicePoint) {
            return servicePoint["openingHours"].some(function (o) {
                return o.day == dayInit;
            }) ? PostNord.FormatClock(servicePoint["openingHours"].find(function (o) {
                return o.day == dayInit;
            }).from1) : "";
        },
        /// get closingtime of a day
        GetClosingTime: function GetClosingTime(dayInit, servicePoint) {
            return servicePoint["openingHours"].some(function (o) {
                return o.day == dayInit;
            }) ? PostNord.FormatClock(servicePoint["openingHours"].find(function (o) {
                return o.day == dayInit;
            }).to1) : "";
        },
        /// Formating clock from "HHMM" to "HH:MM"
        FormatClock: function FormatClock(input) {
            return input.substring(0, input.length - 2) + ":" + input.slice(-2);
        },
        /// get the servicepoint previusly loaded and stored in the variable "servicePoints".
        GetServicePoint: function GetServicePoint(id) {
            return postNordServicePoints.find(function (x) {
                return x.servicePointId === String(id);
            });
        },
        /// Read the data from response
        ReadData: function ReadData(data, clearMarkers) {
            if ((postNordFirstLoadOfMarkers == true || postNordPostalCodeByUser == true) && postNordServicePointSelected != null) PostNord.UnChoiceServicePoint();

            postNordServicePointListElement.scrollTop = 0;
            var servicePoints = [];
            data.servicePointInformationResponse.servicePoints = data.servicePointInformationResponse.servicePoints.filter(function (s) {
                return !s.name.includes("Pakkeboks");
            })
            
            if (clearMarkers) {
                data.servicePointInformationResponse.servicePoints.filter(function (s) {
                    return (s.routeDistance < 30000 || s.visitingAddress.postalCode == postNordZipCode);
                }).forEach(function (servicePoint) {
                    if (postNordZipCode == servicePoint.visitingAddress.postalCode || servicePoints.length < 10) servicePoints.push(servicePoint);
                });
            } else {
                data.servicePointInformationResponse.servicePoints.forEach(function (servicePoint) {
                    if (servicePoints.length < 10) servicePoints.push(servicePoint);
                });
            }

            var idArray = [];
            if (postNordFirstLoadOfMarkers) PostNord.InitMap();else if (clearMarkers) {

                postNordServicePoints.forEach(function (servicePoint) {
                    if (!servicePoints.some(function (s) {
                        return s.servicePointId == servicePoint.servicePointId;
                    }) && (postNordServicePointSelected == null || servicePoint.servicePointId != postNordServicePointSelected.servicePointId)) {
                        idArray.push(servicePoint.servicePointId);
                    }
                });
                idArray.forEach(function (id) {
                    return PostNord.RemoveServicePoint(id);
                });
            }

            servicePoints.forEach(function (servicePoint) {
                if (!postNordServicePoints.some(function (s) {
                    return s.servicePointId == servicePoint.servicePointId;
                })) PostNord.AddServicePoint(servicePoint);
            });
            setTimeout(function () {
                postNordGoogleMapsZoomEventListner = google.maps.event.addListener(postNordMap, 'zoom_changed', function () {
                    PostNord.GetServicePointsByCoordinates();
                });
                if (postNordFirstLoadOfMarkers == true) {
                    if (document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address1").value != "") {
                        Array.from(document.getElementsByClassName("PostNord-ServicePoint")).forEach(function (element) {
                            if (element.innerHTML.indexOf(document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_address1").value)!== -1) {
                                element.click();
                            }
                        });
                    }
                    if (document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value != "") document.getElementById("postNordZipCodeInput").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value;
                }
                if (postNordFirstLoadOfMarkers == true || postNordPostalCodeByUser == true) {
                    PostNord.ZoomMap(postNordMarkers);
                    postNordFirstLoadOfMarkers = false;
                    postNordPostalCodeByUser = false;
                }
                if (postNordServicePointSelected != null) PostNord.ChoiceServicePoint(postNordServicePointSelected.servicePointId, true);
            }, postNordFirstLoadOfMarkers ? 2000 : 0);
        },
        ///Add servicepoint to list and map
        AddServicePoint: function AddServicePoint(servicePoint) {
            PostNord.InsertServicePoint(servicePoint);
            postNordServicePoints.push(servicePoint);
            PostNord.AddPointToMap(servicePoint);
        },
        ///Remove servicepoint from list and map
        RemoveServicePoint: function RemoveServicePoint(id) {
            PostNord.RemoveServicePointFromMap(id);
            PostNord.RemoveServicePointFromList(id);
        },
        ///Remove servicepoint from map
        RemoveServicePointFromMap: function RemoveServicePointFromMap(id) {
            var markerToRemove = postNordMarkers.find(function (x) {
                return x.id == "GoogleMapsMarker" + id;
            });
            markerToRemove.setMap(null);
            PostNordCommon.RemoveFromArray(postNordMarkers, markerToRemove);
        },
        ///Remove servicepoint from list
        RemoveServicePointFromList: function RemoveServicePointFromList(id) {
            var servicePointToRemove = postNordServicePoints.find(function (x) {
                return x.servicePointId == id;
            });
            var element = document.getElementById("PostNordServicePoint-" + id);
            element.parentNode.removeChild(element);
            PostNordCommon.RemoveFromArray(postNordServicePoints, servicePointToRemove);
        },
        RunPostNord: function RunPostNord() {
            if (!document.getElementById("deliveryAdress").checked) {
                var button = PostNordCommon.CreateButton("Vælg posthus", function () {
                    document.getElementById("deliveryAdress").click();return false;
                });
                button.style.marginTop = "20px";
                document.getElementById("deliveryAdressSameChoiceBox").appendChild(button);
                document.getElementById("deliveryAdressSameChoice").style.display = 'none';
                document.getElementById("mandatoryPrivacyPolicyDiv").style.display = "none";
                document.getElementById("tier7").style.display = 'none';
            } else {
                ///Load Google Maps
                var script = PostNordCommon.CreateElement('script', '', '', '', null);
                script.src = 'https://maps.googleapis.com/maps/api/js?client=gme-ikeaitab&libraries=geometry';
                setTimeout(function () {
                    document.head.appendChild(script);
                }, 10);

                document.getElementById("deliveryBox").style.display = 'none';
                document.getElementById("deliveryAdressSameChoice").style.display = 'none';

                var postNordServicePointListElement = PostNordCommon.CreateElement('div', 'PostNord-ServicePointContainer1', 'postNordServicePointListElement', '', null);
                var postNordServicePointListElement2 = PostNordCommon.CreateElement('div', 'PostNord-ServicePointContainer PostNord-width-40-with-scroll', 'postNordServicePointListElement1', '', postNordServicePointListElement);
                var main = document.getElementById("main");

                //Creating the button to make the activate loading servicepoints

                var postNordButtonCaption = PostNordCommon.CreateElement('div', 'PostNord-width-20', '', '<input type="number" id="postNordZipCodeInput" onkeyup="PostNord.GetServicePointsByPostalCode(this.value,true);return false;" placeholder="Skriv postnummer" class="PostNord-PostalCode PostNord-width-100"/>', null);
                var postNordButtonContainer = PostNordCommon.CreateElement('div', 'PostNord-width-100', '', 'Postnummer:<br>', postNordButtonCaption);
                postNordButtonContainer.appendChild(PostNordCommon.CreateButton("Skift postnummer", function () {
                    PostNord.GetServicePointsByPostalCode(document.getElementById('postNordZipCodeInput').value, true);return false;
                }));
                var PostNordModule = PostNordCommon.CreateElement('div', 'PostNord-width-100 PostNord-Module', 'PostNordModule', '<h2><span class="welcomeHeading">Vælg posthus</span></h2>', postNordButtonContainer);

                //Create the container to 
                var postNordservicePointContainer = PostNordCommon.CreateElement('div', '', 'PostNordElement', '', postNordServicePointListElement2);
                postNordservicePointContainer.appendChild(PostNordCommon.CreateElement('div', 'PostNord-width-60 PostNord-OpeningHours PostNord-GoogleMaps', 'map', '', null));

                PostNordModule.appendChild(postNordservicePointContainer);

                document.getElementById("tier7").style.display = 'none';

                var moveChild = document.getElementById("tier8");
                main.removeChild(document.getElementById("tier8"));
                main.appendChild(PostNordModule);
                main.appendChild(moveChild);

                if (document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value != "") 
                    document.getElementById("postNordZipCodeInput").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_ship_zipCode").value;
                else 
                    document.getElementById("postNordZipCodeInput").value = document.getElementById("signup_checkout_" + postNordCustomerType + "_zipCode").value;

                PostNord.GetServicePointsByPostalCode(document.getElementById("postNordZipCodeInput").value, true);
                document.getElementById("submitButton_IrwAddressDetails").onclick = function () {
                    PostNord.CopyBillingAddressToDeliveryAddress();return false;
                };

                var copyAcceptPersonData = document.getElementById("mandatoryPrivacyPolicyDiv").cloneNode(true);
                if (document.getElementById("termsAndConditions_id").checked == true) copyAcceptPersonData.click();
                copyAcceptPersonData.id = "copyAcceptPersonData";
                copyAcceptPersonData.innerHTML = copyAcceptPersonData.innerHTML.replace("termsAndConditions_id", "copyTermsAndConditions_id");
                copyAcceptPersonData.onclick = function () {
                    document.getElementById("termsAndConditions_id").click();return false;
                };
                document.getElementById("mandatoryPrivacyPolicyDiv").style.display = "none";
                PostNordModule.appendChild(copyAcceptPersonData);
                document.getElementById("termsAndConditions_id").onclick = function () {
                    setTimeout(function () {
                        if (document.getElementById("termsAndConditions_id").checked == true) document.getElementById("copyTermsAndConditions_id").checked = true;else document.getElementById("copyTermsAndConditions_id").checked = false;
                    }, 10);
                };
                var button = PostNordCommon.CreateButton("Fortsæt til leveringsoplysninger", function () {
                    document.getElementById("submitButton_IrwAddressDetails").click();
                });
                button.style.marginTop = "20px";

                PostNordModule.appendChild(button);

                PostNordModule.scrollIntoView(true);
                window.scrollBy(0, -30);
            }
        },
        VerifyDeliveryPrice: function VerifyDeliveryPrice() {
            if (!document.getElementById("signup_checkout_business_organizationName")) 
                postNordCustomerType = "private";
            else 
                postNordCustomerType = "business";
            var signup_checkout_private_savedaddress = document.getElementById("signup_checkout_"+ postNordCustomerType +"_savedaddress");
            if(signup_checkout_private_savedaddress && 0==signup_checkout_private_savedaddress.selectedIndex &&  signup_checkout_private_savedaddress.length >9)
                window.location = signup_checkout_private_savedaddress.lastElementChild.value;
    
            var zipcode = document.getElementById("signup_checkout_"+ postNordCustomerType +"_zipCode").value;
            if (zipcode == "")
                zipcode = "2632";
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "POST", "https://secure.ikea.com/webapp/wcs/stores/servlet/IrwWSCfbDeliveryDetail?zipCode=" + zipcode + "&state=null&storeId=14&langId=-12&priceexclvat=", false );
            xmlHttp.send( null );
            var jsonResponse = JSON.parse(xmlHttp.responseText);
            var price = jsonResponse[1][0]["unFormatedPreShippingCost"];
            if(price < 100 && price > 0)
            {
                PostNord.RunPostNord();
            } 
            return false;
        }
    };
    var postNordServicePoint = PostNordCommon.CreateElement('div', 'PostNord-ServicePoint PostNord-ClickAble PostNordRadio-container', 'PostNordServicePoint-[servicePointId]', '<input type="radio" id="PostNordServicePointRadio-[servicePointId]" class="PostNordRadio"/>' + '<div class="PostNord-width-50 PostNord-margin-left-10">' + ' <div><strong>[name]</strong></div>' + ' <div>[streetName] [streetNumber]</div>' + ' <div>[postalCode] [city], [countryCode]</div>' + '</div>', null);
    var postNordOpeningHoursContainer = PostNordCommon.CreateElement('div', 'PostNord-width-40 postNordOpeningHoursContainer', '', '', null);
    var postNordOpeningHoursScope = PostNordCommon.CreateElement('div', '', '', '[StartDay] - [EndDay]: [StartTime]-[EndTime]', null);
    var postNordOpeningHoursTemplate = PostNordCommon.CreateElement('div', '', '', '<div class="PostNord-Address">[Day]: </div>' + ' <div class="PostNord-Address">[Open]</div> - ' + ' <div class="PostNord-Address">[Close]</div></br>', null);

    var postNordOpeningHoursElement = PostNordCommon.CreateElement('div', 'PostNord-OpeningHoursInfoBox', 'postNordOpeningHoursElement', "", null);
    var postNordOpeningHoursTitleElement = PostNordCommon.CreateElement('div', '', '', '<strong>Åbningstider:</strong>', null);
    ///Verify the order is a parcel delivery.
    ///If the price is not loaded or above 100 kr. the program will stop
    PostNordCommon.WaitForElement("txtTotalDelivery", function () {
        PostNordCommon.WaitForElement("tier8", function () {
            PostNord.VerifyDeliveryPrice();
        });
    });
})();