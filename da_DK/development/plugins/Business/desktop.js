"use strict";
var BusinessPostNord;
(function () {
    window.BusinessCommon = {
        WaitForElement: function WaitForElement(elementId, callBack) {
            window.setTimeout(function () {
                if (document.getElementById(elementId)) {
                    callBack(elementId);
                } else {
                    BusinessCommon.WaitForElement(elementId, callBack);
                }
            }, 10);
        },

        /// Replace all instances of a substring from the string.
        //Function to create elements with type classlist id and innerHTML
        CreateElement: function CreateElement(type, classList, id, innnerHTML, child) {
            var element = document.createElement(type);
            element.innerHTML = innnerHTML;
            element.id = id;
            if (classList != null && classList != "") classList.split(" ").forEach(function (className) {
                element.classList.add(className);
            });
            if (child != null) element.appendChild(child);
            return element;
        },
        CreateInputElement: function CreateInputElement(type, id, placeholder,classList){
            var input = BusinessCommon.CreateElement("input",classList, id,"",null);
            input.type = type;
            input.placeholder = placeholder;
            return input;
        },
        RemoveFromArray: function RemoveFromArray(array, value) {
            for (var i = array.length - 1; i >= 0; i--) {
                if (array[i] === value) {
                    array.splice(i, 1);
                }
            }
        },
        ReplaceAll: function ReplaceAll(string, cut, paste) {
            while (string.indexOf(cut)!== -1) {
                string = string.replace(cut, paste);
            }
            return string;
        },
        ReplaceAllInInnerHtml: function ReplaceAllInInnerHtml(obj, cut, paste) {
            obj.innerHTML = BusinessCommon.ReplaceAll(obj.innerHTML, cut, paste);
        },
        CreateBlueButton: function CreateBlueButton(value, functionToExecute, id){
            return BusinessCommon.CreateElement('div', 'buttonContainer', id + "Container", '', BusinessCommon.CreateButton(value, functionToExecute,id));
        },
        CreateButton: function CreateButton(value, functionToExecute, id) {
            var button = BusinessCommon.CreateElement('input', 'Business-width-100', value, '', null);
            button.type = "button";
            button.onclick = functionToExecute;
            button.value = value;
            button.id = id;
            return BusinessCommon.CreateElement('div', 'buttonCaption', id + "SubContainer", '', button);
        }
    };
    if (document.URL.indexOf("IrwCheckoutWelcomeView") != -1 || document.URL.indexOf("IrwCheckoutAddressView") != -1)
    {
        BusinessCommon.WaitForElement("tier8", function () {
            var zipcode = document.getElementById("signup_checkout_business_zipCode").value;
            if(zipcode == "")
                return;
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "POST", "https://secure.ikea.com/webapp/wcs/stores/servlet/IrwWSCfbDeliveryDetail?zipCode=" + zipcode + "&state=null&storeId=14&langId=-12&priceexclvat=", false );
            xmlHttp.send( null );
            var jsonResponse = JSON.parse(xmlHttp.responseText);
            var price = parseInt(jsonResponse[1][0]["unFormatedPreShippingCost"]);
            if(price < 100 && price > 0)
            {
                BusinessPostNord = true;
                if (document.getElementById("deliveryAdress").checked==false) {
                    return;
                }
            }
            else
                BusinessPostNord = false;
            if(document.getElementById("signup_checkout_business_organizationName")){

                window.Business = {
                    ShowInfoBox: function ShowInfoBox(){
                        var content = document.getElementById("content_IKEABUSINESSCARD");
                        if (content.style.display != "none") {
                            content.style.display = "none";
                        } else {
                            content.style.display = "block";
                        }
                    },

                    EmptyBasket: function EmptyBasket(){
                        var xhr = new XMLHttpRequest();
                        xhr.open("GET", "https://secure.ikea.com/webapp/wcs/stores/servlet/OrderItemDelete?orderItemId=*&URL=OrderItemDisplay&storeId=14&langId=-12&catalogId=11001",false);
                        xhr.send(null);
                        window.location="https://www.ikea.com/dk/da/";
                    },
                    PopUp: function PopUp(){
                        var error = ""
                        if(document.getElementById("signup_checkout_business_organizationName").value =="")
                            error += "Firmanavn skal udfyldes\n";
                        if(document.getElementById("signup_checkout_business_firstName").value =="")
                            error += "Fornavn skal udfyldes\n";
                        if(document.getElementById("signup_checkout_business_lastName").value =="")
                            error += "Efternavn skal udfyldes\n";
                        if(document.getElementById("signup_checkout_business_zipCode").value =="")
                            error += "Postnummer skal udfyldes\n";
                        if( document.getElementById("signup_checkout_business_phone1").value =="" &&
                            document.getElementById("signup_checkout_business_phone2").value =="")
                            error += "Telefonnummer skal udfyldes\n";
                        if(document.getElementById("termsAndConditions_id").checked == false)
                            error += "Håndtering af persondata skal accepteres\n";
                        if(error == "")
                        {
                            document.getElementById('infoBoxBusinessCard').style.display = "block";
                            document.getElementById('lightboxBusinessCard').style.display = "block";    
                        }
                        else{
                            alert(error);
                        }
                    },
                    CloseBusinessCard: function CloseBusinessCard(){
                        document.getElementById('infoBoxBusinessCard').style.display = "none";
                        document.getElementById('lightboxBusinessCard').style.display = "none";
                    },
                    VaildateCardNumber: function VaildateCardNumber(){
                        var validation = true;
                        if(document.getElementById("orderByEMailBusinessCardNumber").value==""){
                            document.getElementById("businessCardNumberError").classList.remove("BusinessCardHidden");
                            validation = false;
                        }
                        if(document.getElementById("orderByEMailBusinessCardNumber").value==""){
                            document.getElementById("businessCardReferenceError").classList.remove("BusinessCardHidden");
                            validation = false;    
                        }
                        return validation;
                    },
                    PlaceOrder: function PlaceOrder(){
                        if(Business.VaildateCardNumber() == false){
                            return;
                        }
                        var order = {};
                        order["cardnumber"] = document.getElementById("orderByEMailBusinessCardNumber").value;
                        order["reference"] = document.getElementById("orderByEMailBusinessCardReference").value;
                
                        order["orderAddressCompanyName"] = document.getElementById("signup_checkout_business_organizationName").value;
                        order["orderAddressFirstName"] = document.getElementById("signup_checkout_business_firstName").value;
                        order["orderAddressLastName"] = document.getElementById("signup_checkout_business_lastName").value;
                        order["orderAddressAddress1"] = document.getElementById("signup_checkout_business_address1").value;
                        order["orderAddressAddress2"] = document.getElementById("signup_checkout_business_address2").value;
                        order["orderAddressPostalNumber"] = document.getElementById("signup_checkout_business_zipCode").value;
                        order["orderAddressCity"] = document.getElementById("signup_checkout_business_city").value;
                        order["orderAddressCountry"] = "Denmark";
                        order["orderAddressEmail"] = document.getElementById("signup_checkout_business_email1").value;
                        order["orderAddressEmailRepeat"] = document.getElementById("signup_checkout_business_email1retype").value;
                        order["orderAddressMobilePhoneNumber"] = document.getElementById("signup_checkout_business_phone1").value;
                        order["orderAddressPhoneNumber"] = document.getElementById("signup_checkout_business_phone2").value;
                        order["orderAddressLocalNumber"] = "";
                
                        order["deliveryAddress"] = [];
                        var deliveryAddress = {};
                        if(document.getElementById("signup_checkout_business_ship_firstName")){
                            deliveryAddress["deliveryAddressCompanyName"] = document.getElementById("signup_checkout_business_organizationName").value;
                            deliveryAddress["deliveryAddressFirstName"] = document.getElementById("signup_checkout_business_ship_firstName").value;
                            deliveryAddress["deliveryAddressLastName"] = document.getElementById("signup_checkout_business_ship_lastName").value;
                            deliveryAddress["deliveryAddressAddress1"] = document.getElementById("signup_checkout_business_ship_address1").value;
                            deliveryAddress["deliveryAddressAddress2"] = document.getElementById("signup_checkout_business_ship_address2").value;
                            deliveryAddress["deliveryAddressPostalNumber"] = document.getElementById("signup_checkout_business_ship_zipCode").value;
                            deliveryAddress["deliveryAddressCity"] = document.getElementById("signup_checkout_business_ship_city").value;
                            deliveryAddress["deliveryAddressMobilePhoneNumber"] = document.getElementById("signup_checkout_business_ship_phone1").value;
                            deliveryAddress["deliveryAddressPhoneNumber"] = document.getElementById("signup_checkout_business_ship_phone2").value;
                            deliveryAddress["deliveryAddressLocalNumber"] = "";    
                        }
                        else{
                            deliveryAddress["deliveryAddressCompanyName"] = order["orderAddressCompanyName"];
                            deliveryAddress["deliveryAddressFirstName"] = order["orderAddressFirstName"];
                            deliveryAddress["deliveryAddressLastName"] = order["orderAddressLastName"];
                            deliveryAddress["deliveryAddressAddress1"] = order["orderAddressAddress1"];
                            deliveryAddress["deliveryAddressAddress2"] = order["orderAddressAddress2"];
                            deliveryAddress["deliveryAddressPostalNumber"] = order["orderAddressPostalNumber"];
                            deliveryAddress["deliveryAddressCity"] = order["orderAddressCity"];
                            deliveryAddress["deliveryAddressMobilePhoneNumber"] = order["orderAddressMobilePhoneNumber"];
                            deliveryAddress["deliveryAddressPhoneNumber"] = order["orderAddressPhoneNumber"];
                            deliveryAddress["deliveryAddressLocalNumber"] = order["orderAddressLocalNumber"];    
                        }
                        order["deliveryAddress"].push(deliveryAddress);
                        order["products"] = [];
                        var productCounter = 1
                        while(document.getElementById("tr_" + productCounter)){
                            var productHTML = document.getElementById("tr_" + productCounter);
                            var product = {};
                            product["amount"] = productHTML.children[0].children[0].innerHTML;
                            product["id"] = productHTML.getAttribute("data-productid");
                            product["name"] = productHTML.children[2].children[0].children[0].children[0].innerHTML;
                            productCounter ++;
                            order["products"].push(product);    
                        }
                        
                        document.getElementById('infoBoxBusinessCard').style.display = "none";
                        document.getElementById('lightboxBusinessCard').style.display = "none";
                        document.getElementById('infoBoxThanksForThePurches').style.display = "block";
                        document.getElementById('lightboxThanksForThePurches').style.display = "block";
                        
                        console.log("Mail sendt afsted");
                        var body = {"body":order};
                        var xhr = new XMLHttpRequest();
                        xhr.open("POST", "https://europe-west1-magnetic-port-235206.cloudfunctions.net/IkeaBusinessCard",true);
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.send(JSON.stringify(body));
                        console.log(body);
                    },
                    ShowButton: function ShowButton(){
                        if(BusinessPostNord===false){
                            if(document.getElementById("choiceBusinessCardCheckbox").checked === true)
                            {
                                document.getElementById("businessCardButtonContainer").style.display="";
                                document.getElementById("submitButton_IrwAddressDetails").parentElement.style.display="none";
                            }else{
                                document.getElementById("businessCardButtonContainer").style.display="none";
                                document.getElementById("submitButton_IrwAddressDetails").parentElement.style.display="";
                            }    
                        }
                        else{
                            if(document.getElementById("choiceBusinessCardCheckbox").checked === true)
                            {
                                document.getElementById("businessCardButtonContainer").style.display="";
                                document.getElementById("businessCardButtonContainer").nextSibling.style.display="none";
                            }else{
                                document.getElementById("businessCardButtonContainer").style.display="none";
                                document.getElementById("businessCardButtonContainer").nextSibling.style.display="";
                            }    
                        }
                    }
                };    
                BusinessCommon.WaitForElement("mandatoryPrivacyPolicyDiv",function(){
                    BusinessCommon.WaitForElement("tier7",function(){
                        if(BusinessPostNord===false){
                            if(document.getElementById("signup_checkout_business_organizationName")){
                                BusinessCommon.WaitForElement("submitButton_IrwAddressDetails",function(){
                                    var businessCardButton = BusinessCommon.CreateBlueButton("Køb med IKEA BUSINESS kort ", function(){Business.PopUp()},"businessCardButton");
                                    var submitButton_IrwAddressDetails_parentElement = document.getElementById("submitButton_IrwAddressDetails").parentElement;
                                    submitButton_IrwAddressDetails_parentElement.parentElement.insertBefore(businessCardButton, submitButton_IrwAddressDetails_parentElement.nextSibling);
                                    businessCardButton.style.display = "none";
                                })
                            }
                            var choiceBusinessCardCheckbox = BusinessCommon.CreateInputElement("checkbox","choiceBusinessCardCheckbox","","");
                            choiceBusinessCardCheckbox.onclick = function(){Business.ShowButton();};
                            var choiceBusinessCard = BusinessCommon.CreateElement("div","","ChoiceBusinessCard","",choiceBusinessCardCheckbox);
                            choiceBusinessCard.appendChild(BusinessCommon.CreateElement("label","","","Ønsker du at bruge dit IKEA BUSINESS kort?",null));
                            document.getElementById("tier7").parentElement.parentElement.insertBefore(choiceBusinessCard, document.getElementById("tier7").parentElement);
                        }
                        else{
                            var choiceBusinessCardCheckbox = BusinessCommon.CreateInputElement("checkbox","choiceBusinessCardCheckbox","","");
                            choiceBusinessCardCheckbox.onclick = function(){Business.ShowButton();};
                            var choiceBusinessCard = BusinessCommon.CreateElement("div","","ChoiceBusinessCard","",choiceBusinessCardCheckbox);
                            choiceBusinessCard.appendChild(BusinessCommon.CreateElement("label","","","Ønsker du at bruge dit IKEA BUSINESS kort?",null));
                            document.getElementById("copyAcceptPersonData").parentElement.insertBefore(choiceBusinessCard, document.getElementById("copyAcceptPersonData"));
                            
                            var businessCardButton = BusinessCommon.CreateBlueButton("Køb med IKEA BUSINESS kort ", function(){Business.PopUp()},"businessCardButton");
                            businessCardButton.style.display="none"
                            var submitButton_IrwAddressDetails_parentElement = document.getElementById("copyAcceptPersonData");
                            submitButton_IrwAddressDetails_parentElement.parentElement.insertBefore(businessCardButton, submitButton_IrwAddressDetails_parentElement.nextSibling);
                        }
                    });
                });

                var lightboxBusinessCard = BusinessCommon.CreateElement("div","lightbox-country", "lightboxBusinessCard","", null);
                document.body.appendChild(lightboxBusinessCard);

                var orderByEMailHeader = BusinessCommon.CreateElement("h3","","orderByEMailHeader","Køb med IKEA BUSINESS kort ",null);
                var orderByEMail = BusinessCommon.CreateElement("div","Business-Inline-Block Business-width-100","orderByEMail","", orderByEMailHeader);
                var businessCardNumber = BusinessCommon.CreateInputElement("text","orderByEMailBusinessCardNumber","Kortnummer","businessInput");
                var businessCardNumberError = BusinessCommon.CreateElement("div","BusinessCardErrorMessage BusinessCardHidden","businessCardNumberError","Kortnummer mangler", null);
                var businessCardReference = BusinessCommon.CreateInputElement("text","orderByEMailBusinessCardReference","Reference","businessInput");
                var businessCardReferenceError = BusinessCommon.CreateElement("div","BusinessCardErrorMessage BusinessCardHidden","businessCardReferenceError","Reference mangler", null);
                var orderByEMailText = BusinessCommon.CreateElement("p","","",'Køb med IKEA BUSINESS kort sker midlertidig via vores varehuse. Derfor sker bestillinger denne vej. Når du har tastet dine oplysninger (inkl. IKEA BUSINESS kortnummer) sendes de til dit lokale IKEA BUSINESS varehus.<br><br>Når vi modtager din ordre, vil vi bekræfte og behandle den.<br><br>Har du spørgsmål, er du velkommen til at <a target="_blank" href="https://www.ikea.com/dk/da/catalog/categories/business/ikea_business/?preferedui=desktop&amp;_ga=2.57672748.762621152.1554192701-105539999.1548418464#businesslokal">dit nærmeste IKEA BUSINESS</a>', null);
                orderByEMailText.style.marginBottom = "30px";
                var businessCardEMailButton = BusinessCommon.CreateBlueButton("Send din ordre", function(){Business.PlaceOrder();return false;},"GenerateEmail");
                businessCardEMailButton.classList.add("Business-width-100");

                orderByEMail.appendChild(orderByEMailText);
                orderByEMail.appendChild(businessCardNumber);
                orderByEMail.appendChild(businessCardNumberError);
                orderByEMail.appendChild(businessCardReference);
                orderByEMail.appendChild(businessCardReferenceError);
                orderByEMail.appendChild(businessCardEMailButton);
                
                var infoBoxBusinessCard = BusinessCommon.CreateElement("div","BusinessCardPopUp","infoBoxBusinessCard",'<div class=\'CloseBusinessPopUp\' >\n <a id=\'lightbox-close-btn\' onclick="Business.CloseBusinessCard()" href=\'#\'>x</a>\n </div> </div>', null);
                infoBoxBusinessCard.appendChild(orderByEMail);

                lightboxBusinessCard.appendChild(infoBoxBusinessCard);
                document.body.appendChild(lightboxBusinessCard);
        
                var lightboxThanksForThePurches = BusinessCommon.CreateElement("div","lightbox-country","lightboxThanksForThePurches","", null);
                document.body.appendChild(lightboxThanksForThePurches);
        
                var thankYouHeader = BusinessCommon.CreateElement("h3","","orderByEMailHeader","Tak for din ordre",null);
                var thankYouText = BusinessCommon.CreateElement("p","","lightboxThanksForThePurches","Vi behandler nu din ordre.<br><br>Hvis din ordre skal leveres med fragtbil, vil vi kontakte dig for at aftale leveringstidspunkt.<br>Hvis din ordre skal leveres med PostNord, vil du løbende blive orienteret om din ordres status.", null);
                var infoBoxThanksForThePurches = BusinessCommon.CreateElement("div","BusinessCardPopUp","infoBoxThanksForThePurches",'<div style="float:right; cursor:pointer"><strong onclick="Business.EmptyBasket()">X</strong></div>', thankYouHeader);
                infoBoxThanksForThePurches.appendChild(thankYouText);
                lightboxThanksForThePurches.appendChild(infoBoxThanksForThePurches);    
                
            };
        })
    }
})();