"use strict";

(function () {
  // Check ready state
  function readyNow(fn1) {
    if (document.attachEvent ? document.readyState == 'complete' : document.readyState !== 'loading') {
      fn1();
    } else {
      document.addEventListener('DOMContentLoaded', fn1);
    }
  }

  var runNow = function runNow() {
    setTimeout(function(){
    if (document.getElementsByClassName("row customerData") != null) {
      /**************** STEP 2 START *****************/
      var DeliveryInformation = function DeliveryInformation() {
        /*** START STEP 2 CLICK AND COLLECT CHANGES ***/
        if (document.URL.indexOf("/clickandcollect/dk/collect/customerdata") > -1 && document.getElementsByClassName("col-xs-4 pickUpLocationOpeningHours")[0]) {
          var yourInfo = document.getElementById("addresses");

          if (yourInfo.children[0].innerText === "Dine oplysninger") {
            yourInfo.classList.add("col-xs-5");
            yourInfo.setAttribute("style", "padding-left:12px");
            var btnLink = document.getElementsByClassName("col-sm-6")[0];
            btnLink.innerHTML = "<a href='https://secure.ikea.com/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=14&amp;langId=-12' >tilbage</a>";
          }
        }

        /*** END CLICK AND COLLECT CHANGES ***/
        // remove login link. 
        var removeLogin = document.getElementsByClassName("loginContainer")[0];
        removeLogin.children[0].setAttribute("style", "display:none"); // changhe back button to textlink...

        var btnLink = document.getElementsByClassName("col-sm-6")[0];
        btnLink.innerHTML = "<a href='https://secure.ikea.com/webapp/wcs/stores/servlet/OrderItemDisplay?storeId=14&amp;langId=-12' >tilbage</a>"; // move elment delivery information to the top of the from. 
    
        var HeadlineDelivery = document.getElementsByClassName("row")[5];
        HeadlineDelivery.append(addresses);
        HeadlineDelivery.setAttribute("style", "margin-left: -57px;"); // text and drob down
        
        var TexAndDropDown = document.getElementsByClassName("row")[6];
        TexAndDropDown.append(addresses);
        TexAndDropDown.setAttribute("style", "margin-left: -42px"); // Format the text below delivery date 

        var deliveryText2 = document.getElementsByClassName("col-xs-12")[2];
        var newText2 = "<b>Kantstenslevering 599.- </b><br> Du bærer selv dine varer ind.<br><br><b> Levering med fuld indbæring 799.- </b><br>Vi bærer dine varer helt ind i et rum, som du vælger.<br><hr><br>";
        deliveryText2.innerHTML = newText2; // style formating hr delivery information.

        var hrOne = document.getElementsByTagName("hr")[0];
        hrOne.setAttribute("style", "margin-left:-42px; border-color:#cccccc");
        var hrsecond = document.getElementsByTagName("hr")[1];
        hrsecond.setAttribute("style", "margin-left:-18px; border-color:#cccccc"); // hide survery

        var customerSurvery = document.getElementsByClassName("customerConsentSurvey")[0];
        customerSurvery.setAttribute("style", "display:none"); // checked the radio button to hous

        var checkboxWhereDoYouLive = document.getElementsByName('basket[invoiceAddress][voQuestionnaireAnswerSet][QuestionnaireType]')[0];
        checkboxWhereDoYouLive.setAttribute("checked", "checked");
        checkboxWhereDoYouLive.setAttribute("value", "HOUSE"); // show the hidden mandatory elements

        var expressData = document.getElementsByClassName('expressData row')[0];
        expressData.children[0].children[2].removeAttribute('style'); // the dom is changin postions.. 
        // so some times form-group [28] is a text field and some time it is a checkbox.
        // the same gos for form-group [29]

        var mandatoryNumberOne = document.getElementsByClassName('form-group')[28];
        var mandatoryNumberTwo = document.getElementsByClassName('form-group')[29];

        if (mandatoryNumberOne.children[1].children[3].type === "text") {
          mandatoryNumberOne.children[1].children[3].removeAttribute("style");
          mandatoryNumberOne.children[1].children[3].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[3].setAttribute("value", "000");
          mandatoryNumberOne.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[1].removeAttribute("disabled");
        }

        if (mandatoryNumberOne.children[1].children[4].type === "text") {
          mandatoryNumberOne.children[1].children[4].removeAttribute("style");
          mandatoryNumberOne.children[1].children[4].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[4].setAttribute('value', '000');
          mandatoryNumberOne.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[1].removeAttribute("disabled");
        } else if (mandatoryNumberOne.children[1].children[2].type === "checkbox") {
          mandatoryNumberOne.children[1].children[2].removeAttribute("style");
          mandatoryNumberOne.children[1].children[2].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[2].setAttribute("checked", "checked");
          mandatoryNumberOne.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberOne.children[1].children[1].removeAttribute("disabled");
        }

        if (mandatoryNumberTwo.children[1].children[3].type === "text") {
          mandatoryNumberTwo.children[1].children[3].removeAttribute('style');
          mandatoryNumberTwo.children[1].children[3].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[3].setAttribute("value", "000");
          mandatoryNumberTwo.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[1].removeAttribute("disabled");
        }

        if (mandatoryNumberTwo.children[1].children[4].type === "text") {
          mandatoryNumberTwo.children[1].children[4].removeAttribute('style');
          mandatoryNumberTwo.children[1].children[4].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[4].setAttribute("value", "000");
          mandatoryNumberTwo.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[1].removeAttribute("disabled");
        } else if (mandatoryNumberTwo.children[1].children[2].type === "checkbox") {
          mandatoryNumberTwo.children[1].children[2].removeAttribute('style');
          mandatoryNumberTwo.children[1].children[2].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[2].setAttribute("checked", "checked");
          mandatoryNumberTwo.children[1].children[0].removeAttribute("disabled");
          mandatoryNumberTwo.children[1].children[1].removeAttribute("disabled");
        } // hide deliverly infomation. 


        var deliverlyInfomation = document.getElementById("questionnaireBlock");
        deliverlyInfomation.setAttribute("style", "display:none");
        var mandatoryField = document.getElementsByClassName("mandatory_hint")[0];
        mandatoryField.setAttribute("style", "display:none"); // remove h1 click & collect
        //const removeClickAndCollect = document.getElementsByClassName('visuallyHiddenidden')[1];
        //removeClickAndCollect.setAttribute('style','display:none');
      };
      /************* STEP 2 END ****************/

      /************* STEP 3 START **************/


      var ChoosePaymentMethodStep = function ChoosePaymentMethodStep() {
        // call click and collect changes. 
        clickandcollect(); // remove login link. 

        var removeLogin = document.getElementsByClassName("loginContainer")[0];
        removeLogin.children[0].setAttribute("style", "display:none"); // remove h1 click & collect

        var removeClickAndCollect = document.getElementsByClassName('visuallyHiddenidden')[1];
        removeClickAndCollect.setAttribute('style', 'display:none'); // Remove blue "Tilbage til indkøbskurv" button           

        var btnStep3 = document.getElementsByClassName('processButtons')[0];
        btnStep3.setAttribute('style', 'display: none'); // change wording "Vælg levering"     

        var pickupHeadline = document.getElementsByClassName('headline pickUpDateHeadline')[1];
        var pickupText = 'Du har valgt levering den: ';
        pickupHeadline.innerHTML = pickupText; // Format the text below delivery date

        var deliveryTxt = document.getElementsByClassName('col-xs-12')[6];
        var newText = '<b>Kantstenslevering til 599.-</b><br>Du bærer selv dine varer ind<br><br><b>Levering med fuld indbæring for 799.-</b><br>Vi bærer dine varer helt ind i et rum, som du selv vælger';
        deliveryTxt.innerHTML = newText;
        deliveryTxt.setAttribute("style", "display: block; padding-top: -25px"); // Make sure col-xs-12 is empty on Click&Collect    

        var CandC = document.getElementsByClassName("col-xs-6")[0].children[0].children[3];
        CandC.setAttribute("style", "display: none"); // Change wording for "Den valgte service"

        var yourSelectionTxt = document.getElementsByClassName('pickUpDateHeadline headline col-xs-12')[0];
        var newSelectionTxt = 'Hvis du har valgt:';
        yourSelectionTxt.innerHTML = newSelectionTxt;
        yourSelectionTxt.setAttribute('style', 'display: block; padding-top: 15px; font-size: 14px; margin-bottom: -7px'); // Move IKEA BUSINESS card inputfield up

        var variabelNavn = setInterval(function () {
          var inputfield = document.getElementsByClassName('col-xs-7')[2]; // check if the element is there

          if (inputfield != null) {
            inputfield.setAttribute('style', 'display: block; margin-top: -120px; padding-left: 13px');
            clearInterval(variabelNavn);
          }
        }, 10);
        var movebusinessCard = document.getElementsByClassName("col-xs-7")[0];
        movebusinessCard.setAttribute('style', 'display: block; margin-top: -120px; padding-left: 13px; z-index:1'); // Move payment window up

        var movePayment = document.getElementsByClassName('col-xs-7 paymentProviders aci_creditcard adapterContent')[0];
        movePayment.setAttribute('style', 'display: block; margin-top: -160px; margin-left: -49px; padding-left: 0px'); // move C&C payment window

        var moveCCPayment = document.getElementById("paymentBoxAci");
        moveCCPayment.setAttribute('style', 'display: block; margin-top: 10%; margin-left: -49px; padding-left: 0px'); // move headline 60px to the right 

        var interval = setInterval(function () {
          var newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0]; // check if the element is there

          if (newHeadline != null) {
            newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 110px; margin-top: -45px; margin-bottom: 60px; z-index: 2');
            clearInterval(interval);
          }
        }, 10);
        var bcInputfield = setInterval(function () {
          var businesscardInput = document.getElementsByClassName('col-md-12')[0]; // check if the element is there

          if (businesscardInput != null) {
            businesscardInput.setAttribute('style', 'display: none');
            clearInterval(bcInputfield);
          }
        }, 10);

        function check() {
          if (document.getElementById('payment-IkeaBusinessPayCardAdapter').checked === true) {
            // Ikea Business
            var moveIkeaBusiness = document.getElementsByClassName('col-xs-7')[0];
            moveIkeaBusiness.setAttribute('style', 'display: block; margin-top: -114px; margin-left: -2px; z-index:1');
            var moveIkeaBusinesInput = document.getElementsByClassName('col-md-12 col-xs-11 ikeaBusinessPayCardMobileBlock')[0];
            moveIkeaBusinesInput.setAttribute('style', 'margin-top: -188px; margin-left: -160px');
            var businesscardInput = document.getElementsByClassName("col-md-12")[0];
            businesscardInput.setAttribute("style", "display:show; padding-top: 115px");
            var newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0];
            newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 110px; margin-top: -45px; margin-bottom: 60px; z-index: 2');
          } else if (document.getElementById('payment-aci_creditcard').checked === true) {
            var newHeadline2 = document.getElementsByClassName('paymentHeadingBlockHead')[0];
            newHeadline2.setAttribute('style', 'display: block; font-size: 18px; margin-left: 110px; margin-top: -45px; margin-bottom: 60px; z-index: 2'); // Move payment card 

            var movebusinessCard1 = document.getElementsByClassName("col-xs-7")[0];
            movebusinessCard1.setAttribute('style', 'display: block; margin-top: -120px; padding-left: 13px; z-index:1'); // Move payment window up

            var _movePayment = document.getElementsByClassName('col-xs-7 paymentProviders aci_creditcard adapterContent')[0];

            _movePayment.setAttribute('style', 'display: block; margin-top: -160px; margin-left: -49px; padding-left: 0px');
          }
        } // Calling check radio button


        document.getElementById('payment-IkeaBusinessPayCardAdapter').onclick = check;
        document.getElementById('payment-aci_creditcard').onclick = check; // payment methods - there is two headline sp remove one of them

        var betalingmetode = document.getElementsByTagName('p')[2];
        betalingmetode.setAttribute('style', 'display:none'); // radio button group img visa and so on. 

        var NumberTwoPaymentRadioBtn = document.getElementById('radioButtonGroup');
        NumberTwoPaymentRadioBtn.setAttribute('style', 'margin-top: 9px;');
      }; // Click & Collect
      // Fjerner den blå knap, placerer betalingselementet og formaterer overskriften------------------------------------------


      var clickandcollect = function clickandcollect() {
        if (document.URL.indexOf('/clickandcollect/dk/basketupdate/updatebasketaddresses/') > -1 && document.getElementsByClassName("col-xs-4 pickUpLocationOpeningHours")[0]) {
          // remove login link. 
          var _removeLogin = document.getElementsByClassName("loginContainer")[0];

          _removeLogin.children[0].setAttribute("style", "display:none");

          var btnStep3 = document.getElementsByClassName('processButtons')[0];
          btnStep3.setAttribute('style', 'display: none');
          var interval = setInterval(function () {
            var newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0];

            if (newHeadline != null) {
              newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 90px; margin-bottom: 10px; z-index: 2');
              clearInterval(interval);
            }
          }, 10);
          var CandCPayment = document.getElementById("paymentBoxAci");
          CandCPayment.setAttribute('style', 'display: block; margin-left: -105px'); // Placere IKEA BUSINESS radio buttons

          var CaCRadioButtons = document.getElementsByClassName("col-xs-7")[0];
          CaCRadioButtons.setAttribute('style', 'display: block; margin-top: 30px');

          if (document.getElementById('payment-IkeaBusinessPayCardAdapter').checked === true) {
            // Ikea Business
            var moveIkeaBusiness = document.getElementsByClassName('col-xs-7')[0];
            moveIkeaBusiness.setAttribute('style', 'display: block; margin-top: 30px; margin-left: -2px; z-index:1');
            var moveIkeaBusinesInput = document.getElementsByClassName('col-md-12 col-xs-11 ikeaBusinessPayCardMobileBlock')[0];
            moveIkeaBusinesInput.setAttribute('style', 'margin-top: 10px; margin-left: -160px');
            var businesscardInput = document.getElementsByClassName("col-md-12")[0];
            businesscardInput.setAttribute("style", "display:show; padding-top: 115px");
            var newHeadline = document.getElementsByClassName('paymentHeadingBlockHead')[0];
            newHeadline.setAttribute('style', 'display: block; font-size: 18px; margin-left: 60px; margin-bottom: 60px');
          } else if (document.getElementById('payment-aci_creditcard').checked === true) {
            // Move payment card 
            var movebusinessCard1 = document.getElementsByClassName("col-xs-7")[0];
            movebusinessCard1.setAttribute('style', 'display: block; margin-top: 41px; margin-left: -2px; z-index:1'); // Move payment window up

            var movePayment = document.getElementsByClassName('col-xs-7 paymentProviders aci_creditcard adapterContent')[0];
            movePayment.setAttribute('style', 'display: block; margin-top: -22px; margin-left: -49px; padding-left: 0px');
          } // Calling check radio button


          document.getElementById('payment-IkeaBusinessPayCardAdapter').onclick = check;
          document.getElementById('payment-aci_creditcard').onclick = check;
        }
      };

      var Payment = function Payment() {
        // remove login link. 
        var removeLogin = document.getElementsByClassName("loginContainer")[0];
        removeLogin.children[0].setAttribute("style", "display:none");
        var pickupHeadline = document.getElementsByClassName('headline pickUpDateHeadline')[1];
        var pickupText = 'Du har valgt levering den: ';
        pickupHeadline.innerHTML = pickupText; // Format the text below delivery date

        var deliveryTxt = document.getElementsByClassName('col-xs-12')[6];
        var newText = '<b>Kantstenslevering til 599.-</b><br>Du bærer selv dine varer ind<br><br><b>Levering med fuld indbæring for 799.-</b><br>Vi bærer dine varer helt ind i et rum, som du selv vælger';
        deliveryTxt.innerHTML = newText;
        deliveryTxt.setAttribute("style", "display: block; padding-top: -25px"); // Change wording for "Den valgte service"

        var yourSelectionTxt = document.getElementsByClassName('pickUpDateHeadline headline col-xs-12')[0];
        var newSelectionTxt = 'Hvis du har valgt:';
        yourSelectionTxt.innerHTML = newSelectionTxt;
        yourSelectionTxt.setAttribute('style', 'display: block; padding-top: 15px; font-size: 14px; margin-bottom: -7px'); // remove h1 click & collect
        //const removeClickAndCollect = document.getElementsByClassName('visuallyHiddenidden')[1];
        //removeClickAndCollect.setAttribute('style','display:none');
      };
      /************* STEP 3 END ****************/
      // call function in the right order. 


      if (document.URL.indexOf('/clickandcollect/dk/collect/customerdata') > -1) {
        DeliveryInformation();
      } else if (document.URL.indexOf('/clickandcollect/dk/basketupdate/updatebasketaddresses/redirect') > -1 && document.getElementById('addresses')) {
        DeliveryInformation();
      } else if (document.URL.indexOf('/clickandcollect/dk/basketupdate/updatebasketaddresses/redirect') > -1) {
        ChoosePaymentMethodStep();
      } else if (document.URL.indexOf('/clickandcollect/dk/payment/receive/') > -1) {
        Payment();
      }
     }
  }, 1000);
  }

  readyNow(runNow);
})();