'use strict';

(function () {
   /* var path = "css";
    var style = document.createElement('link');
    style.rel = 'stylesheet';
    style.type = 'text/css';
    style.href = 'https://www.ikea.com/ms/da_DK/XCLm55MBAS/USP-PLP-IRW.css';
    document.getElementsByTagName('head')[0].appendChild(style); */
    //console.log ("CSS loaded")

    if (document.URL.indexOf("/catalog/categories/departments/") > -1) {
        // Load the JSON file 

        var loadopvaskemaskiner = function loadopvaskemaskiner() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var opvaskemaskiner = JSON.parse(this.responseText);
                    //console.log(this.responseText)

                    var allProducts = document.getElementsByClassName("threeColumn product");
                    for (var i = 0; i < allProducts.length; i++) {

                        var Product = allProducts[i];
                        for (var x = 0; x < opvaskemaskiner.length; x++) {

                            //get productID in div
                            var link = Product.children[1].childNodes[1].pathname;
                            var substrings = link.split("/");
                            var productID = substrings[5];

                            //get productID in JSON
                            var productID_json = opvaskemaskiner[x].Article.toString();

                            if (productID === productID_json) {
                                console.log("Product matches from div with JSON");
                                console.log("ID from DIV:" + productID);
                                console.log("ID from JSON:" + productID_json);

                                var container = document.createElement("mark");

                                container.innerHTML = '\n                          <ul.a>\n                              <br>\n                              <li class = "usp">Lydniveau max.<span class="text-yes"> ' + opvaskemaskiner[x].Lydniveau + ' </span></li> \n                              <li class = "usp">Antal kuverter<span class ="text-yes">   ' + opvaskemaskiner[x].Kapacitet + '  </span></li> \n                              <li class = "usp">Antal programmer<span class="text-yes"> ' + opvaskemaskiner[x].Programmer + ' </span></li> \n                              <li class = "usp"> ' + (opvaskemaskiner[x].AquaStop ? 'AquaStop<span class = "icon-yes"' + opvaskemaskiner[x].AquaStop + '</span>' : '<div class = "text-no">AquaStop</div><span class = "icon-no" ' + opvaskemaskiner[x].AquaStop) + '</span></li>\n                              <li class = "usp"> ' + (opvaskemaskiner[x].Forsinketstart ? 'Forsinket start<span class = "icon-yes"' + opvaskemaskiner[x].Forsinketstart + '</span>' : '<div class = "text-no">Forsinket start</div><span class = "icon-no" ' + opvaskemaskiner[x].Forsinketstart) + '</span></li>\n                              <li class = "usp"> ' + (opvaskemaskiner[x].Belysning ? 'LED belysning<span class = "icon-yes"' + opvaskemaskiner[x].Belysning + '</span>' : '<div class = "text-no">LED belysning</div><span class = "icon-no" ' + opvaskemaskiner[x].Belysning) + '</span></li>\n                              <li class = "usp"> ' + (opvaskemaskiner[x].Garanti ? 'Garanti<span class = "text-yes">' + opvaskemaskiner[x].Garanti + '</span>' : '<div class = "text-no">Garanti</div><span class = "icon-no" ' + opvaskemaskiner[x].Garanti) + '</span></li>\n                          </ul>';

                                var product = document.body.querySelector('span.productDesp').parentNode;
                                var sp2 = document.body.querySelector('span.productDesp');
                                product.replaceChild(container, sp2);
                            }
                        }
                    }
                }
            };
            //Get the JSON file
            xhttp.open('GET', 'https://m2.ikea.com/dk/da/data-sources/15eff2b0c55c11e88e36f73a205cf3a9.json', true);
            xhttp.send();
        };

        loadopvaskemaskiner();
    };
})();