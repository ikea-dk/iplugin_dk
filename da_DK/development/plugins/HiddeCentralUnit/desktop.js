(function () {
    window.HideCentralUnit = {
        HideCentralUnit: function HideCentralUnit(selectElement, value) {
            if(document.getElementById(selectElement)){
                var selectWarehouse = document.getElementById(selectElement)
                for (var i = 0; i < selectWarehouse.children.length; i++) {
                    if( selectWarehouse.children[i].getAttribute("data-value") == value || 
                        selectWarehouse.children[i].value == value ||
                        selectWarehouse.children[i].getAttribute("aria-label") == value
                        ){
                        selectWarehouse.children[i].style.display = "none"
                    }
                }
            };
            
        }
    };

    setTimeout(function () {
        //Removes Central unit from PIP
        HideCentralUnit.HideCentralUnit("selectionDropDownList", "591");
        //Removes Central unit from IKEA Family sign up page
        HideCentralUnit.HideCentralUnit("createFamilyUser_storeNumber", "591");
        //Removes Central unit from "Huskelisten"
        HideCentralUnit.HideCentralUnit("localStoreSelector", "591");
        //Removes Central unit from local warehouse info page
        HideCentralUnit.HideCentralUnit("localStore", "/dk/da/store/591");
        //Removes Central unit from "StockCheck"
        HideCentralUnit.HideCentralUnit("ikeaStoreNumber1", "591");
    }, 1000);
})();